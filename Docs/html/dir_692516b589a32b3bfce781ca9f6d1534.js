var dir_692516b589a32b3bfce781ca9f6d1534 =
[
    [ "bluetooth.hpp", "bluetooth_8hpp.html", "bluetooth_8hpp" ],
    [ "bluetoothdevice.hpp", "bluetoothdevice_8hpp.html", [
      [ "BluetoothDevice", "class_bluetooth_device.html", "class_bluetooth_device" ]
    ] ],
    [ "bluetoothinfo.hpp", "bluetoothinfo_8hpp.html", [
      [ "BluetoothInfo", "class_bluetooth_info.html", "class_bluetooth_info" ]
    ] ],
    [ "bluetoothservice.hpp", "bluetoothservice_8hpp.html", [
      [ "BluetoothService", "class_bluetooth_service.html", "class_bluetooth_service" ]
    ] ],
    [ "client.hpp", "client_8hpp.html", [
      [ "Client", "class_client.html", "class_client" ]
    ] ],
    [ "exercise.hpp", "exercise_8hpp.html", [
      [ "Exercise", "class_exercise.html", "class_exercise" ]
    ] ],
    [ "extensions.hpp", "extensions_8hpp.html", [
      [ "Extensions", "struct_extensions.html", "struct_extensions" ]
    ] ],
    [ "hasher.hpp", "hasher_8hpp.html", [
      [ "Hasher", "class_hasher.html", "class_hasher" ]
    ] ],
    [ "mainwindow.hpp", "mainwindow_8hpp.html", "mainwindow_8hpp" ],
    [ "sensorcomponent.hpp", "sensorcomponent_8hpp.html", "sensorcomponent_8hpp" ],
    [ "sensormodule.hpp", "sensormodule_8hpp.html", "sensormodule_8hpp" ],
    [ "session.hpp", "session_8hpp.html", [
      [ "Session", "class_session.html", "class_session" ]
    ] ],
    [ "settings.hpp", "settings_8hpp.html", "settings_8hpp" ],
    [ "xmlparser.hpp", "xmlparser_8hpp.html", [
      [ "XmlParser", "class_xml_parser.html", "class_xml_parser" ]
    ] ]
];