var class_client =
[
    [ "Client", "class_client.html#ae51af7aa6b8f591496a8f6a4a87a14bf", null ],
    [ "Client", "class_client.html#a68d25386b17fdb02191fefd0c71c9774", null ],
    [ "~Client", "class_client.html#a840e519ca781888cbd54181572ebe3a7", null ],
    [ "Client_AddSession", "class_client.html#afdccfd5a20dc91c40c2be5fe6f257e10", null ],
    [ "Client_GetAllFormatted", "class_client.html#a299d126640d6dc60526dfe149335fe13", null ],
    [ "Client_GetEmail", "class_client.html#ae5940b2983994a7703df5107d7b7fb5c", null ],
    [ "Client_GetFirstName", "class_client.html#af962d2842c6a1a24cd70ea5a076967d0", null ],
    [ "Client_GetID", "class_client.html#a3724f44b9b3e77a5e74d66a469761062", null ],
    [ "Client_GetLastName", "class_client.html#a57e902524a57a4177c445511749393df", null ],
    [ "Client_GetSessionList", "class_client.html#a9f72f282346f216651e4ea18b2fea6ba", null ],
    [ "mEmail", "class_client.html#a1b32c71ccdfeefe8799a4885ae877431", null ],
    [ "mFirstName", "class_client.html#afcfdf4c71a093be47443e74b2e9f24b3", null ],
    [ "mID", "class_client.html#a888dae1c7be39c3a111f5df835014617", null ],
    [ "mLastName", "class_client.html#ac7028c7f5740b6cb980f6a91f20ed37f", null ],
    [ "mSessionList", "class_client.html#ab7558a13b0373b3fc8aebae00e7a62c7", null ]
];