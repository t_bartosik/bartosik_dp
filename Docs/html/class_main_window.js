var class_main_window =
[
    [ "MainWindow", "class_main_window.html#a996c5a2b6f77944776856f08ec30858d", null ],
    [ "~MainWindow", "class_main_window.html#ae98d00a93bc118200eeef9f9bba1dba7", null ],
    [ "MainWindow_AboutActivateAbout", "class_main_window.html#a80a09f0109f10aca409d83c9b54b9d88", null ],
    [ "MainWindow_AboutActivateHowto", "class_main_window.html#aa856b90214de71a734c4ce1097d4829f", null ],
    [ "MainWindow_CleanAndQuit", "class_main_window.html#aa1d2d3bd7c377758be77bcbc6637c032", null ],
    [ "MainWindow_ClearResults", "class_main_window.html#aae4f335273942b4e7a021a04558dd269", null ],
    [ "MainWindow_ClientAdd", "class_main_window.html#abf718efa8471b4db19611652b92a3f0a", null ],
    [ "MainWindow_ClientCheck", "class_main_window.html#a265066127e2ec1dd594d162dd6bb40de", null ],
    [ "MainWindow_ClientDrawTable", "class_main_window.html#a58d44aaebcaf7bb2e0650ba04c870fb7", null ],
    [ "MainWindow_ClientRemove", "class_main_window.html#a78bf0a90f37bedeb0ef11827b3569899", null ],
    [ "MainWindow_ClientResizeTable", "class_main_window.html#a4386e82c1d5266bccc68f0e34876f2ce", null ],
    [ "MainWindow_ClientSelect", "class_main_window.html#a2a94fea7dd6fe408570606f8af8065ee", null ],
    [ "MainWindow_Create3DScene", "class_main_window.html#a3b74d93deb54a13969dad87f92383532", null ],
    [ "MainWindow_DeviceBeyondLimitReport", "class_main_window.html#aad08560ec277474cd443b905587c7ab8", null ],
    [ "MainWindow_DeviceConnectedReport", "class_main_window.html#a683b43bb9c382450da1303f39a0250ab", null ],
    [ "MainWindow_DeviceFilterExercises", "class_main_window.html#a69d97ed1c8b2e179e81df4b9437f6dcd", null ],
    [ "MainWindow_DeviceIncompatibleReport", "class_main_window.html#a298e55adf9874c868421c2ba67ee3a9f", null ],
    [ "MainWindow_DeviceMeasure", "class_main_window.html#a5ba1ba983bb188e7485c8102438671ec", null ],
    [ "MainWindow_DeviceNoConnectionReport", "class_main_window.html#ae9040883a7b7219ba3bf5523dd2cec36", null ],
    [ "MainWindow_DevicesConnect", "class_main_window.html#a3a471de34b3b1ee4b8f005e0d0efad44", null ],
    [ "MainWindow_DevicesDoneScanning", "class_main_window.html#a63fe8de677871140eb8245febee7c946", null ],
    [ "MainWindow_DevicesSelectionChanged", "class_main_window.html#a216ff4fc6caf39f6eaa1f43ddf43674f", null ],
    [ "MainWindow_DevicesStartScanning", "class_main_window.html#a4e8e163979235b2502f662293bc9cc2c", null ],
    [ "MainWindow_DevicesUpdateStatus", "class_main_window.html#a3f790cbaeb77d423cb152916f829065a", null ],
    [ "MainWindow_DeviceSwitchBodyPart", "class_main_window.html#a806c8284bf20224b1df30b9c001848f2", null ],
    [ "MainWindow_DeviceUnreachableReport", "class_main_window.html#ab879ed959aa5b57caf50afefd989b722", null ],
    [ "MainWindow_EnableAboutPage", "class_main_window.html#afd1f67969ee8d8f44c0c1411fe19bdd1", null ],
    [ "MainWindow_EnableClientsPage", "class_main_window.html#a8ec8d384a49707d10e301b9714c26813", null ],
    [ "MainWindow_EnableDevicesPage", "class_main_window.html#abebb06f316176039c2ee06fff2cbbff9", null ],
    [ "MainWindow_EnableHomePage", "class_main_window.html#a0b5748326bcf6cb32a9a021d478956b0", null ],
    [ "MainWindow_EnableMeasurementPage", "class_main_window.html#a6441d7a364128fb819e44219603fd2c8", null ],
    [ "MainWindow_EnableSettingsPage", "class_main_window.html#ab28fa73b36bf992f8403b082037553a4", null ],
    [ "MainWindow_HistoryRecalculateBars", "class_main_window.html#a33261b5c268f46e6f207ce4dc9920bb1", null ],
    [ "MainWindow_HistoryResizeTable", "class_main_window.html#a00b108889ce35b215d0f50ec7f752022", null ],
    [ "MainWindow_InitializeUIProperties", "class_main_window.html#a4d1f5dd51d47682916e8c5db6c821bfe", null ],
    [ "MainWindow_InitializeUIStrings", "class_main_window.html#a6e69455682d1ae80c758639730cae20d", null ],
    [ "MainWindow_MeasurementDisplayHistory", "class_main_window.html#a319668f1d151263d10adaa778ac19feb", null ],
    [ "MainWindow_MeasurementRotateLeft", "class_main_window.html#af586814ad2d206197ccf5a0d699e508a", null ],
    [ "MainWindow_MeasurementRotateRight", "class_main_window.html#a0b627c45c6f0e37652f63b43650bd0ad", null ],
    [ "MainWindow_MeasurementSaveRecord", "class_main_window.html#a5c32ac51ffef2f014b9ecf42673b5292", null ],
    [ "MainWindow_MeasurementZoomIn", "class_main_window.html#aaa524967438401c56080dd37261bf8ae", null ],
    [ "MainWindow_MeasurementZoomOut", "class_main_window.html#ae2e796d645263b1dd57fdba8d9fea45c", null ],
    [ "MainWindow_ReportResults", "class_main_window.html#a4afb96ecb3185d1e45153677d4c653a8", null ],
    [ "MainWindow_Reset3DScene", "class_main_window.html#a0073587f532a7256272ca859fb9debe3", null ],
    [ "MainWindow_Set3DScene", "class_main_window.html#a15f1c2bb50bc9d18672497d6f24101bb", null ],
    [ "MainWindow_SettingsUpdate", "class_main_window.html#ac14606b92b6a901560f84d7468f52ba5", null ],
    [ "MainWindow_SwitchTranslation", "class_main_window.html#a23850b52a458f2d88a77548951b62f16", null ],
    [ "resizeEvent", "class_main_window.html#ae12f8f63791595567b6250f8bb002bda", null ],
    [ "mAnimation", "class_main_window.html#af7adf6d31e749407ce2857d0860afe02", null ],
    [ "mBluetooth", "class_main_window.html#af81b39087226f73a108521e58741caf9", null ],
    [ "mBodyPart", "class_main_window.html#a00e653884b9a0718138428c720b90ae0", null ],
    [ "mClient", "class_main_window.html#a07d6e9747e8a9f429d84b1ad8e6d3451", null ],
    [ "mClientList", "class_main_window.html#acf8e4bbfb4aab3f21f87208d39f990de", null ],
    [ "mClientSelectionLine", "class_main_window.html#af6b6710a1dda556e9c6536cbd11d3f86", null ],
    [ "mDeviceSelectionLine", "class_main_window.html#ac07a0c360de648085cbe64cad193e3b9", null ],
    [ "mEditingEnabled", "class_main_window.html#ad4b93d29b4dfcc6f95440728c60c450b", null ],
    [ "mExerciseIndexList", "class_main_window.html#a8a2988a5c80126c4c48a60424d2278df", null ],
    [ "mExerciseList", "class_main_window.html#ad0b1683a13d1289f14cf3b637bfbe83a", null ],
    [ "mExerciseNameList", "class_main_window.html#ad76f851bd7fb03d73f5fae50e0e470dc", null ],
    [ "mExerciseTypeList", "class_main_window.html#a1dd037fd6f9188200020a1e0bee11c8a", null ],
    [ "mGraphicsContainer", "class_main_window.html#ab51ca28a9c3b1eee5208d7a7643518c0", null ],
    [ "mGraphicsRootEntity", "class_main_window.html#a596f23f8a23bfb09f938c503ad3ff06d", null ],
    [ "mGraphicsView", "class_main_window.html#a8f0445835e319fed057ed515867ba100", null ],
    [ "mHasher", "class_main_window.html#a5763291fcbeb81dbe03d49e1cf5e3f0b", null ],
    [ "mLeftCalfTransform", "class_main_window.html#a334e44c7be5bd9bf8c363eacb9ceb06b", null ],
    [ "mLeftThighTransform", "class_main_window.html#a54661b04b3f834a3c302f23a008629c2", null ],
    [ "mLowerLeftArmTransform", "class_main_window.html#a7ce875a9aeddf5c91e96757defe7d38b", null ],
    [ "mLowerRightArmTransform", "class_main_window.html#adaef35bb7eeafa9dc483a0c69da31aca", null ],
    [ "mMaxRangeTransform", "class_main_window.html#a92989e4f083e4a8486d1f24c2cf5b3c1", null ],
    [ "mMeasurementRunning", "class_main_window.html#a072c97b29b2354fcbf46f3957a3f0aae", null ],
    [ "mMinRangeTransform", "class_main_window.html#ae1a965492176dc1c8fe1dcba854e69bf", null ],
    [ "mMovementCounter", "class_main_window.html#a259cbff734e1a1beae96eed3fb330220", null ],
    [ "mMovementOrbsTransform", "class_main_window.html#a022b64b60df10433d002a85ff349f9d0", null ],
    [ "mPrimaryBlueCoinTransform", "class_main_window.html#a88a5915ef15fd86921f4c05f70c0e2ec", null ],
    [ "mRangeMaterial", "class_main_window.html#a8660694fb01c0ee4b3d2756414f38c4a", null ],
    [ "mRightCalfTransform", "class_main_window.html#abce47df1e8ef1a4ed213ac531e4b3bce", null ],
    [ "mRightThighTransform", "class_main_window.html#a62710f3c07275b33fa3f3462d429df8e", null ],
    [ "mSecondaryBlueCoinTransform", "class_main_window.html#a4b36a165ef67b957bea22269bf0e785b", null ],
    [ "mSelectedExercise", "class_main_window.html#a82ba08bce2501efe5af1715ca0bdb04e", null ],
    [ "mSensorModule", "class_main_window.html#a1d55e63d9c68681002599ec9f0b303cf", null ],
    [ "mSessionList", "class_main_window.html#a3adaf6a671961c7c1c5c23caebf09fd8", null ],
    [ "mSettings", "class_main_window.html#a4a77e85f5576f1d78fdb61622b8c6ffd", null ],
    [ "mTranslator", "class_main_window.html#a751988ba6ce5f6e058c3701e13246930", null ],
    [ "mUpperLeftArmTransform", "class_main_window.html#a0ac7d68e9cdd068ee0ffec811890841b", null ],
    [ "mUpperRightArmTransform", "class_main_window.html#ada37e747750fa834b7d0103c791916f8", null ],
    [ "mUserInterface", "class_main_window.html#af95ede74a0ecd0081bcbc206d57b292c", null ],
    [ "mXmlParser", "class_main_window.html#aaa62fe279f97b33acf2dc02b0e6d39f6", null ]
];