var mainwindow_8hpp =
[
    [ "MainWindow", "class_main_window.html", "class_main_window" ],
    [ "BodyPart", "mainwindow_8hpp.html#a7a4875f2b53773a016c13bcfc2a55ac3", [
      [ "eAll", "mainwindow_8hpp.html#a7a4875f2b53773a016c13bcfc2a55ac3a205a605c9a22391af5e09fa60fe30678", null ],
      [ "eLeftArm", "mainwindow_8hpp.html#a7a4875f2b53773a016c13bcfc2a55ac3aa4feb542a1b156f12ee57d66f120a55a", null ],
      [ "eRightArm", "mainwindow_8hpp.html#a7a4875f2b53773a016c13bcfc2a55ac3a88471fef0cf8e4f080ca9a7ec53f3bdd", null ],
      [ "eLeftLeg", "mainwindow_8hpp.html#a7a4875f2b53773a016c13bcfc2a55ac3aedd82b4da30531b5e313eed55511361c", null ],
      [ "eRightLeg", "mainwindow_8hpp.html#a7a4875f2b53773a016c13bcfc2a55ac3a781ef2bddd758c7011cf31598418d9fc", null ]
    ] ],
    [ "ExerciseType", "mainwindow_8hpp.html#af1aa7cfdc8e091a94d56af2ac04d8e2e", [
      [ "eLimbUpper", "mainwindow_8hpp.html#af1aa7cfdc8e091a94d56af2ac04d8e2ea265b3e734a0cb12328448777171a2745", null ],
      [ "eLimbUpperDual", "mainwindow_8hpp.html#af1aa7cfdc8e091a94d56af2ac04d8e2ea9b511cdf5604d70a3b574a944e732894", null ],
      [ "eLimbLower", "mainwindow_8hpp.html#af1aa7cfdc8e091a94d56af2ac04d8e2ea3816663f3c16494957267f18749a30ea", null ],
      [ "eLimbLowerDual", "mainwindow_8hpp.html#af1aa7cfdc8e091a94d56af2ac04d8e2ea72d225b040b3f2cba71b6a69ccb972d0", null ]
    ] ]
];