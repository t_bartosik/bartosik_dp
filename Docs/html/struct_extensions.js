var struct_extensions =
[
    [ "sAccRange", "struct_extensions.html#a4dc8a1cf13755853f2f1739f1ae29a59", null ],
    [ "sAccToDegs", "struct_extensions.html#a91247965f62628661f675356a891bc84", null ],
    [ "sAxisSwitchThreshold", "struct_extensions.html#a01123d70eb63a70c4a09b7bf03d45c73", null ],
    [ "sClientTableColumns", "struct_extensions.html#abc7deda2a0140423912f3ebba1561363", null ],
    [ "sDegsToRads", "struct_extensions.html#a680fd18cd544ecd9b653daf1755b8be5", null ],
    [ "sFilterBeta", "struct_extensions.html#a89e6d4d7e3613f5abbd86981ca6a258c", null ],
    [ "sFilterSampleFrequency", "struct_extensions.html#a890711a45b838bf300fc3f65e22c9cec", null ],
    [ "sGyroDPS", "struct_extensions.html#a5ee4b1a8cef41d957e81624f0bb85d57", null ],
    [ "sHistoryTableColumns", "struct_extensions.html#a1b41c4ecf888adb473d157c856e2209b", null ],
    [ "sIMURangeMax", "struct_extensions.html#a6e794e170620631b050e5a31527597eb", null ],
    [ "sMovementOrbsCount", "struct_extensions.html#a7076b407f8143bf0d9aa9c53af0a14e0", null ],
    [ "sNoExerciseSelected", "struct_extensions.html#a693c8246aa00c4653ba1d49072fe2fb9", null ],
    [ "sRadsToDegs", "struct_extensions.html#a150a5475c4153a6d7af06468d39a8e4f", null ]
];