var sensorcomponent_8hpp =
[
    [ "SensorComponent", "struct_sensor_component.html", "struct_sensor_component" ],
    [ "AxisSelection", "sensorcomponent_8hpp.html#ab6cae0d6f332c616a2367fb7918624a6", [
      [ "eUndefined", "sensorcomponent_8hpp.html#ab6cae0d6f332c616a2367fb7918624a6a46e5240f02f262bbbb359a08c45c083d", null ],
      [ "eUsingRoll", "sensorcomponent_8hpp.html#ab6cae0d6f332c616a2367fb7918624a6a9cdfbdc99219d0b85dd1a6943a45ab6a", null ],
      [ "eUsingPitch", "sensorcomponent_8hpp.html#ab6cae0d6f332c616a2367fb7918624a6a3cdb283c7654c6253846d08c49a4cbfc", null ]
    ] ],
    [ "ProcessingSelection", "sensorcomponent_8hpp.html#a45e4c312bb7399d2ca6f1a87d3674113", [
      [ "eDisabled", "sensorcomponent_8hpp.html#a45e4c312bb7399d2ca6f1a87d3674113ab32b520e58e1334ea07d4df5a4c5c402", null ],
      [ "eProcessingXY", "sensorcomponent_8hpp.html#a45e4c312bb7399d2ca6f1a87d3674113ad23067962e351eb759551fd44affeb13", null ],
      [ "eProcessingZ", "sensorcomponent_8hpp.html#a45e4c312bb7399d2ca6f1a87d3674113aa1cc35a6f68fb2404fe394a5391e9714", null ]
    ] ]
];