var class_bluetooth_device =
[
    [ "BluetoothDevice", "class_bluetooth_device.html#a47205d595d871f078e0e4b1fc9f4d856", null ],
    [ "BluetoothDevice", "class_bluetooth_device.html#aacec82df6beaea6e3a253a8cd1b6c9d5", null ],
    [ "~BluetoothDevice", "class_bluetooth_device.html#a47428d8f51ce83b2001d68e772e4eef2", null ],
    [ "BluetoothDevice_GetAddress", "class_bluetooth_device.html#a7f28612c721936481114cb4777e1eb17", null ],
    [ "BluetoothDevice_GetDevice", "class_bluetooth_device.html#a145c5a77afc3b991b349baecc872a36f", null ],
    [ "BluetoothDevice_GetName", "class_bluetooth_device.html#a0abc6bcaab470ba275fd39205518afdc", null ],
    [ "BluetoothDevice_SetDevice", "class_bluetooth_device.html#a91c8cdd982e7feaec4ac1ba3d14df0a1", null ],
    [ "BluetoothDevice_SignalDeviceChanged", "class_bluetooth_device.html#a913ad37020e0d4e3527a4893a3b62001", null ],
    [ "mDevice", "class_bluetooth_device.html#ae4b7527e15069b774a4bba679a62cc8b", null ]
];