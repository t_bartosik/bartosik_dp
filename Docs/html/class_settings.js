var class_settings =
[
    [ "Settings", "class_settings.html#ab7169a6eefce79566dd07db3b1e5e967", null ],
    [ "~Settings", "class_settings.html#a4a65be5921dfc9fddc476e5320541d89", null ],
    [ "Settings_GetDebugInformations", "class_settings.html#afdd27fbe6f1261418686705e178504e1", null ],
    [ "Settings_GetLanguage", "class_settings.html#adbc74ab8a68ceba546674f3ee2000169", null ],
    [ "Settings_GetUnrestrictedLimbMovement", "class_settings.html#ab4751aa0e83d8bf066f346161753784e", null ],
    [ "Settings_SetDebugInformations", "class_settings.html#a0b6ef46f281a587fd3f513a41c726da2", null ],
    [ "Settings_SetLanguage", "class_settings.html#a59d47e339a799019e73e15bc08168fd3", null ],
    [ "Settings_SetUnrestrictedLimbMovement", "class_settings.html#a2808159cbf6ad2caa3ffa0a511315cd0", null ],
    [ "mEnableDebugInformations", "class_settings.html#aa411df2b568e475e0c6ec4f7ad416acc", null ],
    [ "mEnableUnrestrictedLimbMovement", "class_settings.html#aba47961fbe624f2486f3631aa216cc4a", null ],
    [ "mLanguage", "class_settings.html#a2702bc23fde29b64e8da3d1c069d4aa8", null ]
];