var hierarchy =
[
    [ "Extensions", "struct_extensions.html", null ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "QObject", null, [
      [ "Bluetooth", "class_bluetooth.html", null ],
      [ "BluetoothDevice", "class_bluetooth_device.html", null ],
      [ "BluetoothInfo", "class_bluetooth_info.html", null ],
      [ "BluetoothService", "class_bluetooth_service.html", null ],
      [ "Client", "class_client.html", null ],
      [ "Exercise", "class_exercise.html", null ],
      [ "Hasher", "class_hasher.html", null ],
      [ "SensorComponent", "struct_sensor_component.html", null ],
      [ "SensorModule", "class_sensor_module.html", null ],
      [ "Session", "class_session.html", null ],
      [ "XmlParser", "class_xml_parser.html", null ]
    ] ],
    [ "Settings", "class_settings.html", null ]
];