var searchData=
[
  ['saccrange_0',['sAccRange',['../struct_extensions.html#a4dc8a1cf13755853f2f1739f1ae29a59',1,'Extensions']]],
  ['sacctodegs_1',['sAccToDegs',['../struct_extensions.html#a91247965f62628661f675356a891bc84',1,'Extensions']]],
  ['saxisswitchthreshold_2',['sAxisSwitchThreshold',['../struct_extensions.html#a01123d70eb63a70c4a09b7bf03d45c73',1,'Extensions']]],
  ['sclienttablecolumns_3',['sClientTableColumns',['../struct_extensions.html#abc7deda2a0140423912f3ebba1561363',1,'Extensions']]],
  ['sdegstorads_4',['sDegsToRads',['../struct_extensions.html#a680fd18cd544ecd9b653daf1755b8be5',1,'Extensions']]],
  ['sfilterbeta_5',['sFilterBeta',['../struct_extensions.html#a89e6d4d7e3613f5abbd86981ca6a258c',1,'Extensions']]],
  ['sfiltersamplefrequency_6',['sFilterSampleFrequency',['../struct_extensions.html#a890711a45b838bf300fc3f65e22c9cec',1,'Extensions']]],
  ['sgyrodps_7',['sGyroDPS',['../struct_extensions.html#a5ee4b1a8cef41d957e81624f0bb85d57',1,'Extensions']]],
  ['shistorytablecolumns_8',['sHistoryTableColumns',['../struct_extensions.html#a1b41c4ecf888adb473d157c856e2209b',1,'Extensions']]],
  ['simurangemax_9',['sIMURangeMax',['../struct_extensions.html#a6e794e170620631b050e5a31527597eb',1,'Extensions']]],
  ['smovementorbscount_10',['sMovementOrbsCount',['../struct_extensions.html#a7076b407f8143bf0d9aa9c53af0a14e0',1,'Extensions']]],
  ['snoexerciseselected_11',['sNoExerciseSelected',['../struct_extensions.html#a693c8246aa00c4653ba1d49072fe2fb9',1,'Extensions']]],
  ['sradstodegs_12',['sRadsToDegs',['../struct_extensions.html#a150a5475c4153a6d7af06468d39a8e4f',1,'Extensions']]]
];
