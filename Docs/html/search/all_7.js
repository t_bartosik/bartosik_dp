var searchData=
[
  ['maccelerometerx_0',['mAccelerometerX',['../struct_sensor_component.html#abb19170ff464539633b96d80d3e19b0f',1,'SensorComponent']]],
  ['maccelerometery_1',['mAccelerometerY',['../struct_sensor_component.html#a741d06848d75c1f32f8f9ca919483ab7',1,'SensorComponent']]],
  ['maccelerometerz_2',['mAccelerometerZ',['../struct_sensor_component.html#aa88c3c2a198d24e69b9af45e7cab6326',1,'SensorComponent']]],
  ['maddresslist_3',['mAddressList',['../class_bluetooth.html#a3927efe61e64cc080710c94f0efbb928',1,'Bluetooth']]],
  ['main_4',['main',['../main_8cpp.html#ae0665038b72011f5c680c660fcb59459',1,'main.cpp']]],
  ['main_2ecpp_5',['main.cpp',['../main_8cpp.html',1,'']]],
  ['mainwindow_6',['MainWindow',['../class_main_window.html',1,'MainWindow'],['../class_main_window.html#a996c5a2b6f77944776856f08ec30858d',1,'MainWindow::MainWindow()']]],
  ['mainwindow_2ecpp_7',['mainwindow.cpp',['../mainwindow_8cpp.html',1,'']]],
  ['mainwindow_2ehpp_8',['mainwindow.hpp',['../mainwindow_8hpp.html',1,'']]],
  ['mainwindow_5faboutactivateabout_9',['MainWindow_AboutActivateAbout',['../class_main_window.html#a80a09f0109f10aca409d83c9b54b9d88',1,'MainWindow']]],
  ['mainwindow_5faboutactivatehowto_10',['MainWindow_AboutActivateHowto',['../class_main_window.html#aa856b90214de71a734c4ce1097d4829f',1,'MainWindow']]],
  ['mainwindow_5fcleanandquit_11',['MainWindow_CleanAndQuit',['../class_main_window.html#aa1d2d3bd7c377758be77bcbc6637c032',1,'MainWindow']]],
  ['mainwindow_5fclearresults_12',['MainWindow_ClearResults',['../class_main_window.html#aae4f335273942b4e7a021a04558dd269',1,'MainWindow']]],
  ['mainwindow_5fclientadd_13',['MainWindow_ClientAdd',['../class_main_window.html#abf718efa8471b4db19611652b92a3f0a',1,'MainWindow']]],
  ['mainwindow_5fclientcheck_14',['MainWindow_ClientCheck',['../class_main_window.html#a265066127e2ec1dd594d162dd6bb40de',1,'MainWindow']]],
  ['mainwindow_5fclientdrawtable_15',['MainWindow_ClientDrawTable',['../class_main_window.html#a58d44aaebcaf7bb2e0650ba04c870fb7',1,'MainWindow']]],
  ['mainwindow_5fclientremove_16',['MainWindow_ClientRemove',['../class_main_window.html#a78bf0a90f37bedeb0ef11827b3569899',1,'MainWindow']]],
  ['mainwindow_5fclientresizetable_17',['MainWindow_ClientResizeTable',['../class_main_window.html#a4386e82c1d5266bccc68f0e34876f2ce',1,'MainWindow']]],
  ['mainwindow_5fclientselect_18',['MainWindow_ClientSelect',['../class_main_window.html#a2a94fea7dd6fe408570606f8af8065ee',1,'MainWindow']]],
  ['mainwindow_5fcreate3dscene_19',['MainWindow_Create3DScene',['../class_main_window.html#a3b74d93deb54a13969dad87f92383532',1,'MainWindow']]],
  ['mainwindow_5fdevicebeyondlimitreport_20',['MainWindow_DeviceBeyondLimitReport',['../class_main_window.html#aad08560ec277474cd443b905587c7ab8',1,'MainWindow']]],
  ['mainwindow_5fdeviceconnectedreport_21',['MainWindow_DeviceConnectedReport',['../class_main_window.html#a683b43bb9c382450da1303f39a0250ab',1,'MainWindow']]],
  ['mainwindow_5fdevicefilterexercises_22',['MainWindow_DeviceFilterExercises',['../class_main_window.html#a69d97ed1c8b2e179e81df4b9437f6dcd',1,'MainWindow']]],
  ['mainwindow_5fdeviceincompatiblereport_23',['MainWindow_DeviceIncompatibleReport',['../class_main_window.html#a298e55adf9874c868421c2ba67ee3a9f',1,'MainWindow']]],
  ['mainwindow_5fdevicemeasure_24',['MainWindow_DeviceMeasure',['../class_main_window.html#a5ba1ba983bb188e7485c8102438671ec',1,'MainWindow']]],
  ['mainwindow_5fdevicenoconnectionreport_25',['MainWindow_DeviceNoConnectionReport',['../class_main_window.html#ae9040883a7b7219ba3bf5523dd2cec36',1,'MainWindow']]],
  ['mainwindow_5fdevicesconnect_26',['MainWindow_DevicesConnect',['../class_main_window.html#a3a471de34b3b1ee4b8f005e0d0efad44',1,'MainWindow']]],
  ['mainwindow_5fdevicesdonescanning_27',['MainWindow_DevicesDoneScanning',['../class_main_window.html#a63fe8de677871140eb8245febee7c946',1,'MainWindow']]],
  ['mainwindow_5fdevicesselectionchanged_28',['MainWindow_DevicesSelectionChanged',['../class_main_window.html#a216ff4fc6caf39f6eaa1f43ddf43674f',1,'MainWindow']]],
  ['mainwindow_5fdevicesstartscanning_29',['MainWindow_DevicesStartScanning',['../class_main_window.html#a4e8e163979235b2502f662293bc9cc2c',1,'MainWindow']]],
  ['mainwindow_5fdevicesupdatestatus_30',['MainWindow_DevicesUpdateStatus',['../class_main_window.html#a3f790cbaeb77d423cb152916f829065a',1,'MainWindow']]],
  ['mainwindow_5fdeviceswitchbodypart_31',['MainWindow_DeviceSwitchBodyPart',['../class_main_window.html#a806c8284bf20224b1df30b9c001848f2',1,'MainWindow']]],
  ['mainwindow_5fdeviceunreachablereport_32',['MainWindow_DeviceUnreachableReport',['../class_main_window.html#ab879ed959aa5b57caf50afefd989b722',1,'MainWindow']]],
  ['mainwindow_5fenableaboutpage_33',['MainWindow_EnableAboutPage',['../class_main_window.html#afd1f67969ee8d8f44c0c1411fe19bdd1',1,'MainWindow']]],
  ['mainwindow_5fenableclientspage_34',['MainWindow_EnableClientsPage',['../class_main_window.html#a8ec8d384a49707d10e301b9714c26813',1,'MainWindow']]],
  ['mainwindow_5fenabledevicespage_35',['MainWindow_EnableDevicesPage',['../class_main_window.html#abebb06f316176039c2ee06fff2cbbff9',1,'MainWindow']]],
  ['mainwindow_5fenablehomepage_36',['MainWindow_EnableHomePage',['../class_main_window.html#a0b5748326bcf6cb32a9a021d478956b0',1,'MainWindow']]],
  ['mainwindow_5fenablemeasurementpage_37',['MainWindow_EnableMeasurementPage',['../class_main_window.html#a6441d7a364128fb819e44219603fd2c8',1,'MainWindow']]],
  ['mainwindow_5fenablesettingspage_38',['MainWindow_EnableSettingsPage',['../class_main_window.html#ab28fa73b36bf992f8403b082037553a4',1,'MainWindow']]],
  ['mainwindow_5fhistoryrecalculatebars_39',['MainWindow_HistoryRecalculateBars',['../class_main_window.html#a33261b5c268f46e6f207ce4dc9920bb1',1,'MainWindow']]],
  ['mainwindow_5fhistoryresizetable_40',['MainWindow_HistoryResizeTable',['../class_main_window.html#a00b108889ce35b215d0f50ec7f752022',1,'MainWindow']]],
  ['mainwindow_5finitializeuiproperties_41',['MainWindow_InitializeUIProperties',['../class_main_window.html#a4d1f5dd51d47682916e8c5db6c821bfe',1,'MainWindow']]],
  ['mainwindow_5finitializeuistrings_42',['MainWindow_InitializeUIStrings',['../class_main_window.html#a6e69455682d1ae80c758639730cae20d',1,'MainWindow']]],
  ['mainwindow_5fmeasurementdisplayhistory_43',['MainWindow_MeasurementDisplayHistory',['../class_main_window.html#a319668f1d151263d10adaa778ac19feb',1,'MainWindow']]],
  ['mainwindow_5fmeasurementrotateleft_44',['MainWindow_MeasurementRotateLeft',['../class_main_window.html#af586814ad2d206197ccf5a0d699e508a',1,'MainWindow']]],
  ['mainwindow_5fmeasurementrotateright_45',['MainWindow_MeasurementRotateRight',['../class_main_window.html#a0b627c45c6f0e37652f63b43650bd0ad',1,'MainWindow']]],
  ['mainwindow_5fmeasurementsaverecord_46',['MainWindow_MeasurementSaveRecord',['../class_main_window.html#a5c32ac51ffef2f014b9ecf42673b5292',1,'MainWindow']]],
  ['mainwindow_5fmeasurementzoomin_47',['MainWindow_MeasurementZoomIn',['../class_main_window.html#aaa524967438401c56080dd37261bf8ae',1,'MainWindow']]],
  ['mainwindow_5fmeasurementzoomout_48',['MainWindow_MeasurementZoomOut',['../class_main_window.html#ae2e796d645263b1dd57fdba8d9fea45c',1,'MainWindow']]],
  ['mainwindow_5freportresults_49',['MainWindow_ReportResults',['../class_main_window.html#a4afb96ecb3185d1e45153677d4c653a8',1,'MainWindow']]],
  ['mainwindow_5freset3dscene_50',['MainWindow_Reset3DScene',['../class_main_window.html#a0073587f532a7256272ca859fb9debe3',1,'MainWindow']]],
  ['mainwindow_5fset3dscene_51',['MainWindow_Set3DScene',['../class_main_window.html#a15f1c2bb50bc9d18672497d6f24101bb',1,'MainWindow']]],
  ['mainwindow_5fsettingsupdate_52',['MainWindow_SettingsUpdate',['../class_main_window.html#ac14606b92b6a901560f84d7468f52ba5',1,'MainWindow']]],
  ['mainwindow_5fswitchtranslation_53',['MainWindow_SwitchTranslation',['../class_main_window.html#a23850b52a458f2d88a77548951b62f16',1,'MainWindow']]],
  ['manimation_54',['mAnimation',['../class_main_window.html#af7adf6d31e749407ce2857d0860afe02',1,'MainWindow']]],
  ['mauxiliary_55',['mAuxiliary',['../class_sensor_module.html#a9c63fa67278a11e002d17dc3123f1acb',1,'SensorModule']]],
  ['maxisselected_56',['mAxisSelected',['../struct_sensor_component.html#a7fc4bfb0a7ef1814864dea2f15427659',1,'SensorComponent']]],
  ['mbaselowlevelserviceuuid_57',['mBaseLowLevelServiceUUID',['../class_bluetooth.html#a9930e9e9c4b6dae78f96bd968544f5aa',1,'Bluetooth']]],
  ['mbluetooth_58',['mBluetooth',['../class_main_window.html#af81b39087226f73a108521e58741caf9',1,'MainWindow']]],
  ['mbluetoothdiscoverytimer_59',['mBluetoothDiscoveryTimer',['../class_bluetooth.html#a7177fcc99603670cb0fcea3a1b38c303',1,'Bluetooth']]],
  ['mbluetoothretrievetimer_60',['mBluetoothRetrieveTimer',['../class_bluetooth.html#a20f005f40cd27b18dfd8ea65b14fa9e1',1,'Bluetooth']]],
  ['mbodypart_61',['mBodyPart',['../class_main_window.html#a00e653884b9a0718138428c720b90ae0',1,'MainWindow']]],
  ['mcharacteristic_62',['mCharacteristic',['../class_bluetooth_info.html#a32a45de626e0a248facdf6c7266596b7',1,'BluetoothInfo']]],
  ['mclient_63',['mClient',['../class_main_window.html#a07d6e9747e8a9f429d84b1ad8e6d3451',1,'MainWindow']]],
  ['mclientlist_64',['mClientList',['../class_main_window.html#acf8e4bbfb4aab3f21f87208d39f990de',1,'MainWindow']]],
  ['mclientselectionline_65',['mClientSelectionLine',['../class_main_window.html#af6b6710a1dda556e9c6536cbd11d3f86',1,'MainWindow']]],
  ['mcombinedmax_66',['mCombinedMax',['../class_sensor_module.html#a3db8c97027ce85cb83e2559b86a5d03d',1,'SensorModule']]],
  ['mcombinedmin_67',['mCombinedMin',['../class_sensor_module.html#a7085cd9ee3da4aa7d21cafbd21df25f6',1,'SensorModule']]],
  ['mcomponent_68',['mComponent',['../class_sensor_module.html#a0bd35b9a9b1a0a5002fa4a755ed9bf2c',1,'SensorModule']]],
  ['mconversionok_69',['mConversionOK',['../class_sensor_module.html#a5cc88c9a9382dd6e0e2fda8a1a1ecc48',1,'SensorModule']]],
  ['mcurrentangle_70',['mCurrentAngle',['../struct_sensor_component.html#a3201d549811203a1a7069f2f1029df1a',1,'SensorComponent']]],
  ['mdevice_71',['mDevice',['../class_bluetooth_device.html#ae4b7527e15069b774a4bba679a62cc8b',1,'BluetoothDevice']]],
  ['mdevices_72',['mDevices',['../class_bluetooth.html#a28272cda8d8d0a23297cae312089f737',1,'Bluetooth']]],
  ['mdeviceselectionline_73',['mDeviceSelectionLine',['../class_main_window.html#ac07a0c360de648085cbe64cad193e3b9',1,'MainWindow']]],
  ['mdeviceslist_74',['mDevicesList',['../class_bluetooth.html#a74978c4ed9e754e787e96755b4019f44',1,'Bluetooth']]],
  ['mdirpath_75',['mDirPath',['../class_xml_parser.html#ae33276105e417a07fbfedbda7e635185',1,'XmlParser']]],
  ['mdiscoveryagent_76',['mDiscoveryAgent',['../class_bluetooth.html#aba1034bb57153746fe987d48e7704cdc',1,'Bluetooth']]],
  ['meditingenabled_77',['mEditingEnabled',['../class_main_window.html#ad4b93d29b4dfcc6f95440728c60c450b',1,'MainWindow']]],
  ['memail_78',['mEmail',['../class_client.html#a1b32c71ccdfeefe8799a4885ae877431',1,'Client']]],
  ['menabledebuginformations_79',['mEnableDebugInformations',['../class_bluetooth.html#a139a569149b4e07d3a39863f721898df',1,'Bluetooth::mEnableDebugInformations()'],['../class_settings.html#aa411df2b568e475e0c6ec4f7ad416acc',1,'Settings::mEnableDebugInformations()']]],
  ['menableunrestrictedlimbmovement_80',['mEnableUnrestrictedLimbMovement',['../class_settings.html#aba47961fbe624f2486f3631aa216cc4a',1,'Settings']]],
  ['mexerciseindexlist_81',['mExerciseIndexList',['../class_main_window.html#a8a2988a5c80126c4c48a60424d2278df',1,'MainWindow']]],
  ['mexerciselist_82',['mExerciseList',['../class_main_window.html#ad0b1683a13d1289f14cf3b637bfbe83a',1,'MainWindow::mExerciseList()'],['../class_session.html#acde231ea92dec9ca1deb40a363a3f543',1,'Session::mExerciseList()']]],
  ['mexercisenamelist_83',['mExerciseNameList',['../class_main_window.html#ad76f851bd7fb03d73f5fae50e0e470dc',1,'MainWindow']]],
  ['mexercisetypelist_84',['mExerciseTypeList',['../class_main_window.html#a1dd037fd6f9188200020a1e0bee11c8a',1,'MainWindow']]],
  ['mfilterq0_85',['mFilterQ0',['../struct_sensor_component.html#af34972ece86ac39d78630140a6c1efc4',1,'SensorComponent']]],
  ['mfilterq1_86',['mFilterQ1',['../struct_sensor_component.html#a02945e8f30349e7136ccb1996b0c1b8a',1,'SensorComponent']]],
  ['mfilterq2_87',['mFilterQ2',['../struct_sensor_component.html#a72e187c5d9500240037270e8596c7636',1,'SensorComponent']]],
  ['mfilterq3_88',['mFilterQ3',['../struct_sensor_component.html#a87a88759993de3a6cd10ae45d2344316',1,'SensorComponent']]],
  ['mfirstname_89',['mFirstName',['../class_client.html#afcfdf4c71a093be47443e74b2e9f24b3',1,'Client']]],
  ['mfullhexvalue_90',['mFullHexValue',['../class_sensor_module.html#a8e61451c979961ab37737f225ede525e',1,'SensorModule']]],
  ['mgraphicscontainer_91',['mGraphicsContainer',['../class_main_window.html#ab51ca28a9c3b1eee5208d7a7643518c0',1,'MainWindow']]],
  ['mgraphicsrootentity_92',['mGraphicsRootEntity',['../class_main_window.html#a596f23f8a23bfb09f938c503ad3ff06d',1,'MainWindow']]],
  ['mgraphicsview_93',['mGraphicsView',['../class_main_window.html#a8f0445835e319fed057ed515867ba100',1,'MainWindow']]],
  ['mgyroscopex_94',['mGyroscopeX',['../struct_sensor_component.html#ab38de7e87ec2604ada9d0cbc9c56b7b5',1,'SensorComponent']]],
  ['mgyroscopey_95',['mGyroscopeY',['../struct_sensor_component.html#ad790cf49c2573ee895a00203b3f17398',1,'SensorComponent']]],
  ['mgyroscopez_96',['mGyroscopeZ',['../struct_sensor_component.html#a9fbb3aad039d9384b3a43e362faddaec',1,'SensorComponent']]],
  ['mhash_97',['mHash',['../class_hasher.html#a8e4bcd7814ea5c4cb10e59522c9368e3',1,'Hasher']]],
  ['mhasher_98',['mHasher',['../class_main_window.html#a5763291fcbeb81dbe03d49e1cf5e3f0b',1,'MainWindow']]],
  ['mhexvalue_99',['mHexValue',['../class_sensor_module.html#a83a65f46fe88332347b780fbba637b96',1,'SensorModule']]],
  ['mhighleveluuidexpression_100',['mHighLevelUUIDExpression',['../class_bluetooth.html#acab7225b64ac2132cdad83eda515fb5d',1,'Bluetooth']]],
  ['mid_101',['mID',['../class_client.html#a888dae1c7be39c3a111f5df835014617',1,'Client::mID()'],['../class_exercise.html#a99b9ca107afa5a3e6625f5a5775a32ff',1,'Exercise::mID()'],['../class_session.html#a539eb7bce04af7a0304c30671bee2f7d',1,'Session::mID()']]],
  ['mlanguage_102',['mLanguage',['../class_settings.html#a2702bc23fde29b64e8da3d1c069d4aa8',1,'Settings']]],
  ['mlastname_103',['mLastName',['../class_client.html#ac7028c7f5740b6cb980f6a91f20ed37f',1,'Client']]],
  ['mlatestdeviceaddress_104',['mLatestDeviceAddress',['../class_bluetooth.html#a63e448ef3e9ac039f09fd9580be1e05c',1,'Bluetooth']]],
  ['mleftcalftransform_105',['mLeftCalfTransform',['../class_main_window.html#a334e44c7be5bd9bf8c363eacb9ceb06b',1,'MainWindow']]],
  ['mleftthightransform_106',['mLeftThighTransform',['../class_main_window.html#a54661b04b3f834a3c302f23a008629c2',1,'MainWindow']]],
  ['mlowerleftarmtransform_107',['mLowerLeftArmTransform',['../class_main_window.html#a7ce875a9aeddf5c91e96757defe7d38b',1,'MainWindow']]],
  ['mlowerrightarmtransform_108',['mLowerRightArmTransform',['../class_main_window.html#adaef35bb7eeafa9dc483a0c69da31aca',1,'MainWindow']]],
  ['mmax_109',['mMax',['../class_exercise.html#a93a48061bb9da146c0dd9d6d8e547120',1,'Exercise::mMax()'],['../struct_sensor_component.html#af7c5879b1b99fa59adc247c3da97220a',1,'SensorComponent::mMax()']]],
  ['mmaxrangetransform_110',['mMaxRangeTransform',['../class_main_window.html#a92989e4f083e4a8486d1f24c2cf5b3c1',1,'MainWindow']]],
  ['mmeasurementrunning_111',['mMeasurementRunning',['../class_main_window.html#a072c97b29b2354fcbf46f3957a3f0aae',1,'MainWindow']]],
  ['mmin_112',['mMin',['../class_exercise.html#a1d4051bf283031a1da9d7da8bb9965fd',1,'Exercise::mMin()'],['../struct_sensor_component.html#a1992ce1ca8ff91a516e4c98165ad4cfb',1,'SensorComponent::mMin()']]],
  ['mminrangetransform_113',['mMinRangeTransform',['../class_main_window.html#ae1a965492176dc1c8fe1dcba854e69bf',1,'MainWindow']]],
  ['mmovementcounter_114',['mMovementCounter',['../class_main_window.html#a259cbff734e1a1beae96eed3fb330220',1,'MainWindow']]],
  ['mmovementorbstransform_115',['mMovementOrbsTransform',['../class_main_window.html#a022b64b60df10433d002a85ff349f9d0',1,'MainWindow']]],
  ['mpitch_116',['mPitch',['../struct_sensor_component.html#aeb787c2c06184f233f05a05a063d5c57',1,'SensorComponent']]],
  ['mpitchmax_117',['mPitchMax',['../struct_sensor_component.html#ad50e73a04306212d7897f50118fda6fe',1,'SensorComponent']]],
  ['mpitchmin_118',['mPitchMin',['../struct_sensor_component.html#a55de87edef35927ef957cc771dbb2b7b',1,'SensorComponent']]],
  ['mpitchprevone_119',['mPitchPrevOne',['../struct_sensor_component.html#adc2e1603fdf0c7f10811799a0a1ba926',1,'SensorComponent']]],
  ['mpitchprevtwo_120',['mPitchPrevTwo',['../struct_sensor_component.html#ac2e3017e3681c59dcb32e96e2fb5f563',1,'SensorComponent']]],
  ['mprimarybluecointransform_121',['mPrimaryBlueCoinTransform',['../class_main_window.html#a88a5915ef15fd86921f4c05f70c0e2ec',1,'MainWindow']]],
  ['mprimarybluetoothdevice_122',['mPrimaryBluetoothDevice',['../class_bluetooth.html#ab48bfef0f8f35e53bffeed291e36bf85',1,'Bluetooth']]],
  ['mprimarycomponent_123',['mPrimaryComponent',['../class_sensor_module.html#a12f33f69901ce5b7fdcf0213d2c39bd3',1,'SensorModule']]],
  ['mprimaryconnectedaddress_124',['mPrimaryConnectedAddress',['../class_bluetooth.html#ad481667e9de9c06ba1de329a4150a644',1,'Bluetooth']]],
  ['mprimarycontroller_125',['mPrimaryController',['../class_bluetooth.html#a58504cea897f608c43b7f1e722736b46',1,'Bluetooth']]],
  ['mprimaryhighlevelserviceuuid_126',['mPrimaryHighLevelServiceUUID',['../class_bluetooth.html#a66002394bdae94bca263b60db1cac024',1,'Bluetooth']]],
  ['mprimarylowlevelserviceuuid_127',['mPrimaryLowLevelServiceUUID',['../class_bluetooth.html#a1570eace5299b6593f4b41d40181c453',1,'Bluetooth']]],
  ['mprimaryservice_128',['mPrimaryService',['../class_bluetooth.html#ab9c42440c3fb0999df1a1fdfa59419d1',1,'Bluetooth']]],
  ['mprocessingselected_129',['mProcessingSelected',['../struct_sensor_component.html#aa6b55c8ffd6712e9676f6edf6cec078d',1,'SensorComponent']]],
  ['mrange_130',['mRange',['../class_exercise.html#af6a38f44fb9a139fde1179c91d1dd681',1,'Exercise::mRange()'],['../struct_sensor_component.html#a797cca7f7a816662cb931692ef259612',1,'SensorComponent::mRange()']]],
  ['mrangematerial_131',['mRangeMaterial',['../class_main_window.html#a8660694fb01c0ee4b3d2756414f38c4a',1,'MainWindow']]],
  ['mrightcalftransform_132',['mRightCalfTransform',['../class_main_window.html#abce47df1e8ef1a4ed213ac531e4b3bce',1,'MainWindow']]],
  ['mrightthightransform_133',['mRightThighTransform',['../class_main_window.html#a62710f3c07275b33fa3f3462d429df8e',1,'MainWindow']]],
  ['mroll_134',['mRoll',['../struct_sensor_component.html#a0981d6e78f1947597d98c7f359bb7464',1,'SensorComponent']]],
  ['mrollmax_135',['mRollMax',['../struct_sensor_component.html#af5972e99f198b9dc1fef05f1a3326a08',1,'SensorComponent']]],
  ['mrollmin_136',['mRollMin',['../struct_sensor_component.html#a7baf7901041cee0df34d35034f03024e',1,'SensorComponent']]],
  ['mrollprevone_137',['mRollPrevOne',['../struct_sensor_component.html#a793f3ea8808f5e5ba048288d53295d7a',1,'SensorComponent']]],
  ['mrollprevtwo_138',['mRollPrevTwo',['../struct_sensor_component.html#abe055eac1caa1e6f3973047faa810127',1,'SensorComponent']]],
  ['mscanningcompleted_139',['mScanningCompleted',['../class_bluetooth.html#a7748838e402876035a0fd67c3c121646',1,'Bluetooth']]],
  ['msecondarybluecointransform_140',['mSecondaryBlueCoinTransform',['../class_main_window.html#a4b36a165ef67b957bea22269bf0e785b',1,'MainWindow']]],
  ['msecondarybluetoothdevice_141',['mSecondaryBluetoothDevice',['../class_bluetooth.html#a38b7e0c9fbe1215ec69113c823e3dca7',1,'Bluetooth']]],
  ['msecondarycomponent_142',['mSecondaryComponent',['../class_sensor_module.html#a0eb655aab02af53e7a0ef602ae89bb4e',1,'SensorModule']]],
  ['msecondaryconnectedaddress_143',['mSecondaryConnectedAddress',['../class_bluetooth.html#a4d25961e3af78e58b6d4380e8739e5f3',1,'Bluetooth']]],
  ['msecondarycontroller_144',['mSecondaryController',['../class_bluetooth.html#a35cd60d2957a57ec210f3c62c6b0111c',1,'Bluetooth']]],
  ['msecondaryhighlevelserviceuuid_145',['mSecondaryHighLevelServiceUUID',['../class_bluetooth.html#afe8b2d71785e6f948082496465c0b69c',1,'Bluetooth']]],
  ['msecondarylowlevelserviceuuid_146',['mSecondaryLowLevelServiceUUID',['../class_bluetooth.html#a6256b38416878c0aa58f410ccbd6bafe',1,'Bluetooth']]],
  ['msecondaryservice_147',['mSecondaryService',['../class_bluetooth.html#a104def5e1eb5cfe6343dcae73c8be77b',1,'Bluetooth']]],
  ['mselectedexercise_148',['mSelectedExercise',['../class_main_window.html#a82ba08bce2501efe5af1715ca0bdb04e',1,'MainWindow']]],
  ['msensormodule_149',['mSensorModule',['../class_main_window.html#a1d55e63d9c68681002599ec9f0b303cf',1,'MainWindow']]],
  ['mservice_150',['mService',['../class_bluetooth_service.html#a8c89d79f29d9dd502fedb89774748d44',1,'BluetoothService']]],
  ['mserviceretrievalspeed_151',['mServiceRetrievalSpeed',['../class_bluetooth.html#ab524030add0a66fe15970bb1bc6f7604',1,'Bluetooth']]],
  ['mservices_152',['mServices',['../class_bluetooth.html#a2bff51d2cf3fa8e54bd2ceab432e7646',1,'Bluetooth']]],
  ['msessionlist_153',['mSessionList',['../class_main_window.html#a3adaf6a671961c7c1c5c23caebf09fd8',1,'MainWindow::mSessionList()'],['../class_client.html#ab7558a13b0373b3fc8aebae00e7a62c7',1,'Client::mSessionList()']]],
  ['msettings_154',['mSettings',['../class_main_window.html#a4a77e85f5576f1d78fdb61622b8c6ffd',1,'MainWindow']]],
  ['mstartupsidedown_155',['mStartUpsideDown',['../struct_sensor_component.html#a2048a626c0b00717d041ed5775231649',1,'SensorComponent']]],
  ['mtranslator_156',['mTranslator',['../class_main_window.html#a751988ba6ce5f6e058c3701e13246930',1,'MainWindow']]],
  ['mupperleftarmtransform_157',['mUpperLeftArmTransform',['../class_main_window.html#a0ac7d68e9cdd068ee0ffec811890841b',1,'MainWindow']]],
  ['mupperrightarmtransform_158',['mUpperRightArmTransform',['../class_main_window.html#ada37e747750fa834b7d0103c791916f8',1,'MainWindow']]],
  ['muserinterface_159',['mUserInterface',['../class_main_window.html#af95ede74a0ecd0081bcbc206d57b292c',1,'MainWindow']]],
  ['mxmlparser_160',['mXmlParser',['../class_main_window.html#aaa62fe279f97b33acf2dc02b0e6d39f6',1,'MainWindow']]],
  ['myaw_161',['mYaw',['../struct_sensor_component.html#a24753a1aa7ae6c41683f78c9f066c829',1,'SensorComponent']]]
];
