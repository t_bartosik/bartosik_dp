var searchData=
[
  ['client_0',['Client',['../class_client.html',1,'Client'],['../class_client.html#ae51af7aa6b8f591496a8f6a4a87a14bf',1,'Client::Client()'],['../class_client.html#a68d25386b17fdb02191fefd0c71c9774',1,'Client::Client(QString id, QString firstName, QString lastName, QString email, QList&lt; QSharedPointer&lt; Session &gt; &gt; sessionList)']]],
  ['client_2ecpp_1',['client.cpp',['../client_8cpp.html',1,'']]],
  ['client_2ehpp_2',['client.hpp',['../client_8hpp.html',1,'']]],
  ['client_5faddsession_3',['Client_AddSession',['../class_client.html#afdccfd5a20dc91c40c2be5fe6f257e10',1,'Client']]],
  ['client_5fgetallformatted_4',['Client_GetAllFormatted',['../class_client.html#a299d126640d6dc60526dfe149335fe13',1,'Client']]],
  ['client_5fgetemail_5',['Client_GetEmail',['../class_client.html#ae5940b2983994a7703df5107d7b7fb5c',1,'Client']]],
  ['client_5fgetfirstname_6',['Client_GetFirstName',['../class_client.html#af962d2842c6a1a24cd70ea5a076967d0',1,'Client']]],
  ['client_5fgetid_7',['Client_GetID',['../class_client.html#a3724f44b9b3e77a5e74d66a469761062',1,'Client']]],
  ['client_5fgetlastname_8',['Client_GetLastName',['../class_client.html#a57e902524a57a4177c445511749393df',1,'Client']]],
  ['client_5fgetsessionlist_9',['Client_GetSessionList',['../class_client.html#a9f72f282346f216651e4ea18b2fea6ba',1,'Client']]],
  ['controllertype_10',['ControllerType',['../bluetooth_8hpp.html#a81059b4122c9dd4608d347eb117ae8c9',1,'bluetooth.hpp']]]
];
