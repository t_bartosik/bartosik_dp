var class_exercise =
[
    [ "Exercise", "class_exercise.html#a9707094804f79ebc6aeaa597600db682", null ],
    [ "Exercise", "class_exercise.html#ad52ee361b1726fe869dc6f7225ac9e86", null ],
    [ "~Exercise", "class_exercise.html#a156f3ee458ddc9918b54e276860a7181", null ],
    [ "Exercise_GetAllFormatted", "class_exercise.html#ab4afe2fa2f4fda37ee750949b6a32611", null ],
    [ "Exercise_GetID", "class_exercise.html#a145f35fabea1efed090374f049877348", null ],
    [ "Exercise_GetMax", "class_exercise.html#ae774f45e7bb79e5a41f3b03d8c949971", null ],
    [ "Exercise_GetMin", "class_exercise.html#a181b688e1e6095a349b68d8909f37c39", null ],
    [ "Exercise_GetRange", "class_exercise.html#ab92fbd7d148d9eb698a17c96ba9cb695", null ],
    [ "mID", "class_exercise.html#a99b9ca107afa5a3e6625f5a5775a32ff", null ],
    [ "mMax", "class_exercise.html#a93a48061bb9da146c0dd9d6d8e547120", null ],
    [ "mMin", "class_exercise.html#a1d4051bf283031a1da9d7da8bb9965fd", null ],
    [ "mRange", "class_exercise.html#af6a38f44fb9a139fde1179c91d1dd681", null ]
];