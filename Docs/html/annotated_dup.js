var annotated_dup =
[
    [ "Bluetooth", "class_bluetooth.html", "class_bluetooth" ],
    [ "BluetoothDevice", "class_bluetooth_device.html", "class_bluetooth_device" ],
    [ "BluetoothInfo", "class_bluetooth_info.html", "class_bluetooth_info" ],
    [ "BluetoothService", "class_bluetooth_service.html", "class_bluetooth_service" ],
    [ "Client", "class_client.html", "class_client" ],
    [ "Exercise", "class_exercise.html", "class_exercise" ],
    [ "Extensions", "struct_extensions.html", "struct_extensions" ],
    [ "Hasher", "class_hasher.html", "class_hasher" ],
    [ "MainWindow", "class_main_window.html", "class_main_window" ],
    [ "SensorComponent", "struct_sensor_component.html", "struct_sensor_component" ],
    [ "SensorModule", "class_sensor_module.html", "class_sensor_module" ],
    [ "Session", "class_session.html", "class_session" ],
    [ "Settings", "class_settings.html", "class_settings" ],
    [ "XmlParser", "class_xml_parser.html", "class_xml_parser" ]
];