/**
 * @file        lra_tests.cpp
 * @author      Tomas Bartosik
 * @date        20.09.2021
 * @brief       tests definition file for Limb Range Analyzer project
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include <QtTest>
#include <QCoreApplication>
#include <QProcess>
#include <QElapsedTimer>
#include "../Headers/sensormodule.hpp"
#include "../Headers/xmlparser.hpp"
#include "../Headers/hasher.hpp"

/*Private defines: ----------------------------------------------------------*/
#define REPEAT_COUNT 5

/*Class definition: ---------------------------------------------------------*/
class LimbRangeAnalyzerTest : public QObject
{
    Q_OBJECT

public:
    LimbRangeAnalyzerTest();
    ~LimbRangeAnalyzerTest();

    void LRA_IncrementCompletionCount();

private slots:
    void LRA_StringFormattingTest();
    void LRA_RollFilteringTest();
    void LRA_PitchFilteringTest();
    void LRA_YawFilteringTest();
    void LRA_FilteringSpeedTest();
    void LRA_ClientBlankCreationTest();
    void LRA_ClientCtorCreationTest();
    void LRA_ClientFullCreationTest();
    void LRA_SessionBlankCreationTest();
    void LRA_SessionCtorCreationTest();
    void LRA_SessionFullCreationTest();
    void LRA_ExerciseBlankCreationTest();
    void LRA_ExerciseCtorCreationTest();
    void LRA_HasherTest();
    void LRA_ClientXMLFunctionalityTest();
    void LRA_SettingsXMLFunctionalityTest();

    void summaryTestCase();

private:
    SensorModule *mSensorModule;
    QProcess mExporter;
    quint16 mAssertions;
    quint32 mCompletions;
    quint32 mCompletionsSingle;
    quint32 mCompletionsDual;
};

LimbRangeAnalyzerTest::LimbRangeAnalyzerTest() : mSensorModule(nullptr), mAssertions(0),
    mCompletions(0), mCompletionsSingle(0), mCompletionsDual(0)
{
    mExporter.setWorkingDirectory(QCoreApplication::applicationDirPath());
    mSensorModule = new SensorModule();

    connect(mSensorModule, &SensorModule::SensorModule_SignalProvideResults, this, &LimbRangeAnalyzerTest::LRA_IncrementCompletionCount);
}

LimbRangeAnalyzerTest::~LimbRangeAnalyzerTest()
{
    mExporter.terminate();
    delete mSensorModule;
}

/*Limb Range Analyzer tests: ---------------------------------------------------*/
void LimbRangeAnalyzerTest::LRA_StringFormattingTest()
{
    SensorModule sensorModule;
    /*Initial state test: ---------------------------------------------------*/
    QVERIFY(sensorModule.SensorModule_GetAccelerometerX(DeviceType::ePrimary) == 0);
    QVERIFY(sensorModule.SensorModule_GetAccelerometerY(DeviceType::ePrimary) == 0);
    QVERIFY(sensorModule.SensorModule_GetAccelerometerZ(DeviceType::ePrimary) == 0);
    QVERIFY(sensorModule.SensorModule_GetGyroscopeZ(DeviceType::ePrimary) == 0);
    QVERIFY(sensorModule.SensorModule_GetGyroscopeY(DeviceType::ePrimary) == 0);
    QVERIFY(sensorModule.SensorModule_GetGyroscopeX(DeviceType::ePrimary) == 0);
    mAssertions += 6;

    QString hexadecimalInput = QString("20000000")   /*32 in decimal (accelerometer X)*/
            + QString("07010000")   /*263 in decimal (accelerometer Y)*/
            + QString("40000100")   /*65600 in decimal (accelerometer Z)*/
            + QString("00030000")   /*768 in decimal (gyroscope Z)*/
            + QString("10000000")   /*16 in decimal (gyroscope Y)*/
            + QString("09000000");  /*9 in decimal (gyroscope X)*/
    sensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    /*Defined state test: ---------------------------------------------------*/
    QVERIFY(sensorModule.SensorModule_GetAccelerometerX(DeviceType::ePrimary) == 32);
    QVERIFY(sensorModule.SensorModule_GetAccelerometerY(DeviceType::ePrimary) == 263);
    QVERIFY(sensorModule.SensorModule_GetAccelerometerZ(DeviceType::ePrimary) == 65600);
    QVERIFY(sensorModule.SensorModule_GetGyroscopeZ(DeviceType::ePrimary) == 768);
    QVERIFY(sensorModule.SensorModule_GetGyroscopeY(DeviceType::ePrimary) == 16);
    QVERIFY(sensorModule.SensorModule_GetGyroscopeX(DeviceType::ePrimary) == 9);
    mAssertions += 6;

    hexadecimalInput = QString("FFFFFFFF")   /*-1 in decimal (accelerometer X)*/
            + QString("F9FEFFFF")   /*-263 in decimal (accelerometer Y)*/
            + QString("C0FFFEFF")   /*-65600 in decimal (accelerometer Z)*/
            + QString("00FDFFFF")   /*-768 in decimal (gyroscope Z)*/
            + QString("F0FFFFFF")   /*-16 in decimal (gyroscope Y)*/
            + QString("F7FFFFFF");  /*-9 in decimal (gyroscope X)*/
    sensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    /*Defined state test (negative values): ---------------------------------*/
    QVERIFY(sensorModule.SensorModule_GetAccelerometerX(DeviceType::ePrimary) == -1);
    QVERIFY(sensorModule.SensorModule_GetAccelerometerY(DeviceType::ePrimary) == -263);
    QVERIFY(sensorModule.SensorModule_GetAccelerometerZ(DeviceType::ePrimary) == -65600);
    QVERIFY(sensorModule.SensorModule_GetGyroscopeZ(DeviceType::ePrimary) == -768);
    QVERIFY(sensorModule.SensorModule_GetGyroscopeY(DeviceType::ePrimary) == -16);
    QVERIFY(sensorModule.SensorModule_GetGyroscopeX(DeviceType::ePrimary) == -9);
    mAssertions += 6;
}

void LimbRangeAnalyzerTest::LRA_RollFilteringTest()
{
    /*
     * Roll range rules: input range from 0 to 1023 equals 0 to 90° rotation
     * */

    SensorModule sensorModule;
    QString hexadecimalInput = QString("04000000")   /*4 in decimal (accelerometer X)*/
            + QString("00000000")   /*0 in decimal (accelerometer Y)*/
            + QString("00000000")   /*0 in decimal (accelerometer Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Y)*/
            + QString("00000000");  /*0 in decimal (gyroscope X)*/
    sensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    QVERIFY(sensorModule.SensorModule_GetCurrentRoll(DeviceType::ePrimary) == 0);

    hexadecimalInput = QString("CC010000")   /*460 in decimal (accelerometer X)*/
            + QString("00000000")   /*0 in decimal (accelerometer Y)*/
            + QString("00000000")   /*0 in decimal (accelerometer Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Y)*/
            + QString("00000000");  /*0 in decimal (gyroscope X)*/
    sensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    QVERIFY(sensorModule.SensorModule_GetCurrentRoll(DeviceType::ePrimary) == 45);

    hexadecimalInput = QString("9C030000")   /*924 in decimal (accelerometer X)*/
            + QString("00000000")   /*0 in decimal (accelerometer Y)*/
            + QString("00000000")   /*0 in decimal (accelerometer Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Y)*/
            + QString("00000000");  /*0 in decimal (gyroscope X)*/
    sensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    QVERIFY(sensorModule.SensorModule_GetCurrentRoll(DeviceType::ePrimary) == 90);

    hexadecimalInput = QString("CCFEFFFF")   /*-308 in decimal (accelerometer X)*/
            + QString("00000000")   /*0 in decimal (accelerometer Y)*/
            + QString("00000000")   /*0 in decimal (accelerometer Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Y)*/
            + QString("00000000");  /*0 in decimal (gyroscope X)*/
    sensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    QVERIFY(sensorModule.SensorModule_GetCurrentRoll(DeviceType::ePrimary) == -30);

    mAssertions += 4;
}

void LimbRangeAnalyzerTest::LRA_PitchFilteringTest()
{
    /*
     * Pitch range rules: input range from 0 to 1023 equals 0 to 90° rotation
     * */

    SensorModule sensorModule;
    QString hexadecimalInput = QString("00000000")   /*0 in decimal (accelerometer X)*/
            + QString("00000000")   /*0 in decimal (accelerometer Y)*/
            + QString("00000000")   /*0 in decimal (accelerometer Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Y)*/
            + QString("00000000");  /*0 in decimal (gyroscope X)*/
    sensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    QVERIFY(sensorModule.SensorModule_GetCurrentPitch(DeviceType::ePrimary) == 0);

    hexadecimalInput = QString("00000000")   /*0 in decimal (accelerometer X)*/
            + QString("34010000")   /*308 in decimal (accelerometer Y)*/
            + QString("00000000")   /*0 in decimal (accelerometer Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Y)*/
            + QString("00000000");  /*0 in decimal (gyroscope X)*/
    sensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    QVERIFY(sensorModule.SensorModule_GetCurrentPitch(DeviceType::ePrimary) == 30);

    hexadecimalInput = QString("00000000")   /*0 in decimal (accelerometer X)*/
            + QString("9C030000")   /*924 in decimal (accelerometer Y)*/
            + QString("00000000")   /*0 in decimal (accelerometer Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Y)*/
            + QString("00000000");  /*0 in decimal (gyroscope X)*/
    sensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    QVERIFY(sensorModule.SensorModule_GetCurrentPitch(DeviceType::ePrimary) == 90);

    hexadecimalInput = QString("00000000")   /*0 in decimal (accelerometer X)*/
            + QString("34FEFFFF")   /*-460 in decimal (accelerometer Y)*/
            + QString("00000000")   /*0 in decimal (accelerometer Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Y)*/
            + QString("00000000");  /*0 in decimal (gyroscope X)*/
    sensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    QVERIFY(sensorModule.SensorModule_GetCurrentPitch(DeviceType::ePrimary) == -45);

    mAssertions += 4;
}

void LimbRangeAnalyzerTest::LRA_YawFilteringTest()
{
    /*
     * Initial state test only (due to continuous processing technique)
     * */

    SensorModule firstSensorModule;
    /*Zero values: ----------------------------------------------------------*/
    QString hexadecimalInput = QString("00000000")   /*0 in decimal (accelerometer X)*/
            + QString("00000000")   /*0 in decimal (accelerometer Y)*/
            + QString("00000000")   /*0 in decimal (accelerometer Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Z)*/
            + QString("00000000")   /*0 in decimal (gyroscope Y)*/
            + QString("00000000");  /*0 in decimal (gyroscope X)*/
    firstSensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    QVERIFY(firstSensorModule.SensorModule_GetCurrentYaw(DeviceType::ePrimary) == 0);

    SensorModule secondSensorModule;
    /*With initial state: ---------------------------------------------------*/
    hexadecimalInput = QString("0C400000")   /*16396 in decimal (accelerometer X)*/
            + QString("2F000000")   /*47 in decimal (accelerometer Y)*/
            + QString("01000000")   /*1 in decimal (accelerometer Z)*/
            + QString("30000000")   /*48 in decimal (gyroscope Z)*/
            + QString("A20F0000")   /*4002 in decimal (gyroscope Y)*/
            + QString("00500000");  /*20480 in decimal (gyroscope X)*/
    secondSensorModule.SensorModule_SetSensorValues(hexadecimalInput);
    QVERIFY(secondSensorModule.SensorModule_GetCurrentYaw(DeviceType::ePrimary) == 0);

    mAssertions += 2;
}

void LimbRangeAnalyzerTest::LRA_FilteringSpeedTest()
{
    QString hexadecimalInputSingle = QString("30000000")   /*48 in decimal (accelerometer X)*/
            + QString("08000000")   /*8 in decimal (accelerometer Y)*/
            + QString("10000000")   /*16 in decimal (accelerometer Z)*/
            + QString("0E000000")   /*14 in decimal (gyroscope Z)*/
            + QString("12000000")   /*18 in decimal (gyroscope Y)*/
            + QString("04000000");  /*4 in decimal (gyroscope X)*/

    QString hexadecimalInputDual = QString("30000000")   /*48 in decimal (accelerometer X)*/
            + QString("08000000")   /*8 in decimal (accelerometer Y)*/
            + QString("10000000")   /*16 in decimal (accelerometer Z)*/
            + QString("0E000000")   /*14 in decimal (gyroscope Z)*/
            + QString("12000000")   /*18 in decimal (gyroscope Y)*/
            + QString("04000000")  /*4 in decimal (gyroscope X)*/
            + QString("30000000")   /*48 in decimal (accelerometer X)*/
            + QString("08000000")   /*8 in decimal (accelerometer Y)*/
            + QString("10000000")   /*16 in decimal (accelerometer Z)*/
            + QString("0E000000")   /*14 in decimal (gyroscope Z)*/
            + QString("12000000")   /*18 in decimal (gyroscope Y)*/
            + QString("04000000");  /*4 in decimal (gyroscope X)*/

    QElapsedTimer eTimer;

    qDebug() << "\t Measuring average filtering speed, please wait...";

    for(int index = 0; index < REPEAT_COUNT; index++)
    {
         eTimer.start();
         while(eTimer.elapsed() < 1000)
         {
             mSensorModule->SensorModule_SetSensorValues(hexadecimalInputSingle);
         }
    }
    mCompletionsSingle = mCompletions / REPEAT_COUNT;
    mCompletions = 0;

    for(int index = 0; index < REPEAT_COUNT; index++)
    {
         eTimer.start();
         while(eTimer.elapsed() < 1000)
         {
             mSensorModule->SensorModule_SetSensorValues(hexadecimalInputDual);
         }
    }
    mCompletionsDual = mCompletions / REPEAT_COUNT;
    mCompletions = 0;

    qDebug() << "\t Average filtering completions per second (1 device): " << mCompletionsSingle;
    qDebug() << "\t Average filtering completions per second (2 devices): " << mCompletionsDual;

    /*Require at least 200 Hz filtering speed (single device): --------------*/
    QVERIFY(mCompletionsSingle >= 200);

    /*Require at least 100 Hz filtering speed (dual devices) ----------------*/
    QVERIFY(mCompletionsDual >= 100);

    mAssertions += 2;
}

void LimbRangeAnalyzerTest::LRA_ClientBlankCreationTest()
{
    Client client;
    bool formattedListCheck = false;

    QVERIFY(QString::compare(client.Client_GetID(), QString(), Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(client.Client_GetFirstName(), QString(), Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(client.Client_GetLastName(), QString(), Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(client.Client_GetEmail(), QString(), Qt::CaseSensitive) == 0);
    QVERIFY(client.Client_GetSessionList().isEmpty() == true);
    formattedListCheck = (QString::compare(client.Client_GetAllFormatted().at(0), QString(), Qt::CaseSensitive) == 0
                     && QString::compare(client.Client_GetAllFormatted().at(1), QString(), Qt::CaseSensitive) == 0
                     && QString::compare(client.Client_GetAllFormatted().at(2), QString(), Qt::CaseSensitive) == 0
                     && QString::compare(client.Client_GetAllFormatted().at(3), QString(), Qt::CaseSensitive) == 0);
    QVERIFY(formattedListCheck == true);

    mAssertions += 6;
}

void LimbRangeAnalyzerTest::LRA_ClientCtorCreationTest()
{
    Client client("test_client", "John", "Doe", "jdoe@utb.cz", QList<QSharedPointer<Session>> ());
    bool formattedListCheck = false;

    QVERIFY(QString::compare(client.Client_GetID(), "test_client", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(client.Client_GetFirstName(), "John", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(client.Client_GetLastName(), "Doe", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(client.Client_GetEmail(), "jdoe@utb.cz", Qt::CaseSensitive) == 0);
    QVERIFY(client.Client_GetSessionList().isEmpty() == true);
    formattedListCheck = (QString::compare(client.Client_GetAllFormatted().at(0), "test_client", Qt::CaseSensitive) == 0
                     && QString::compare(client.Client_GetAllFormatted().at(1), "John", Qt::CaseSensitive) == 0
                     && QString::compare(client.Client_GetAllFormatted().at(2), "Doe", Qt::CaseSensitive) == 0
                     && QString::compare(client.Client_GetAllFormatted().at(3), "jdoe@utb.cz", Qt::CaseSensitive) == 0);
    QVERIFY(formattedListCheck == true);

    mAssertions += 6;
}

void LimbRangeAnalyzerTest::LRA_ClientFullCreationTest()
{
    bool formattedListCheck = false;
    QSharedPointer<Exercise> firstExercise = QSharedPointer<Exercise>(new Exercise("R1", -51, 25, 76));
    QSharedPointer<Exercise> secondExercise = QSharedPointer<Exercise>(new Exercise("R4", -109, -47, 62));
    QSharedPointer<Exercise> thirdExercise = QSharedPointer<Exercise>(new Exercise("L2", 0, 84, 84));

    QSharedPointer<Session> firstSession = QSharedPointer<Session> (new Session("2021-12-18", QList<QSharedPointer<Exercise>> ()));
    firstSession->Session_AddExercise(firstExercise);
    firstSession->Session_AddExercise(secondExercise);

    QSharedPointer<Session> secondSession = QSharedPointer<Session>(new Session("2021-12-26",
          {thirdExercise, QSharedPointer<Exercise> (new Exercise("R2", -182, -34, 148))}));

    QSharedPointer<Client> client;
    QVERIFY(client.isNull() == true);

    client = QSharedPointer<Client>(new Client("6791795fefe34a8532743461dfd91a7c", "Tony", "Tester",
          "tonytest@seznam.cz", QList<QSharedPointer<Session>> ()));

    QVERIFY(QString::compare(client->Client_GetID(), "6791795fefe34a8532743461dfd91a7c", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(client->Client_GetFirstName(), "Tony", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(client->Client_GetLastName(), "Tester", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(client->Client_GetEmail(), "tonytest@seznam.cz", Qt::CaseSensitive) == 0);
    QVERIFY(client->Client_GetSessionList().isEmpty() == true);
    formattedListCheck = (QString::compare(client->Client_GetAllFormatted().at(0), "6791795fefe34a8532743461dfd91a7c", Qt::CaseSensitive) == 0
                     && QString::compare(client->Client_GetAllFormatted().at(1), "Tony", Qt::CaseSensitive) == 0
                     && QString::compare(client->Client_GetAllFormatted().at(2), "Tester", Qt::CaseSensitive) == 0
                     && QString::compare(client->Client_GetAllFormatted().at(3), "tonytest@seznam.cz", Qt::CaseSensitive) == 0);
    QVERIFY(formattedListCheck == true);

    client->Client_AddSession(firstSession);
    QVERIFY(client->Client_GetSessionList().length() == 1);
    QVERIFY(QString::compare(client->Client_GetSessionList().first()->Session_GetID(), "2021-12-18", Qt::CaseSensitive) == 0);
    QVERIFY(client->Client_GetSessionList().first()->Session_GetExerciseList().length() == 2);
    QVERIFY(QString::compare(client->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetID(), "R1", Qt::CaseSensitive) == 0);
    QVERIFY(client->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetMin() == -51);
    QVERIFY(client->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetMax() == 25);
    QVERIFY(client->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetRange() == 76);
    QVERIFY(QString::compare(client->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetID(), "R4", Qt::CaseSensitive) == 0);
    QVERIFY(client->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetMin() == -109);
    QVERIFY(client->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetMax() == -47);
    QVERIFY(client->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetRange() == 62);

    client->Client_AddSession(secondSession);
    QVERIFY(client->Client_GetSessionList().length() == 2);
    QVERIFY(QString::compare(client->Client_GetSessionList().last()->Session_GetID(), "2021-12-26", Qt::CaseSensitive) == 0);
    QVERIFY(client->Client_GetSessionList().last()->Session_GetExerciseList().length() == 2);
    QVERIFY(QString::compare(client->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetID(), "L2", Qt::CaseSensitive) == 0);
    QVERIFY(client->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetMin() == 0);
    QVERIFY(client->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetMax() == 84);
    QVERIFY(client->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetRange() == 84);
    QVERIFY(QString::compare(client->Client_GetSessionList().last()->Session_GetExerciseList().last()->Exercise_GetID(), "R2", Qt::CaseSensitive) == 0);
    QVERIFY(client->Client_GetSessionList().last()->Session_GetExerciseList().last()->Exercise_GetMin() == -182);
    QVERIFY(client->Client_GetSessionList().last()->Session_GetExerciseList().last()->Exercise_GetMax() == -34);
    QVERIFY(client->Client_GetSessionList().last()->Session_GetExerciseList().last()->Exercise_GetRange() == 148);

    mAssertions += 29;
}

void LimbRangeAnalyzerTest::LRA_SessionBlankCreationTest()
{
    Session session;

    QVERIFY(QString::compare(session.Session_GetID(), QString(), Qt::CaseSensitive) == 0);
    QVERIFY(session.Session_GetExerciseList().isEmpty() == true);

    mAssertions += 2;
}

void LimbRangeAnalyzerTest::LRA_SessionCtorCreationTest()
{
    Session session("test_session", QList<QSharedPointer<Exercise>> ());

    QVERIFY(QString::compare(session.Session_GetID(), "test_session", Qt::CaseSensitive) == 0);
    QVERIFY(session.Session_GetExerciseList().isEmpty() == true);

    mAssertions += 2;
}

void LimbRangeAnalyzerTest::LRA_SessionFullCreationTest()
{
    QSharedPointer<Exercise> firstExercise = QSharedPointer<Exercise>(new Exercise("R1", -51, 25, 76));
    QSharedPointer<Exercise> secondExercise = QSharedPointer<Exercise>(new Exercise("R4", -109, -47, 62));
    bool firstLoadedCheck = false;
    bool secondLoadedCheck = false;

    Session session("2022-03-12", {firstExercise});
    QVERIFY(session.Session_GetExerciseList().length() == 1);
    session.Session_AddExercise(secondExercise);
    QVERIFY(session.Session_GetExerciseList().length() == 2);

    QVERIFY(QString::compare(session.Session_GetID(), "2022-03-12", Qt::CaseSensitive) == 0);
    firstLoadedCheck = (QString::compare(session.Session_GetExerciseList().first()->Exercise_GetID(), "R1", Qt::CaseSensitive) == 0
                        && session.Session_GetExerciseList().first()->Exercise_GetMin() == -51
                        && session.Session_GetExerciseList().first()->Exercise_GetMax() == 25
                        && session.Session_GetExerciseList().first()->Exercise_GetRange() == 76);
    QVERIFY(firstLoadedCheck == true);
    secondLoadedCheck = (QString::compare(session.Session_GetExerciseList().last()->Exercise_GetID(), "R4", Qt::CaseSensitive) == 0
                        && session.Session_GetExerciseList().last()->Exercise_GetMin() == -109
                        && session.Session_GetExerciseList().last()->Exercise_GetMax() == -47
                        && session.Session_GetExerciseList().last()->Exercise_GetRange() == 62);
    QVERIFY(secondLoadedCheck == true);

    mAssertions += 5;
}

void LimbRangeAnalyzerTest::LRA_ExerciseBlankCreationTest()
{
    Exercise exercise;
    bool formattedListCheck = false;

    QVERIFY(QString::compare(exercise.Exercise_GetID(), QString(), Qt::CaseSensitive) == 0);
    QVERIFY(exercise.Exercise_GetMin() == 0);
    QVERIFY(exercise.Exercise_GetMax() == 0);
    QVERIFY(exercise.Exercise_GetRange() == 0);
    formattedListCheck = (QString::compare(exercise.Exercise_GetAllFormatted().at(0), QString(), Qt::CaseSensitive) == 0
                     && QString::compare(exercise.Exercise_GetAllFormatted().at(1), "0", Qt::CaseSensitive) == 0
                     && QString::compare(exercise.Exercise_GetAllFormatted().at(2), "0", Qt::CaseSensitive) == 0
                     && QString::compare(exercise.Exercise_GetAllFormatted().at(3), "0", Qt::CaseSensitive) == 0);
    QVERIFY(formattedListCheck == true);

    mAssertions += 5;
}

void LimbRangeAnalyzerTest::LRA_ExerciseCtorCreationTest()
{
    Exercise exercise("test_exercise", -45, 61, 106);
    bool formattedListCheck = false;

    QVERIFY(QString::compare(exercise.Exercise_GetID(), "test_exercise", Qt::CaseSensitive) == 0);
    QVERIFY(exercise.Exercise_GetMin() == -45);
    QVERIFY(exercise.Exercise_GetMax() == 61);
    QVERIFY(exercise.Exercise_GetRange() == 106);
    formattedListCheck = (QString::compare(exercise.Exercise_GetAllFormatted().at(0), "test_exercise", Qt::CaseSensitive) == 0
                     && QString::compare(exercise.Exercise_GetAllFormatted().at(1), "-45", Qt::CaseSensitive) == 0
                     && QString::compare(exercise.Exercise_GetAllFormatted().at(2), "61", Qt::CaseSensitive) == 0
                     && QString::compare(exercise.Exercise_GetAllFormatted().at(3), "106", Qt::CaseSensitive) == 0);
    QVERIFY(formattedListCheck == true);

    mAssertions += 5;
}

void LimbRangeAnalyzerTest::LRA_HasherTest()
{
    Hasher hasher;

    QString firstHash = hasher.Hasher_CalculateHash("JohnDoejdoe@utb.cz");

    Client client("temporary_id", "John", "Doe", "jdoe@utb.cz", QList<QSharedPointer<Session>> ());
    QStringList clientList = QStringList({client.Client_GetFirstName(), client.Client_GetLastName(), client.Client_GetEmail()});

    QVERIFY(QString::compare(hasher.Hasher_CalculateHash(clientList.join("")), firstHash, Qt::CaseSensitive) == 0);

    mAssertions += 1;
}

void LimbRangeAnalyzerTest::LRA_ClientXMLFunctionalityTest()
{
    XmlParser xmlParser;

    QSharedPointer<Exercise> firstExerciseToBeSaved = QSharedPointer<Exercise> (new Exercise("R1", 20, 60, 40));
    QSharedPointer<Exercise> secondExerciseToBeSaved = QSharedPointer<Exercise> (new Exercise("L4", -61, -10, 50));
    QSharedPointer<Exercise> thirdExerciseToBeSaved = QSharedPointer<Exercise> (new Exercise("R1", -5, 68, 73));
    QSharedPointer<Exercise> fourthExerciseToBeSaved = QSharedPointer<Exercise> (new Exercise("R3", 0, 114, 114));

    QSharedPointer<Session> firstSessionToBeSaved = QSharedPointer<Session> (new Session("2021-11-30",
                        {firstExerciseToBeSaved, secondExerciseToBeSaved}));
    QSharedPointer<Session> secondSessionToBeSaved = QSharedPointer<Session> (new Session("2021-12-05",
                        {thirdExerciseToBeSaved, fourthExerciseToBeSaved}));

    QSharedPointer<Client> firstClientToBeSaved = QSharedPointer<Client> (new Client("client_first", "Joyce", "Bright",
                        "j_bright@utb.cz", {firstSessionToBeSaved, secondSessionToBeSaved}));
    QSharedPointer<Client> secondClientToBeSaved = QSharedPointer<Client> (new Client("client_second", "Derek",
                        "Trustworthy", "d_trustworthy@utb.cz", {QSharedPointer<Session> (new Session("2022-01-14",
                        {QSharedPointer<Exercise> (new Exercise("L2", 12, 68, 56)), QSharedPointer<Exercise> (new Exercise("R4", -57, 184, 241))})),
                        QSharedPointer<Session> (new Session("2022-01-28", QList<QSharedPointer<Exercise>> ()))}));
    secondClientToBeSaved->Client_GetSessionList().last()->Session_AddExercise(QSharedPointer<Exercise> (new Exercise("L2", 8, 79, 71)));
    QList<QSharedPointer<Client>> clientListToBeSaved = {firstClientToBeSaved, secondClientToBeSaved};

    xmlParser.XmlParser_SaveClientData(clientListToBeSaved);

    QList<QSharedPointer<Client>> clientListToBeLoaded = xmlParser.XmlParser_LoadClientData();

    /*First client loading test: --------------------------------------------*/
    QVERIFY(QString::compare(clientListToBeLoaded.first()->Client_GetID(), "client_first", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(clientListToBeLoaded.first()->Client_GetFirstName(), "Joyce", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(clientListToBeLoaded.first()->Client_GetLastName(), "Bright", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(clientListToBeLoaded.first()->Client_GetEmail(), "j_bright@utb.cz", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().length() == 2);
    QVERIFY(QString::compare(clientListToBeLoaded.first()->Client_GetSessionList().first()->Session_GetID(), "2021-11-30", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().first()->Session_GetExerciseList().length() == 2);
    QVERIFY(QString::compare(clientListToBeLoaded.first()->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetID(),
                        "R1", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetMin() == 20);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetMax() == 60);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetRange() == 40);
    QVERIFY(QString::compare(clientListToBeLoaded.first()->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetID(),
                        "L4", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetMin() == -61);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetMax() == -10);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetRange() == 50);
    QVERIFY(QString::compare(clientListToBeLoaded.first()->Client_GetSessionList().last()->Session_GetID(), "2021-12-05", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().last()->Session_GetExerciseList().length() == 2);
    QVERIFY(QString::compare(clientListToBeLoaded.first()->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetID(),
                        "R1", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetMin() == -5);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetMax() == 68);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetRange() == 73);
    QVERIFY(QString::compare(clientListToBeLoaded.first()->Client_GetSessionList().last()->Session_GetExerciseList().last()->Exercise_GetID(),
                        "R3", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().last()->Session_GetExerciseList().last()->Exercise_GetMin() == 0);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().last()->Session_GetExerciseList().last()->Exercise_GetMax() == 114);
    QVERIFY(clientListToBeLoaded.first()->Client_GetSessionList().last()->Session_GetExerciseList().last()->Exercise_GetRange() == 114);

    /*Second client loading test: -------------------------------------------*/
    QVERIFY(QString::compare(clientListToBeLoaded.last()->Client_GetID(), "client_second", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(clientListToBeLoaded.last()->Client_GetFirstName(), "Derek", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(clientListToBeLoaded.last()->Client_GetLastName(), "Trustworthy", Qt::CaseSensitive) == 0);
    QVERIFY(QString::compare(clientListToBeLoaded.last()->Client_GetEmail(), "d_trustworthy@utb.cz", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().length() == 2);
    QVERIFY(QString::compare(clientListToBeLoaded.last()->Client_GetSessionList().first()->Session_GetID(), "2022-01-14", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().first()->Session_GetExerciseList().length() == 2);
    QVERIFY(QString::compare(clientListToBeLoaded.last()->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetID(),
                        "L2", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetMin() == 12);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetMax() == 68);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().first()->Session_GetExerciseList().first()->Exercise_GetRange() == 56);
    QVERIFY(QString::compare(clientListToBeLoaded.last()->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetID(),
                        "R4", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetMin() == -57);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetMax() == 184);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().first()->Session_GetExerciseList().last()->Exercise_GetRange() == 241);
    QVERIFY(QString::compare(clientListToBeLoaded.last()->Client_GetSessionList().last()->Session_GetID(), "2022-01-28", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().last()->Session_GetExerciseList().length() == 1);
    QVERIFY(QString::compare(clientListToBeLoaded.last()->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetID(),
                        "L2", Qt::CaseSensitive) == 0);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetMin() == 8);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetMax() == 79);
    QVERIFY(clientListToBeLoaded.last()->Client_GetSessionList().last()->Session_GetExerciseList().first()->Exercise_GetRange() == 71);

    mAssertions += 46;
}

void LimbRangeAnalyzerTest::LRA_SettingsXMLFunctionalityTest()
{
    XmlParser xmlParser;

    QSharedPointer<Settings> settingsToBeSaved = QSharedPointer<Settings> (new Settings);
    /*Settings: enable debug infos, disable free movement & use english: ----*/
    settingsToBeSaved->Settings_SetDebugInformations(true);
    settingsToBeSaved->Settings_SetUnrestrictedLimbMovement(false);
    settingsToBeSaved->Settings_SetLanguage(Language::eEnglish);

    xmlParser.XmlParser_SaveSettings(settingsToBeSaved);

    QSharedPointer<Settings> settingsToBeLoaded = xmlParser.XmlParser_LoadSettings();

    QVERIFY(settingsToBeLoaded->Settings_GetDebugInformations() == true);
    QVERIFY(settingsToBeLoaded->Settings_GetUnrestrictedLimbMovement() == false);
    QVERIFY(QString::compare(settingsToBeLoaded->Settings_GetLanguage(true), "1", Qt::CaseSensitive) == 0);

    /*Settings: disable debug infos, enable free movement & use czech: ----*/
    settingsToBeSaved->Settings_SetDebugInformations(false);
    settingsToBeSaved->Settings_SetUnrestrictedLimbMovement(true);
    settingsToBeSaved->Settings_SetLanguage(Language::eCzech);

    xmlParser.XmlParser_SaveSettings(settingsToBeSaved);

    settingsToBeLoaded = xmlParser.XmlParser_LoadSettings();

    QVERIFY(settingsToBeLoaded->Settings_GetDebugInformations() == false);
    QVERIFY(settingsToBeLoaded->Settings_GetUnrestrictedLimbMovement() == true);
    QVERIFY(QString::compare(settingsToBeLoaded->Settings_GetLanguage(true), "2", Qt::CaseSensitive) == 0);

    mAssertions += 6;
}

void LimbRangeAnalyzerTest::LRA_IncrementCompletionCount() {mCompletions++;}

void LimbRangeAnalyzerTest::summaryTestCase()
{
    qDebug() << "\t\t -----------------------------------------------------";
    qDebug() << "\t\t";
    qDebug() << "\t\t Total assertions: " << mAssertions;
    qDebug() << "\t\t Saving unit test output to file, please wait...";

    QStringList args;
    args << "-o" << "testResults.txt";
    mExporter.start("LimbRangeAnalyzerTests.exe", args);
    mExporter.waitForFinished(16000);

    qDebug() << "\t\t Test output saved to file 'testResults.txt'.";
}

QTEST_MAIN(LimbRangeAnalyzerTest)

/*MOC file inclusion: -------------------------------------------------------*/
#include "lra_tests.moc"
