QT += core gui widgets bluetooth testlib

CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

HEADERS = $$files(../Headers/*.hpp, false)
HEADERS -= $$files(../Headers/mainwindow.hpp, false)

SOURCES = $$files(../*.cpp, true)
SOURCES -= $$files(../Sources/mainwindow.cpp, false)
SOURCES -= $$files(../Sources/main.cpp, false)

