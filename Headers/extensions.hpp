/**
 * @file        extensions.hpp
 * @author      Tomas Bartosik
 * @date        25.03.2022
 * @brief       declaration file for static data extension structure
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

#ifndef EXTENSIONS_HPP
#define EXTENSIONS_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QObject>

/*Struct declaration: -------------------------------------------------------*/
/*!
 *  \struct Extensions
 *  \brief Extensions struct principle lies in covering of various
 *  Limb Range Analyzer application constants.
 *
 *  Extensions struct introduces constants that are designed for use by
 *  the application. While sensor filtering related and other numerical
 *  values are included, the struct in its publicly accessible mode serves
 *  as a replacement of preprocessor DEFINEs.
 */
struct Extensions
{
        /*!
         * \var static qreal sFilterSampleFrequency
         * \brief static qreal member attribute that represents
         * sensor fusion filtering sample frequency.
         */
        static qreal sFilterSampleFrequency;

        /*!
         * \var static qreal sFilterBeta
         * \brief static qreal member attribute used while
         * processing sensor fusion and representing proportional
         * gain.
         */
        static qreal sFilterBeta;

        /*!
         * \var static qreal sIMURangeMax
         * \brief static qreal attribute numerical real value
         * that holds the inertial measurement unit's maximum
         * defined range.
         */
        static qreal sIMURangeMax;

        /*!
         * \var static qreal sAccToDegs
         * \brief static qreal member attribute that holds the
         * BlueCoin accelerometer degrees conversion multiplier.
         */
        static qreal sAccToDegs;

        /*!
         * \var static qreal sAccRange
         * \brief static qreal member attribute that keeps the
         * maximum accelerometer range used (2g).
         */
        static qreal sAccRange;

        /*!
         * \var static qreal sGyroDPS
         * \brief static qreal numerical value that
         * defines the BlueCoin gyroscope degrees per
         * second parameter.
         */
        static qreal sGyroDPS;

        /*!
         * \var static qreal sRadsToDegs
         * \brief static qreal member attribute that serves
         * as a RAD/s to DEG/s conversion multiplier.
         */
        static qreal sRadsToDegs;

        /*!
         * \var static qreal sDegsToRads
         * \brief static qreal numerical constant that
         * represents the DEG/s to RAD/s conversion multiplier.
         */
        static qreal sDegsToRads;


        /*!
         * \var static qint32 sNoExerciseSelected
         * \brief static qint32 integral value interpreting
         * an easy-to-read description of no selected exercise.
         */
        static qint32 sNoExerciseSelected;

        /*!
         * \var static quint16 sAxisSwitchThreshold
         * \brief static quint16 non-negative integral value that
         * holds the limit of measured range in degrees before
         * a vertical axis choice (\b X or \b Y) is made.
         */
        static quint16 sAxisSwitchThreshold;

        /*!
         * \var static quint16 sMovementOrbsCount
         * \brief static quint16 non-negative integral value
         * meant for keeping the amount of auxiliary orbs used
         * while measuring and presenting 3D graphics.
         */
        static quint16 sMovementOrbsCount;


        /*!
         * \var static quint16 sClientTableColumns
         * \brief static quint16 non-negative integer that holds
         * the number of Client \c QTableWidget columns.
         */
        static quint16 sClientTableColumns;

        /*!
         * \var static quint16 sHistoryTableColumns
         * \brief static quint16 non-negative integer that holds
         * the number of measurement history \c QTableWidget
         * columns.
         */
        static quint16 sHistoryTableColumns;
};

#endif // EXTENSIONS_HPP
