/**
 * @file        bluetoothinfo.hpp
 * @author      Tomas Bartosik
 * @date        27.07.2021
 * @brief       declaration file for Bluetooth service informations
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef BLUETOOTHINFO_HPP
#define BLUETOOTHINFO_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QObject>
#include <QtBluetooth/QLowEnergyCharacteristic>
#include <QString>

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class BluetoothInfo
 *  \brief Auxiliary class that manages Bluetooth characteristics object.
 *
 *  Class BluetoothInfo is used in order to process Bluetooth characteristics
 *  contained within a service, thus UUID and respective values are obtained.
 *  This class is used alongside BluetoothDevice and BluetoothService as a part
 *  of Bluetooth module.
 *  \see Bluetooth, BluetoothDevice, BluetoothService
 */
class BluetoothInfo : public QObject
{
    Q_OBJECT

public:
    /*!
     *  \fn BluetoothInfo()
     *  \brief Class constructor of BluetoothInfo.
     *
     *  Default constructor of BluetoothInfo class
     *  with no parameters.
     *  \see ~BluetoothInfo()
     */
    BluetoothInfo();

    /*!
     *  \fn BluetoothInfo(const QLowEnergyCharacteristic &characteristic)
     *  \brief Parametric class copy constructor of BluetoothInfo.
     *
     *  Copy constructor of class BluetoothInfo that uses parameter-given
     *  Bluetooth characteristics object.
     *  \see ~BluetoothInfo()
     */
    BluetoothInfo(const QLowEnergyCharacteristic &characteristic);

    /*!
     *  \fn ~BluetoothInfo()
     *  \brief Default BluetoothInfo destructor.
     *
     *  Secures memory related operations when
     *  object instance gets destoyed.
     *  \see BluetoothInfo(), BluetoothInfo(const QLowEnergyCharacteristic &characteristic)
     */
    ~BluetoothInfo();

    /*!
     *  \fn BluetoothInfo_SetCharacteristics(const QLowEnergyCharacteristic &characteristic)
     *  \brief Setter function that updates \c mCharacteristic value.
     *
     *  This setter function updates \c QLowEnergyCharacteristic member attribute
     *  with the use of given parameter.
     *
     *  \param characteristic - characteristic description parameter meant for
     *  Bluetooth service characteristics
     */
    void BluetoothInfo_SetCharacteristics(const QLowEnergyCharacteristic &characteristic);

    /*!
     *  \fn Bluetooth_GetCharacteristics() const
     *  \brief Getter function for Bluetooth service characteristics.
     *
     *  This function returns current characteristics included
     *  in a previously obtained Bluetooth service.
     *  \return QLowEnergyCharacteristic - Bluetooth characteristic
     *  object with its respective properties
     */
    QLowEnergyCharacteristic Bluetooth_GetCharacteristics() const;

    /*!
     *  \fn BluetoothInfo_GetUUID() const
     *  \brief Getter function for converted characteristics UUID.
     *
     *  Getter function that transforms UUID of current characteristics
     *  and returns it as a \c QString without its unnecessary characters.
     *  \return QString - Bluetooth characteristics identifier (UUID)
     */
    QString BluetoothInfo_GetUUID() const;

    /*!
     *  \fn BluetoothInfo_GetValue() const
     *  \brief Getter function for characteristics value.
     *
     *  This getter member function receives value of a characteristics,
     *  transforms it into hexadecimal output and returns it as a \c QString .
     *  Note that empty read value causes empty string returning.
     *  \return QString - characteristics value returned as a hexadecimal text string
     */
    QString BluetoothInfo_GetValue() const;

private:
    /*!
     * \var QLowEnergyCharacteristic mCharacteristic
     * \brief QLowEnergyCharacteristic object that describes
     * Bluetooth Low Energy characteristics.
     */
    QLowEnergyCharacteristic mCharacteristic;
};

#endif // BLUETOOTHINFO_HPP
