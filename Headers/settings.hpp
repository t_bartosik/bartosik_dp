/**
 * @file        settings.hpp
 * @author      Tomas Bartosik
 * @date        26.03.2022
 * @brief       declaration file for application settings class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

#ifndef SETTINGS_HPP
#define SETTINGS_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QObject>

/*Enumeration declarations: -------------------------------------------------*/
/*!
 * \enum Language
 * \brief Language enumeration type distinguishes between possible Limb
 * Range Analyzer UI languages.
 *
 * Two languages are currently supported - \b English and \b Czech,
 * hence two options are available.
 */
enum class Language
{
    eEnglish = 1,       /*!< English language will be used. */
    eCzech              /*!< Czech language will be used. */
};

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class Settings
 *  \brief Class responsible for application settings description and management.
 *
 *  Application settings are handled through user interface section (see MainWindow),
 *  as well as permanently stored via XmlParser module. This class encapsulates
 *  possible application options, which users may change to fit their requirements.
 *  \see MainWindow, XmlParser
 */
class Settings
{
    public:
        /*!
         *  \fn Settings()
         *  \brief Default class constructor for Settings module.
         *
         *  A default constructor function responsible for actions
         *  bound with instance creation.
         *
         *  \see ~Settings()
         */
        Settings();

        /*!
         *  \fn ~Settings()
         *  \brief Destructor designed for class Settings.
         *
         *  Base destructor function with no custom-defined
         *  properties.
         *
         *  \see Settings()
         */
        ~Settings();


        /*!
         *  \fn Settings_SetDebugInformations(bool debugInformations)
         *  \brief Setter function designed for debugging informations
         *  preference changes.
         *
         *  This setter function accepts a parameter that is set to
         *  \c mEnableDebugInformations member variable.
         *
         *  \param debugInformations - a boolean value that in its
         *  \c TRUE state enables additional debugging informations,
         *  \c FALSE stands for its deactivation
         */
        void Settings_SetDebugInformations(bool debugInformations);

        /*!
         *  \fn Settings_SetUnrestrictedLimbMovement(bool unrestrictedLimbMovement)
         *  \brief Setter function designed for unrestricted limb movement
         *  preference setting.
         *
         *  Setter function that modifies \c mEnableUnrestrictedLimbMovement member
         *  through received parameter.
         *
         *  \param unrestrictedLimbMovement - a boolean value that in its
         *  \c TRUE state enables unrestricted limb movement while 3D visualization
         *  is running, \c FALSE sets it otherwise
         */
        void Settings_SetUnrestrictedLimbMovement(bool unrestrictedLimbMovement);

        /*!
         *  \fn Settings_SetLanguage(Language language)
         *  \brief Setter function meant for language preference changing.
         *
         *  This function changes the language of Limb Range Analyzer to
         *  value set by parameter.
         *
         *  \param language - enumeration item that represents
         *  the preferred language
         *  \see Language
         */
        void Settings_SetLanguage(Language language);

        /*!
         *  \fn Settings_GetDebugInformations
         *  \brief Getter member function that returns mEnableDebugInformations
         *  member variable.
         *
         *  Function is designed for reading of member describing the choice
         *  of additional debugging informations.
         *
         *  \return bool - \c TRUE in case of enabled debugging informations,
         *  \c FALSE otherwise
         */
        bool Settings_GetDebugInformations();

        /*!
         *  \fn Settings_GetUnrestrictedLimbMovement
         *  \brief Getter function that returns previously set
         *  option of unrestricted limb movement.
         *
         *  Function returns mEnableUnrestrictedLimbMovement for
         *  its further processing.
         *
         *  \return bool - \c TRUE if unrestricted movement is enabled,
         *  \c FALSE otherwise
         */
        bool Settings_GetUnrestrictedLimbMovement();

        /*!
         *  \fn Settings_GetLanguage(bool getByID)
         *  \brief Getter function with parameter meant for
         *  selected language choice reading.
         *
         *  Function accepts a parameter and returns a text string
         *  with the description of selected language. If \c TRUE is
         *  passed to this function, a string containing an index of
         *  selected language is returned. In case of \c FALSE at
         *  function input, this function returns a text string with
         *  selected and translated language description.
         *  \param getByID - a choice of output format
         *  \return QString - a language index text string or its
         *  description
         */
        QString Settings_GetLanguage(bool getByID);

    private:
        /*!
         * \var bool mEnableDebugInformations
         * \brief bool that keeps the information whether
         * debugging informations are meant to be reported.
         */
        bool mEnableDebugInformations;

        /*!
         * \var bool mEnableUnrestrictedLimbMovement
         * \brief bool value that describes the choice of
         * unrestricted dual limb movement.
         */
        bool mEnableUnrestrictedLimbMovement;

        /*!
         * \var Language mLanguage
         * \brief Language member variable that keeps the
         * currently selected language.
         */
        Language mLanguage;
};

#endif // SETTINGS_HPP
