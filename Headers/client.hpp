/**
 * @file        client.hpp
 * @author      Tomas Bartosik
 * @date        01.03.2022
 * @brief       declaration file for class Client
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

#ifndef CLIENT_HPP
#define CLIENT_HPP

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/session.hpp"
#include <QObject>

/*Structure declaration: ----------------------------------------------------*/
/*!
 *  \class Client
 *  \brief Class Client is designed for Limb Range Analyzer application
 *  patient data representation.
 *
 *  Client encapsulates necessary member variables and functions
 *  that (along with Session lists and their respective Exercise records)
 *  depict a patient data model, which is used by the application for
 *  measurement range advancement monitoring. Each Client record
 *  is unique. Measurement records are saved via XmlParser module,
 *  while Hasher class provides unique identifier creation.
 *  \see Session, Exercise, XmlParser, Hasher
 */
class Client : public QObject
{
    Q_OBJECT

    public:
        /*!
         *  \fn Client()
         *  \brief Default constructor for class Client.
         *
         *  Non-parametric Client class constructor.
         *
         *  \see ~Client()
         */
        Client();

        /*!
         *  \fn Client(QString id, QString firstName, QString lastName,
         *  QString email, QList<QSharedPointer<Session>> sessionList)
         *  \brief Parametric constructor designed for Client class.
         *
         *  Constructor that allows instance creation with defined
         *  Client attributes. These include patient's \b ID, \b first \b name,
         *  \b last \b name, \b e-mail and a \b list \b of \b sessions.
         *
         *  \see ~Client()
         */
        Client(QString id, QString firstName, QString lastName, QString email, QList<QSharedPointer<Session>> sessionList);

        /*!
         *  \fn ~Client()
         *  \brief Class destructor in its base form.
         *
         *  Class destructor function responsible for
         *  previously allocated memory cleaning.
         *
         *  \see Client(), Client(QString id, QString firstName, QString lastName,
         *  QString email, QList<QSharedPointer<Session>> sessionList)
         */
        ~Client();

        /*!
         *  \fn Client_GetID
         *  \brief Getter function for mID obtaining.
         *
         *  Member function that returns member variable
         *  \c mID .
         *
         *  \return QString - Client specific hashed string
         *  identifier
         */
        QString& Client_GetID();

        /*!
         *  \fn Client_GetFirstName
         *  \brief Member getter function that returns
         *  mFirstName member attribute.
         *
         *  Function designed for \c mFirstName string
         *  retrieving.
         *
         *  \return QString - text string with the first name
         *  of a patient
         */
        QString& Client_GetFirstName();

        /*!
         *  \fn Client_GetLastName
         *  \brief Getter function that retrieves mLastName.
         *
         *  The sole purpose of this function is to return
         *  Client's last name.
         *
         *  \return QString - text string containing last
         *  name of a person
         */
        QString& Client_GetLastName();

        /*!
         *  \fn Client_GetEmail
         *  \brief Function that returns patient's e-mail address.
         *
         *  This function returns a client's e-mail address. Such
         *  address is unique for each Client.
         *
         *  \return QString - unique e-mail address of a client
         */
        QString& Client_GetEmail();

        /*!
         *  \fn Client_GetSessionList
         *  \brief Getter function that allows the possibility
         *  of private member mSessionList reading.
         *
         *  This function returns \c mSessionList - a client
         *  specific list of sessions, which may also be empty.
         *
         *  \return QList<QSharedPointer<Session>> - a list of
         *  Session records
         */
        QList<QSharedPointer<Session>>& Client_GetSessionList();

        /*!
         *  \fn Client_GetAllFormatted
         *  \brief Public getter function that returns patient's
         *  attributes in a text form.
         *
         *  Member function designed for client's \b ID, \b first
         *  \b name, \b last \b name and \b e-mail \b address
         *  reading.
         *
         *  \return QStringList - a list of client's attributes (without
         *  session list retrieval)
         */
        QStringList Client_GetAllFormatted();


        /*!
         *  \fn Client_AddSession(QSharedPointer<Session> session)
         *  \brief Setter function responsible for new Session adding.
         *
         *  This function introduces the possibility of new Session
         *  adding.
         *
         *  \param session - a new Session record that may or may
         *  not include a list of Exercise measurement data
         */
        void Client_AddSession(QSharedPointer<Session> session);

    private:
        /*!
         * \var QString mID
         * \brief QString member variable that holds the unique
         * Client identifier constructed by Hasher module.
         */
        QString mID;

        /*!
         * \var QString mFirstName
         * \brief QString member that keeps patient's
         * first name.
         */
        QString mFirstName;

        /*!
         * \var QString mLastName
         * \brief QString text variable introduced for
         * last name keeping.
         */
        QString mLastName;

        /*!
         * \var QString mEmail
         * \brief QString private member variable
         * containing a unique e-mail address of a patient.
         */
        QString mEmail;

        /*!
         * \brief QList<QSharedPointer<Session>> member composed
         * of Client's Session records.
         */
        QList<QSharedPointer<Session>> mSessionList;
};

#endif // CLIENT_HPP
