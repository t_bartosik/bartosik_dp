/**
 * @file        session.hpp
 * @author      Tomas Bartosik
 * @date        01.03.2022
 * @brief       declaration file for session informations class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

#ifndef SESSION_HPP
#define SESSION_HPP

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/exercise.hpp"
#include <QObject>
#include <QSharedPointer>
#include <QDateTime>
#include <QList>

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class Session
 *  \brief Session class encapsulates daily progress of
 *  a Client's exercise measurement.
 *
 *  This class is introduced in order to keep session records
 *  of a Client. A session is composed of a list of Exercise
 *  instances and an identifier of a Session's date.
 *  \see Client, Exercise
 */
class Session : public QObject
{
    Q_OBJECT

    public:
        /*!
         *  \fn Session()
         *  \brief Default class constructor of Session.
         *
         *  Base constructor of class Session.
         *
         *  \see ~Session()
         */
        Session();

        /*!
         *  \fn Session(QString id, QList<QSharedPointer<Exercise>> exerciseList)
         *  \brief Parametric Session class constructor.
         *
         *  Through this constructor, new instance of Session is
         *  created, while session ID and a list of Exercise instances
         *  is provided.
         *  \see ~Session(), Exercise
         */
        Session(QString id, QList<QSharedPointer<Exercise>> exerciseList);

        /*!
         *  \fn ~Session()
         *  \brief Session class base destructor.
         *
         *  Default class destructor needed for memory
         *  deallocation.
         *  \see Session(), Session(QString id, QList<QSharedPointer<Exercise>> exerciseList)
         */
        ~Session();


        /*!
         *  \fn Session_GetID
         *  \brief Member function meant for session identifier
         *  returning.
         *
         *  Getter function used in order to acquire \c mID member
         *  variable.
         *  \return QString - text string carrying Session date ID
         */
        QString& Session_GetID();

        /*!
         *  \fn Session_GetExerciseList
         *  \brief Function designed for Exercise list retrieving.
         *
         *  This function returns Exercise list member variable.
         *  \return QList<QSharedPointer<Exercise>> - a list
         *  of Exercise object pointers to be returned
         */
        QList<QSharedPointer<Exercise>>& Session_GetExerciseList();

        /*!
         *  \fn Session_AddExercise(QSharedPointer<Exercise> exercise)
         *  \brief Member function with a purpose of adding single Exercise
         *  record.
         *
         *  Function that uses a parameter-given Exercise and manages its
         *  insertion into Session. New entries are made in case of empty
         *  session list or if a specific exercise record does not yet exist.
         *  Previously created exercise record is overwritten.
         *  \param exercise - an exercise record meant for addition to a session
         *  \see Exercise
         */
        void Session_AddExercise(QSharedPointer<Exercise> exercise);

    private:
        /*!
         * \var QString mID
         * \brief QString member variable that keeps the
         * identifier (in form of a date) of a session.
         */
        QString mID;

        /*!
         * \brief QList<QSharedPointer<Exercise>> member representing
         * a list of exercise records.
         */
        QList<QSharedPointer<Exercise>> mExerciseList;
};

#endif // SESSION_HPP
