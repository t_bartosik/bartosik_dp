/**
 * @file        mainwindow.hpp
 * @author      Tomas Bartosik
 * @date        21.07.2021
 * @brief       header file for MainWindow
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/sensormodule.hpp"
#include "../Headers/xmlparser.hpp"
#include "../Headers/hasher.hpp"
#include <QMainWindow>
#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>
#include <Qt3DExtras/Qt3DWindow>
#include <Qt3DExtras/QForwardRenderer>
#include <Qt3DExtras/QOrbitCameraController>
#include <Qt3DExtras/QSphereMesh>
#include <Qt3DExtras/QTorusMesh>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/QPhongAlphaMaterial>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QPointLight>
#include <Qt3DRender/QSceneLoader>
#include <Qt3DRender/QMaterial>
#include <Qt3DRender/QMesh>
#include <QTranslator>
#include <QScreen>
#include <QPropertyAnimation>
#include <QScrollBar>
#include <QListWidgetItem>
#include <QMouseEvent>
#include <QPair>

/*Namespace declaration: ----------------------------------------------------*/
/*!
 * \namespace Ui
 * \brief Namespace Ui that encapsulates MainWindow class.
 *
 * This is the default namespace used for main application window.
 */
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow;}
QT_END_NAMESPACE

/*Enumeration declarations: -------------------------------------------------*/
/*!
 * \enum ExerciseType
 * \brief The ExerciseType enumeration provides exercise recognition.
 *
 * Four members are enlisted and applied for exercise filtering
 * through application user interface.
 */
enum class ExerciseType
{
    eLimbUpper = 0,     /*!< Only upper limb exercises are to be displayed. */
    eLimbUpperDual,     /*!< Only dual upper limb exercises are to be displayed. */
    eLimbLower,         /*!< Only lower limb exercises are to be displayed. */
    eLimbLowerDual      /*!< Only dual lower limb exercises are to be displayed. */
};

/*!
 * \enum BodyPart
 * \brief The BodyPart enum provides human limb recognition.
 *
 * Five members are enlisted, each of them provides exact limb
 * selection through user interface.
 */
enum class BodyPart
{
    eAll = 0,           /*!< All body parts are considered. */
    eLeftArm,           /*!< Consider left upper limb only. */
    eRightArm,          /*!< Consider right upper limb only. */
    eLeftLeg,           /*!< Consider left lower limb only. */
    eRightLeg           /*!< Consider right lower limb only. */
};

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class MainWindow
 *  \brief Represents the main class for user interface components.
 *
 *  MainWindow is \c QMainWindow derived class that implements
 *  member functions and variables. Those serve according to user
 *  interaction through user interface objects and use aggregated
 *  class instances for Bluetooth connection, measurement filtering
 *  and client-related services (among others).
 *  \see Bluetooth, SensorModule, Client, XmlParser
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     *  \fn MainWindow(QWidget *parent = nullptr)
     *  \brief Parametric default class constructor.
     *
     *  Manages initial instance creation and user interface preparation.
     *
     *  \param parent - depicts parental QWidget parameter.
     *
     *  \see ~MainWindow()
     */
    MainWindow(QWidget *parent = nullptr);

    /*!
     *  \fn ~MainWindow()
     *  \brief Default class destructor.
     *
     *  Provides memory cleaning according to previous
     *  long-term heap allocation needs.
     */
    ~MainWindow();

    /*!
     *  \fn resizeEvent(QResizeEvent *event)
     *  \brief Overrided function responding to window size changes.
     *
     *  This function provides necessary user interface content
     *  size changes triggered by main window resizing.
     *
     *  \param event - event of window size changes with event parameters.
     */
    void resizeEvent(QResizeEvent *event);

    /*!
     *  \fn MainWindow_InitializeUIProperties
     *  \brief Initially called function for UI content settings.
     *
     *  Function manages initial user interface content
     *  properties such as item visibility, shortcuts setting
     *  and exercise related attributes.
     *
     *  \see MainWindow_InitializeUIStrings
     */
    void MainWindow_InitializeUIProperties();

    /*!
     *  \fn MainWindow_InitializeUIStrings
     *  \brief This function updates user interface strings.
     *
     *  Responds to initial UI creation and language selection
     *  changes that require fixed content retranslation.
     */
    void MainWindow_InitializeUIStrings();

    /*!
     *  \fn MainWindow_ClientResizeTable
     *  \brief Serves as a client table item ratio updater.
     *
     *  Due to possible window resizing, width of each column
     *  has to be updated, which is handled in this function.
     */
    void MainWindow_ClientResizeTable();

    /*!
     *  \fn MainWindow_HistoryResizeTable
     *  \brief Manages history table visual updating.
     *
     *  According to possible main window size changes,
     *  column width of measurement history table has to
     *  be updated. \c MainWindow_HistoryResizeTable manages this
     *  necessary operation.
     */
    void MainWindow_HistoryResizeTable();

    /*!
     *  \fn MainWindow_ClientDrawTable
     *  \brief Manages client table filling.
     *
     *  This function uses received Client list
     *  data to spread it across client table
     *  \c QTableWidget accordingly.
     *  \see Client
     */
    void MainWindow_ClientDrawTable();

    /*!
     *  \fn MainWindow_ClientCheck(QStringList newEntry)
     *  \brief Provides user defined client entry check.
     *
     *  MainWindow_ClientCheck uses provided client table
     *  row data and manages first name, last name and
     *  e-mail address checking. Note that the return value
     *  includes either \c "OK" string or respective error
     *  message, that would be shown to user.
     *
     *  \param newEntry - new table row entry for checking.
     *  \return QString - checked evaluation string
     *  \see Client
     */
    QString MainWindow_ClientCheck(QStringList newEntry);

    /*!
     *  \fn MainWindow_Create3DScene
     *  \brief Initialization function for 3D vizualization content
     *
     *  Function is responsible for graphics content memory
     *  allocation and member variables definition due to
     *  graphics scene needs. A body model is defined along
     *  with coloring and lights system, movable camera and
     *  item positioning. Execution of this function enables
     *  further body parts movement while measuring exercises.
     */
    void MainWindow_Create3DScene();

    /*!
     *  \fn MainWindow_Set3DScene(bool blueCoinOnly, bool clearResults)
     *  \brief Manages body and BlueCoin device repositioning within 3D scene.
     *
     *  This function is used for exercise scene preparation. While
     *  \b blueCoinOnly parameter is \c TRUE , only BlueCoin graphics
     *  object is moved or rotated. Its \c FALSE value provides body
     *  parts movement while mSelectedExercise is evaluated. In case
     *  that \b clearResults is \c TRUE , lines containing the MIN, MAX
     *  and RANGE values are emptied.
     *
     *  \param blueCoinOnly - defines whether only BlueCoin is to be repositioned.
     *  \param clearResults - defines whether previous results are to be cleared.
     */
    void MainWindow_Set3DScene(bool blueCoinOnly, bool clearResults);

    /*!
     *  \fn MainWindow_Reset3DScene
     *  \brief Restores initial position setting of 3D scene.
     *
     *  MainWindow_Reset3DScene uses values initially introduced
     *  in \c MainWindow_Create3DScene. Resetting is processed
     *  when measurement processing gets turned off.
     *  \see MainWindow_Create3DScene
     */
    void MainWindow_Reset3DScene();

    /*!
     *  \fn MainWindow_ReportResults
     *  \brief Manages measurement results reporting.
     *
     *  This function responds to SensorModule signal emitting
     *  that marks new results availability. Those values, namely
     *  MIN, MAX and RANGE, along with ROLL, PITCH and YAW angles
     *  are processed visually in 3D scene and with according
     *  numerical value output. Note that it depends on currently
     *  selected exercise.
     */
    void MainWindow_ReportResults();

    /*!
     *  \fn MainWindow_ClearResults
     *  \brief Manages measurement results clearing.
     *
     *  MainWindow_ClearResults is triggered when
     *  limb range analysis measurement is stopped, thus
     *  user interface numerical text output goes empty again.
     */
    void MainWindow_ClearResults();

private slots:
    /*!
     *  \fn MainWindow_DevicesStartScanning
     *  \brief Slot describing Bluetooth devices analysis beginning.
     *
     *  This slot manages necessary UI changes and triggers
     *  Bluetooth module device discovery function \c Bluetooth_StartScanning .
     *  \see Bluetooth
     */
    void MainWindow_DevicesStartScanning();

    /*!
     *  \fn MainWindow_DevicesUpdateStatus
     *  \brief Slot used for Bluetooth discovery progress visualization.
     *
     *  Slot function serves for UI \c QProgressBar updating while scanning
     *  for Bluetooth devices.
     *  \see Bluetooth
     */
    void MainWindow_DevicesUpdateStatus();

    /*!
     *  \fn MainWindow_DevicesDoneScanning
     *  \brief Slot defined for Bluetooth devices discovery finish.
     *
     *  This slot function manages user interface changes due
     *  to device discovery finish. Devices are enlisted in \c QListWidget
     *  and actively connected devices are depicted by a different color.
     *  \see Bluetooth
     */
    void MainWindow_DevicesDoneScanning();

    /*!
     *  \fn MainWindow_DevicesSelectionChanged
     *  \brief Slot responding to Bluetooth devices selection change.
     *
     *  This function modifies connection button text according to
     *  selected device connection status. Connected devices can be
     *  disconnected, while disconnected ones will offer a possibility
     *  for connection.
     *  \see Bluetooth
     */
    void MainWindow_DevicesSelectionChanged();

    /*!
     *  \fn MainWindow_DevicesConnect
     *  \brief This slot is used for Bluetooth connection triggering.
     *
     *  This slot manages checking whether a device is selected and then
     *  processes its connection or disconnection (depending on its connection
     *  state) through Bluetooth member functions \c Bluetooth_Connect resp.
     *  \c Bluetooth_Disconnect .
     *  \see Bluetooth
     */
    void MainWindow_DevicesConnect();

    /*!
     *  \fn MainWindow_DeviceSwitchBodyPart
     *  \brief Slot updates BodyPart selection status.
     *
     *  According to selected \c BodyPart contained in \c QComboBox
     *  on the measurement page, a member variable mBodyPart gets
     *  updated. Exercises filtering is then processed via
     *  \c MainWindow_DeviceFilterExercises . Any measurement results are
     *  also cleared.
     *  \see BodyPart, MainWindow_DeviceFilterExercises
     */
    void MainWindow_DeviceSwitchBodyPart();

    /*!
     *  \fn MainWindow_DeviceFilterExercises
     *  \brief Slot function is responding to exercise selection changes.
     *
     *  Depending on the selected \c BodyPart , an exercise is selected from
     *  list of all exercises, which is then added to the \c QComboBox of
     *  possible measurement selections. Note that it depends on the number
     *  of actively connected devices. Necessary UI components are also updated.
     *  \see BodyPart
     */
    void MainWindow_DeviceFilterExercises();

    /*!
     *  \fn MainWindow_DeviceMeasure
     *  \brief Slot defined for measurement beginning or stopping.
     *
     *  This slot checks whether an active measurement is being processed,
     *  if not, then a new measurement of selected exercise is started, which
     *  allows further visualization and range analysis reporting. Running exercise
     *  processing gets stopped by this slot function and the scene would be reset.
     */
    void MainWindow_DeviceMeasure();

    /*!
     *  \fn MainWindow_DeviceNoConnectionReport(const QString &error)
     *  \brief Slot function that serves for no connection reporting.
     *
     *  Slot function uses \c QMessageBox to show information dialog
     *  describing the connection error.
     *  \param error - text string of captured error
     *  \see Bluetooth
     */
    void MainWindow_DeviceNoConnectionReport(const QString &error);

    /*!
     *  \fn MainWindow_DeviceConnectedReport
     *  \brief Slot that informs about Bluetooth device connection.
     *
     *  Function (slot) informs about a successful connection request
     *  that marks the selected device with different color and enables
     *  measurement page.
     *  \see Bluetooth
     */
    void MainWindow_DeviceConnectedReport();

    /*!
     *  \fn MainWindow_DeviceIncompatibleReport
     *  \brief Slot defined for device incompatibility reporting.
     *
     *  This slot informs the application user that selected device
     *  cannot be connected to due to incompatibility (meaning that
     *  the device is not BlueCoin, which is mandatory). Such device
     *  also gets painted by a different background color in the user
     *  interface device list widget.
     *  \see Bluetooth
     */
    void MainWindow_DeviceIncompatibleReport();

    /*!
     *  \fn MainWindow_DeviceUnreachableReport
     *  \brief This slot informs users about device unreachability.
     *
     *  A \c QMessageBox is used along with a different background
     *  color of selected device in order to inform users about chosen
     *  device unreachability.
     *  \see Bluetooth
     */
    void MainWindow_DeviceUnreachableReport();

    /*!
     *  \fn MainWindow_DeviceBeyondLimitReport
     *  \brief Slot function defined for connection limit reaching information.
     *
     *  This slot uses \c QMessageBox to inform users about impossibility
     *  to connect more devices - up to two BlueCoin devices can be connected.
     *  \see Bluetooth
     */
    void MainWindow_DeviceBeyondLimitReport();

    /*!
     *  \fn MainWindow_MeasurementZoomIn
     *  \brief Slot bound to one of 3D scene buttons for zooming in.
     *
     *  Slot manages 3D measurement vizualization widget zooming, namely
     *  zooming in.
     */
    void MainWindow_MeasurementZoomIn();

    /*!
     *  \fn MainWindow_MeasurementZoomOut
     *  \brief Slot manages 3D scene zooming out.
     *
     *  This slot manages selected camera attributes in order
     *  to allow zooming out.
     */
    void MainWindow_MeasurementZoomOut();

    /*!
     *  \fn MainWindow_MeasurementRotateLeft
     *  \brief Slot is used for 3D scene clockwise rotation.
     *
     *  When a specific \c QPushButton is pressed, the scene rotates left.
     */
    void MainWindow_MeasurementRotateLeft();

    /*!
     *  \fn MainWindow_MeasurementRotateRight
     *  \brief Slot manages 3D scene counter-clockwise rotation.
     *
     *  Pressing a button in application UI results in 3D measurement
     *  scene rotation, namely rightwards.
     */
    void MainWindow_MeasurementRotateRight();

    /*!
     *  \fn MainWindow_MeasurementSaveRecord
     *  \brief Slot is bound to saving process of client measurement record.
     *
     *  This slot triggers capture of currently measured exercise
     *  results and checks whether a client is selected. If that is true,
     *  then a new exercise gets recorded for this client. This slot also manages
     *  XML data saving of client changes. Note that exercise measurement data
     *  must be ready before model updating is processed.
     *  \see Client, XmlParser
     */
    void MainWindow_MeasurementSaveRecord();

    /*!
     *  \fn MainWindow_MeasurementDisplayHistory
     *  \brief Slot enables measurement history page for selected exercise and client.
     *
     *  Function (slot) collects selected client's measurement data for chosen exercise,
     *  then a new UI page is shown along with a \c QTableWidget area containing date, MIN, MAX
     *  and RANGE values of selected exercise captured in the past. Note that a client has to
     *  be previously selected and historical records of selected exercise must exist as well.
     *  \see Client
     */
    void MainWindow_MeasurementDisplayHistory();

    /*!
     *  \fn MainWindow_ClientAdd
     *  \brief Slot is responsible for new client adding.
     *
     *  This slot manages \c QTableWidget row contents capturing along with
     *  Client creation and further XML saving (if user input is correct). This
     *  slot also uses Hasher class instance for unique user ID creation.
     *  \see Client, XmlParser, Hasher
     */
    void MainWindow_ClientAdd();

    /*!
     *  \fn MainWindow_ClientRemove
     *  \brief Slot manages client removal.
     *
     *  When an appropriate \c QPushButton is pressed, this slot verifies that
     *  user surely wishes to remove currently selected client. If that is the case,
     *  selected client is removed both from the data model and patient records
     *  XML file.
     *  \see Client, XmlParser
     */
    void MainWindow_ClientRemove();

    /*!
     *  \fn MainWindow_ClientSelect
     *  \brief Slot has a purpose of client selection.
     *
     *  Slot function is responsible for client selection from
     *  user interface \c QTableWidget while exactly one line
     *  has to be selected, user is otherwise informed through \c QMessageBox .
     *  Such line is also marked by a different background color. Note
     *  that this selection is needed for measurement records
     *  saving and history displaying functionality.
     *  \see Client
     */
    void MainWindow_ClientSelect();

    /*!
     *  \fn MainWindow_HistoryRecalculateBars
     *  \brief Slot manages range visualization on history page.
     *
     *  This slot function updates the \c QProgressBar elements on
     *  history page according to selected \c QTableWidget row. This
     *  enhances user experience while observing historical records
     *  for selected exercise.
     *  \see Client
     */
    void MainWindow_HistoryRecalculateBars();

    /*!
     *  \fn MainWindow_AboutActivateAbout
     *  \brief Slot is defined for primary About page activation.
     *
     *  Slot function toggles the primary part of About page while
     *  needed user interface content goes visible with this action.
     */
    void MainWindow_AboutActivateAbout();

    /*!
     *  \fn MainWindow_AboutActivateHowto
     *  \brief Slot is used for About page description section activation.
     *
     *  Slot manages the visibility of required UI parts that contain
     *  application informations and hints.
     */
    void MainWindow_AboutActivateHowto();

    /*!
     *  \fn MainWindow_SettingsUpdate
     *  \brief This slot updates application settings through user choices.
     *
     *  This slot function collects user defined preferences (including
     *  selected Language) and manages saving to XML file.
     *  \see Settings
     */
    void MainWindow_SettingsUpdate();

    /*!
     *  \fn MainWindow_SwitchTranslation(Language language)
     *  \brief Slot manages retranslation of UI according to selected language.
     *
     *  Selected Language is applied through \c mTranslator object along with
     *  needed execution of \c MainWindow_InitializeUIStrings member function.
     *  \param language - depicts selected language option
     *  \see Language
     */
    void MainWindow_SwitchTranslation(Language language);

    /*!
     *  \fn MainWindow_EnableHomePage
     *  \brief Slot displays Home Page of Limb Range Analyzer.
     *
     *  This slot enables Home Page right after the application
     *  is started.
     */
    void MainWindow_EnableHomePage();

    /*!
     *  \fn MainWindow_EnableDevicesPage
     *  \brief Slot serves for displaying the Devices Page.
     *
     *  Slot uses \c QAction selected from the application
     *  main menu and displays Devices Page with its related content.
     */
    void MainWindow_EnableDevicesPage();

    /*!
     *  \fn MainWindow_EnableMeasurementPage
     *  \brief Slot shows the Measurement Page of LRA.
     *
     *  This slot activates the visibility of Measurement Page, which
     *  contains 3D visualisation elements and numerical output
     *  text fields (along with other active UI items).
     */
    void MainWindow_EnableMeasurementPage();

    /*!
     *  \fn MainWindow_EnableClientsPage
     *  \brief Slot function that enables Clients Page.
     *
     *  The function depicting this slot enables the visibility
     *  of Clients Page content. This option is likewise selectable
     *  from the application main menu.
     */
    void MainWindow_EnableClientsPage();

    /*!
     *  \fn MainWindow_EnableAboutPage
     *  \brief This slot makes the About Page visible.
     *
     *  Selection of a \c QAction from the main menu results
     *  in visibility of About Page.
     */
    void MainWindow_EnableAboutPage();

    /*!
     *  \fn MainWindow_EnableSettingsPage
     *  \brief Slot manages the visibility of Settings Page.
     *
     *  Slot enables \c QWidget that holds the necessary check
     *  boxes and dropdown menus of application settings.
     */
    void MainWindow_EnableSettingsPage();

    /*!
     *  \fn MainWindow_CleanAndQuit
     *  \brief Slot function for stopping and cleaning procedure.
     *
     *  The content of this slot involves Bluetooth measurement stopping
     *  and disconnection, if not previously handled manually, before it
     *  closes the main application window and frees allocated memory.
     */
    void MainWindow_CleanAndQuit();

protected:
    /*!
     * \var QTranslator *mTranslator
     * \brief This \c QTranslator pointer holds reference to application
     * translation object.
     */
    QTranslator *mTranslator;

private:
    /*!
     * \var Ui::MainWindow *mUserInterface
     * \brief MainWindow (namespace Ui) member variable depicting
     * user interface content.
     */
    Ui::MainWindow *mUserInterface;

    /*!
     * \var Qt3DExtras::Qt3DWindow *mGraphicsView
     * \brief Qt3DWindow (namespace Qt3DExtras) member variable
     * that holds graphics view used for 3D measurement content.
     */
    Qt3DExtras::Qt3DWindow *mGraphicsView;

    /*!
     * \var Qt3DCore::QEntity *mGraphicsRootEntity
     * \brief QEntity (namespace Qt3DCore) member variable that
     * represents the graphics root entity.
     */
    Qt3DCore::QEntity *mGraphicsRootEntity;

    /*!
     * \var QWidget *mGraphicsContainer
     * \brief QWidget member variable that serves as a container
     * of graphics content.
     */
    QWidget *mGraphicsContainer;

    /*!
     * \var QPropertyAnimation *mAnimation
     * \brief QPropertyAnimation variable used for window resizing.
     */
    QPropertyAnimation *mAnimation;

    /*!
     * \var Qt3DCore::QTransform *mUpperLeftArmTransform
     * \brief Qt3DCore::QTransform member variable kept for upper
     * left arm transformations.
     */
    Qt3DCore::QTransform *mUpperLeftArmTransform;

    /*!
     * \var Qt3DCore::QTransform *mUpperRightArmTransform
     * \brief Qt3DCore::QTransform member variable kept for upper
     * right arm transformations.
     */
    Qt3DCore::QTransform *mUpperRightArmTransform;

    /*!
     * \var Qt3DCore::QTransform *mLowerLeftArmTransform
     * \brief Qt3DCore::QTransform member variable kept for lower
     * left arm transformations.
     */
    Qt3DCore::QTransform *mLowerLeftArmTransform;

    /*!
     * \var Qt3DCore::QTransform *mLowerRightArmTransform
     * \brief Qt3DCore::QTransform member variable kept for lower
     * right arm transformations.
     */
    Qt3DCore::QTransform *mLowerRightArmTransform;

    /*!
     * \var Qt3DCore::QTransform *mLeftThighTransform
     * \brief Qt3DCore::QTransform member variable kept for upper
     * left leg transformations.
     */
    Qt3DCore::QTransform *mLeftThighTransform;

    /*!
     * \var Qt3DCore::QTransform *mRightThighTransform
     * \brief Qt3DCore::QTransform member variable kept for upper
     * right leg transformations.
     */
    Qt3DCore::QTransform *mRightThighTransform;

    /*!
     * \var Qt3DCore::QTransform *mLeftCalfTransform
     * \brief Qt3DCore::QTransform member variable kept for lower
     * left leg transformations.
     */
    Qt3DCore::QTransform *mLeftCalfTransform;

    /*!
     * \var Qt3DCore::QTransform *mRightCalfTransform
     * \brief Qt3DCore::QTransform member variable kept for lower
     * right leg transformations.
     */
    Qt3DCore::QTransform *mRightCalfTransform;

    /*!
     * \var Qt3DCore::QTransform *mPrimaryBlueCoinTransform
     * \brief Qt3DCore::QTransform member variable maintained for
     * primary BlueCoin device transformations.
     */
    Qt3DCore::QTransform *mPrimaryBlueCoinTransform;

    /*!
     * \var Qt3DCore::QTransform *mSecondaryBlueCoinTransform
     * \brief Qt3DCore::QTransform member variable maintained for
     * secondary BlueCoin device transformations.
     */
    Qt3DCore::QTransform *mSecondaryBlueCoinTransform;

    /*!
     * \var Qt3DCore::QTransform *mMinRangeTransform
     * \brief Qt3DCore::QTransform member variable used for minimum
     * reached range orb transformations.
     */
    Qt3DCore::QTransform *mMinRangeTransform;

    /*!
     * \var Qt3DCore::QTransform *mMaxRangeTransform
     * \brief Qt3DCore::QTransform member variable used for maximum
     * reached range orb transformations.
     */
    Qt3DCore::QTransform *mMaxRangeTransform;

    /*!
     * \var QList<Qt3DCore::QTransform*> mMovementOrbsTransform
     * \brief QList<Qt3DCore::QTransform> member variable used for
     * auxiliary small orbs transformations.
     */
    QList<Qt3DCore::QTransform*> mMovementOrbsTransform;

    /*!
     * \var Qt3DExtras::QPhongMaterial *mRangeMaterial
     * \brief Qt3DExtras::QPhongMaterial member variable used for
     * keeping the movement orbs color information.
     */
    Qt3DExtras::QPhongMaterial *mRangeMaterial;


    /*!
     * \var Bluetooth *mBluetooth
     * \brief Bluetooth is a member object that manages Bluetooth
     * connection and data retrieval.
     */
    Bluetooth *mBluetooth;

    /*!
     * \var SensorModule *mSensorModule
     * \brief SensorModule is a member that serves as a filtration
     * tool while processing BlueCoin measured values.
     */
    SensorModule *mSensorModule;

    /*!
     * \var XmlParser mXmlParser
     * \brief XmlParser depicts a member that introduces XML loading
     * and saving possibilities.
     */
    XmlParser mXmlParser;

    /*!
     * \var Hasher mHasher
     * \brief Hasher member object serves for unique hash calculation
     * based on Client data.
     */
    Hasher mHasher;

    /*!
     * \var QSharedPointer<Settings> mSettings
     * \brief QSharedPointer<Settings> is a member object used across
     * application for keeping application settings data.
     */
    QSharedPointer<Settings> mSettings;

    /*!
     * \brief QList<QSharedPointer<Client>> represents a list of pointers
     * to Client data used across application.
     */
    QList<QSharedPointer<Client>> mClientList;

    /*!
     * \brief QList<QSharedPointer<Session>> represents a list of pointers
     * to Session data used across application.
     */
    QList<QSharedPointer<Session>> mSessionList;

    /*!
     * \brief QList<QSharedPointer<Exercise>> represents a list of pointers
     * to Exercise data used across application.
     */
    QList<QSharedPointer<Exercise>> mExerciseList;

    /*!
     * \var QSharedPointer<Client> mClient
     * \brief QSharedPointer<Client> holds a pointer member
     * attribute of a single Client.
     */
    QSharedPointer<Client> mClient;


    /*!
     * \var QStringList mExerciseNameList
     * \brief QStringList member variable serves for keeping
     * exercise names in a list.
     */
    QStringList mExerciseNameList;

    /*!
     * \brief QList<QPair<ProcessingSelection, ExerciseType>> member
     * variable represents a list of exercise types used for filtering.
     */
    QList<QPair<ProcessingSelection, ExerciseType>> mExerciseTypeList;

    /*!
     * \var QList<qint32> mExerciseIndexList
     * \brief QList<qint32> member variable is used for keeping
     * exercise index values.
     */
    QList<qint32> mExerciseIndexList;

    /*!
     * \var qint32 mSelectedExercise
     * \brief qint32 numerical member variable that holds the index
     * of selected exercise.
     */
    qint32 mSelectedExercise;

    /*!
     * \var qint32 mDeviceSelectionLine
     * \brief qint32 numerical member variable that holds the index
     * of selected device.
     */
    qint32 mDeviceSelectionLine;

    /*!
     * \var qint32 mClientSelectionLine
     * \brief qint32 numerical member variable that holds the index
     * of selected client.
     */
    qint32 mClientSelectionLine;

    /*!
     * \var qint32 mMovementCounter
     * \brief qint32 numerical member that holds current step of
     * measurement used while positioning movement orbs.
     */
    qint32 mMovementCounter;

    /*!
     * \var bool mMeasurementRunning
     * \brief bool member that holds information whether
     * a measurement is in progress.
     */
    bool mMeasurementRunning;

    /*!
     * \var bool mEditingEnabled
     * \brief bool member that holds information whether
     * a new Client is currently being added.
     */
    bool mEditingEnabled;

    /*!
     * \var BodyPart mBodyPart
     * \brief BodyPart serves for recognition of human
     * body part.
     */
    BodyPart mBodyPart;
};

#endif // MAINWINDOW_HPP
