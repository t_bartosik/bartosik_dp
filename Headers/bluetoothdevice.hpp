/**
 * @file        bluetoothdevice.hpp
 * @author      Tomas Bartosik
 * @date        22.07.2021
 * @brief       declaration file for Bluetooth device
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef BLUETOOTHDEVICE_HPP
#define BLUETOOTHDEVICE_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QObject>
#include <QBluetoothDeviceInfo>
#include <QBluetoothAddress>
#include <QBluetoothUuid>
#include <QList>
#include <QString>
#include <QDebug>

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class BluetoothDevice
 *  \brief Auxiliary class that represents single Bluetooth device.
 *
 *  This class is used for encapsulation of Bluetooth device needed
 *  properties - its name and address. The object generated through
 *  this class description is then used in Bluetooth module.
 *  \see Bluetooth
 */
class BluetoothDevice : public QObject
{
    Q_OBJECT

public:
    /*!
     *  \fn BluetoothDevice()
     *  \brief Class constructor of BluetoothDevice.
     *
     *  Non-parametric default constructor of BluetoothDevice class.
     *  \see ~BluetoothDevice()
     */
    BluetoothDevice();

    /*!
     *  \fn BluetoothDevice(const QBluetoothDeviceInfo &device)
     *  \brief Copy constructor of BluetoothDevice.
     *
     *  Single parameter class copy constructor.
     *
     *  \param device - parameter that keeps Bluetooth device
     *  informations for class member
     *  \see ~BluetoothDevice()
     */
    BluetoothDevice(const QBluetoothDeviceInfo &device);

    /*!
     *  \fn ~BluetoothDevice()
     *  \brief Default BluetoothDevice class destructor.
     *
     *  Manages base memory cleaning when BluetoothDevice
     *  object gets destroyed.
     *  \see BluetoothDevice(), BluetoothDevice(const QBluetoothDeviceInfo &device)
     */
    ~BluetoothDevice();

    /*!
     *  \fn BluetoothDevice_SetDevice(const QBluetoothDeviceInfo &device)
     *  \brief Setter member function that updates Bluetooth device
     *  informations.
     *
     *  Class setter function that modifies BluetoothDevice informations
     *  member \c mDevice and emits singal of device changes
     *  through \c BluetoothDevice_SignalDeviceChanged .
     *
     *  \param device - parameter that keeps Bluetooth device
     *  informations for class member
     */
    void BluetoothDevice_SetDevice(const QBluetoothDeviceInfo &device);

    /*!
     *  \fn BluetoothDevice_GetDevice
     *  \brief Getter function for Bluetooth device informations.
     *
     *  Member function that returns up-to-date Bluetooth device
     *  informations member attribute \c mDevice .
     *  \return QBluetoothDeviceInfo - device informations member
     */
    QBluetoothDeviceInfo BluetoothDevice_GetDevice();

    /*!
     *  \fn BluetoothDevice_GetName() const
     *  \brief Getter function retrieving device name string.
     *
     *  This function uses \c mDevice member object
     *  function that returns the name of device.
     *  \return QString - device name text string
     */
    QString BluetoothDevice_GetName() const;

    /*!
     *  \fn BluetoothDevice_GetAddress() const
     *  \brief Getter function retrieving device address.
     *
     *  Member function that returns current device's
     *  address. Note that in case of macOS operating
     *  system, device UUID is returned instead (due
     *  to OS-specific functionality requirements).
     *  \return QString - text string containing device
     *  address
     */
    QString BluetoothDevice_GetAddress() const;

Q_SIGNALS:
    /*!
     *  \fn BluetoothDevice_SignalDeviceChanged
     *  \brief Signal that gets emitted when device
     *  member properties are modified.
     */
    void BluetoothDevice_SignalDeviceChanged();

private:
    /*!
     * \var QBluetoothDeviceInfo mDevice
     * \brief QBluetoothDeviceInfo, a member object that holds
     * informations about current device.
     */
    QBluetoothDeviceInfo mDevice;
};

#endif // BLUETOOTHDEVICE_HPP
