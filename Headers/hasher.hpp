/**
 * @file        hasher.hpp
 * @author      Tomas Bartosik
 * @date        01.03.2022
 * @brief       declaration file for hashing functionality class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

#ifndef HASHER_HPP
#define HASHER_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QObject>
#include <QString>
#include <QCryptographicHash>

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class Hasher
 *  \brief Class Hasher is introduced for Client hash ID management.
 *
 *  This class is used for unique hash creation that is used alongside
 *  Client data model. Each client entry requires a unique identifier
 *  assembled through client member variables and saved via
 *  XmlParser.
 *  \see Client, XmlParser
 */
class Hasher : public QObject
{
    Q_OBJECT

    public:
        /*!
         *  \fn Hasher()
         *  \brief Hasher class default constructor.
         *
         *  Constructor function that is executed when
         *  an object is created.
         *
         *  \see ~Hasher()
         */
        Hasher();

        /*!
         *  \fn ~Hasher()
         *  \brief Destructor function of class Hasher.
         *
         *  Base destructor responsible for required
         *  memory cleaning.
         *
         *  \see Hasher()
         */
        ~Hasher();

        /*!
         *  \fn Hasher_CalculateHash(QString dataString)
         *  \brief Member function used for unique hash creation.
         *
         *  This function receives a text string assembled through
         *  Client member variables \b first \b name, \b last \b name
         *  and \b unique \b e-mail \b address, which are accordingly
         *  transformed into a \c MD5 hash string via \c QCryptographicHash
         *  member function.
         *
         *  \param dataString - client specific unique
         *  text string composed of Client member variables
         *  \return QString - computed hash in its text string form
         *  \see Client
         */
        QString& Hasher_CalculateHash(QString dataString);

    private:
        /*!
         * \var QString mHash
         * \brief QString member variable that holds the output
         * hash in a string form.
         */
        QString mHash;
};

#endif // HASHER_HPP
