/**
 * @file        sensorcomponent.hpp
 * @author      Tomas Bartosik
 * @date        15.12.2021
 * @brief       declaration file for motion sensors component
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef SENSORCOMPONENT_HPP
#define SENSORCOMPONENT_HPP

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/extensions.hpp"
#include <QObject>
#include <QtMath>

/*Enumeration declarations: -------------------------------------------------*/
/*!
 * \enum AxisSelection
 * \brief AxisSelection enum depicts axis selection (roll vs. pitch)
 * when vertical measurement is handled.
 *
 * Enumeration introduces three options: either roll, pitch or none
 * of these is used. The undefined option marks initial state.
*/
enum class AxisSelection
{
    eUndefined = 0,     /*!< Initial enum state - undecided choice. */
    eUsingRoll,         /*!< Device uses \b ROLL for vertical measurement. */
    eUsingPitch         /*!< Device uses \b PITCH for vertical measurement. */
};

/*!
 * \enum ProcessingSelection
 * \brief ProcessingSelection enumeration determines which 3D axis
 * will be measured.
 *
 * This enum estimates measurement processing style: three possible
 * states depict \b NO processing, \b vertical or \b horizontal processing.
*/
enum class ProcessingSelection
{
    eDisabled = 0,      /*!< Initial enum state - no processing is handled. */
    eProcessingXY,      /*!< Selected measurement requires vertical handling. */
    eProcessingZ        /*!< Selected measurement requires horizontal handling. */
};

/*Structure declaration: ----------------------------------------------------*/
/*!
 *  \struct SensorComponent
 *  \brief This structure provides data management helper for SensorModule,
 *  as well as sensor fusion filtering algorithm.
 *
 *  SensorComponent handles needed member variables that are bound to
 *  sensor fusion algorithm used for filtering. That way, device rotation angles
 *  and exercise ranges are kept. These results are then, through SensorModule,
 *  provided for visualization and numerical output for user to see.
 *  \see SensorModule
 */
struct SensorComponent : public QObject
{
    Q_OBJECT

public:
    /*!
     *  \fn SensorComponent()
     *  \brief Default constructor of struct SensorComponent.
     *
     *  Through this function, member variables are initially set.
     */
    SensorComponent();

    /*!
     *  \fn ~SensorComponent()
     *  \brief Default destructor.
     *
     *  Destructor function for struct SensorComponent.
     */
    ~SensorComponent();


    /*!
     *  \fn SensorComponent_SetComponent(const SensorComponent& component)
     *  \brief Setter function that serves as an object copy constructor.
     *
     *  With the usage of this member function, member variables are set through
     *  a copy of a struct object.
     *  \param component - struct instance used for member values copying
     */
    void SensorComponent_SetComponent(const SensorComponent& component);

    /*!
     *  \fn SensorComponent_FilterSensorValues(qreal accX, qreal accY, qreal accZ, qreal gyroX, qreal gyroY, qreal gyroZ)
     *  \brief Sensor fusion struct member function.
     *
     *  This function updates member variables depicting quaternions and output
     *  filter values that are used for drift-reduced sensing possibility for
     *  human limb range analysis. The algorithm principle is based on
     *  Madgwick's filter, which is in turn based on Kalman filtering properties.
     *  \param accX - modified accelerometer X real number input parameter
     *  \param accY - modified accelerometer Y real number input parameter
     *  \param accZ - modified accelerometer Z real number input parameter
     *  \param gyroX - modified gyroscope X real number input parameter
     *  \param gyroY - modified gyroscope Y real number input parameter
     *  \param gyroZ - modified gyroscope Z real number input parameter
     */
    void SensorComponent_FilterSensorValues(qreal accX, qreal accY, qreal accZ, qreal gyroX, qreal gyroY, qreal gyroZ);

    /*!
     * \var qint32 mAccelerometerX
     * \brief qint32 value, this member is used for accelerometer
     * X axis value storage.
     */
    qint32 mAccelerometerX;

    /*!
     * \var qint32 mAccelerometerY
     * \brief qint32 value, this member is used for accelerometer
     * Y axis value storage.
     */
    qint32 mAccelerometerY;

    /*!
     * \var qint32 mAccelerometerZ
     * \brief qint32 value, this member is used for accelerometer
     * Z axis value storage.
     */
    qint32 mAccelerometerZ;

    /*!
     * \var qint32 mGyroscopeX
     * \brief qint32 value, this member is used for gyroscope
     * X axis value storage.
     */
    qint32 mGyroscopeX;

    /*!
     * \var qint32 mGyroscopeY
     * \brief qint32 value, this member is used for gyroscope
     * Y axis value storage.
     */
    qint32 mGyroscopeY;

    /*!
     * \var qint32 mGyroscopeZ
     * \brief qint32 value, this member is used for gyroscope
     * Z axis value storage.
     */
    qint32 mGyroscopeZ;

    /*!
     * \var qint16 mRoll
     * \brief qint16 member that holds current \b ROLL value.
     */
    qint16 mRoll;

    /*!
     * \var qint16 mRollPrevOne
     * \brief qint16 member that holds first previous \b ROLL value.
     *
     * Member is used for \b ROLL precision enhancement.
     */
    qint16 mRollPrevOne;

    /*!
     * \var qint16 mRollPrevTwo
     * \brief qint16 member that holds second previous \b ROLL value.
     *
     * Member is used for \b ROLL precision enhancement.
     */
    qint16 mRollPrevTwo;

    /*!
     * \var qint16 mRollMin
     * \brief qint16 member that holds measured \b MINIMUM of \b ROLL value.
     */
    qint16 mRollMin;

    /*!
     * \var qint16 mRollMax
     * \brief qint16 member that holds measured \b MAXIMUM of \b ROLL value.
     */
    qint16 mRollMax;

    /*!
     * \var qint16 mPitch
     * \brief qint16 member that holds current \b PITCH value.
     */
    qint16 mPitch;

    /*!
     * \var qint16 mPitchPrevOne
     * \brief qint16 member that holds first previous \b PITCH value.
     *
     * Member is used for \b PITCH precision enhancement.
     */
    qint16 mPitchPrevOne;

    /*!
     * \var qint16 mPitchPrevTwo
     * \brief qint16 member that holds second previous \b PITCH value.
     *
     * Member is used for \b PITCH precision enhancement.
     */
    qint16 mPitchPrevTwo;

    /*!
     * \var qint16 mPitchMin
     * \brief qint16 member that holds measured \b MINIMUM of \b PITCH value.
     */
    qint16 mPitchMin;

    /*!
     * \var qint16 mPitchMax
     * \brief qint16 member that holds measured \b MAXIMUM of \b PITCH value.
     */
    qint16 mPitchMax;

    /*!
     * \var qint16 mYaw
     * \brief qint16 member that holds current \b YAW value.
     */
    qint16 mYaw;

    /*!
     * \var qint16 mMin
     * \brief qint16 that keeps the currently reported \b MINIMUM value.
     */
    qint16 mMin;

    /*!
     * \var qint16 mMax
     * \brief qint16 that keeps the currently reported \b MAXIMUM value.
     */
    qint16 mMax;

    /*!
     * \var qint16 mRange
     * \brief qint16 that keeps the currently reported \b RANGE value.
     */
    qint16 mRange;

    /*!
     * \var qint16 mCurrentAngle
     * \brief qint16 that keeps the currently reported \b ANGLE value.
     */
    qint16 mCurrentAngle;

    /*!
     * \var qreal mFilterQ0
     * \brief qreal value that depicts first quaternion filter value
     * (real number).
     */
    qreal mFilterQ0;

    /*!
     * \var qreal mFilterQ1
     * \brief qreal value that depicts second quaternion filter value
     * (real number).
     */
    qreal mFilterQ1;

    /*!
     * \var qreal mFilterQ2
     * \brief qreal value that depicts third quaternion filter value
     * (real number).
     */
    qreal mFilterQ2;

    /*!
     * \var qreal mFilterQ3
     * \brief qreal value that depicts fourth quaternion filter value
     * (real number).
     */
    qreal mFilterQ3;

    /*!
     * \var bool mStartUpsideDown
     * \brief bool that in case of \c TRUE state means that a BlueCoin
     * device starts measurement while upside down, \c FALSE otherwise.
     */
    bool mStartUpsideDown;

    /*!
     * \var AxisSelection mAxisSelected
     * \brief AxisSelection enumeration member value that helps to
     * recognise vertically processed measurement (by estimating
     * its axis).
     * \see AxisSelection
     */
    AxisSelection mAxisSelected;

    /*!
     * \var ProcessingSelection mProcessingSelected
     * \brief ProcessingSelection enum member variable that
     * specifies whether vertical or horizontal measurement
     * is processed.
     * \see AxisSelection
     */
    ProcessingSelection mProcessingSelected;
};

#endif // SENSORCOMPONENT_HPP
