/**
 * @file        sensormodule.hpp
 * @author      Tomas Bartosik
 * @date        28.07.2021
 * @brief       declaration file for motion sensors module
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef SENSORMODULE_HPP
#define SENSORMODULE_HPP

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/bluetooth.hpp"
#include "../Headers/sensorcomponent.hpp"
#include <QSharedPointer>

/*Enumeration declarations: -------------------------------------------------*/
/*!
 * \enum DeviceType
 * \brief DeviceType enumeration introduces the usage of two devices.
 *
 * This enumeration distinguishes between two
 * available BlueCoin devices and their sensor data
 * processing.
*/
enum class DeviceType
{
    ePrimary = 1,       /*!< Processing of primary device will be managed. */
    eSecondary          /*!< Processing of secondary device will be managed. */
};

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class SensorModule
 *  \brief Introduces possibilities of retrieved sensor data processing.
 *
 *  This class uses obtained BlueCoin sensor data and manages
 *  its processing. This project module relies on SensorComponent
 *  struct that is maintained for single or both active Bluetooth
 *  connections.
 *  \see SensorComponent, Bluetooth
 */
class SensorModule : public QObject
{
    Q_OBJECT

public:
    /*!
     *  \fn SensorModule()
     *  \brief Default constructor of class SensorModule.
     *
     *  This function is responsible for member variables initialization.
     */
    SensorModule();

    /*!
     *  \fn ~SensorModule()
     *  \brief Default destructor function.
     *
     *  Destructor function for class SensorModule.
     */
    ~SensorModule();

    /*!
     *  \fn SensorModule_SetSensorValues(const QString &hexValue)
     *  \brief Provides received string splitting and further processing.
     *
     *  This function manages the division of obtained hexadecimal
     *  string that will be processed through filtration (via SensorComponent)
     *  and then prepared for user interface presentation. When the necessary
     *  data describing three-dimensional rotation is ready, this function
     *  manages emitting of \c SensorModule_SignalProvideResults signal. Note
     *  that this member function is triggered immediately after Bluetooth
     *  manages characteristics reading.
     *  \param hexValue - hexadecimal measurement string
     *  \see SensorComponent, Bluetooth
     */
    void SensorModule_SetSensorValues(const QString &hexValue);

    /*!
     *  \fn SensorModule_ProcessSensorComponent(const QString &hexValue,
     *  DeviceType deviceType)
     *  \brief This member function retrieves accelerometer and gyroscope
     *  measurements dispatched by a BlueCoin device and estimates its rotation.
     *
     *  A key part of SensorModule class that uses accelerometer and gyroscope
     *  data that gets processed in order to estimate device rotation - \b roll,
     *  \b pitch and \b yaw. Calculated \b angle, \b minimum, \b maximum and \b range values
     *  are also updated based on 3D space axis used when evaluating selected exercise.
     *  \param hexValue - hexadecimal measurement string
     *  \param deviceType - selection parameter of primary or secondary Bluetooth device
     *  \see Bluetooth
     */
    void SensorModule_ProcessSensorComponent(const QString &hexValue, DeviceType deviceType);

    /*!
     *  \fn SensorModule_ConvertAccelerometerValue(qint32 accValue)
     *  \brief Member function that converts raw accelerometer value to
     *  required form.
     *
     *  A member function that uses raw accelerometer data transmitted
     *  by a BlueCoin device and reshapes it into needed form that is
     *  further used by a filtering algorithm, while accelerometer range and IMU
     *  maximum range values are used.
     *  \param accValue - raw accelerometer value
     *  \return qreal - converted accelerometer value
     */
    qreal SensorModule_ConvertAccelerometerValue(qint32 accValue);

    /*!
     *  \fn SensorModule_ConvertGyroscopeValue(qint32 gyroValue)
     *  \brief This member function manages conversion of raw
     *  gyroscope value so that it can be further used.
     *
     *  Function that serves as a converter of raw gyroscope value
     *  that uses gyroscope DPS parameter and IMU maximum
     *  range constants.
     *  \param gyroValue - raw gyroscope value
     *  \return qreal - converted gyroscope value
     */
    qreal SensorModule_ConvertGyroscopeValue(qint32 gyroValue);

    /*!
     *  \fn SensorModule_SetProcessingStatus(ProcessingSelection processingSelected,
     *  DeviceType deviceType)
     *  \brief Function that sets SensorComponent variables through processing
     *  type and device selection parameters.
     *
     *  Measurement related SensorComponent properties are set accordingly to
     *  exercise related axis used and selected BlueCoin device. Processing is
     *  either stopped through \c SensorModule_ResetMeasurement or established
     *  for new measurement needs.
     *  \param processingSelected - choice whether \b X/Y or \b Z axis is used
     *  \param deviceType - selection of primary or secondary device
     *  \see SensorComponent
     */
    void SensorModule_SetProcessingStatus(ProcessingSelection processingSelected, DeviceType deviceType);

    /*!
     *  \fn SensorModule_SetCombinedMin(qint16 combinedMin)
     *  \brief Setter member function of \c mCombinedMin member variable.
     *
     *  This function manages \c mCombinedMin setting according to parameter
     *  value.
     *  \param combinedMin - numerical parameter of combined minimum value
     */
    void SensorModule_SetCombinedMin(qint16 combinedMin);

    /*!
     *  \fn SensorModule_SetCombinedMax(qint16 combinedMax)
     *  \brief Setter member function of \c mCombinedMax member variable.
     *
     *  This function manages \c mCombinedMax setting according to parameter
     *  value.
     *  \param combinedMax - numerical parameter of combined maximum value
     */
    void SensorModule_SetCombinedMax(qint16 combinedMax);

    /*!
     *  \fn SensorModule_ResetMeasurement(DeviceType deviceType)
     *  \brief Function that manages exercise related measurement
     *  variables resetting to their initial state.
     *
     *  Member function that re-sets \b rotation \b variables along with current
     *  \b angle, \b minimum, \b maximum and \b range of a selected device.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     */
    void SensorModule_ResetMeasurement(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetAccelerometerX(DeviceType deviceType)
     *  \brief Getter function established for \c mAccelerometerX.
     *
     *  Function that returns mAccelerometerX value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint32 - value of accelerometer measured along X axis
     */
    qint32 SensorModule_GetAccelerometerX(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetAccelerometerY(DeviceType deviceType)
     *  \brief Getter function established for \c mAccelerometerY.
     *
     *  Function that returns mAccelerometerY value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint32 - value of accelerometer measured along Y axis
     */
    qint32 SensorModule_GetAccelerometerY(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetAccelerometerZ(DeviceType deviceType)
     *  \brief Getter function established for \c mAccelerometerZ.
     *
     *  Function that returns mAccelerometerZ value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint32 - value of accelerometer measured along Z axis
     */
    qint32 SensorModule_GetAccelerometerZ(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetGyroscopeX(DeviceType deviceType)
     *  \brief Getter function established for \c mGyroscopeX.
     *
     *  Function that returns mGyroscopeX value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint32 - value of gyroscope measured along X axis
     */
    qint32 SensorModule_GetGyroscopeX(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetGyroscopeY(DeviceType deviceType)
     *  \brief Getter function established for \c mGyroscopeY.
     *
     *  Function that returns mGyroscopeY value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint32 - value of gyroscope measured along Y axis
     */
    qint32 SensorModule_GetGyroscopeY(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetGyroscopeZ(DeviceType deviceType)
     *  \brief Getter function established for \c mGyroscopeZ.
     *
     *  Function that returns mGyroscopeZ value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint32 - value of gyroscope measured along Z axis
     */
    qint32 SensorModule_GetGyroscopeZ(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetCurrentRoll(DeviceType deviceType)
     *  \brief Getter member function retrieving current \b roll value.
     *
     *  Member function that serves for retrieval of roll angle value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint16 - up-to-date roll value
     */
    qint16 SensorModule_GetCurrentRoll(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetCurrentPitch(DeviceType deviceType)
     *  \brief Getter member function retrieving current \b pitch value.
     *
     *  Member function that serves for retrieval of pitch angle value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint16 - up-to-date pitch value
     */
    qint16 SensorModule_GetCurrentPitch(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetCurrentYaw(DeviceType deviceType)
     *  \brief Getter member function retrieving current \b yaw value.
     *
     *  Member function that serves for retrieval of yaw angle value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint16 - up-to-date yaw value
     */
    qint16 SensorModule_GetCurrentYaw(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetCurrentMin(DeviceType deviceType)
     *  \brief Getter function that returns currently measured \b minimum.
     *
     *  This member function is used in order to obtain latest minimum value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint16 - currently valid minimum value
     */
    qint16 SensorModule_GetCurrentMin(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetCurrentMax(DeviceType deviceType)
     *  \brief Getter function that returns currently measured \b maximum.
     *
     *  This member function is used in order to obtain latest maximum value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint16 - currently valid maximum value
     */
    qint16 SensorModule_GetCurrentMax(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetCurrentRange(DeviceType deviceType)
     *  \brief Getter function that returns currently measured \b range.
     *
     *  This member function is used in order to obtain latest range value.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint16 - currently valid range value
     */
    qint16 SensorModule_GetCurrentRange(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetCurrentAngle(DeviceType deviceType)
     *  \brief Getter function that returns currently measured \b angle.
     *
     *  This member function is used in order to obtain latest angle.
     *  \param deviceType - parameter distinguishing primary
     *  and secondary measurement device
     *  \return qint16 - currently valid angle
     */
    qint16 SensorModule_GetCurrentAngle(DeviceType deviceType);

    /*!
     *  \fn SensorModule_GetCombinedMin
     *  \brief Getter for mCombinedMin member variable.
     *
     *  Function provides current \b mCombinedMin value that is
     *  used for dual-device minimum measurement.
     *  \return qint16 - recent combined minimum value
     */
    qint16 SensorModule_GetCombinedMin();

    /*!
     *  \fn SensorModule_GetCombinedMax
     *  \brief Getter for mCombinedMax member variable.
     *
     *  Function provides current \b mCombinedMax value that is
     *  used for dual-device maximum measurement.
     *  \return qint16 - recent combined maximum value
     */
    qint16 SensorModule_GetCombinedMax();

signals:
    /*!
     *  \fn SensorModule_SignalProvideResults
     *  \brief Signal that gets emitted when measurement
     *  results are ready to be presented in the main UI class
     */
    void SensorModule_SignalProvideResults();

private:
    /*!
     * \var SensorComponent mComponent
     * \brief SensorComponent is a member representing currently
     * processed sensor measurement structure.
     */
    SensorComponent mComponent;

    /*!
     * \var SensorComponent mPrimaryComponent
     * \brief SensorComponent, a member representing primary
     * device related sensor measurement structure.
     */
    SensorComponent mPrimaryComponent;

    /*!
     * \var SensorComponent mSecondaryComponent
     * \brief SensorComponent is member representing secondary
     * device related sensor measurement structure.
     */
    SensorComponent mSecondaryComponent;

    /*!
     * \var QString mFullHexValue
     * \brief QString, a text string member variable that
     * holds the entire Bluetooth received hexadecimal-form data.
     *
     * This string may include data from both BlueCoin devices.
     */
    QString mFullHexValue;

    /*!
     * \var QString mHexValue
     * \brief QString, a text string member variable that
     * holds Bluetooth received measurement data.
     *
     * This string includes one BlueCoin device data only.
     */
    QString mHexValue;

    /*!
     * \var QString mAuxiliary
     * \brief QString, a helper text member variable that
     * separates parts of received hexadecimal string.
     */
    QString mAuxiliary;


    /*!
     * \var QString mCombinedMin
     * \brief qint16 numerical value that holds dual devices
     * estimated minimum angle reached.
     */
    qint16 mCombinedMin;

    /*!
     * \var QString mCombinedMax
     * \brief qint16 numerical value that holds dual devices
     * estimated maximum angle reached.
     */
    qint16 mCombinedMax;

    /*!
     * \var bool mConversionOK
     * \brief bool value that in its \c TRUE state confirms
     * successful hexadecimal string to number conversion, \c FALSE
     * otherwise.
     */
    bool mConversionOK;
};

#endif // SENSORMODULE_HPP
