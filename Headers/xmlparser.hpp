/**
 * @file        xmlparser.hpp
 * @author      Tomas Bartosik
 * @date        05.03.2022
 * @brief       declaration file for XML saving/loading possibility
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

#ifndef XMLPARSER_HPP
#define XMLPARSER_HPP

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/client.hpp"
#include "../Headers/settings.hpp"
#include <QObject>
#include <QDir>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QMessageBox>
#include <QIcon>

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class XmlParser
 *  \brief Class introduced in order to maintain XML loading and
 *  saving possibilities.
 *
 *  Class XmlParser is responsible for Limb Range Analyzer Client
 *  and Settings data model saving through XML documents. Needed
 *  files can be created and extended according to program needs,
 *  just as they can be accessed and read from.
 *  \see Client, Settings
 */
class XmlParser : public QObject
{
    Q_OBJECT

    public:
        /*!
         *  \fn XmlParser()
         *  \brief Class XmlParser default constructor.
         *
         *  This default function is used as a member initializer
         *  when new instance is created.
         *
         *  \see ~XmlParser()
         */
        XmlParser();

        /*!
         *  \fn ~XmlParser()
         *  \brief Default destructor function of XmlParser.
         *
         *  Unextended form of a default destructor function.
         *
         *  \see XmlParser()
         */
        ~XmlParser();

        /*!
         *  \fn XmlParser_InitializePath
         *  \brief Member function that manages required
         *  directory check and creation if it does not yet exist.
         *
         *  This member function defines XML directory meant
         *  for usage and creates it, if it does not exist.
         */
        void XmlParser_InitializePath();

        /*!
         *  \fn XmlParser_InitializeSettings
         *  \brief Function needed for initial settings XML file creation.
         *
         *  Member function that uses XmlParser_InitializePath and
         *  creates default Settings file with pre-set application
         *  configurables. Initial XML file \c settings.xml is created only if it
         *  does not yet exist.
         *  \see Settings
         */
        void XmlParser_InitializeSettings();

        /*!
         *  \fn XmlParser_InitializeClientData
         *  \brief Function that is responsible for initial client
         *  XML file creation.
         *
         *  Function that uses XmlParser_InitializePath and creates
         *  initial client data XML file. Such file would contain initial Client
         *  entry, but is created only in case that \c clients.xml does not
         *  yet exist.
         *  \see Client
         */
        void XmlParser_InitializeClientData();

        /*!
         *  \fn XmlParser_LoadSettings
         *  \brief Member function meant for application Settings
         *  loading.
         *
         *  Member function responsible for XML file loading. Processed
         *  text strings are used for Settings instance creation that is
         *  returned by this function and further used by the application.
         *  \return QSharedPointer<Settings> - loaded settings data pointer
         *  that will be used within the application
         *  \see Settings
         */
        QSharedPointer<Settings> XmlParser_LoadSettings();

        /*!
         *  \fn XmlParser_LoadClientData
         *  \brief Function designed for Client data loading from XML file.
         *
         *  Function serves as an obtainer of client related data stored in
         *  a XML file created by \c XmlParser_SaveClientData function. A required
         *  file is opened and read through until its end, while client instances
         *  are constructed. A list of Client instances is returned for further use
         *  by the application.
         *  \return QList<QSharedPointer<Client>> - list pointer structure that
         *  represents previously stored and managed clients data
         *  \see Client
         */
        QList<QSharedPointer<Client>> XmlParser_LoadClientData();

        /*!
         *  \fn XmlParser_SaveSettings(QSharedPointer<Settings> settings)
         *  \brief Member function that manages Settings XML saving.
         *
         *  This function is designed for application settings saving, which is
         *  handled through XML file usage. A Settings object transferred as a
         *  pointer parameter is decomposed and saved to \c settings.xml file
         *  with the use of \c QXmlStreamWriter .
         *  \param settings - currently valid application settings to be saved
         */
        void XmlParser_SaveSettings(QSharedPointer<Settings> settings);

        /*!
         *  \fn XmlParser_SaveClientData(QList<QSharedPointer<Client>> clientList)
         *  \brief Function that receives a list of clients and saves it to XML file.
         *
         *  Parametric member function that uses a list of Client data and saves
         *  it to a specific \c clients.xml file. The list is iterated through and for
         *  each entry a new record is made, so that it can be loaded via
         *  \c XmlParser_LoadClientData again when needed.
         *  \param clientList - a list of pointers that carries application clients
         *  data meant for saving
         */
        void XmlParser_SaveClientData(QList<QSharedPointer<Client>> clientList);

    private:
        /*!
         * \var QString mDirPath
         * \brief QString member variable that carries the
         * directory path used by XML files.
         */
        QString mDirPath;
};

#endif // XMLPARSER_HPP
