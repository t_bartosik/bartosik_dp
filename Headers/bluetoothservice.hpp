/**
 * @file        bluetoothservice.hpp
 * @author      Tomas Bartosik
 * @date        27.07.2021
 * @brief       declaration file for Bluetooth service
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef BLUETOOTHSERVICE_HPP
#define BLUETOOTHSERVICE_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QObject>
#include <QtBluetooth/QLowEnergyService>
#include <QString>

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class BluetoothService
 *  \brief Auxiliary class that is used alongside Bluetooth class when describing
 *  a service.
 *
 *  BluetoothService helps recognise Bluetooth services and their properties,
 *  of which their UUID is important for Limb Range Analyzer application. This class
 *  is an aggregated component used alongside Bluetooth module.
 *  \see Bluetooth
 */
class BluetoothService : public QObject
{
    Q_OBJECT

public:
    /*!
     *  \fn BluetoothService()
     *  \brief BluetoothService class default constructor.
     *
     *  This function represents default constructor of
     *  class BluetoothService.
     *  \see ~BluetoothService()
     */
    BluetoothService();

    /*!
     *  \fn BluetoothService(QLowEnergyService *service)
     *  \brief BluetoothService class copy constructor.
     *
     *  A parametric constructor function for BluetoothService,
     *  which sets \c mService member attribute according to
     *  received parameter.
     *  \param service - service object used for BluetoothService
     *  instance creation
     *  \see ~BluetoothService()
     */
    BluetoothService(QLowEnergyService *service);

    /*!
     *  \fn ~BluetoothService()
     *  \brief Default BluetoothService class destructor.
     *
     *  Function describing default class destructor.
     *  \see BluetoothService(), BluetoothService(QLowEnergyService *service)
     */
    ~BluetoothService();


    /*!
     *  \fn BluetoothService_GetService() const
     *  \brief Class defined service member getter function.
     *
     *  This member function returns \c mService member.
     *  \return QLowEnergyService - maintaned service member
     *  object
     */
    QLowEnergyService *BluetoothService_GetService() const;

    /*!
     *  \fn BluetoothService_GetUUID
     *  \brief This member function returns service UUID string.
     *
     *  Member function that verifies the existence of \c mService
     *  object and then converts its UUID, so that it can be returned
     *  as a hexadecimal string. Unnecessary characters are omitted.
     *  \return QString - text string with service UUID description
     */
    QString BluetoothService_GetUUID();

private:
    /*!
     * \var QLowEnergyService *mService
     * \brief QLowEnergyService object that maintains
     * Bluetooth service informations.
     */
    QLowEnergyService *mService;
};

#endif // BLUETOOTHSERVICE_HPP
