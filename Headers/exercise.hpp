/**
 * @file        exercise.hpp
 * @author      Tomas Bartosik
 * @date        01.03.2022
 * @brief       declaration file for class Exercise
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

#ifndef EXERCISE_HPP
#define EXERCISE_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QObject>
#include <QStringList>
#include <QString>

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class Exercise
 *  \brief Class that represents single limb analysis exercise record.
 *
 *  Class responsible for exercise specification, which is later used
 *  as a component of client's Session. The class consists of key
 *  measurement attributes such as minimum and maximum measured
 *  value or total range. Exercises within a Session can be distinguished
 *  through their identifier.
 *  \see Session
 */
class Exercise : public QObject
{
    Q_OBJECT

    public:
        /*!
         *  \fn Exercise()
         *  \brief Constructor of Exercise class.
         *
         *  Constructor function responsible for initial
         *  member setting with no parameters.
         *  \see ~Exercise()
         */
        Exercise();

        /*!
         *  \fn Exercise(QString id, qint16 min, qint16 max, qint16 range)
         *  \brief Parametric constructor of Exercise class.
         *
         *  Parametric constructor that is used while creating instance
         *  of previously measured exercise.
         *  \param id - an exercise identifier to be set
         *  \param min - minimum measured exercise value (degrees)
         *  \param max - maximum measured exercise value (degrees)
         *  \param range - measured range of an exercise in degrees
         *  \see ~Exercise()
         */
        Exercise(QString id, qint16 min, qint16 max, qint16 range);

        /*!
         *  \fn ~Exercise()
         *  \brief Base destructor of class Exercise.
         *
         *  Destructor function created for memory deallocation.
         *  \see Exercise(), Exercise(QString id, qint16 min, qint16 max, qint16 range)
         */
        ~Exercise();

        /*!
         *  \fn Exercise_GetID
         *  \brief Getter function meant for exercise \c mID retrieval.
         *
         *  Function is used as a getter of mID attribute.
         *
         *  \return QString - an identifier that consists of side
         *  resembling letter \c L or \c R (left/right limb) and an
         *  exercise index.
         */
        QString& Exercise_GetID();

        /*!
         *  \fn Exercise_GetMin
         *  \brief Getter function used as a \c mMin retriever.
         *
         *  Function that allows reading of minimum property.
         *
         *  \return qint16 - a numerical value representing
         *  exercise minimum in degrees
         */
        qint16 Exercise_GetMin();

        /*!
         *  \fn Exercise_GetMax
         *  \brief Getter function used as a \c mMax retriever.
         *
         *  Function allows exercise maximum value reading.
         *
         *  \return qint16 - a numerical value representing
         *  exercise maximum in degrees
         */
        qint16 Exercise_GetMax();

        /*!
         *  \fn Exercise_GetRange
         *  \brief Getter function used as a \c mRange obtainer.
         *
         *  This function provides reading of exercise range
         *  member variable.
         *
         *  \return qint16 - an integral value that holds the
         *  measurement range in degrees
         */
        qint16 Exercise_GetRange();

        /*!
         *  \fn Exercise_GetAllFormatted
         *  \brief Extended getter function that returns all members
         *  in a formatted manner.
         *
         *  Function reads current member variables and returns
         *  them in a QString form placed into a list.
         *
         *  \return QStringList - a list of text strings that holds exercise
         *  ID, minimum, maximum and range
         */
        QStringList Exercise_GetAllFormatted();

    private:
        /*!
         * \var QString mID
         * \brief QString text string member that holds exercise
         * identifier.
         */
        QString mID;

        /*!
         * \var qint16 mMin
         * \brief qint16 minimum measured exercise value
         * in degrees.
         */
        qint16 mMin;

        /*!
         * \var qint16 mMax
         * \brief qint16 maximum value of a measured exercise
         * in degrees.
         */
        qint16 mMax;

        /*!
         * \var qint16 mRange
         * \brief qint16 measurement range value in degrees.
         */
        qint16 mRange;
};

#endif // EXERCISE_HPP
