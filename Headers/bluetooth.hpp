/**
 * @file        bluetooth.hpp
 * @author      Tomas Bartosik
 * @date        21.07.2021
 * @brief       declaration file for Bluetooth connection class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef BLUETOOTH_HPP
#define BLUETOOTH_HPP

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/bluetoothdevice.hpp"
#include "../Headers/bluetoothservice.hpp"
#include "../Headers/bluetoothinfo.hpp"
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothDeviceInfo>
#include <QLowEnergyController>
#include <QRegularExpression>
#include <QTimer>
#include <QList>
#include <QPair>

QT_FORWARD_DECLARE_CLASS (QBluetoothDeviceInfo)

/*!
 * \enum ControllerType
 * \brief ControllerType enumeration distinguishes between BlueCoin devices.
 *
 * Just as two devices are supported, this enum presents two options
 * for choosing between primary and secondary device.
 */
enum class ControllerType
{
    ePrimary = 1,       /*!< Operations for primary device are selected. */
    eSecondary          /*!< Operations for secondary device are selected. */
};

/*Class declaration: --------------------------------------------------------*/
/*!
 *  \class Bluetooth
 *  \brief This class manages Bluetooth connection capabilities.
 *
 *  Bluetooth is \c QObject derived class meant for implementation
 *  of Bluetooth Low Energy technology device discovery and
 *  connection along with services and characteristics reading
 *  in a periodically repeated manner. It relies on helper classes
 *  BluetoothDevice, BluetoothService and BluetoothInfo.
 *  \see BluetoothDevice, BluetoothService, BluetoothInfo
 */
class Bluetooth : public QObject
{
    Q_OBJECT

public:
    /*!
     *  \fn Bluetooth()
     *  \brief Default class constructor for Bluetooth.
     *
     *  Function is used for members initialization and Qt's
     *  signals and slots connection.
     *
     *  \see ~Bluetooth()
     */
    Bluetooth();

    /*!
     *  \fn ~Bluetooth()
     *  \brief Default class destructor.
     *
     *  Provides memory cleaning and object destruction
     *  process.
     */
    ~Bluetooth();

    /*!
     *  \fn Bluetooth_StartScanning
     *  \brief Initiates the Bluetooth scanning process.
     *
     *  This function responds to user need of device
     *  discovery in area, which is monitored through
     *  a \c QTimer that manages periodical reporting.
     */
    void Bluetooth_StartScanning();

    /*!
     *  \fn Bluetooth_Connect(const QString &deviceAddress)
     *  \brief Manages selected device connection.
     *
     *  Function handles Bluetooth connection request and uses
     *  the function parameter \c deviceAddress for connection to
     *  a previously discovered device. The function uses member
     *  variables to recognise which ControllerType handles connection
     *  and quits prematurely if both primary and secondary devices are
     *  already connected.
     *
     *  \param deviceAddress - device address to be connected
     *  \return bool - \c TRUE in case of connection success, \c FALSE otherwise
     */
    bool Bluetooth_Connect(const QString &deviceAddress);

    /*!
     *  \fn Bluetooth_Disconnect(const QString &deviceAddress)
     *  \brief Manages chosen device's disconnection.
     *
     *  This function manages disconnection of selected device (hence
     *  the address parameter). If two devices are connected, the remaining
     *  device is re-connected through primary controller.
     *
     *  \param deviceAddress - device address to be disconnected
     */
    void Bluetooth_Disconnect(const QString &deviceAddress);

    /*!
     *  \fn Bluetooth_IsDeviceConnected(const QString &deviceAddress)
     *  \brief Provides checking whether a device is connected or not.
     *
     *  Function handles checking of a device address and informs whether
     *  it is connected or not.
     *
     *  \param deviceAddress - device address to be checked
     *  \return bool - \c TRUE if device with given address is connected, \c FALSE
     *  otherwise
     */
    bool Bluetooth_IsDeviceConnected(const QString &deviceAddress);

    /*!
     *  \fn Bluetooth_RetrieveSensorData
     *  \brief Handles the data recovery and possibility of further processing.
     *
     *  This function reads connected device's service characteristics and provides
     *  signal emitting, so that it can be processed outside Bluetooth module, namely
     *  in SensorModule.
     *  \see SensorModule
     */
    void Bluetooth_RetrieveSensorData();

    /*!
     *  \fn Bluetooth_StopRetrievingSensorData
     *  \brief Function stops repeated data recovery.
     *
     *  Function is used for stopping the \c QTimer that periodically
     *  retrieves BlueCoin measured data obtained via Bluetooth.
     */
    void Bluetooth_StopRetrievingSensorData();

    /*!
     *  \fn Bluetooth_SetServiceRetrievalSpeed(qint16 serviceRetrievalSpeed)
     *  \brief Setter function for recovery timer member variable.
     *
     *  This is a setter function that manages the value held by
     *  \c mServiceRetrievalSpeed member variable.
     *  \param serviceRetrievalSpeed - retrieval speed for recovery in \b ms
     */
    void Bluetooth_SetServiceRetrievalSpeed(qint16 serviceRetrievalSpeed);

    /*!
     *  \fn Bluetooth_SetDebugInformations(bool debugInformations)
     *  \brief Setter function for debugging informations member variable.
     *
     *  This is a setter function that manages the value held by
     *  \c mEnableDebugInformations member variable.
     *  \param debugInformations - \c TRUE if debug info enabled, \c FALSE
     *  otherwise
     */
    void Bluetooth_SetDebugInformations(bool debugInformations);

    /*!
     *  \fn Bluetooth_GetDevicesList
     *  \brief Getter function for device names list.
     *
     *  This is a getter function that returns the value of
     *  \c mDevicesList member variable - list of device names.
     *  \return QStringList - a list of text strings with device names
     */
    QStringList Bluetooth_GetDevicesList();

    /*!
     *  \fn Bluetooth_GetAddressList
     *  \brief Getter function for device addresses list.
     *
     *  This is a getter function that returns the value of
     *  \c mAddressList member variable - list of device addresses.
     *  \return QStringList - a list of text strings with device addresses
     */
    QStringList Bluetooth_GetAddressList();

    /*!
     *  \fn Bluetooth_GetConnectedDevicesCount
     *  \brief Function for connected devices calculation.
     *
     *  This function returns the number of connected Bluetooth devices.
     *  \return qint16 - number of connected devices [0, 1 or 2]
     */
    quint16 Bluetooth_GetConnectedDevicesCount();

private slots:
    /*!
     *  \fn Bluetooth_AddDevice(const QBluetoothDeviceInfo &device)
     *  \brief Slot that adds new found device to the list.
     *
     *  This slot manages enlisting of a newly discovered device
     *  to the list of all found devices. Device names and addresses list
     *  are also expanded. This function adds Bluetooth Low Energy
     *  devices only, such as BlueCoin.
     *  \param device - a device found by \c QBluetoothDeviceDiscoveryAgent
     */
    void Bluetooth_AddDevice(const QBluetoothDeviceInfo &device);

    /*!
     *  \fn Bluetooth_CatchError(QBluetoothDeviceDiscoveryAgent::Error error)
     *  \brief Slot used for error catching while discovering devices.
     *
     *  This slot function filters incoming error types and provides informations
     *  to the application user.
     *  \param error - error identified by device discovery of
     *  \c QBluetoothDeviceDiscoveryAgent
     */
    void Bluetooth_CatchError(QBluetoothDeviceDiscoveryAgent::Error error);

    /*!
     *  \fn Bluetooth_ReportStatus
     *  \brief Slot that manages discovery status reporting to MainWindow.
     *
     *  Slot (function) that emits signal \c Bluetooth_SignalStatusReady .
     */
    void Bluetooth_ReportStatus();

    /*!
     *  \fn Bluetooth_StopScanning
     *  \brief Slot that is processed when device discovery finishes.
     *
     *  Slot manages discovery \c QTimer stopping and handles
     *  \c Bluetooth_SignalDevicesReady signal emitting for further
     *  device processing.
     */
    void Bluetooth_StopScanning();

    /*!
     *  \fn Bluetooth_DeviceConnected(ControllerType controllerType)
     *  \brief Slot that initiates service discovery.
     *
     *  This slot starts service discovery when a Bluetooth device is connected.
     *  \param controllerType - determines which controller starts
     *  the service discovery
     */
    void Bluetooth_DeviceConnected(ControllerType controllerType);

    /*!
     *  \fn Bluetooth_DeviceHasError(ControllerType controllerType)
     *  \brief Slot that reports device error.
     *
     *  If debugging informations are turned on, report that a controller
     *  (primary or secondary) has an error, if such occurs.
     *  \param controllerType - determines which controller has
     *  encountered an error
     */
    void Bluetooth_DeviceHasError(ControllerType controllerType);

    /*!
     *  \fn Bluetooth_DeviceDisconnected
     *  \brief Slot that reports device disconnection.
     *
     *  This slot manages \c Bluetooth_SignalDisconnected signal emitting
     *  and informs the user through standard application output if debugging
     *  information notifications are turned on.
     */
    void Bluetooth_DeviceDisconnected();

    /*!
     *  \fn Bluetooth_RegisterService(ControllerType controllerType, const QBluetoothUuid &uuid)
     *  \brief Slot function for service registration.
     *
     *  This slot uses obtained service UUID for registration while using
     *  selected ControllerType, both received via function parameters.
     *  This slot uses custom class BluetoothService which is then added
     *  to the list of found and registered services.
     *  \param controllerType - determines which controller has
     *  discovered a service
     *  \param uuid - service UUID to be registered
     */
    void Bluetooth_RegisterService(ControllerType controllerType, const QBluetoothUuid &uuid);

    /*!
     *  \fn Bluetooth_ServicesRegistered
     *  \brief Slot function for service and characteristics analysis.
     *
     *  Slot function manages Bluetooth services analysis after their discovery
     *  is finished. If a BlueCoin specific service UUID is found and low-level UUID
     *  of characteristics interpreting the important measured data can also be
     *  confirmed, the device is successfully connected to and data can be periodically
     *  received. Note that user is informed if a required service is not found, which means
     *  that the device is incompatible (it is not a BlueCoin device).
     */
    void Bluetooth_ServicesRegistered();

    /*!
     *  \fn Bluetooth_ServiceDetailsObtained(qint16 serviceID, QLowEnergyService::ServiceState newState)
     *  \brief Slot that enables repeated characteristics reading.
     *
     *  This slot function responds to signal/slot connection established while
     *  processing \c Bluetooth_ServicesRegistered and finishes the characteristics
     *  discovery for a service. If a characteristics UUID matches BlueCoin defined
     *  identifier, periodically maintained characteristics reading can be started.
     *  \param serviceID - an identifier of a service
     *  \param newState - a currently observed state received as a parameter
     */
    void Bluetooth_ServiceDetailsObtained(qint16 serviceID, QLowEnergyService::ServiceState newState);

signals:
    /*!
     *  \fn Bluetooth_SignalStatusReady
     *  \brief Signal that depicts a need to update
     *  device discovery status bar in MainWindow.
     */
    void Bluetooth_SignalStatusReady();

    /*!
     *  \fn Bluetooth_SignalDevicesReady
     *  \brief Signal that gets emitted when
     *  device discovery finishes.
     */
    void Bluetooth_SignalDevicesReady();

    /*!
     *  \fn Bluetooth_SignalDisconnected
     *  \brief Signal that gets emitted when a
     *  device gets disconnected.
     */
    void Bluetooth_SignalDisconnected();

    /*!
     *  \fn Bluetooth_SignalConnectionError(const QString &error)
     *  \brief Signal that gets emitted when
     *  errors occur while discovering devices.
     */
    void Bluetooth_SignalConnectionError(const QString &error);

    /*!
     *  \fn Bluetooth_SignalDeviceConnected
     *  \brief Signal that gets emitted when a
     *  device gets successfully connected.
     */
    void Bluetooth_SignalDeviceConnected();

    /*!
     *  \fn Bluetooth_SignalDeviceIncompatible
     *  \brief Signal that gets emitted if a
     *  device is incompatible (not a BlueCoin).
     */
    void Bluetooth_SignalDeviceIncompatible();

    /*!
     *  \fn Bluetooth_SignalDeviceUnreachable
     *  \brief Signal that gets emitted if a
     *  device goes out of reach/availability.
     */
    void Bluetooth_SignalDeviceUnreachable();

    /*!
     *  \fn Bluetooth_SignalDeviceBeyondLimit
     *  \brief Signal that gets emitted if a connection
     *  occurs when all controllers are in use.
     */
    void Bluetooth_SignalDeviceBeyondLimit();

    /*!
     *  \fn Bluetooth_SignalCharacteristicsUpdated
     *  \brief Signal that gets emitted when
     *  characteristics properties change.
     */
    void Bluetooth_SignalCharacteristicsUpdated();

    /*!
     *  \fn Bluetooth_SignalSensorDataReady(const QString &hexValue)
     *  \brief Signal that gets emitted each time
     *  when measurement retrieving timer ticks.
     */
    void Bluetooth_SignalSensorDataReady(const QString &hexValue);

private:
    /*!
     * \var QBluetoothDeviceDiscoveryAgent *mDiscoveryAgent
     * \brief QBluetoothDeviceDiscoveryAgent member variable pointer
     * for device discovery functionalities.
     */
    QBluetoothDeviceDiscoveryAgent *mDiscoveryAgent;

    /*!
     * \var QLowEnergyController *mPrimaryController
     * \brief QLowEnergyController, a primary controller member object.
     */
    QLowEnergyController *mPrimaryController;

    /*!
     * \var QLowEnergyController *mSecondaryController
     * \brief QLowEnergyController, a secondary controller member object.
     */
    QLowEnergyController *mSecondaryController;

    /*!
     * \var QLowEnergyService *mPrimaryService
     * \brief QLowEnergyService member for primary BLE service object.
     */
    QLowEnergyService *mPrimaryService;

    /*!
     * \var QLowEnergyService *mSecondaryService
     * \brief QLowEnergyService member for secondary BLE service object.
     */
    QLowEnergyService *mSecondaryService;

    /*!
     * \var QList<QObject*> mDevices
     * \brief QList<QObject>, a list of BLE devices held as
     * a \c QObject pointers.
     */
    QList<QObject*> mDevices;

    /*!
     * \var QList<QObject*> mServices
     * \brief QList<QObject>, a list of discovered Bluetooth services
     * held as a \c QObject pointers.
     */
    QList<QObject*> mServices;

    /*!
     * \var QRegularExpression mHighLevelUUIDExpression
     * \brief QRegularExpression, a member object for UUID pattern
     * recognition used for services and characteristics.
     */
    QRegularExpression mHighLevelUUIDExpression;

    /*!
     * \var BluetoothDevice mPrimaryBluetoothDevice
     * \brief BluetoothDevice is custom class based member
     * depicting primary Bluetooth device.
     */
    BluetoothDevice mPrimaryBluetoothDevice;

    /*!
     * \var BluetoothDevice mSecondaryBluetoothDevice
     * \brief BluetoothDevice is custom class based member
     * depicting secondary Bluetooth device.
     */
    BluetoothDevice mSecondaryBluetoothDevice;

    /*!
     * \var QStringList mDevicesList
     * \brief QStringList, it is a list of text strings with
     * device names information.
     */
    QStringList mDevicesList;

    /*!
     * \var QStringList mAddressList
     * \brief QStringList that is a list of text strings with
     * device addresses information.
     */
    QStringList mAddressList;

    /*!
     * \var QString mPrimaryConnectedAddress
     * \brief QString that holds information about
     * the primary connected device address.
     */
    QString mPrimaryConnectedAddress;

    /*!
     * \var QString mSecondaryConnectedAddress
     * \brief QString that holds information about
     * the secondary connected device address.
     */
    QString mSecondaryConnectedAddress;

    /*!
     * \var QString mLatestDeviceAddress
     * \brief QString that holds information about
     * the latest connected device address used.
     */
    QString mLatestDeviceAddress;

    /*!
     * \var QString mPrimaryHighLevelServiceUUID
     * \brief QString with a pre-set value of service
     * UUID used by BlueCoin primary device.
     */
    QString mPrimaryHighLevelServiceUUID;

    /*!
     * \var QString mSecondaryHighLevelServiceUUID
     * \brief QString with a pre-set value of service
     * UUID used by BlueCoin secondary device.
     */
    QString mSecondaryHighLevelServiceUUID;

    /*!
     * \var QString mBaseLowLevelServiceUUID
     * \brief QString with a pre-set value of characteristics
     * UUID used by any BlueCoin device.
     */
    QString mBaseLowLevelServiceUUID;

    /*!
     * \var QString mPrimaryLowLevelServiceUUID
     * \brief QString with a pre-set value of characteristics
     * UUID used by primary BlueCoin device.
     */
    QString mPrimaryLowLevelServiceUUID;

    /*!
     * \var QString mSecondaryLowLevelServiceUUID
     * \brief QString with a pre-set value of characteristics
     * UUID used by secondary BlueCoin device.
     */
    QString mSecondaryLowLevelServiceUUID;

    /*!
     * \var QTimer *mBluetoothDiscoveryTimer
     * \brief QTimer that is used for continual
     * Bluetooth devices discovery status reporting.
     */
    QTimer *mBluetoothDiscoveryTimer;

    /*!
     * \var QTimer *mBluetoothRetrieveTimer
     * \brief QTimer that is key part of Bluetooth module,
     * it uses time in \b ms that will elapse before new data
     * retrieval is processed.
     */
    QTimer *mBluetoothRetrieveTimer;

    /*!
     * \var qint16 mServiceRetrievalSpeed
     * \brief qint16 value that defines the time
     * needed before \c mBluetoothRetrieveTimer ticks.
     */
    qint16 mServiceRetrievalSpeed;

    /*!
     * \var bool mScanningCompleted
     * \brief bool value that in its \c TRUE state depicts a
     * successful connection response while \c FALSE means a failure.
     */
    bool mScanningCompleted;

    /*!
     * \var bool mEnableDebugInformations
     * \brief bool value that in its \c TRUE state enables additional debugging
     *  informations reporting while \c FALSE disables it.
     */
    bool mEnableDebugInformations;
};

#endif // BLUETOOTH_HPP
