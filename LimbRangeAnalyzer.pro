QT       += core gui bluetooth 3dcore 3drender 3dinput 3dextras

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES = $$files(Sources/*.cpp, false)

HEADERS = $$files(Headers/*.hpp, false)

FORMS = $$files(Forms/*.ui, false)

RESOURCES = $$files(Resources/*.qrc, false)

TRANSLATIONS = $$files(Resources/languages/*.ts, false)

DISTFILES = $$files(Resources/*.qss, false) \
    Resources/android-sources/AndroidManifest.xml

# Windows & macOS application icons
RC_ICONS += Resources/images/LRALogoIcon.ico
ICON = Resources/images/LRALogoIcon.ico

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

android:ANDROID_PACKAGE_SOURCE_DIR = $$PWD/Resources/android-sources
