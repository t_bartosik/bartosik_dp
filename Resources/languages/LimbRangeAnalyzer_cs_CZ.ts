<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>Bluetooth</name>
    <message>
        <source> Device name: %1    Device address: %2</source>
        <translation type="vanished"> Název zařízení: %1    Adresa zařízení: %2</translation>
    </message>
    <message>
        <source>Bluetooth is turned off. Turn it on first.</source>
        <translation>Bluetooth je vypnuté. Je třeba zapnout Bluetooth.</translation>
    </message>
    <message>
        <source>Writing or reading from connected device resulted in an error.</source>
        <translation>Zápis či čtení z připojeného zařízení vyvolalo chybu.</translation>
    </message>
    <message>
        <source>This platform does not support BLE connection.</source>
        <translation>Tato platforma nepodporuje technologii BLE připojení.</translation>
    </message>
    <message>
        <source>This platform does not support BLE device discovery method.</source>
        <translation>Tato platforma nepodporuje metodu hledání služeb technologie BLE.</translation>
    </message>
    <message>
        <source>An unexpected error has occurred.</source>
        <translation>Nastala neočekávaná chyba.</translation>
    </message>
    <message>
        <source>Scanning for devices started...</source>
        <translation>Začalo vyhledávání dostupných zařízení...</translation>
    </message>
    <message>
        <source>Scanning could not be started.</source>
        <translation>Vyhledávání nebylo možné zahájit.</translation>
    </message>
    <message>
        <source>Primary connected address: %1    Secondary connected address: %2</source>
        <translation>Primární připojená adresa: %1    Sekundární připojená adresa: %2</translation>
    </message>
    <message>
        <source>Scanning completed, no BLE devices found.</source>
        <translation>Vyhledávání dokončeno, nebyla nalezena žádná BLE zařízení.</translation>
    </message>
    <message>
        <source>Scanning completed, found %1 device(s).</source>
        <translation>Vyhledávání dokončeno, nalezených zařízení: %1</translation>
    </message>
    <message>
        <source>Controller: Primary device connected.</source>
        <translation>Ovladač připojení: Připojeno primární zařízení.</translation>
    </message>
    <message>
        <source>Controller: Secondary device connected.</source>
        <translation>Ovladač připojení: Připojeno sekundární zařízení.</translation>
    </message>
    <message>
        <source>Controller: Primary device has error: %1</source>
        <translation>Ovladač připojení: Primární zařízení hlásí chybu: %1</translation>
    </message>
    <message>
        <source>Controller: Secondary device has error: %1</source>
        <translation>Ovladač připojení: Sekundární zařízení hlásí chybu: %1</translation>
    </message>
    <message>
        <source>Controller: Device disconnected.</source>
        <translation>Ovladač připojení: Zařízení odpojeno.</translation>
    </message>
    <message>
        <source>Cannot create service object from received UUID.</source>
        <translation>Nelze vytvořit objekt služby na základě získaného UUID.</translation>
    </message>
    <message>
        <source>Done services registering.</source>
        <translation>Registrace služeb dokončena.</translation>
    </message>
    <message>
        <source>No services available.</source>
        <translation>Nejsou k dispozici žádné služby.</translation>
    </message>
    <message>
        <source>Needed service found (primary device): %1</source>
        <translation>Nalezena potřebná služba (primární zařízení): %1</translation>
    </message>
    <message>
        <source>Needed service found (secondary device): %1</source>
        <translation>Nalezena potřebná služba (sekundární zařízení): %1</translation>
    </message>
    <message>
        <source>Discovering characteristics (primary device).</source>
        <translation>Probíhá identifikace charakteristik (primární zařízení).</translation>
    </message>
    <message>
        <source>Characteristics discovered before (primary device).</source>
        <translation>Charakteristiky byly již dříve objeveny (primární zařízení).</translation>
    </message>
    <message>
        <source>Sensor fetched value (primary device): %1</source>
        <translation>Získané hodnoty senzorů (primární zařízení): %1</translation>
    </message>
    <message>
        <source>Discovering characteristics (secondary device).</source>
        <translation>Probíhá identifikace charakteristik (sekundární zařízení).</translation>
    </message>
    <message>
        <source>Characteristics discovered before (secondary device).</source>
        <translation>Charakteristiky byly již dříve objeveny (sekundární zařízení).</translation>
    </message>
    <message>
        <source>Sensor fetched value (secondary device): %1</source>
        <translation>Získané hodnoty senzorů (sekundární zařízení): %1</translation>
    </message>
    <message>
        <source>Obtaining service details for low level UUID: %1</source>
        <translation>Probíhá získání detailů služby nízkoúrovňového UUID: %1</translation>
    </message>
    <message>
        <source>Needed low service found, starting retrieval timer.</source>
        <translation>Požadovaná služba nalezena, spouštím časovač pro sběr dat.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <source>Connection Page</source>
        <translation>Stránka správy připojení</translation>
    </message>
    <message>
        <source>Start scanning</source>
        <translation>Spustit vyhledávání</translation>
    </message>
    <message>
        <source>available   </source>
        <translation>dostupné   </translation>
    </message>
    <message>
        <source>processing   </source>
        <translation>zpracovávané   </translation>
    </message>
    <message>
        <source>connected   </source>
        <translation>připojené   </translation>
    </message>
    <message>
        <source>incompatible   </source>
        <translation>nekompatibilní   </translation>
    </message>
    <message>
        <source>unreachable   </source>
        <translation>nedostupné   </translation>
    </message>
    <message>
        <source>Manage connection</source>
        <translation>Spravovat připojení</translation>
    </message>
    <message>
        <source>🔎
+</source>
        <translation>🔎
+</translation>
    </message>
    <message>
        <source>🔎
-</source>
        <translation>🔎
-</translation>
    </message>
    <message>
        <source>↩</source>
        <translation>↩</translation>
    </message>
    <message>
        <source>↪</source>
        <translation>↪</translation>
    </message>
    <message>
        <source>Sensor Monitoring Page</source>
        <translation>Stránka měření pohyblivosti</translation>
    </message>
    <message>
        <source>Body part: </source>
        <translation>Část těla: </translation>
    </message>
    <message>
        <source>Exercise: </source>
        <translation>Cvičení: </translation>
    </message>
    <message>
        <source>Start measurement</source>
        <translation>Spustit měření</translation>
    </message>
    <message>
        <source>Processing:</source>
        <translation>Výstup měření:</translation>
    </message>
    <message>
        <source>Min:</source>
        <translation>Min:</translation>
    </message>
    <message>
        <source>Max:</source>
        <translation>Max:</translation>
    </message>
    <message>
        <source>Range:</source>
        <translation>Rozsah:</translation>
    </message>
    <message>
        <source>Client:</source>
        <translation>Klient:</translation>
    </message>
    <message>
        <source>Save session record</source>
        <translation>Uložit záznam sezení</translation>
    </message>
    <message>
        <source>Display exercise history</source>
        <translation>Zobrazit historii cvičení</translation>
    </message>
    <message>
        <source>Client Page</source>
        <translation>Stránka klientů</translation>
    </message>
    <message>
        <source>Confirm client selection</source>
        <translation>Potvrdit výběr klienta</translation>
    </message>
    <message>
        <source>Selected client: None</source>
        <translation>Vybraný klient: Žádný</translation>
    </message>
    <message>
        <source>Add new client</source>
        <translation>Přidat nového klienta</translation>
    </message>
    <message>
        <source>Remove selected client(s)</source>
        <translation>Odebrat vybrané klienty</translation>
    </message>
    <message>
        <source>Exercise History Page</source>
        <translation>Stránka historie měření</translation>
    </message>
    <message>
        <source>Selected exercise: None</source>
        <translation>Vybraný cvik: Žádný</translation>
    </message>
    <message>
        <source>-80</source>
        <translation>-80</translation>
    </message>
    <message>
        <source>-20</source>
        <translation>-20</translation>
    </message>
    <message>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <source>90</source>
        <translation>90</translation>
    </message>
    <message>
        <source>MIN</source>
        <translation>MIN</translation>
    </message>
    <message>
        <source>MAX</source>
        <translation>MAX</translation>
    </message>
    <message>
        <source>Return</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <source>Settings Page</source>
        <translation>Stránka nastavení aplikace</translation>
    </message>
    <message>
        <source>General settings: </source>
        <translation>Základní nastavení: </translation>
    </message>
    <message>
        <source>Enable debug informations: </source>
        <translation>Povolit ladící informace: </translation>
    </message>
    <message>
        <source>Enable unrestricted dual limb movement: </source>
        <translation>Povolit neomezený pohyb dvou končetin: </translation>
    </message>
    <message>
        <source>Language: </source>
        <translation>Jazyk: </translation>
    </message>
    <message>
        <source>About Page</source>
        <translation>Stránka s popisem aplikace</translation>
    </message>
    <message>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <source>How to use Limb Range Analyzer</source>
        <translation>Jak ovládat Limb Range Analyzer</translation>
    </message>
    <message>
        <source>Limb Range Analyzer</source>
        <translation>Limb Range Analyzer</translation>
    </message>
    <message>
        <source>version</source>
        <translation>verze</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>footer</source>
        <translation>patička</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Konec</translation>
    </message>
    <message>
        <source>Devices</source>
        <translation>Zařízení</translation>
    </message>
    <message>
        <source>Measurement</source>
        <translation>Měření</translation>
    </message>
    <message>
        <source>Clients</source>
        <translation>Klienti</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <source>Settings loading error</source>
        <translation>Potíže s načtením nastavení</translation>
    </message>
    <message>
        <source>All body parts</source>
        <translation>Všechny části těla</translation>
    </message>
    <message>
        <source>Left arm</source>
        <translation>Levá horní končetina</translation>
    </message>
    <message>
        <source>Right arm</source>
        <translation>Pravá horní končetina</translation>
    </message>
    <message>
        <source>Left leg</source>
        <translation>Levá dolní končetina</translation>
    </message>
    <message>
        <source>Right leg</source>
        <translation>Pravá dolní končetina</translation>
    </message>
    <message>
        <source>Exercise I: arm biceps</source>
        <translation>Cvičení I: paže biceps</translation>
    </message>
    <message>
        <source>Exercise II: arm sideways</source>
        <translation>Cvičení II: paže do strany</translation>
    </message>
    <message>
        <source>Exercise III: arm horizontal</source>
        <translation>Cvičení III: paže vodorovně</translation>
    </message>
    <message>
        <source>Exercise IV: arm up</source>
        <translation>Cvičení IV: paže vzhůru</translation>
    </message>
    <message>
        <source>Exercise V: knee up</source>
        <translation>Cvičení V: koleno vzhůru</translation>
    </message>
    <message>
        <source>Exercise VI: arm biceps (dual)</source>
        <translation>Cvičení VI: paže biceps (duální)</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <source>Range</source>
        <translation>Rozsah</translation>
    </message>
    <message>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <source>Version: %1</source>
        <translation>Verze: %1</translation>
    </message>
    <message>
        <source>&lt;b&gt;Limb Range Analyzer&lt;/b&gt; is a diploma-thesis related application that uses &lt;b&gt;Bluetooth LE&lt;/b&gt; technology for connection to &lt;b&gt;BlueCoin&lt;/b&gt; wristband devices. Up to two such devices can be connected while they provide IMU sensors measurement data that is processed and visualised in this application in order to allow human limb range analysis. Limb Range Analyzer application provides multiple pages (enlisted below) that can be accessed through the main menu of user interface.</source>
        <translation>&lt;b&gt;Limb Range Analyzer&lt;/b&gt; je aplikace utvářená v rámci diplomové práce, která využívá technologie &lt;b&gt;Bluetooth LE&lt;/b&gt; při připojení k náramkovým zařízením &lt;b&gt;BlueCoin&lt;/b&gt;. Připojit lze až dvě taková zařízení, zatímco poskytují měřená data IMU senzorů, která jsou dále zpracovávána a vizualizována v této aplikaci tak, aby bylo možné realizovat analýzu pohyblivosti lidských končetin. Aplikace Limb Range Analyzer nabízí více stránek (viz popis níže), k nimž lze přistupovat skrze hlavní nabídku uživatelského rozhraní.</translation>
    </message>
    <message>
        <source>About Connection Page</source>
        <translation>O stránce správy připojení</translation>
    </message>
    <message>
        <source>The &lt;b&gt;Connection Page&lt;/b&gt; allows the &lt;i&gt;discovery&lt;/i&gt; and &lt;i&gt;connection/disconnection&lt;/i&gt; management of &lt;b&gt;Bluetooth LE&lt;/b&gt; devices. Note that &lt;b&gt;BlueCoin&lt;/b&gt; devices only can become successfully connected, any other device shall be marked as &lt;i&gt;incompatible&lt;/i&gt;. If a previously discovered device goes out of reach scope while a connection request approaches, it will &lt;i&gt;not be possible&lt;/i&gt; to establish connection. Connection states are color-marked and user is informed in case of connection failure.</source>
        <translation>&lt;b&gt;Stránka správy připojení&lt;/b&gt; umožňuje &lt;i&gt;vyhledávat&lt;/i&gt; a &lt;i&gt;připojovat/odpojovat&lt;/i&gt; zařízení využívající technologii &lt;b&gt;Bluetooth LE&lt;/b&gt;. Nutno doplnit, že úspěšně připojit lze pouze zařízení &lt;b&gt;BlueCoin&lt;/b&gt;, jakékoliv jiné zařízení bude označeno za &lt;i&gt;nekompatibilní&lt;/i&gt;. Pokud se dříve objevené zařízení dostane mimo dosah během žádosti o připojení, &lt;i&gt;nebude možné&lt;/i&gt; navázat spojení. Stavy interakce se zařízením jsou barevně rozlišeny a uživatel je v případě neúspěšného pokusu o připojení upozorněn.</translation>
    </message>
    <message>
        <source>About Sensor Monitoring Page</source>
        <translation>O stránce měření pohyblivosti</translation>
    </message>
    <message>
        <source>The &lt;b&gt;Sensor Monitoring Page&lt;/b&gt; is a key component of this application, it provides both &lt;i&gt;graphic output&lt;/i&gt; of limb range analysis and corresponding &lt;i&gt;numerical values&lt;/i&gt;. On the right side of screen, user is free to &lt;i&gt;choose an exercise&lt;/i&gt; that gets measured and animated in the graphics section. If a client is selected (see client page info below), their exercise progress can be &lt;i&gt;saved&lt;/i&gt; and &lt;i&gt;accessed&lt;/i&gt; along with their previous records - the possibility to &lt;i&gt;observe changes&lt;/i&gt; is thus secured. Note that previous device connection is mandatory for the measurement to function.</source>
        <translation>&lt;b&gt;Stránka měření pohyblivosti&lt;/b&gt; je klíčovou součástí této aplikace, která poskytuje jak &lt;i&gt;grafický výstup&lt;/i&gt; měřené pohyblivosti lidských končetin, tak i odpovídající &lt;i&gt;číselné hodnoty&lt;/i&gt;. V pravé části obrazovky má uživatel možnost &lt;i&gt;vybrat cvičení&lt;/i&gt;, které je dále zpracováno a animováno v grafické oblasti. Je-li vybrán klient (pro bližší informace viz stránka klientů níže), pak lze výsledky cvičení &lt;i&gt;uložit&lt;/i&gt; a &lt;i&gt;přistoupit k nim&lt;/i&gt; společně s dříve uskutečněnými záznamy, čímž je zajištěna možnost &lt;i&gt;sledování změn&lt;/i&gt; v čase. Nutno ovšem dodat, že pro správnou funkcionalitu měření je potřebné dříve realizované připojení odpovídajícího zařízení.</translation>
    </message>
    <message>
        <source>About Client Page</source>
        <translation>O stránce klientů</translation>
    </message>
    <message>
        <source>The &lt;b&gt;Client Page&lt;/b&gt; involves patient management, namely their &lt;i&gt;creation&lt;/i&gt;, &lt;i&gt;removal&lt;/i&gt; and &lt;i&gt;selection&lt;/i&gt;. In case of creation, &lt;i&gt;first name&lt;/i&gt;, &lt;i&gt;last name&lt;/i&gt; and &lt;i&gt;e-mail address&lt;/i&gt; have to be provided. The selection funcionality toggled on this page allows measurement results to be linked with corresponding client.</source>
        <translation>&lt;b&gt;Stránka klientů&lt;/b&gt; zahrnuje nástroje pro správu pacientů, jmenovitě jejich &lt;i&gt;vytváření&lt;/i&gt;, &lt;i&gt;odstranění&lt;/i&gt; a &lt;i&gt;výběr&lt;/i&gt;. V případě jejich tvorby je třeba uvést &lt;i&gt;jméno&lt;/i&gt;, &lt;i&gt;příjmení&lt;/i&gt; a &lt;i&gt;e-mailovou adresu&lt;/i&gt;. Funkcionalita výběru klienta, dostupná skrze tlačítko na této stránce, umožňuje přidružení měřených výsledků k odpovídající osobě.</translation>
    </message>
    <message>
        <source>Contents of Settings Page</source>
        <translation>Obsah stránky nastavení aplikace</translation>
    </message>
    <message>
        <source>&lt;b&gt;Enable debug informations&lt;/b&gt;: This option &lt;i&gt;enables&lt;/i&gt; or &lt;i&gt;disables&lt;/i&gt; various debug informations displayed in application console output.</source>
        <translation>&lt;b&gt;Povolit ladící informace&lt;/b&gt;: Tato možnost &lt;i&gt;povoluje&lt;/i&gt; nebo &lt;i&gt;zakazuje&lt;/i&gt; výpis různých ladících informací aplikace ve výstupní konzoli.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Enable unrestricted dual limb movement&lt;/b&gt;: An option for dual limb measurement exercises, that allows limbs depicted in 3D graphics output to move and bend &lt;i&gt;freely&lt;/i&gt;, unnaturally even.</source>
        <translation>&lt;b&gt;Povolit neomezený pohyb dvou končetin&lt;/b&gt;: Umožňuje přepínání možnosti spojené s cviky duálních zařízení, která v případě povolení nerespektuje zákonitosti ohybu kloubů, čímž se v prostorovém grafickém výstupu aplikuje &lt;i&gt;volný&lt;/i&gt;, místy i nepřirozený pohyb.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Language&lt;/b&gt;: Provides a choice of preferred &lt;i&gt;application language&lt;/i&gt;. Note that only few languages are supported.</source>
        <translation>&lt;b&gt;Jazyk&lt;/b&gt;: Poskytuje možnost výběru preferovaného &lt;i&gt;jazyka aplikace&lt;/i&gt;. Nutno doplnit, že podporovány jsou pouze některé jazyky.</translation>
    </message>
    <message>
        <source>Tomáš Bartošík, ©2021-2022 Faculty of applied informatics, TBU in Zlin</source>
        <translation>Tomáš Bartošík, ©2021-2022 Fakulta aplikované informatiky, UTB ve Zlíně</translation>
    </message>
    <message>
        <source>Choose a page from the main menu.</source>
        <translation>Pro pokračování vyberte stránku z hlavní nabídky.</translation>
    </message>
    <message>
        <source>Scanning for Bluetooth LE devices in vicinity...</source>
        <translation>Probíhá vyhledávání Bluetooth LE zařízení v okolí...</translation>
    </message>
    <message>
        <source>Scanning for Bluetooth LE devices completed.</source>
        <translation>Vyhledávání Bluetooth LE zařízení dokončeno.</translation>
    </message>
    <message>
        <source>Disconnect from selected device</source>
        <translation>Odpojit se od vybraného zařízení</translation>
    </message>
    <message>
        <source>Connect to selected device</source>
        <translation>Připojit se k vybranému zařízení</translation>
    </message>
    <message>
        <source>Connection error</source>
        <translation>Chyba připojení</translation>
    </message>
    <message>
        <source>Choose one device in order to manage connection.</source>
        <translation>Pro správu připojení je nutné vybrat právě jedno zařízení.</translation>
    </message>
    <message>
        <source>Disconnecting from selected device...</source>
        <translation>Probíhá odpojení od vybraného zařízení...</translation>
    </message>
    <message>
        <source>Disconnection completed.</source>
        <translation>Odpojení dokončeno.</translation>
    </message>
    <message>
        <source>Connecting to selected device...</source>
        <translation>Probíhá připojení k vybranému zařízení...</translation>
    </message>
    <message>
        <source>Stop measurement</source>
        <translation>Zastavit měření</translation>
    </message>
    <message>
        <source>Measurement is now in process.</source>
        <translation>Probíhá měření.</translation>
    </message>
    <message>
        <source>Measurement has been stopped.</source>
        <translation>Měření bylo zastaveno.</translation>
    </message>
    <message>
        <source>Connection completed.</source>
        <translation>Připojení dokončeno.</translation>
    </message>
    <message>
        <source>Compatibility error</source>
        <translation>Chyba kompatibility</translation>
    </message>
    <message>
        <source>This device does not support required services.</source>
        <translation>Toto zařízení nepodporuje požadované služby.</translation>
    </message>
    <message>
        <source>Devices with yellow background do not contain necessary service.</source>
        <translation>Zařízení se žlutým pozadím nezahrnují požadovanou službu.</translation>
    </message>
    <message>
        <source>Access error</source>
        <translation>Chyba v přístupnosti</translation>
    </message>
    <message>
        <source>This device is unfortunately unreachable.</source>
        <translation>Toto zařízení je v současnosti nedosažitelné.</translation>
    </message>
    <message>
        <source>Devices with red background are unreachable.</source>
        <translation>Zařízení s červeným pozadím jsou nedosažitelná.</translation>
    </message>
    <message>
        <source>Limit error</source>
        <translation>Chyba počtu zařízení</translation>
    </message>
    <message>
        <source>All possible connections are already established.</source>
        <translation>Všechna povolená připojení jsou již navázána.</translation>
    </message>
    <message>
        <source>All possible connections are active, disconnect other devices first.</source>
        <translation>Všechna povolená připojení jsou aktivní, nejdříve odpojte jiná zařízení.</translation>
    </message>
    <message>
        <source>Record saving error</source>
        <translation>Chyba ukládání záznamu</translation>
    </message>
    <message>
        <source>Daily exercise record successfully overwritten.</source>
        <translation>Denní rekord cvičení byl úspěšně aktualizován.</translation>
    </message>
    <message>
        <source>Daily exercise record successfully created.</source>
        <translation>Denní rekord cvičení byl úspěšně vytvořen.</translation>
    </message>
    <message>
        <source>There is no measurement data ready.</source>
        <translation>Nejsou k dispozici žádná naměřená data.</translation>
    </message>
    <message>
        <source>Client selection is empty.</source>
        <translation>Ze seznamu není vybrán žádný klient.</translation>
    </message>
    <message>
        <source>History display error</source>
        <translation>Chyba zobrazení historie</translation>
    </message>
    <message>
        <source>Selected exercise: %1, left limb</source>
        <translation>Vybraný cvik: %1, levá končetina</translation>
    </message>
    <message>
        <source>Selected exercise: %1, right limb</source>
        <translation>Vybraný cvik: %1, pravá končetina</translation>
    </message>
    <message>
        <source>Client has no records for selected exercise.</source>
        <translation>Klient nemá žádné předchozí záznamy vybraného cviku.</translation>
    </message>
    <message>
        <source>First name should contain at least one character.</source>
        <translation>Jméno musí obsahovat alespoň jeden znak.</translation>
    </message>
    <message>
        <source>Last name should contain at least one character.</source>
        <translation>Příjmení musí obsahovat alespoň jeden znak.</translation>
    </message>
    <message>
        <source>E-mail format does not match requirements.</source>
        <translation>E-mailová adresa nemá požadovaný formát.</translation>
    </message>
    <message>
        <source>Some columns have not been filled.</source>
        <translation>Některé sloupce nebyly vyplněny.</translation>
    </message>
    <message>
        <source>Entry error</source>
        <translation>Chyba na vstupu</translation>
    </message>
    <message>
        <source>Confirm new client entry</source>
        <translation>Potvrdit tvorbu nového klienta</translation>
    </message>
    <message>
        <source>Please define client&apos;s name and e-mail address.</source>
        <translation>Je třeba uvést celé jméno a e-mailovou adresu klienta.</translation>
    </message>
    <message>
        <source>New client was successfully created.</source>
        <translation>Nový záznam klienta byl úspěšně vytvořen.</translation>
    </message>
    <message>
        <source>Such client already exists.</source>
        <translation>Nově vkládaný klient již existuje.</translation>
    </message>
    <message>
        <source>Removal error</source>
        <translation>Chyba v odstranění</translation>
    </message>
    <message>
        <source>Select one or more clients in order to remove them.</source>
        <translation>Pro odstranění je nutné vybrat jednoho nebo více klientů.</translation>
    </message>
    <message>
        <source>Removal confirmation</source>
        <translation>Potvrzení odstranění</translation>
    </message>
    <message>
        <source>Do you really want to remove selected client(s)?</source>
        <translation>Opravdu chcete odstranit vybraný záznam (či záznamy)?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <source>Selected client was successfully deleted.</source>
        <translation>Vybrané záznamy byly úspěšně odstraněny.</translation>
    </message>
    <message>
        <source>Selection error</source>
        <translation>Chyba ve výběru</translation>
    </message>
    <message>
        <source>Select exactly one client from the list below.</source>
        <translation>Ze seznamu níže je nutné vybrat právě jednoho klienta.</translation>
    </message>
    <message>
        <source>Selected client: %1 %2</source>
        <translation>Vybraný klient: %1 %2</translation>
    </message>
    <message>
        <source>Client was successfully selected.</source>
        <translation>Klient byl úspěšně vybrán.</translation>
    </message>
    <message>
        <source>Application settings have been overwritten.</source>
        <translation>Aplikační nastavení byla aktualizována.</translation>
    </message>
    <message>
        <source>Limb Range Analyzer is about to quit.</source>
        <translation>Limb Range Analyzer bude ukončen.</translation>
    </message>
    <message>
        <source>Going to connect to/disconnect from devices.</source>
        <translation>Bude provedeno připojení/odpojení vybraného zařízení.</translation>
    </message>
    <message>
        <source>Device is to be connected.</source>
        <translation>U vybraného zařízení bude zpracováno připojení.</translation>
    </message>
    <message>
        <source> Device name: %1    Device address: %2</source>
        <translation> Název zařízení: %1    Adresa zařízení: %2</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <source>XML loading error</source>
        <translation>Chyba v načítání XML</translation>
    </message>
    <message>
        <source>XML file could not be opened in order to load application settings.</source>
        <translation>Nepodařilo se otevřít XML soubor v rámci načítání aplikačního nastavení.</translation>
    </message>
    <message>
        <source>XML file could not be opened in order to load client data.</source>
        <translation>Nepodařilo se otevřít XML soubor v rámci načítání uživatelských dat.</translation>
    </message>
    <message>
        <source>XML saving error</source>
        <translation>Chyba v ukládání XML</translation>
    </message>
    <message>
        <source>XML file could not be opened in order to save application settings.</source>
        <translation>Nepodařilo se otevřít XML soubor v rámci ukládání aplikačního nastavení.</translation>
    </message>
    <message>
        <source>XML file could not be opened in order to save client data.</source>
        <translation>Nepodařilo se otevřít XML soubor v rámci ukládání uživatelských dat.</translation>
    </message>
</context>
</TS>
