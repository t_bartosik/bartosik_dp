<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>Bluetooth</name>
    <message>
        <source> Device name: %1    Device address: %2</source>
        <translation type="vanished"> Device name: %1    Device address: %2</translation>
    </message>
    <message>
        <source>Bluetooth is turned off. Turn it on first.</source>
        <translation>Bluetooth is turned off. Turn it on first.</translation>
    </message>
    <message>
        <source>Writing or reading from connected device resulted in an error.</source>
        <translation>Writing or reading from connected device resulted in an error.</translation>
    </message>
    <message>
        <source>This platform does not support BLE connection.</source>
        <translation>This platform does not support BLE connection.</translation>
    </message>
    <message>
        <source>This platform does not support BLE device discovery method.</source>
        <translation>This platform does not support BLE device discovery method.</translation>
    </message>
    <message>
        <source>An unexpected error has occurred.</source>
        <translation>An unexpected error has occurred.</translation>
    </message>
    <message>
        <source>Scanning for devices started...</source>
        <translation>Scanning for devices started...</translation>
    </message>
    <message>
        <source>Scanning could not be started.</source>
        <translation>Scanning could not be started.</translation>
    </message>
    <message>
        <source>Primary connected address: %1    Secondary connected address: %2</source>
        <translation>Primary connected address: %1    Secondary connected address: %2</translation>
    </message>
    <message>
        <source>Scanning completed, no BLE devices found.</source>
        <translation>Scanning completed, no BLE devices found.</translation>
    </message>
    <message>
        <source>Scanning completed, found %1 device(s).</source>
        <translation>Scanning completed, found %1 device(s).</translation>
    </message>
    <message>
        <source>Controller: Primary device connected.</source>
        <translation>Controller: Primary device connected.</translation>
    </message>
    <message>
        <source>Controller: Secondary device connected.</source>
        <translation>Controller: Secondary device connected.</translation>
    </message>
    <message>
        <source>Controller: Primary device has error: %1</source>
        <translation>Controller: Primary device has error: %1</translation>
    </message>
    <message>
        <source>Controller: Secondary device has error: %1</source>
        <translation>Controller: Secondary device has error: %1</translation>
    </message>
    <message>
        <source>Controller: Device disconnected.</source>
        <translation>Controller: Device disconnected.</translation>
    </message>
    <message>
        <source>Cannot create service object from received UUID.</source>
        <translation>Cannot create service object from received UUID.</translation>
    </message>
    <message>
        <source>Done services registering.</source>
        <translation>Done services registering.</translation>
    </message>
    <message>
        <source>No services available.</source>
        <translation>No services available.</translation>
    </message>
    <message>
        <source>Needed service found (primary device): %1</source>
        <translation>Needed service found (primary device): %1</translation>
    </message>
    <message>
        <source>Needed service found (secondary device): %1</source>
        <translation>Needed service found (secondary device): %1</translation>
    </message>
    <message>
        <source>Discovering characteristics (primary device).</source>
        <translation>Discovering characteristics (primary device).</translation>
    </message>
    <message>
        <source>Characteristics discovered before (primary device).</source>
        <translation>Characteristics discovered before (primary device).</translation>
    </message>
    <message>
        <source>Sensor fetched value (primary device): %1</source>
        <translation>Sensor fetched value (primary device): %1</translation>
    </message>
    <message>
        <source>Discovering characteristics (secondary device).</source>
        <translation>Discovering characteristics (secondary device).</translation>
    </message>
    <message>
        <source>Characteristics discovered before (secondary device).</source>
        <translation>Characteristics discovered before (secondary device).</translation>
    </message>
    <message>
        <source>Sensor fetched value (secondary device): %1</source>
        <translation>Sensor fetched value (secondary device): %1</translation>
    </message>
    <message>
        <source>Obtaining service details for low level UUID: %1</source>
        <translation>Obtaining service details for low level UUID: %1</translation>
    </message>
    <message>
        <source>Needed low service found, starting retrieval timer.</source>
        <translation>Needed low service found, starting retrieval timer.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <source>Connection Page</source>
        <translation>Connection Page</translation>
    </message>
    <message>
        <source>Start scanning</source>
        <translation>Start scanning</translation>
    </message>
    <message>
        <source>available   </source>
        <translation>available   </translation>
    </message>
    <message>
        <source>processing   </source>
        <translation>processing   </translation>
    </message>
    <message>
        <source>connected   </source>
        <translation>connected   </translation>
    </message>
    <message>
        <source>incompatible   </source>
        <translation>incompatible   </translation>
    </message>
    <message>
        <source>unreachable   </source>
        <translation>unreachable   </translation>
    </message>
    <message>
        <source>Manage connection</source>
        <translation>Manage connection</translation>
    </message>
    <message>
        <source>🔎
+</source>
        <translation>🔎
+</translation>
    </message>
    <message>
        <source>🔎
-</source>
        <translation>🔎
-</translation>
    </message>
    <message>
        <source>↩</source>
        <translation>↩</translation>
    </message>
    <message>
        <source>↪</source>
        <translation>↪</translation>
    </message>
    <message>
        <source>Sensor Monitoring Page</source>
        <translation>Sensor Monitoring Page</translation>
    </message>
    <message>
        <source>Body part: </source>
        <translation>Body part: </translation>
    </message>
    <message>
        <source>Exercise: </source>
        <translation>Exercise: </translation>
    </message>
    <message>
        <source>Start measurement</source>
        <translation>Start measurement</translation>
    </message>
    <message>
        <source>Processing:</source>
        <translation>Processing:</translation>
    </message>
    <message>
        <source>Min:</source>
        <translation>Min:</translation>
    </message>
    <message>
        <source>Max:</source>
        <translation>Max:</translation>
    </message>
    <message>
        <source>Range:</source>
        <translation>Range:</translation>
    </message>
    <message>
        <source>Client:</source>
        <translation>Client:</translation>
    </message>
    <message>
        <source>Save session record</source>
        <translation>Save session record</translation>
    </message>
    <message>
        <source>Display exercise history</source>
        <translation>Display exercise history</translation>
    </message>
    <message>
        <source>Client Page</source>
        <translation>Client Page</translation>
    </message>
    <message>
        <source>Confirm client selection</source>
        <translation>Confirm client selection</translation>
    </message>
    <message>
        <source>Selected client: None</source>
        <translation>Selected client: None</translation>
    </message>
    <message>
        <source>Add new client</source>
        <translation>Add new client</translation>
    </message>
    <message>
        <source>Remove selected client(s)</source>
        <translation>Remove selected client(s)</translation>
    </message>
    <message>
        <source>Exercise History Page</source>
        <translation>Exercise History Page</translation>
    </message>
    <message>
        <source>Selected exercise: None</source>
        <translation>Selected exercise: None</translation>
    </message>
    <message>
        <source>-80</source>
        <translation>-80</translation>
    </message>
    <message>
        <source>-20</source>
        <translation>-20</translation>
    </message>
    <message>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <source>90</source>
        <translation>90</translation>
    </message>
    <message>
        <source>MIN</source>
        <translation>MIN</translation>
    </message>
    <message>
        <source>MAX</source>
        <translation>MAX</translation>
    </message>
    <message>
        <source>Return</source>
        <translation>Return</translation>
    </message>
    <message>
        <source>Settings Page</source>
        <translation>Settings Page</translation>
    </message>
    <message>
        <source>General settings: </source>
        <translation>General settings: </translation>
    </message>
    <message>
        <source>Enable debug informations: </source>
        <translation>Enable debug informations: </translation>
    </message>
    <message>
        <source>Enable unrestricted dual limb movement: </source>
        <translation>Enable unrestricted dual limb movement: </translation>
    </message>
    <message>
        <source>Language: </source>
        <translation>Language: </translation>
    </message>
    <message>
        <source>About Page</source>
        <translation>About Page</translation>
    </message>
    <message>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <source>How to use Limb Range Analyzer</source>
        <translation>How to use Limb Range Analyzer</translation>
    </message>
    <message>
        <source>Limb Range Analyzer</source>
        <translation>Limb Range Analyzer</translation>
    </message>
    <message>
        <source>version</source>
        <translation>version</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>footer</source>
        <translation>footer</translation>
    </message>
    <message>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
    <message>
        <source>Devices</source>
        <translation>Devices</translation>
    </message>
    <message>
        <source>Measurement</source>
        <translation>Measurement</translation>
    </message>
    <message>
        <source>Clients</source>
        <translation>Clients</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <source>Settings loading error</source>
        <translation>Settings loading error</translation>
    </message>
    <message>
        <source>All body parts</source>
        <translation>All body parts</translation>
    </message>
    <message>
        <source>Left arm</source>
        <translation>Left arm</translation>
    </message>
    <message>
        <source>Right arm</source>
        <translation>Right arm</translation>
    </message>
    <message>
        <source>Left leg</source>
        <translation>Left leg</translation>
    </message>
    <message>
        <source>Right leg</source>
        <translation>Right leg</translation>
    </message>
    <message>
        <source>Exercise I: arm biceps</source>
        <translation>Exercise I: arm biceps</translation>
    </message>
    <message>
        <source>Exercise II: arm sideways</source>
        <translation>Exercise II: arm sideways</translation>
    </message>
    <message>
        <source>Exercise III: arm horizontal</source>
        <translation>Exercise III: arm horizontal</translation>
    </message>
    <message>
        <source>Exercise IV: arm up</source>
        <translation>Exercise IV: arm up</translation>
    </message>
    <message>
        <source>Exercise V: knee up</source>
        <translation>Exercise V: knee up</translation>
    </message>
    <message>
        <source>Exercise VI: arm biceps (dual)</source>
        <translation>Exercise VI: arm biceps (dual)</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>First name</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Last name</translation>
    </message>
    <message>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <source>Range</source>
        <translation>Range</translation>
    </message>
    <message>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <source>Czech</source>
        <translation>Czech</translation>
    </message>
    <message>
        <source>Version: %1</source>
        <translation>Version: %1</translation>
    </message>
    <message>
        <source>&lt;b&gt;Limb Range Analyzer&lt;/b&gt; is a diploma-thesis related application that uses &lt;b&gt;Bluetooth LE&lt;/b&gt; technology for connection to &lt;b&gt;BlueCoin&lt;/b&gt; wristband devices. Up to two such devices can be connected while they provide IMU sensors measurement data that is processed and visualised in this application in order to allow human limb range analysis. Limb Range Analyzer application provides multiple pages (enlisted below) that can be accessed through the main menu of user interface.</source>
        <translation>&lt;b&gt;Limb Range Analyzer&lt;/b&gt; is a diploma-thesis related application that uses &lt;b&gt;Bluetooth LE&lt;/b&gt; technology for connection to &lt;b&gt;BlueCoin&lt;/b&gt; wristband devices. Up to two such devices can be connected while they provide IMU sensors measurement data that is processed and visualised in this application in order to allow human limb range analysis. Limb Range Analyzer application provides multiple pages (enlisted below) that can be accessed through the main menu of user interface.</translation>
    </message>
    <message>
        <source>About Connection Page</source>
        <translation>About Connection Page</translation>
    </message>
    <message>
        <source>The &lt;b&gt;Connection Page&lt;/b&gt; allows the &lt;i&gt;discovery&lt;/i&gt; and &lt;i&gt;connection/disconnection&lt;/i&gt; management of &lt;b&gt;Bluetooth LE&lt;/b&gt; devices. Note that &lt;b&gt;BlueCoin&lt;/b&gt; devices only can become successfully connected, any other device shall be marked as &lt;i&gt;incompatible&lt;/i&gt;. If a previously discovered device goes out of reach scope while a connection request approaches, it will &lt;i&gt;not be possible&lt;/i&gt; to establish connection. Connection states are color-marked and user is informed in case of connection failure.</source>
        <translation>The &lt;b&gt;Connection Page&lt;/b&gt; allows the &lt;i&gt;discovery&lt;/i&gt; and &lt;i&gt;connection/disconnection&lt;/i&gt; management of &lt;b&gt;Bluetooth LE&lt;/b&gt; devices. Note that &lt;b&gt;BlueCoin&lt;/b&gt; devices only can become successfully connected, any other device shall be marked as &lt;i&gt;incompatible&lt;/i&gt;. If a previously discovered device goes out of reach scope while a connection request approaches, it will &lt;i&gt;not be possible&lt;/i&gt; to establish connection. Connection states are color-marked and user is informed in case of connection failure.</translation>
    </message>
    <message>
        <source>About Sensor Monitoring Page</source>
        <translation>About Sensor Monitoring Page</translation>
    </message>
    <message>
        <source>The &lt;b&gt;Sensor Monitoring Page&lt;/b&gt; is a key component of this application, it provides both &lt;i&gt;graphic output&lt;/i&gt; of limb range analysis and corresponding &lt;i&gt;numerical values&lt;/i&gt;. On the right side of screen, user is free to &lt;i&gt;choose an exercise&lt;/i&gt; that gets measured and animated in the graphics section. If a client is selected (see client page info below), their exercise progress can be &lt;i&gt;saved&lt;/i&gt; and &lt;i&gt;accessed&lt;/i&gt; along with their previous records - the possibility to &lt;i&gt;observe changes&lt;/i&gt; is thus secured. Note that previous device connection is mandatory for the measurement to function.</source>
        <translation>The &lt;b&gt;Sensor Monitoring Page&lt;/b&gt; is a key component of this application, it provides both &lt;i&gt;graphic output&lt;/i&gt; of limb range analysis and corresponding &lt;i&gt;numerical values&lt;/i&gt;. On the right side of screen, user is free to &lt;i&gt;choose an exercise&lt;/i&gt; that gets measured and animated in the graphics section. If a client is selected (see client page info below), their exercise progress can be &lt;i&gt;saved&lt;/i&gt; and &lt;i&gt;accessed&lt;/i&gt; along with their previous records - the possibility to &lt;i&gt;observe changes&lt;/i&gt; is thus secured. Note that previous device connection is mandatory for the measurement to function.</translation>
    </message>
    <message>
        <source>About Client Page</source>
        <translation>About Client Page</translation>
    </message>
    <message>
        <source>The &lt;b&gt;Client Page&lt;/b&gt; involves patient management, namely their &lt;i&gt;creation&lt;/i&gt;, &lt;i&gt;removal&lt;/i&gt; and &lt;i&gt;selection&lt;/i&gt;. In case of creation, &lt;i&gt;first name&lt;/i&gt;, &lt;i&gt;last name&lt;/i&gt; and &lt;i&gt;e-mail address&lt;/i&gt; have to be provided. The selection funcionality toggled on this page allows measurement results to be linked with corresponding client.</source>
        <translation>The &lt;b&gt;Client Page&lt;/b&gt; involves patient management, namely their &lt;i&gt;creation&lt;/i&gt;, &lt;i&gt;removal&lt;/i&gt; and &lt;i&gt;selection&lt;/i&gt;. In case of creation, &lt;i&gt;first name&lt;/i&gt;, &lt;i&gt;last name&lt;/i&gt; and &lt;i&gt;e-mail address&lt;/i&gt; have to be provided. The selection funcionality toggled on this page allows measurement results to be linked with corresponding client.</translation>
    </message>
    <message>
        <source>Contents of Settings Page</source>
        <translation>Contents of Settings Page</translation>
    </message>
    <message>
        <source>&lt;b&gt;Enable debug informations&lt;/b&gt;: This option &lt;i&gt;enables&lt;/i&gt; or &lt;i&gt;disables&lt;/i&gt; various debug informations displayed in application console output.</source>
        <translation>&lt;b&gt;Enable debug informations&lt;/b&gt;: This option &lt;i&gt;enables&lt;/i&gt; or &lt;i&gt;disables&lt;/i&gt; various debug informations displayed in application console output.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Enable unrestricted dual limb movement&lt;/b&gt;: An option for dual limb measurement exercises, that allows limbs depicted in 3D graphics output to move and bend &lt;i&gt;freely&lt;/i&gt;, unnaturally even.</source>
        <translation>&lt;b&gt;Enable unrestricted dual limb movement&lt;/b&gt;: An option for dual limb measurement exercises, that allows limbs depicted in 3D graphics output to move and bend &lt;i&gt;freely&lt;/i&gt;, unnaturally even.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Language&lt;/b&gt;: Provides a choice of preferred &lt;i&gt;application language&lt;/i&gt;. Note that only few languages are supported.</source>
        <translation>&lt;b&gt;Language&lt;/b&gt;: Provides a choice of preferred &lt;i&gt;application language&lt;/i&gt;. Note that only few languages are supported.</translation>
    </message>
    <message>
        <source>Tomáš Bartošík, ©2021-2022 Faculty of applied informatics, TBU in Zlin</source>
        <translation>Tomáš Bartošík, ©2021-2022 Faculty of applied informatics, TBU in Zlin</translation>
    </message>
    <message>
        <source>Choose a page from the main menu.</source>
        <translation>Choose a page from the main menu.</translation>
    </message>
    <message>
        <source>Scanning for Bluetooth LE devices in vicinity...</source>
        <translation>Scanning for Bluetooth LE devices in vicinity...</translation>
    </message>
    <message>
        <source>Scanning for Bluetooth LE devices completed.</source>
        <translation>Scanning for Bluetooth LE devices completed.</translation>
    </message>
    <message>
        <source>Disconnect from selected device</source>
        <translation>Disconnect from selected device</translation>
    </message>
    <message>
        <source>Connect to selected device</source>
        <translation>Connect to selected device</translation>
    </message>
    <message>
        <source>Connection error</source>
        <translation>Connection error</translation>
    </message>
    <message>
        <source>Choose one device in order to manage connection.</source>
        <translation>Choose one device in order to manage connection.</translation>
    </message>
    <message>
        <source>Disconnecting from selected device...</source>
        <translation>Disconnecting from selected device...</translation>
    </message>
    <message>
        <source>Disconnection completed.</source>
        <translation>Disconnection completed.</translation>
    </message>
    <message>
        <source>Connecting to selected device...</source>
        <translation>Connecting to selected device...</translation>
    </message>
    <message>
        <source>Stop measurement</source>
        <translation>Stop measurement</translation>
    </message>
    <message>
        <source>Measurement is now in process.</source>
        <translation>Measurement is now in process.</translation>
    </message>
    <message>
        <source>Measurement has been stopped.</source>
        <translation>Measurement has been stopped.</translation>
    </message>
    <message>
        <source>Connection completed.</source>
        <translation>Connection completed.</translation>
    </message>
    <message>
        <source>Compatibility error</source>
        <translation>Compatibility error</translation>
    </message>
    <message>
        <source>This device does not support required services.</source>
        <translation>This device does not support required services.</translation>
    </message>
    <message>
        <source>Devices with yellow background do not contain necessary service.</source>
        <translation>Devices with yellow background do not contain necessary service.</translation>
    </message>
    <message>
        <source>Access error</source>
        <translation>Access error</translation>
    </message>
    <message>
        <source>This device is unfortunately unreachable.</source>
        <translation>This device is unfortunately unreachable.</translation>
    </message>
    <message>
        <source>Devices with red background are unreachable.</source>
        <translation>Devices with red background are unreachable.</translation>
    </message>
    <message>
        <source>Limit error</source>
        <translation>Limit error</translation>
    </message>
    <message>
        <source>All possible connections are already established.</source>
        <translation>All possible connections are already established.</translation>
    </message>
    <message>
        <source>All possible connections are active, disconnect other devices first.</source>
        <translation>All possible connections are active, disconnect other devices first.</translation>
    </message>
    <message>
        <source>Record saving error</source>
        <translation>Record saving error</translation>
    </message>
    <message>
        <source>Daily exercise record successfully overwritten.</source>
        <translation>Daily exercise record successfully overwritten.</translation>
    </message>
    <message>
        <source>Daily exercise record successfully created.</source>
        <translation>Daily exercise record successfully created.</translation>
    </message>
    <message>
        <source>There is no measurement data ready.</source>
        <translation>There is no measurement data ready.</translation>
    </message>
    <message>
        <source>Client selection is empty.</source>
        <translation>Client selection is empty.</translation>
    </message>
    <message>
        <source>History display error</source>
        <translation>History display error</translation>
    </message>
    <message>
        <source>Selected exercise: %1, left limb</source>
        <translation>Selected exercise: %1, left limb</translation>
    </message>
    <message>
        <source>Selected exercise: %1, right limb</source>
        <translation>Selected exercise: %1, right limb</translation>
    </message>
    <message>
        <source>Client has no records for selected exercise.</source>
        <translation>Client has no records for selected exercise.</translation>
    </message>
    <message>
        <source>First name should contain at least one character.</source>
        <translation>First name should contain at least one character.</translation>
    </message>
    <message>
        <source>Last name should contain at least one character.</source>
        <translation>Last name should contain at least one character.</translation>
    </message>
    <message>
        <source>E-mail format does not match requirements.</source>
        <translation>E-mail format does not match requirements.</translation>
    </message>
    <message>
        <source>Some columns have not been filled.</source>
        <translation>Some columns have not been filled.</translation>
    </message>
    <message>
        <source>Entry error</source>
        <translation>Entry error</translation>
    </message>
    <message>
        <source>Confirm new client entry</source>
        <translation>Confirm new client entry</translation>
    </message>
    <message>
        <source>Please define client&apos;s name and e-mail address.</source>
        <translation>Please define client&apos;s name and e-mail address.</translation>
    </message>
    <message>
        <source>New client was successfully created.</source>
        <translation>New client was successfully created.</translation>
    </message>
    <message>
        <source>Such client already exists.</source>
        <translation>Such client already exists.</translation>
    </message>
    <message>
        <source>Removal error</source>
        <translation>Removal error</translation>
    </message>
    <message>
        <source>Select one or more clients in order to remove them.</source>
        <translation>Select one or more clients in order to remove them.</translation>
    </message>
    <message>
        <source>Removal confirmation</source>
        <translation>Removal confirmation</translation>
    </message>
    <message>
        <source>Do you really want to remove selected client(s)?</source>
        <translation>Do you really want to remove selected client(s)?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Selected client was successfully deleted.</source>
        <translation>Selected client was successfully deleted.</translation>
    </message>
    <message>
        <source>Selection error</source>
        <translation>Selection error</translation>
    </message>
    <message>
        <source>Select exactly one client from the list below.</source>
        <translation>Select exactly one client from the list below.</translation>
    </message>
    <message>
        <source>Selected client: %1 %2</source>
        <translation>Selected client: %1 %2</translation>
    </message>
    <message>
        <source>Client was successfully selected.</source>
        <translation>Client was successfully selected.</translation>
    </message>
    <message>
        <source>Application settings have been overwritten.</source>
        <translation>Application settings have been overwritten.</translation>
    </message>
    <message>
        <source>Limb Range Analyzer is about to quit.</source>
        <translation>Limb Range Analyzer is about to quit.</translation>
    </message>
    <message>
        <source>Going to connect to/disconnect from devices.</source>
        <translation>Going to connect to/disconnect from devices.</translation>
    </message>
    <message>
        <source>Device is to be connected.</source>
        <translation>Device is to be connected.</translation>
    </message>
    <message>
        <source> Device name: %1    Device address: %2</source>
        <translation> Device name: %1    Device address: %2</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <source>Czech</source>
        <translation>Czech</translation>
    </message>
    <message>
        <source>XML loading error</source>
        <translation>XML loading error</translation>
    </message>
    <message>
        <source>XML file could not be opened in order to load application settings.</source>
        <translation>XML file could not be opened in order to load application settings.</translation>
    </message>
    <message>
        <source>XML file could not be opened in order to load client data.</source>
        <translation>XML file could not be opened in order to load client data.</translation>
    </message>
    <message>
        <source>XML saving error</source>
        <translation>XML saving error</translation>
    </message>
    <message>
        <source>XML file could not be opened in order to save application settings.</source>
        <translation>XML file could not be opened in order to save application settings.</translation>
    </message>
    <message>
        <source>XML file could not be opened in order to save client data.</source>
        <translation>XML file could not be opened in order to save client data.</translation>
    </message>
</context>
</TS>
