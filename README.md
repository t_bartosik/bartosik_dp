LIMB RANGE ANALYZER
===================
&nbsp;

**LIMB RANGE ANALYZER** is a diploma-thesis related application that uses **BLUETOOTH LE** technology for connection to **BLUECOIN** wristband devices. Up to two such devices can be connected while they provide IMU sensors measurement data that is processed and visualised in this application in order to allow human limb range analysis. Limb Range Analyzer application provides multiple pages (enlisted below) that can be accessed through the main menu of user interface.

&nbsp;
### About Connection Page

The **CONNECTION PAGE** allows the *discovery* and *connection/disconnection* management of **BLUETOOTH LE** devices. Note that **BLUECOIN** devices only can become successfully connected, any other device shall be marked as *incompatible*. If a previously discovered device goes out of reach scope while a connection request approaches, it will *not be possible* to establish connection. Connection states are color-marked and user is informed in case of connection failure.

&nbsp;
### About Sensor Monitoring Page

The **SENSOR MONITORING PAGE** is a key component of this application, it provides both *graphic output* of limb range analysis and corresponding *numerical values*. On the right side of screen, user is free to *choose an exercise* that gets measured and animated in the graphics section. If a client is selected (see client page info below), their exercise progress can be *saved* and *accessed* along with their previous records - the possibility to *observe changes* is thus secured. Note that previous device connection is mandatory for the measurement to function.

&nbsp;
### About Client Page

The **CLIENT PAGE** involves patient management, namely their *creation*, *removal* and *selection*. In case of creation, *first name*, *last name* and *e-mail address* have to be provided. The selection funcionality toggled on this page allows measurement results to be linked with corresponding client.

&nbsp;
### Contents of Settings Page

- **Enable debug informations**: This option *enables* or *disables* various debug informations displayed in application console output.
- **Enable unrestricted dual limb movement**: An option for dual limb measurement exercises, that allows limbs depicted in 3D graphics output to move and bend *freely*, unnaturally even.
- **Language**: Provides a choice of preferred *application language*. Note that only few languages are supported.