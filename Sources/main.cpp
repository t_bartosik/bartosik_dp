/**
 * @file        main.cpp
 * @author      Tomas Bartosik
 * @date        21.07.2021
 * @brief       main application file for Limb Range Analyzer
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/mainwindow.hpp"
#include <QtCore/QLoggingCategory>
#include <QApplication>
#include <QFile>

/*Main function: ------------------------------------------------------------*/
/*!
 *  \fn main(int argc, char *argv[])
 *  \brief Main loop function of Limb Range Analyzer.
 *
 *  Main application function that defines the usage of
 *  MainWindow class along with basic application
 *  settings.
 *
 *  \param argc - count of input arguments
 *  \param argv - argument specification
 *  \return int - loop return code where \c 0
 *  stands for standard ending, \c others represent
 *  errors
 *  \see MainWindow
 */
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("Limb Range Analyzer");
    QApplication::setOrganizationName("UTB");
    QApplication::setApplicationVersion("1.0");

    QFile styleFile(":/styles/stylesheet.qss");
    styleFile.open(QFile::ReadOnly);
    QString fileSheet = styleFile.readAll();
    qApp->setStyleSheet(fileSheet);

    MainWindow window;
    window.setWindowTitle("Limb Range Analyzer");
    window.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    window.setFixedWidth(1280);
    window.setMaximumWidth(QWIDGETSIZE_MAX);
    window.setMinimumWidth(1200);
    window.setMinimumHeight(600);
    window.show();

    return app.exec();
}
