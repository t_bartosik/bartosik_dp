/**
 * @file        bluetoothinfo.cpp
 * @author      Tomas Bartosik
 * @date        27.07.2021
 * @brief       definition file for Bluetooth service informations
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/bluetoothinfo.hpp"

/*Class definition: ---------------------------------------------------------*/
BluetoothInfo::BluetoothInfo(){}
BluetoothInfo::BluetoothInfo(const QLowEnergyCharacteristic &characteristic) : mCharacteristic(characteristic) {}
BluetoothInfo::~BluetoothInfo(){}

void BluetoothInfo::BluetoothInfo_SetCharacteristics(const QLowEnergyCharacteristic &characteristic) {mCharacteristic = characteristic;}
QLowEnergyCharacteristic BluetoothInfo::Bluetooth_GetCharacteristics() const {return mCharacteristic;}

QString BluetoothInfo::BluetoothInfo_GetUUID() const
{
    const QBluetoothUuid uuid = mCharacteristic.uuid();
    bool success = false;
    quint16 uuid16 = uuid.toUInt16(&success);
    if(success) return QStringLiteral("0x") + QString::number(uuid16, 16);

    quint32 uuid32 = uuid.toUInt32(&success);
    if(success) return QStringLiteral("0x") + QString::number(uuid32, 16);

    return uuid.toString().remove(QLatin1Char('{')).remove(QLatin1Char('}'));
}

QString BluetoothInfo::BluetoothInfo_GetValue() const
{
    QByteArray resultArray = mCharacteristic.value();
    QString result = QString();
    if(resultArray.isEmpty() == false)
    {
        result = resultArray.toHex();
        return result;
    }
    else return QString();
}

