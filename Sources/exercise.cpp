/**
 * @file        exercise.cpp
 * @author      Tomas Bartosik
 * @date        01.03.2022
 * @brief       definition file of class Exercise
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/exercise.hpp"

/*Class definition: ---------------------------------------------------------*/
Exercise::Exercise() : mID(QString()), mMin(0), mMax(0), mRange(0) {}
Exercise::Exercise(QString id, qint16 min, qint16 max, qint16 range)
    : mID(id), mMin(min), mMax(max), mRange(range) {}
Exercise::~Exercise() {}

QString& Exercise::Exercise_GetID() {return mID;}
qint16 Exercise::Exercise_GetMin() {return mMin;}
qint16 Exercise::Exercise_GetMax() {return mMax;}
qint16 Exercise::Exercise_GetRange() {return mRange;}
QStringList Exercise::Exercise_GetAllFormatted()
{
    return {mID, QString::number(mMin), QString::number(mMax), QString::number(mRange)};
}
