/**
 * @file        xmlparser.cpp
 * @author      Tomas Bartosik
 * @date        05.03.2022
 * @brief       implementation file for XML saving/loading possibility
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/xmlparser.hpp"

/*Class definition: ---------------------------------------------------------*/
XmlParser::XmlParser() : mDirPath(QString()) {}
XmlParser::~XmlParser() {}

void XmlParser::XmlParser_InitializePath()
{
    mDirPath = QString("xmlFiles/");
    QDir directory;

    if(!directory.exists(mDirPath)) directory.mkpath(mDirPath);
}

void XmlParser::XmlParser_InitializeSettings()
{
    XmlParser_InitializePath();
    QFile file(mDirPath + "settings.xml");
    if(!file.open(QIODevice::ReadOnly))
    {
        if(file.open(QIODevice::WriteOnly))
        {
            QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
            writer->setAutoFormatting(true);
            writer->writeStartDocument();
            writer->writeComment("Limb Range Analyzer - Application settings XML file");

            writer->writeStartElement("Settings")/*1*/;
            writer->writeTextElement("EnableDebugInformations", "0");
            writer->writeTextElement("EnableUnrestrictedLimbMovement", "0");
            writer->writeTextElement("Language", "1");
            writer->writeEndElement()/*1*/;

            writer->writeEndDocument();
            delete writer;
        }
    }
}

void XmlParser::XmlParser_InitializeClientData()
{
    XmlParser_InitializePath();
    QFile file(mDirPath + "clients.xml");
    if(!file.open(QIODevice::ReadOnly))
    {
        if(file.open(QIODevice::WriteOnly))
        {
            QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
            writer->setAutoFormatting(true);
            writer->writeStartDocument();
            writer->writeComment("Limb Range Analyzer - Client list XML file");

            writer->writeStartElement("Clients")/*1*/;
            writer->writeStartElement("Client")/*2*/;
            writer->writeAttribute("ID", "initialClient");
            writer->writeTextElement("FirstName", "John");
            writer->writeTextElement("LastName", "Doe");
            writer->writeTextElement("Email", "j_doe@utb.cz");
            writer->writeStartElement("Session")/*3a*/;
            writer->writeAttribute("ID", "2021-11-28");
            writer->writeStartElement("Exercise")/*4a*/;
            writer->writeAttribute("ID", "R1");
            writer->writeTextElement("Min", "20");
            writer->writeTextElement("Max", "96");
            writer->writeTextElement("Range", "76");
            writer->writeEndElement()/*4a*/;
            writer->writeStartElement("Exercise")/*4b*/;
            writer->writeAttribute("ID", "R4");
            writer->writeTextElement("Min", "-64");
            writer->writeTextElement("Max", "30");
            writer->writeTextElement("Range", "94");
            writer->writeEndElement()/*4b*/;
            writer->writeEndElement()/*3a*/;
            writer->writeStartElement("Session")/*3b*/;
            writer->writeAttribute("ID", "2021-12-04");
            writer->writeStartElement("Exercise")/*4a*/;
            writer->writeAttribute("ID", "L2");
            writer->writeTextElement("Min", "0");
            writer->writeTextElement("Max", "40");
            writer->writeTextElement("Range", "40");
            writer->writeEndElement()/*4a*/;
            writer->writeStartElement("Exercise")/*4b*/;
            writer->writeAttribute("ID", "R4");
            writer->writeTextElement("Min", "-90");
            writer->writeTextElement("Max", "30");
            writer->writeTextElement("Range", "120");
            writer->writeEndElement()/*4b*/;
            writer->writeEndElement()/*3b*/;
            writer->writeEndElement()/*2*/;
            writer->writeEndElement()/*1*/;

            writer->writeEndDocument();
            delete writer;
        }
    }
}

QSharedPointer<Settings> XmlParser::XmlParser_LoadSettings()
{
    XmlParser_InitializePath();
    QSharedPointer<Settings> settings;
    QStringList settingsLoaded;

    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML loading error"));
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to load application settings."));
    XmlParser_InitializeSettings();
    QFile file(mDirPath + "settings.xml");

    if(file.open(QIODevice::ReadOnly))
    {
        QXmlStreamReader *reader = new QXmlStreamReader(&file);

        if(reader->readNextStartElement())
        {
            if(reader->name() == "Settings")
            {
                while(reader->readNextStartElement())
                {
                    if(reader->name() == "EnableDebugInformations") settingsLoaded << reader->readElementText();
                    if(reader->name() == "EnableUnrestrictedLimbMovement") settingsLoaded << reader->readElementText();
                    if(reader->name() == "Language")
                    {
                        settingsLoaded << reader->readElementText();

                        settings = QSharedPointer<Settings>(new Settings());
                        settings->Settings_SetDebugInformations(settingsLoaded.at(0).toInt());
                        settings->Settings_SetUnrestrictedLimbMovement(settingsLoaded.at(1).toInt());
                        switch(settingsLoaded.at(2).toInt())
                        {
                            case 1: default: settings->Settings_SetLanguage(Language::eEnglish); break;
                            case 2: settings->Settings_SetLanguage(Language::eCzech); break;
                        }
                    }
                }
            }
            else reader->skipCurrentElement();
        }

        delete reader;
    }
    else
    {
        errMsg.exec();
        return QSharedPointer<Settings> (nullptr);
    }

    return settings;
}

QList<QSharedPointer<Client>> XmlParser::XmlParser_LoadClientData()
{
    XmlParser_InitializePath();
    QList<QSharedPointer<Client>> clientList;
    QList<QSharedPointer<Exercise>> exerciseList;
    QSharedPointer<Client> client;
    QStringList clientLoaded;
    QList<qint16> exerciseLoaded;
    QString sessionID;
    QString exerciseID;

    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML loading error"));
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to load client data."));
    XmlParser_InitializeClientData();
    QFile file(mDirPath + "clients.xml");

    if(file.open(QIODevice::ReadOnly))
    {
        QXmlStreamReader *reader = new QXmlStreamReader(&file);

        if(reader->readNextStartElement())
        {
            if(reader->name() == "Clients")
            {
                while(reader->readNextStartElement())
                {
                    if(reader->name() == "Client")
                    {
                        clientLoaded << reader->attributes().value("ID").toString();
                        while(reader->readNextStartElement())
                        {
                            if(reader->name() == "FirstName") clientLoaded << reader->readElementText();
                            else if(reader->name() == "LastName") clientLoaded << reader->readElementText();
                            else if(reader->name() == "Email")
                            {
                                clientLoaded << reader->readElementText();

                                client = QSharedPointer<Client>(new Client(clientLoaded.at(0), clientLoaded.at(1),
                                       clientLoaded.at(2), clientLoaded.at(3), QList<QSharedPointer<Session>>()));
                                clientLoaded.clear();
                            }
                            else if(reader->name() == "Session")
                            {
                                sessionID = reader->attributes().value("ID").toString();
                                while(reader->readNextStartElement())
                                {
                                    if(reader->name() == "Exercise")
                                    {
                                        exerciseID = reader->attributes().value("ID").toString();
                                        while(reader->readNextStartElement())
                                        {
                                            if(reader->name() == "Min") exerciseLoaded << reader->readElementText().toShort();
                                            else if(reader->name() == "Max") exerciseLoaded << reader->readElementText().toShort();
                                            else if(reader->name() == "Range")
                                            {
                                                exerciseLoaded << reader->readElementText().toShort();

                                                exerciseList.append(QSharedPointer<Exercise>(new Exercise(exerciseID,
                                                        exerciseLoaded.at(0), exerciseLoaded.at(1), exerciseLoaded.at(2))));
                                                exerciseLoaded.clear();
                                            }
                                        }
                                    }
                                }

                                client->Client_AddSession(QSharedPointer<Session>(new Session(sessionID, exerciseList)));
                                exerciseList.clear();
                            }
                        }

                        clientList.append(client);
                    }
                    else reader->skipCurrentElement();
                }
            }
            else reader->skipCurrentElement();
        }

        delete reader;
    }
    else
    {
        errMsg.exec();
        return QList<QSharedPointer<Client>> ();
    }

    return clientList;
}

void XmlParser::XmlParser_SaveSettings(QSharedPointer<Settings> settings)
{
    XmlParser_InitializePath();
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML saving error"));
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to save application settings."));
    QFile file(mDirPath + "settings.xml");
    if(file.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
        writer->setAutoFormatting(true);
        writer->writeStartDocument();
        writer->writeComment("Limb Range Analyzer - Application settings XML file");

        writer->writeStartElement("Settings");
        writer->writeTextElement("EnableDebugInformations", QString::number(settings->Settings_GetDebugInformations()));
        writer->writeTextElement("EnableUnrestrictedLimbMovement", QString::number(settings->Settings_GetUnrestrictedLimbMovement()));
        writer->writeTextElement("Language", settings->Settings_GetLanguage(true));
        writer->writeEndElement()/*end Settings*/;

        writer->writeEndDocument();
        delete writer;
    }
}

void XmlParser::XmlParser_SaveClientData(QList<QSharedPointer<Client>> clientList)
{
    XmlParser_InitializePath();
    QList<QSharedPointer<Session>> sessionList;
    QList<QSharedPointer<Exercise>> exerciseList;
    QStringList clientItems;
    QStringList exerciseItems;
    QString sessionID;
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML saving error"));
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to save client data."));
    QFile file(mDirPath + "clients.xml");
    if(file.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
        writer->setAutoFormatting(true);
        writer->writeStartDocument();
        writer->writeComment("Limb Range Analyzer - Client list XML file");
        writer->writeStartElement("Clients");

        for(qint32 clientIndex = 0; clientIndex < clientList.count(); clientIndex++)
        {
            clientItems = clientList.at(clientIndex)->Client_GetAllFormatted();
            writer->writeStartElement("Client");
            writer->writeAttribute("ID", clientItems.at(0));
            writer->writeTextElement("FirstName", clientItems.at(1));
            writer->writeTextElement("LastName", clientItems.at(2));
            writer->writeTextElement("Email", clientItems.at(3));

            sessionList = clientList.at(clientIndex)->Client_GetSessionList();
            if(sessionList.count() > 0)
            {
                for(qint32 sessionIndex = 0; sessionIndex < sessionList.count(); sessionIndex++)
                {
                    sessionID = sessionList.at(sessionIndex)->Session_GetID();
                    writer->writeStartElement("Session");
                    writer->writeAttribute("ID", sessionID);

                    exerciseList = sessionList.at(sessionIndex)->Session_GetExerciseList();
                    if(exerciseList.count() > 0)
                    {
                        for(qint32 exerciseIndex = 0; exerciseIndex < exerciseList.count(); exerciseIndex++)
                        {
                            exerciseItems = exerciseList.at(exerciseIndex)->Exercise_GetAllFormatted();
                            writer->writeStartElement("Exercise");
                            writer->writeAttribute("ID", exerciseItems.at(0));
                            writer->writeTextElement("Min", exerciseItems.at(1));
                            writer->writeTextElement("Max", exerciseItems.at(2));
                            writer->writeTextElement("Range", exerciseItems.at(3));
                            writer->writeEndElement()/*end Exercise*/;
                        }
                    }

                    writer->writeEndElement()/*end Session*/;
                }
            }

            writer->writeEndElement()/*end Client*/;
        }

        writer->writeEndElement()/*end Clients*/;
        writer->writeEndDocument();
        delete writer;
    }
    else errMsg.exec();
}
