/**
 * @file        sensormodule.cpp
 * @author      Tomas Bartosik
 * @date        28.07.2021
 * @brief       definition file for motion sensors module
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/sensormodule.hpp"

/*Class definition: ---------------------------------------------------------*/
SensorModule::SensorModule() : mFullHexValue(QString()), mHexValue(QString()),
    mAuxiliary(QString()), mCombinedMin(360), mCombinedMax(-360),
    mConversionOK(true) {}
SensorModule::~SensorModule() {}

void SensorModule::SensorModule_SetSensorValues(const QString &hexValue)
{
    mFullHexValue = hexValue;

    SensorModule_ProcessSensorComponent(mFullHexValue.left(48), DeviceType::ePrimary);

    if(mFullHexValue.length() > 48) SensorModule_ProcessSensorComponent(mFullHexValue.right(48), DeviceType::eSecondary);

    emit SensorModule_SignalProvideResults();
}

void SensorModule::SensorModule_ProcessSensorComponent(const QString &hexValue, DeviceType deviceType)
{
    mHexValue = hexValue;

    if(deviceType == DeviceType::ePrimary) mComponent.SensorComponent_SetComponent(mPrimaryComponent);
    else if(deviceType == DeviceType::eSecondary) mComponent.SensorComponent_SetComponent(mSecondaryComponent);

    mAuxiliary = QString("0x") + mHexValue.mid(6, 2) + mHexValue.mid(4, 2) + mHexValue.mid(2, 2) + mHexValue.mid(0, 2);
    mComponent.mAccelerometerX = static_cast<qint32>(mAuxiliary.toLongLong(&mConversionOK, 16));
    mAuxiliary = QString("0x") + mHexValue.mid(14, 2) + mHexValue.mid(12, 2) + mHexValue.mid(10, 2) + mHexValue.mid(8, 2);
    mComponent.mAccelerometerY = static_cast<qint32>(mAuxiliary.toLongLong(&mConversionOK, 16));
    mAuxiliary = QString("0x") + mHexValue.mid(22, 2) + mHexValue.mid(20, 2) + mHexValue.mid(18, 2) + mHexValue.mid(16, 2);
    mComponent.mAccelerometerZ = static_cast<qint32>(mAuxiliary.toLongLong(&mConversionOK, 16));

    mAuxiliary = QString("0x") + mHexValue.mid(30, 2) + mHexValue.mid(28, 2) + mHexValue.mid(26, 2) + mHexValue.mid(24, 2);
    mComponent.mGyroscopeZ = static_cast<qint32>(mAuxiliary.toLongLong(&mConversionOK, 16));
    mAuxiliary = QString("0x") + mHexValue.mid(38, 2) + mHexValue.mid(36, 2) + mHexValue.mid(34, 2) + mHexValue.mid(32, 2);
    mComponent.mGyroscopeY = static_cast<qint32>(mAuxiliary.toLongLong(&mConversionOK, 16));
    mAuxiliary = QString("0x") + mHexValue.mid(46, 2) + mHexValue.mid(44, 2) + mHexValue.mid(42, 2) + mHexValue.mid(40, 2);
    mComponent.mGyroscopeX = static_cast<qint32>(mAuxiliary.toLongLong(&mConversionOK, 16));

    qreal accX = SensorModule_ConvertAccelerometerValue(mComponent.mAccelerometerX);
    qreal accY = SensorModule_ConvertAccelerometerValue(mComponent.mAccelerometerY);
    qreal accZ = SensorModule_ConvertAccelerometerValue(mComponent.mAccelerometerZ);
    qreal gyroX = SensorModule_ConvertGyroscopeValue(mComponent.mGyroscopeX);
    qreal gyroY = SensorModule_ConvertGyroscopeValue(mComponent.mGyroscopeY);
    qreal gyroZ = SensorModule_ConvertGyroscopeValue(mComponent.mGyroscopeZ);

    mComponent.mRoll = static_cast<qint16>(mComponent.mAccelerometerX / Extensions::sAccToDegs);
    if((accZ > 0 && mComponent.mStartUpsideDown == false) || (accZ < 0 && mComponent.mStartUpsideDown == true))
    {
        if(mComponent.mRoll > 0) mComponent.mRoll = 180 - mComponent.mRoll;
        else mComponent.mRoll = -180 - mComponent.mRoll;
    }
    if(mComponent.mStartUpsideDown == true) mComponent.mRoll *= -1;

    if(mComponent.mRoll == mComponent.mRollPrevOne)
        mComponent.mRoll += ((mComponent.mRollPrevOne - mComponent.mRollPrevTwo) / 2);
    mComponent.mRollPrevTwo = mComponent.mRollPrevOne;
    mComponent.mRollPrevOne = mComponent.mRoll;

    mComponent.mPitch = static_cast<qint16>(mComponent.mAccelerometerY / Extensions::sAccToDegs);
    if((accZ > 0 && mComponent.mStartUpsideDown == false) || (accZ < 0 && mComponent.mStartUpsideDown == true))
    {
        if(mComponent.mPitch > 0) mComponent.mPitch = 180 - mComponent.mPitch;
        else mComponent.mPitch = -180 - mComponent.mPitch;
    }
    if(mComponent.mStartUpsideDown == true) mComponent.mPitch *= -1;

    if(mComponent.mPitch == mComponent.mPitchPrevOne)
        mComponent.mPitch += ((mComponent.mPitchPrevOne - mComponent.mPitchPrevTwo) / 2);
    mComponent.mPitchPrevTwo = mComponent.mPitchPrevOne;
    mComponent.mPitchPrevOne = mComponent.mPitch;

    if(mComponent.mProcessingSelected == ProcessingSelection::eProcessingXY)
    {
        if(mComponent.mRoll < mComponent.mRollMin && mComponent.mAxisSelected != AxisSelection::eUsingPitch)
            mComponent.mRollMin = mComponent.mRoll;
        if(mComponent.mRoll > mComponent.mRollMax && mComponent.mAxisSelected != AxisSelection::eUsingPitch)
            mComponent.mRollMax = mComponent.mRoll;
        if(mComponent.mPitch < mComponent.mPitchMin && mComponent.mAxisSelected != AxisSelection::eUsingRoll)
            mComponent.mPitchMin = mComponent.mPitch;
        if(mComponent.mPitch > mComponent.mPitchMax && mComponent.mAxisSelected != AxisSelection::eUsingRoll)
            mComponent.mPitchMax = mComponent.mPitch;
        if((mComponent.mRollMax - mComponent.mRollMin) > (mComponent.mPitchMax - mComponent.mPitchMin))
        {
            mComponent.mCurrentAngle = mComponent.mRoll;
            mComponent.mMin = mComponent.mRollMin;
            mComponent.mMax = mComponent.mRollMax;
            mComponent.mRange = (mComponent.mRollMax - mComponent.mRollMin);
            if(mComponent.mRange > Extensions::sAxisSwitchThreshold) mComponent.mAxisSelected = AxisSelection::eUsingRoll;
        }
        else
        {
            mComponent.mCurrentAngle = mComponent.mPitch;
            mComponent.mMin = mComponent.mPitchMin;
            mComponent.mMax = mComponent.mPitchMax;
            mComponent.mRange = (mComponent.mPitchMax - mComponent.mPitchMin);
            if(mComponent.mRange > Extensions::sAxisSwitchThreshold) mComponent.mAxisSelected = AxisSelection::eUsingPitch;
        }
    }
    else if(mComponent.mProcessingSelected == ProcessingSelection::eProcessingZ)
    {
        mComponent.SensorComponent_FilterSensorValues(accX, accY, accZ, gyroX, gyroY, gyroZ);
        mComponent.mCurrentAngle = mComponent.mYaw;
        if(mComponent.mYaw < mComponent.mMin) mComponent.mMin = mComponent.mYaw;
        if(mComponent.mYaw > mComponent.mMax) mComponent.mMax = mComponent.mYaw;
        mComponent.mRange = (mComponent.mMax - mComponent.mMin);
    }

    if(deviceType == DeviceType::ePrimary) mPrimaryComponent.SensorComponent_SetComponent(mComponent);
    else if(deviceType == DeviceType::eSecondary) mSecondaryComponent.SensorComponent_SetComponent(mComponent);
}

qreal SensorModule::SensorModule_ConvertAccelerometerValue(qint32 accValue)
{
    return (accValue * Extensions::sAccRange) / Extensions::sIMURangeMax;
}
qreal SensorModule::SensorModule_ConvertGyroscopeValue(qint32 gyroValue)
{
    return (gyroValue * Extensions::sGyroDPS) / Extensions::sIMURangeMax;
}

void SensorModule::SensorModule_SetProcessingStatus(ProcessingSelection processingSelected, DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary)
    {
        if(processingSelected == ProcessingSelection::eDisabled) SensorModule_ResetMeasurement(DeviceType::ePrimary);
        else if(processingSelected == ProcessingSelection::eProcessingXY)
        {
            if(SensorModule_GetAccelerometerZ(deviceType) < 0) mPrimaryComponent.mStartUpsideDown = false;
            else mPrimaryComponent.mStartUpsideDown = true;

            mPrimaryComponent.mAxisSelected = AxisSelection::eUndefined;
        }
        else if(processingSelected == ProcessingSelection::eProcessingZ)
        {
            mPrimaryComponent.mFilterQ0 = 1.0;
            mPrimaryComponent.mFilterQ1 = 0.0;
            mPrimaryComponent.mFilterQ2 = 0.0;
            mPrimaryComponent.mFilterQ3 = 0.0;
            mPrimaryComponent.mYaw = 0;
        }
        mPrimaryComponent.mProcessingSelected = processingSelected;
    }
    else
    {
        if(processingSelected == ProcessingSelection::eDisabled) SensorModule_ResetMeasurement(DeviceType::eSecondary);
        else if(processingSelected == ProcessingSelection::eProcessingXY)
        {
            if(SensorModule_GetAccelerometerZ(deviceType) < 0) mSecondaryComponent.mStartUpsideDown = false;
            else mSecondaryComponent.mStartUpsideDown = true;

            mSecondaryComponent.mAxisSelected = AxisSelection::eUndefined;
        }
        else if(processingSelected == ProcessingSelection::eProcessingZ)
        {
            mSecondaryComponent.mFilterQ0 = 1.0;
            mSecondaryComponent.mFilterQ1 = 0.0;
            mSecondaryComponent.mFilterQ2 = 0.0;
            mSecondaryComponent.mFilterQ3 = 0.0;
            mSecondaryComponent.mYaw = 0;
        }
        mSecondaryComponent.mProcessingSelected = processingSelected;
    }
}

void SensorModule::SensorModule_SetCombinedMin(qint16 combinedMin) {mCombinedMin = combinedMin;}
void SensorModule::SensorModule_SetCombinedMax(qint16 combinedMax) {mCombinedMax = combinedMax;}

void SensorModule::SensorModule_ResetMeasurement(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary)
    {
        mPrimaryComponent.mRollMin = 360;
        mPrimaryComponent.mRollMax = -360;
        mPrimaryComponent.mPitchMin = 360;
        mPrimaryComponent.mPitchMax = -360;
        mPrimaryComponent.mMin = 360;
        mPrimaryComponent.mMax = -360;
        mPrimaryComponent.mRange = 0;
        mPrimaryComponent.mCurrentAngle = 0;
    }
    else
    {
        mSecondaryComponent.mRollMin = 360;
        mSecondaryComponent.mRollMax = -360;
        mSecondaryComponent.mPitchMin = 360;
        mSecondaryComponent.mPitchMax = -360;
        mSecondaryComponent.mMin = 360;
        mSecondaryComponent.mMax = -360;
        mSecondaryComponent.mRange = 0;
        mSecondaryComponent.mCurrentAngle = 0;
    }

    mCombinedMax = -360;
    mCombinedMin = 360;
}

qint32 SensorModule::SensorModule_GetAccelerometerX(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mAccelerometerX;
    else return mSecondaryComponent.mAccelerometerX;
}

qint32 SensorModule::SensorModule_GetAccelerometerY(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mAccelerometerY;
    else return mSecondaryComponent.mAccelerometerY;
}

qint32 SensorModule::SensorModule_GetAccelerometerZ(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mAccelerometerZ;
    else return mSecondaryComponent.mAccelerometerZ;
}

qint32 SensorModule::SensorModule_GetGyroscopeX(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mGyroscopeX;
    else return mSecondaryComponent.mGyroscopeX;
}

qint32 SensorModule::SensorModule_GetGyroscopeY(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mGyroscopeY;
    else return mSecondaryComponent.mGyroscopeY;
}

qint32 SensorModule::SensorModule_GetGyroscopeZ(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mGyroscopeZ;
    else return mSecondaryComponent.mGyroscopeZ;
}

qint16 SensorModule::SensorModule_GetCurrentRoll(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mRoll;
    else return mSecondaryComponent.mRoll;
}

qint16 SensorModule::SensorModule_GetCurrentPitch(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mPitch;
    else return mSecondaryComponent.mPitch;
}

qint16 SensorModule::SensorModule_GetCurrentYaw(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mYaw;
    else return mSecondaryComponent.mYaw;
}

qint16 SensorModule::SensorModule_GetCurrentMin(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mMin;
    else return mSecondaryComponent.mMin;
}

qint16 SensorModule::SensorModule_GetCurrentMax(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mMax;
    else return mSecondaryComponent.mMax;
}

qint16 SensorModule::SensorModule_GetCurrentRange(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mRange;
    else return mSecondaryComponent.mRange;
}

qint16 SensorModule::SensorModule_GetCurrentAngle(DeviceType deviceType)
{
    if(deviceType == DeviceType::ePrimary) return mPrimaryComponent.mCurrentAngle;
    else return mSecondaryComponent.mCurrentAngle;
}

qint16 SensorModule::SensorModule_GetCombinedMin() {return mCombinedMin;}
qint16 SensorModule::SensorModule_GetCombinedMax() {return mCombinedMax;}
