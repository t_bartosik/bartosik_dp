/**
 * @file        session.cpp
 * @author      Tomas Bartosik
 * @date        01.03.2022
 * @brief       definition file for session informations class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/session.hpp"

/*Class definition: ---------------------------------------------------------*/
Session::Session() : mID(QString()), mExerciseList(QList<QSharedPointer<Exercise>>()) {}
Session::Session(QString id, QList<QSharedPointer<Exercise>> exerciseList): mID(id),
    mExerciseList(exerciseList) {}
Session::~Session() {mExerciseList.clear();}

QString& Session::Session_GetID() {return mID;}
QList<QSharedPointer<Exercise>>& Session::Session_GetExerciseList() {return mExerciseList;}

void Session::Session_AddExercise(QSharedPointer<Exercise> exercise)
{
    QString exerciseIndex = exercise->Exercise_GetID();

    if(mExerciseList.isEmpty() == true) mExerciseList.append(exercise);
    else
    {
        for(qint32 index = 0; index < mExerciseList.length(); index++)
        {
            if(QString::compare(mExerciseList.at(index)->Exercise_GetID(), exerciseIndex, Qt::CaseSensitive) == 0)
            {
                mExerciseList[index] = exercise;
                return;
            }
        }
        mExerciseList.append(exercise);
    }
}
