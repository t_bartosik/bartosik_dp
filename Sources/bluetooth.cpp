/**
 * @file        bluetooth.cpp
 * @author      Tomas Bartosik
 * @date        21.07.2021
 * @brief       definition file for Bluetooth connection class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/bluetooth.hpp"

/*Class definition: ---------------------------------------------------------*/
Bluetooth::Bluetooth() : mDiscoveryAgent(nullptr), mPrimaryController(0),
    mSecondaryController(0),mPrimaryService(0), mSecondaryService(0), mDevices(QList<QObject*> ()),
    mServices(QList<QObject*> ()), mDevicesList(QStringList()), mAddressList(QStringList()),
    mPrimaryConnectedAddress(QString()), mSecondaryConnectedAddress(QString()),
    mLatestDeviceAddress(QString()), mPrimaryHighLevelServiceUUID(QString()),
    mSecondaryHighLevelServiceUUID(QString()), mBluetoothDiscoveryTimer(nullptr),
    mBluetoothRetrieveTimer(nullptr), mServiceRetrievalSpeed(3000),
    mScanningCompleted(false), mEnableDebugInformations(false)
{
    mDiscoveryAgent = new QBluetoothDeviceDiscoveryAgent(this);
    mDiscoveryAgent->setLowEnergyDiscoveryTimeout(10000);

    mBluetoothDiscoveryTimer = new QTimer(this);
    mBluetoothRetrieveTimer = new QTimer(this);

    /*BLE services UUID setting: -------------------------------------*/
    mHighLevelUUIDExpression.setPattern("[1-9]0000000-0001-11e1-9ab4-0002a5d5c51b");
    mHighLevelUUIDExpression.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
    mBaseLowLevelServiceUUID = QString("0140000-0001-11e1-ac36-0002a5d5c51b");

    connect(mDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered, this, &Bluetooth::Bluetooth_AddDevice);
    connect(mDiscoveryAgent, QOverload<QBluetoothDeviceDiscoveryAgent::Error>::of(&QBluetoothDeviceDiscoveryAgent::error), this, &Bluetooth::Bluetooth_CatchError);
    connect(mDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::finished, this, &Bluetooth::Bluetooth_StopScanning);
    connect(mBluetoothDiscoveryTimer, &QTimer::timeout, this, &Bluetooth::Bluetooth_ReportStatus);
    connect(mBluetoothRetrieveTimer, &QTimer::timeout, this, &Bluetooth::Bluetooth_RetrieveSensorData);
}

Bluetooth::~Bluetooth()
{
    delete mDiscoveryAgent;
    delete mPrimaryController;
    delete mSecondaryController;
    delete mPrimaryService;
    delete mSecondaryService;
    delete mBluetoothDiscoveryTimer;
    delete mBluetoothRetrieveTimer;
    qDeleteAll(mDevices);
    qDeleteAll(mServices);
    mDevices.clear();
    mServices.clear();
}

void Bluetooth::Bluetooth_StartScanning()
{
    qDeleteAll(mDevices);
    mDevices.clear();
    mDevicesList.clear();
    mAddressList.clear();

    mDiscoveryAgent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);
    if(mDiscoveryAgent->isActive())
    {
        if(mEnableDebugInformations) qDebug() << tr("Scanning for devices started...");
        mBluetoothDiscoveryTimer->start(1000);
    }
    else if(mEnableDebugInformations) qDebug() << tr("Scanning could not be started.");
}

bool Bluetooth::Bluetooth_Connect(const QString &deviceAddress)
{
    if(Bluetooth_GetConnectedDevicesCount() > 1)
    {
        emit Bluetooth_SignalDeviceBeyondLimit();
        return false;
    }

    BluetoothDevice bleDevice;
    qint32 deviceIndex = 0;
    for(int index = 0; index < mDevices.count(); index++)
    {
        if(QString::compare(((BluetoothDevice*)mDevices.at(index))->BluetoothDevice_GetAddress(), deviceAddress, Qt::CaseSensitive) == 0)
        {
            bleDevice.BluetoothDevice_SetDevice(((BluetoothDevice*)mDevices.at(index))->BluetoothDevice_GetDevice());
            deviceIndex = index;
        }
    }

    if(bleDevice.BluetoothDevice_GetDevice().isValid())
    {
        bool endWay = true;
        ControllerType controllerType = ControllerType::ePrimary;
        mScanningCompleted = false;

        if(!mPrimaryController)
        {
            /*Primary device ready to connect: ------------------------------------------*/
            controllerType = ControllerType::ePrimary;
            mPrimaryBluetoothDevice.BluetoothDevice_SetDevice(((BluetoothDevice*)mDevices.at(deviceIndex))->BluetoothDevice_GetDevice());
            mPrimaryController = new QLowEnergyController(mPrimaryBluetoothDevice.BluetoothDevice_GetDevice());
            mPrimaryController->setRemoteAddressType(QLowEnergyController::PublicAddress);

            connect(mPrimaryController, &QLowEnergyController::connected, this, [this]{Bluetooth::Bluetooth_DeviceConnected(ControllerType::ePrimary);});
            connect(mPrimaryController, QOverload<QLowEnergyController::Error>::of(&QLowEnergyController::error), this, [this]{Bluetooth::Bluetooth_DeviceHasError(ControllerType::ePrimary);});
            connect(mPrimaryController, &QLowEnergyController::disconnected, this, &Bluetooth::Bluetooth_DeviceDisconnected);
            connect(mPrimaryController, &QLowEnergyController::serviceDiscovered, this, [this](const QBluetoothUuid &uuid){Bluetooth::Bluetooth_RegisterService(ControllerType::ePrimary, uuid);});
            connect(mPrimaryController, &QLowEnergyController::discoveryFinished, this, &Bluetooth::Bluetooth_ServicesRegistered);
        }
        else if(!mSecondaryController && mPrimaryController && mLatestDeviceAddress != bleDevice.BluetoothDevice_GetAddress())
        {
            /*Connect secondary device: ---------------------------------------*/
            controllerType = ControllerType::eSecondary;
            mSecondaryBluetoothDevice.BluetoothDevice_SetDevice(((BluetoothDevice*)mDevices.at(deviceIndex))->BluetoothDevice_GetDevice());
            mSecondaryController = new QLowEnergyController(mSecondaryBluetoothDevice.BluetoothDevice_GetDevice());
            mSecondaryController->setRemoteAddressType(QLowEnergyController::PublicAddress);

            connect(mSecondaryController, &QLowEnergyController::connected, this, [this]{Bluetooth::Bluetooth_DeviceConnected(ControllerType::eSecondary);});
            connect(mSecondaryController, QOverload<QLowEnergyController::Error>::of(&QLowEnergyController::error), this, [this]{Bluetooth::Bluetooth_DeviceHasError(ControllerType::eSecondary);});
            connect(mSecondaryController, &QLowEnergyController::disconnected, this, &Bluetooth::Bluetooth_DeviceDisconnected);
            connect(mSecondaryController, &QLowEnergyController::serviceDiscovered, this, [this](const QBluetoothUuid &uuid){Bluetooth::Bluetooth_RegisterService(ControllerType::eSecondary, uuid);});
            connect(mSecondaryController, &QLowEnergyController::discoveryFinished, this, &Bluetooth::Bluetooth_ServicesRegistered);
        }

        mLatestDeviceAddress = bleDevice.BluetoothDevice_GetAddress();

        /*Break connection process in case of unreachable device: -----------*/
        QTimer::singleShot(20000, this, [&]()
        {
            if(mScanningCompleted == false)
            {
                endWay = false;
                Bluetooth_Disconnect(mLatestDeviceAddress);
                emit Bluetooth_SignalDeviceUnreachable();
            }
        });

        if(controllerType == ControllerType::ePrimary) mPrimaryController->connectToDevice();
        else mSecondaryController->connectToDevice();

        if(endWay == true)
        {
            mScanningCompleted = true;
            if(controllerType == ControllerType::ePrimary) mPrimaryConnectedAddress = mLatestDeviceAddress;
            else
            {
                mSecondaryConnectedAddress = mLatestDeviceAddress;
            }

            if(mEnableDebugInformations)
            {
                qDebug() << tr("Primary connected address: %1    Secondary connected address: %2")
                            .arg(mPrimaryConnectedAddress, mSecondaryConnectedAddress);
            }
            emit Bluetooth_SignalDeviceConnected();
        }

        return true;
    }
    else return false;

    return false;
}

void Bluetooth::Bluetooth_Disconnect(const QString &deviceAddress)
{
    QString reconnectionAddress = QString();
    if(Bluetooth_GetConnectedDevicesCount() > 1)
    {
        if(QString::compare(mPrimaryConnectedAddress, deviceAddress, Qt::CaseSensitive) == 0) reconnectionAddress = mSecondaryConnectedAddress;
        else reconnectionAddress = mPrimaryConnectedAddress;
    }

    if(mPrimaryController != 0)
    {
        if(mPrimaryController->state() != QLowEnergyController::UnconnectedState)
        {
            mPrimaryController->disconnectFromDevice();
            mPrimaryController = 0;
            mPrimaryService = 0;
        }
        else Bluetooth_DeviceDisconnected();
    }

    if(mSecondaryController != 0)
    {
        if(mSecondaryController->state() != QLowEnergyController::UnconnectedState)
        {
            mSecondaryController->disconnectFromDevice();
            mSecondaryController = 0;
            mSecondaryService = 0;
        }
    }

    mServices.clear();
    mPrimaryConnectedAddress = QString();
    mSecondaryConnectedAddress = QString();
    mLatestDeviceAddress = QString();

    if(QString::compare(reconnectionAddress, QString(), Qt::CaseSensitive) != 0) Bluetooth_Connect(reconnectionAddress);
}

bool Bluetooth::Bluetooth_IsDeviceConnected(const QString &deviceAddress)
{
    if(QString::compare(mPrimaryConnectedAddress, deviceAddress, Qt::CaseSensitive) == 0) return true;
    else if(QString::compare(mSecondaryConnectedAddress, deviceAddress, Qt::CaseSensitive) == 0) return true;
    else return false;
}

void Bluetooth::Bluetooth_RetrieveSensorData()
{
    QString hexValue = QString();
    if(mPrimaryService)
    {
        /*First device data ready: ------------------------------------------*/
        const auto &characteristic = mPrimaryService->characteristic(QBluetoothUuid(mPrimaryLowLevelServiceUUID));
        if(characteristic.isValid())
        {
            mPrimaryService->readCharacteristic(characteristic);
            hexValue += characteristic.value().toHex();
        }
    }

    if(mSecondaryService)
    {
        /*Second device data ready: -----------------------------------------*/
        const auto &characteristic = mSecondaryService->characteristic(QBluetoothUuid(mSecondaryLowLevelServiceUUID));
        if(characteristic.isValid())
        {
            mSecondaryService->readCharacteristic(characteristic);
            hexValue += characteristic.value().toHex();
        }
    }

    if(QString::compare(hexValue, QString(), Qt::CaseSensitive) != 0) emit Bluetooth_SignalSensorDataReady(hexValue);
}

void Bluetooth::Bluetooth_StopRetrievingSensorData()
{
    if(mBluetoothRetrieveTimer->isActive()) mBluetoothRetrieveTimer->stop();
}

void Bluetooth::Bluetooth_SetServiceRetrievalSpeed(qint16 serviceRetrievalSpeed) {mBluetoothRetrieveTimer->setInterval(serviceRetrievalSpeed);}
void Bluetooth::Bluetooth_SetDebugInformations(bool debugInformations) {mEnableDebugInformations = debugInformations;}

QStringList Bluetooth::Bluetooth_GetDevicesList() {return mDevicesList;}
QStringList Bluetooth::Bluetooth_GetAddressList() {return mAddressList;}

quint16 Bluetooth::Bluetooth_GetConnectedDevicesCount()
{
    quint16 count = 0;
    if(QString::compare(mPrimaryConnectedAddress, QString(), Qt::CaseSensitive) != 0) count++;
    if(QString::compare(mSecondaryConnectedAddress, QString(), Qt::CaseSensitive) != 0) count++;
    return count;
}

void Bluetooth::Bluetooth_AddDevice(const QBluetoothDeviceInfo &device)
{
    if(device.coreConfigurations() & QBluetoothDeviceInfo::LowEnergyCoreConfiguration)
    {
        BluetoothDevice *bleDevice = new BluetoothDevice(device);
        mDevices.append(bleDevice);
        mDevicesList.append(bleDevice->BluetoothDevice_GetName());
        mAddressList.append(bleDevice->BluetoothDevice_GetAddress());
    }
}

void Bluetooth::Bluetooth_CatchError(QBluetoothDeviceDiscoveryAgent::Error error)
{
    mBluetoothDiscoveryTimer->stop();

    if(error == QBluetoothDeviceDiscoveryAgent::PoweredOffError)
    {
        emit Bluetooth_SignalConnectionError(tr("Bluetooth is turned off. Turn it on first."));
    }
    else if(error == QBluetoothDeviceDiscoveryAgent::InputOutputError)
    {
        emit Bluetooth_SignalConnectionError(tr("Writing or reading from connected device resulted in an error."));
    }
    else if(error == QBluetoothDeviceDiscoveryAgent::UnsupportedPlatformError)
    {
        emit Bluetooth_SignalConnectionError(tr("This platform does not support BLE connection."));
    }
    else if(error == QBluetoothDeviceDiscoveryAgent::UnsupportedDiscoveryMethod)
    {
        emit Bluetooth_SignalConnectionError(tr("This platform does not support BLE device discovery method."));
    }
    else emit Bluetooth_SignalConnectionError(tr("An unexpected error has occurred."));
}

void Bluetooth::Bluetooth_ReportStatus() {emit Bluetooth_SignalStatusReady();}

void Bluetooth::Bluetooth_StopScanning()
{
    if(mDevices.isEmpty() && mEnableDebugInformations) qDebug() << tr("Scanning completed, no BLE devices found.");
    else if(mEnableDebugInformations) qDebug() << tr("Scanning completed, found %1 device(s).").arg(mDevices.count());

    mBluetoothDiscoveryTimer->stop();
    emit Bluetooth_SignalDevicesReady();
}

void Bluetooth::Bluetooth_DeviceConnected(ControllerType controllerType)
{
    if(controllerType == ControllerType::ePrimary)
    {
        if(mEnableDebugInformations) qDebug() << tr("Controller: Primary device connected.");

        mPrimaryController->discoverServices();
    }
    else
    {
        if(mEnableDebugInformations) qDebug() << tr("Controller: Secondary device connected.");

        mSecondaryController->discoverServices();
    }
}

void Bluetooth::Bluetooth_DeviceHasError(ControllerType controllerType)
{
    if(controllerType == ControllerType::ePrimary && mEnableDebugInformations)
        qDebug() << tr("Controller: Primary device has error: %1").arg(mPrimaryController->errorString());
    else if(mEnableDebugInformations)
        qDebug() << tr("Controller: Secondary device has error: %1").arg(mSecondaryController->errorString());
}

void Bluetooth::Bluetooth_DeviceDisconnected()
{
    if(mEnableDebugInformations) qDebug() << tr("Controller: Device disconnected.");
    emit Bluetooth_SignalDisconnected();
}

void Bluetooth::Bluetooth_RegisterService(ControllerType controllerType, const QBluetoothUuid &uuid)
{
    QLowEnergyService *service;
    if(controllerType == ControllerType::ePrimary) service = mPrimaryController->createServiceObject(uuid);
    else service = mSecondaryController->createServiceObject(uuid);

    if(!service && mEnableDebugInformations) qDebug() << tr("Cannot create service object from received UUID.");
    else
    {
        BluetoothService *bleService = new BluetoothService(service);
        mServices.append(bleService);
    }
}

void Bluetooth::Bluetooth_ServicesRegistered()
{
    if(mEnableDebugInformations) qDebug() << tr("Done services registering.");
    if(mServices.isEmpty())
    {
        if(mEnableDebugInformations) qDebug() << tr("No services available.");
        return;
    }

    QLowEnergyService *primaryService = nullptr;
    QLowEnergyService *secondaryService = nullptr;
    for(int index = 0; index < mServices.count(); index++)
    {
        BluetoothService *bleService = (BluetoothService*)mServices.at(index);
        if(mHighLevelUUIDExpression.match(bleService->BluetoothService_GetUUID()).hasMatch())
        {
            if(!mPrimaryService)
            {
                if(mEnableDebugInformations) qDebug() << tr("Needed service found (primary device): %1").arg(bleService->BluetoothService_GetUUID());
                primaryService = bleService->BluetoothService_GetService();
                mPrimaryService = primaryService;
                mPrimaryHighLevelServiceUUID = bleService->BluetoothService_GetUUID();
                mPrimaryLowLevelServiceUUID = mPrimaryHighLevelServiceUUID.front() + mBaseLowLevelServiceUUID;
            }
            else if(!mSecondaryService && QString::compare(mPrimaryHighLevelServiceUUID, bleService->BluetoothService_GetUUID(), Qt::CaseInsensitive) != 0)
            {
                if(mEnableDebugInformations) qDebug() << tr("Needed service found (secondary device): %1").arg(bleService->BluetoothService_GetUUID());
                secondaryService = bleService->BluetoothService_GetService();
                mSecondaryService = secondaryService;
                mSecondaryHighLevelServiceUUID = bleService->BluetoothService_GetUUID();
                mSecondaryLowLevelServiceUUID = mSecondaryHighLevelServiceUUID.front() + mBaseLowLevelServiceUUID;
            }
        }
    }

    if(primaryService)
    {
        bool containsNeededService = false;
        if(primaryService->state() == QLowEnergyService::DiscoveryRequired)
        {
            if(mEnableDebugInformations) qDebug() << tr("Discovering characteristics (primary device).");
            connect(primaryService, &QLowEnergyService::stateChanged, this, [this](QLowEnergyService::ServiceState newState)
                { Bluetooth::Bluetooth_ServiceDetailsObtained(1, newState); });
            primaryService->discoverDetails();
            return;
        }

        if(mEnableDebugInformations) qDebug() << tr("Characteristics discovered before (primary device).");
        const QList<QLowEnergyCharacteristic> characteristics = primaryService->characteristics();
        foreach(const QLowEnergyCharacteristic &ch, characteristics)
        {
            BluetoothInfo *info = new BluetoothInfo(ch);
            if(QString::compare(info->BluetoothInfo_GetUUID(), mPrimaryLowLevelServiceUUID, Qt::CaseInsensitive) == 0)
            {
                if(mEnableDebugInformations) qDebug() << tr("Sensor fetched value (primary device): %1").arg(info->BluetoothInfo_GetValue());
                containsNeededService = true;
            }
        }
        if(containsNeededService == false)
        {
            Bluetooth_Disconnect(mLatestDeviceAddress);
            emit Bluetooth_SignalDeviceIncompatible();
        }
    }
    if(secondaryService)
    {
        bool containsNeededService = false;
        if(secondaryService->state() == QLowEnergyService::DiscoveryRequired)
        {
            if(mEnableDebugInformations) qDebug() << tr("Discovering characteristics (secondary device).");
            connect(secondaryService, &QLowEnergyService::stateChanged, this, [this](QLowEnergyService::ServiceState newState)
                { Bluetooth::Bluetooth_ServiceDetailsObtained(2, newState); });
            secondaryService->discoverDetails();
            return;
        }

        if(mEnableDebugInformations) qDebug() << tr("Characteristics discovered before (secondary device).");
        const QList<QLowEnergyCharacteristic> characteristics = secondaryService->characteristics();
        foreach(const QLowEnergyCharacteristic &ch, characteristics)
        {
            BluetoothInfo *info = new BluetoothInfo(ch);
            if(QString::compare(info->BluetoothInfo_GetUUID(), mSecondaryLowLevelServiceUUID, Qt::CaseInsensitive) == 0)
            {
                if(mEnableDebugInformations) qDebug() << tr("Sensor fetched value (secondary device): %1").arg(info->BluetoothInfo_GetValue());
                containsNeededService = true;
            }
        }
        if(containsNeededService == false)
        {
            Bluetooth_Disconnect(mLatestDeviceAddress);
            emit Bluetooth_SignalDeviceIncompatible();
        }
    }
    if(!primaryService && !secondaryService)
    {
        Bluetooth_Disconnect(mLatestDeviceAddress);
        emit Bluetooth_SignalDeviceIncompatible();
    }
}

void Bluetooth::Bluetooth_ServiceDetailsObtained(qint16 serviceID, QLowEnergyService::ServiceState newState)
{
    bool containsNeededService = false;
    QString lowLevelServiceUUID;
    if(serviceID == 1) lowLevelServiceUUID = mPrimaryLowLevelServiceUUID;
    else lowLevelServiceUUID = mSecondaryLowLevelServiceUUID;

    if(mEnableDebugInformations) qDebug() << tr("Obtaining service details for low level UUID: %1").arg(lowLevelServiceUUID);

    if(newState != QLowEnergyService::ServiceDiscovered)
    {
        if(newState != QLowEnergyService::DiscoveringServices)
        {
            QMetaObject::invokeMethod(this, "Bluetooth_SignalCharacteristicsUpdated", Qt::QueuedConnection);
        }
        return;
    }

    QLowEnergyService *service = qobject_cast<QLowEnergyService*>(sender());
    if(service)
    {
        const QList<QLowEnergyCharacteristic> characteristics = service->characteristics();
        foreach(const QLowEnergyCharacteristic &ch, characteristics)
        {
            BluetoothInfo *info = new BluetoothInfo(ch);
            if(QString::compare(info->BluetoothInfo_GetUUID(), lowLevelServiceUUID, Qt::CaseInsensitive) == 0)
            {
                containsNeededService = true;
                if(mBluetoothRetrieveTimer->isActive() == false)
                {
                    if(mEnableDebugInformations) qDebug() << tr("Needed low service found, starting retrieval timer.");
                    mBluetoothRetrieveTimer->start(mServiceRetrievalSpeed);
                }
            }
        }
        if(containsNeededService == false)
        {
            Bluetooth_Disconnect(mLatestDeviceAddress);
            emit Bluetooth_SignalDeviceIncompatible();
        }
    }
}
