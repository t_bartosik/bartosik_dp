/**
 * @file        sensorcomponent.cpp
 * @author      Tomas Bartosik
 * @date        15.12.2021
 * @brief       definition file for motion sensors component
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/sensorcomponent.hpp"

/*Structure definition: -----------------------------------------------------*/
SensorComponent::SensorComponent() : mAccelerometerX(0), mAccelerometerY(0), mAccelerometerZ(0),
mGyroscopeX(0), mGyroscopeY(0), mGyroscopeZ(0), mRoll(0), mRollPrevOne(0), mRollPrevTwo(0),
mRollMin(360), mRollMax(-360), mPitch(0), mPitchPrevOne(0), mPitchPrevTwo(0), mPitchMin(360),
mPitchMax(-360), mYaw(0), mMin(360), mMax(-360), mRange(0), mCurrentAngle(0), mFilterQ0(1.0),
mFilterQ1(0.0), mFilterQ2(0.0), mFilterQ3(0.0), mStartUpsideDown(false), mAxisSelected(AxisSelection::eUndefined),
mProcessingSelected(ProcessingSelection::eDisabled) {}
SensorComponent::~SensorComponent() {}

void SensorComponent::SensorComponent_SetComponent(const SensorComponent &component)
{
    mAccelerometerX = component.mAccelerometerX;
    mAccelerometerY = component.mAccelerometerY;
    mAccelerometerZ = component.mAccelerometerZ;
    mGyroscopeX = component.mGyroscopeX;
    mGyroscopeY = component.mGyroscopeY;
    mGyroscopeZ = component.mGyroscopeZ;
    mRoll = component.mRoll;
    mRollPrevOne = component.mRollPrevOne;
    mRollPrevTwo = component.mRollPrevTwo;
    mRollMin = component.mRollMin;
    mRollMax = component.mRollMax;
    mPitch = component.mPitch;
    mPitchPrevOne = component.mPitchPrevOne;
    mPitchPrevTwo = component.mPitchPrevTwo;
    mPitchMin = component.mPitchMin;
    mPitchMax = component.mPitchMax;
    mYaw = component.mYaw;
    mMin = component.mMin;
    mMax = component.mMax;
    mRange = component.mRange;
    mCurrentAngle = component.mCurrentAngle;
    mFilterQ0 = component.mFilterQ0;
    mFilterQ1 = component.mFilterQ1;
    mFilterQ2 = component.mFilterQ2;
    mFilterQ3 = component.mFilterQ3;
    mProcessingSelected = component.mProcessingSelected;
    mStartUpsideDown = component.mStartUpsideDown;
    mAxisSelected = component.mAxisSelected;
}

void SensorComponent::SensorComponent_FilterSensorValues(qreal accX, qreal accY, qreal accZ, qreal gyroX, qreal gyroY, qreal gyroZ)
{
    qreal normalizationBase;
    qreal s0, s1, s2, s3;
    qreal qDot1, qDot2, qDot3, qDot4;
    qreal q0Times2, q1Times2, q2Times2, q3Times2, q0Times4, q1Times4, q2Times4 ,q1Times8, q2Times8, q0q0, q1q1, q2q2, q3q3;
    qreal pureYaw = 0.0;

    /*Gyroscope conversion from DEGs to RADs: -------------------------------*/
    gyroX *= Extensions::sDegsToRads;
    gyroY *= Extensions::sDegsToRads;
    gyroZ *= Extensions::sDegsToRads;

    /*Calculating rate of change via gyroscope: -----------------------------*/
    qDot1 = 0.5 * (-mFilterQ1 * gyroX - mFilterQ2 * gyroY - mFilterQ3 * gyroZ);
    qDot2 = 0.5 * (mFilterQ0 * gyroX + mFilterQ2 * gyroZ - mFilterQ3 * gyroY);
    qDot3 = 0.5 * (mFilterQ0 * gyroY - mFilterQ1 * gyroZ + mFilterQ3 * gyroX);
    qDot4 = 0.5 * (mFilterQ0 * gyroZ + mFilterQ1 * gyroY - mFilterQ2 * gyroX);

    if(!((accX == 0.0) && (accY == 0.0) && (accZ == 0.0)))
    {
        /*Accelerometer normalization (using inverse square root): ----------*/
        normalizationBase = 1.0 / qSqrt(accX * accX + accY * accY + accZ * accZ);
        accX *= normalizationBase;
        accY *= normalizationBase;
        accZ *= normalizationBase;

        q0Times2 = 2.0 * mFilterQ0;
        q1Times2 = 2.0 * mFilterQ1;
        q2Times2 = 2.0* mFilterQ2;
        q3Times2 = 2.0 * mFilterQ3;
        q0Times4 = 4.0 * mFilterQ0;
        q1Times4 = 4.0 * mFilterQ1;
        q2Times4 = 4.0 * mFilterQ2;
        q1Times8 = 8.0 * mFilterQ1;
        q2Times8 = 8.0 * mFilterQ2;
        q0q0 = mFilterQ0 * mFilterQ0;
        q1q1 = mFilterQ1 * mFilterQ1;
        q2q2 = mFilterQ2 * mFilterQ2;
        q3q3 = mFilterQ3 * mFilterQ3;

        /*Creating sensor frames ~ gradient descent algorithm: --------------*/
        s0 = q0Times4 * q2q2 + q2Times2 * accX + q0Times4 * q1q1 - q1Times2 * accY;

        s1 = q1Times4 * q3q3 - q3Times2 * accX + 4.0 * q0q0 * mFilterQ1 - q0Times2 * accY - q1Times4
                + q1Times8 * q1q1 + q1Times8 * q2q2 + q1Times4 * accZ;

        s2 = 4.0 * q0q0 * mFilterQ2 + q0Times2 * accX + q2Times4 * q3q3 - q3Times2 * accY - q2Times4
                + q2Times8 * q1q1 + q2Times8 * q2q2 + q2Times4 * accZ;

        s3 = 4.0 * q1q1 * mFilterQ3 - q1Times2 * accX + 4.0 * q2q2 * mFilterQ3 - q2Times2 * accY;

        /*Sensor frames normalization: --------------------------------------*/
        normalizationBase = 1.0 / qSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3);
        s0 *= normalizationBase;
        s1 *= normalizationBase;
        s2 *= normalizationBase;
        s3 *= normalizationBase;

        /*Modify the rate of change: ----------------------------------------*/
        qDot1 -= Extensions::sFilterBeta * s0;
        qDot2 -= Extensions::sFilterBeta * s1;
        qDot3 -= Extensions::sFilterBeta * s2;
        qDot4 -= Extensions::sFilterBeta * s3;
    }

    /*Rate of change integration using defined frequency: -------------------*/
    mFilterQ0 += qDot1 * Extensions::sFilterSampleFrequency;
    mFilterQ1 += qDot2 * Extensions::sFilterSampleFrequency;
    mFilterQ2 += qDot3 * Extensions::sFilterSampleFrequency;
    mFilterQ3 += qDot4 * Extensions::sFilterSampleFrequency;

    /*Quaternion normalization: ---------------------------------------------*/
    normalizationBase = 1.0 / qSqrt(mFilterQ0 * mFilterQ0 + mFilterQ1 * mFilterQ1 + mFilterQ2 * mFilterQ2 + mFilterQ3 * mFilterQ3);
    mFilterQ0 *= normalizationBase;
    mFilterQ1 *= normalizationBase;
    mFilterQ2 *= normalizationBase;
    mFilterQ3 *= normalizationBase;

    pureYaw = qAtan2(mFilterQ1*mFilterQ2 + mFilterQ0*mFilterQ3, 0.5 - mFilterQ2*mFilterQ2 - mFilterQ3*mFilterQ3);
    mYaw = static_cast<qint16>(pureYaw * Extensions::sRadsToDegs * 10);
}
