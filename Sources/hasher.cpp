/**
 * @file        hasher.cpp
 * @author      Tomas Bartosik
 * @date        01.03.2022
 * @brief       definition file of Hasher class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/hasher.hpp"

/*Class definition: ---------------------------------------------------------*/
Hasher::Hasher() : mHash(QString()) {}
Hasher::~Hasher() {}

QString& Hasher::Hasher_CalculateHash(QString dataString)
{
    QByteArray hash = QCryptographicHash::hash(dataString.toUtf8(), QCryptographicHash::Md5);
    mHash = QString(hash.toHex());
    return mHash;
}
