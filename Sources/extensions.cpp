/**
 * @file        extensions.cpp
 * @author      Tomas Bartosik
 * @date        25.03.2022
 * @brief       definition file for static data extension structure
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/extensions.hpp"

/*Struct definition: --------------------------------------------------------*/
qreal Extensions::sFilterSampleFrequency = (1.0 / 1060.0);     /*-- frequency modifier (base frequency 720 Hz) --*/
qreal Extensions::sFilterBeta = 0.1;                           /*-- filter conversion value (proportional gain) --*/
qreal Extensions::sIMURangeMax = 32768.0;                      /*-- accelerometer/gyroscope max value in range --*/
qreal Extensions::sAccToDegs = 10.2;                           /*-- accelerometer range values conversion multiplier --*/
qreal Extensions::sAccRange = 2.0;                             /*-- using 2g range for accelerometer --*/
qreal Extensions::sGyroDPS = 250.0;                            /*-- using 250 degrees per second for gyroscope --*/
qreal Extensions::sRadsToDegs = 114.6;                         /*-- conversion multiplier: modified RAD/s to DEG/s --*/
qreal Extensions::sDegsToRads = 0.0174533;                     /*-- conversion multiplier: modified DEG/s to RAD/s --*/

qint32 Extensions::sNoExerciseSelected = -1;                   /*-- a human-readable shortcut for empty selection --*/
quint16 Extensions::sAxisSwitchThreshold = 60;                 /*-- switch threshold between using ROLL or PITCH in degrees --*/
quint16 Extensions::sMovementOrbsCount = 4;                    /*-- number of semi-transparent orbs created for limb movement tracking --*/

quint16 Extensions::sClientTableColumns = 3;                   /*-- a human-readable definition of client table columns --*/
quint16 Extensions::sHistoryTableColumns = 4;                  /*-- a human-readable definition of history table columns --*/

