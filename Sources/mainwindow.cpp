/**
 * @file        mainwindow.cpp
 * @author      Tomas Bartosik
 * @date        21.07.2021
 * @brief       definition file for MainWindow
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/mainwindow.hpp"
#include "../Headers/extensions.hpp"
#include "ui_mainwindow.h"

/*Class definition: ---------------------------------------------------------*/
MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), mUserInterface(new Ui::MainWindow),
    mGraphicsView(new Qt3DExtras::Qt3DWindow), mGraphicsRootEntity(nullptr), mGraphicsContainer(nullptr),
    mAnimation(nullptr), mUpperLeftArmTransform(nullptr), mUpperRightArmTransform(nullptr),
    mLowerLeftArmTransform(nullptr), mLowerRightArmTransform(nullptr), mLeftThighTransform(nullptr),
    mRightThighTransform(nullptr), mLeftCalfTransform(nullptr), mRightCalfTransform(nullptr),
    mPrimaryBlueCoinTransform(nullptr), mSecondaryBlueCoinTransform(nullptr), mMinRangeTransform(nullptr),
    mMaxRangeTransform(nullptr), mMovementOrbsTransform(QList<Qt3DCore::QTransform*> ()),
    mRangeMaterial(nullptr), mBluetooth(nullptr), mSensorModule(nullptr), mSettings(QSharedPointer<Settings> ()),
    mClientList(QList<QSharedPointer<Client>>()), mSessionList(QList<QSharedPointer<Session>>()),
    mExerciseList(QList<QSharedPointer<Exercise>>()), mClient(new Client),
    mExerciseNameList(QStringList()), mExerciseTypeList(QList<QPair<ProcessingSelection, ExerciseType>> ()),
    mExerciseIndexList(QList<qint32> ()), mSelectedExercise(Extensions::sNoExerciseSelected),
    mDeviceSelectionLine(0), mClientSelectionLine(-1), mMovementCounter(0), mMeasurementRunning(false),
    mEditingEnabled(false), mBodyPart(BodyPart::eAll)
{
    mUserInterface->setupUi(this);
    mTranslator = new QTranslator();

    mSettings = mXmlParser.XmlParser_LoadSettings();
    if(mSettings.isNull() == false)
    {
        switch(mSettings->Settings_GetLanguage(true).toInt())
        {
            case 1: default: MainWindow_SwitchTranslation(Language::eEnglish); break;
            case 2: MainWindow_SwitchTranslation(Language::eCzech); break;
        }
    }
    else
    {
        QMessageBox errMsg;
        errMsg.setIcon(QMessageBox::Critical);
        errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
        errMsg.setWindowTitle(tr("Settings loading error"));
        errMsg.setText(tr("Limb Range Analyzer is about to quit."));
        errMsg.exec();

        this->close();
    }

    MainWindow_InitializeUIProperties();
    MainWindow_Create3DScene();

    mBluetooth = new Bluetooth();
    mSensorModule = new SensorModule();

    mBluetooth->Bluetooth_SetDebugInformations(mSettings->Settings_GetDebugInformations());

    MainWindow_EnableHomePage();

    connect(mUserInterface->devicesScanButton, &QPushButton::clicked, this, &MainWindow::MainWindow_DevicesStartScanning);
    connect(mUserInterface->devicesConnectButton, &QPushButton::clicked, this, &MainWindow::MainWindow_DevicesConnect);
    connect(mUserInterface->devicesListWidget, &QListWidget::itemClicked, this, &MainWindow::MainWindow_DevicesSelectionChanged);
    connect(mUserInterface->processingButton, &QPushButton::clicked, this, &MainWindow::MainWindow_DeviceMeasure);
    connect(mUserInterface->graphicsPlusButton, &QPushButton::clicked, this, &MainWindow::MainWindow_MeasurementZoomIn);
    connect(mUserInterface->graphicsMinusButton, &QPushButton::clicked, this, &MainWindow::MainWindow_MeasurementZoomOut);
    connect(mUserInterface->graphicsLeftButton, &QPushButton::clicked, this, &MainWindow::MainWindow_MeasurementRotateLeft);
    connect(mUserInterface->graphicsRightButton, &QPushButton::clicked, this, &MainWindow::MainWindow_MeasurementRotateRight);
    connect(mUserInterface->saveRecordButton, &QPushButton::clicked, this, &MainWindow::MainWindow_MeasurementSaveRecord);
    connect(mUserInterface->displayHistoryButton, &QPushButton::clicked, this, &MainWindow::MainWindow_MeasurementDisplayHistory);
    connect(mUserInterface->newClientButton, &QPushButton::clicked, this, &MainWindow::MainWindow_ClientAdd);
    connect(mUserInterface->removeClientButton, &QPushButton::clicked, this, &MainWindow::MainWindow_ClientRemove);
    connect(mUserInterface->clientSelectionButton, &QPushButton::clicked, this, &MainWindow::MainWindow_ClientSelect);
    connect(mUserInterface->historyReturnButton, &QPushButton::clicked, this, &MainWindow::MainWindow_EnableMeasurementPage);
    connect(mUserInterface->historyTableWidget, &QTableWidget::itemClicked, this, &MainWindow::MainWindow_HistoryRecalculateBars);
    connect(mUserInterface->aboutAboutButton, &QPushButton::clicked, this, &MainWindow::MainWindow_AboutActivateAbout);
    connect(mUserInterface->aboutHowtoButton, &QPushButton::clicked, this, &MainWindow::MainWindow_AboutActivateHowto);
    connect(mUserInterface->settingsDebugCheckBox, &QCheckBox::clicked, this, &MainWindow::MainWindow_SettingsUpdate);
    connect(mUserInterface->settingsUnrestrictedMovementCheckBox, &QCheckBox::clicked, this, &MainWindow::MainWindow_SettingsUpdate);
    connect(mUserInterface->settingsLanguageComboBox, &QComboBox::textActivated, this, &MainWindow::MainWindow_SettingsUpdate);
    connect(mBluetooth, &Bluetooth::Bluetooth_SignalConnectionError, this, &MainWindow::MainWindow_DeviceNoConnectionReport);
    connect(mBluetooth, &Bluetooth::Bluetooth_SignalStatusReady, this, &MainWindow::MainWindow_DevicesUpdateStatus);
    connect(mBluetooth, &Bluetooth::Bluetooth_SignalDevicesReady, this, &MainWindow::MainWindow_DevicesDoneScanning);
    connect(mBluetooth, &Bluetooth::Bluetooth_SignalDeviceConnected, this, &MainWindow::MainWindow_DeviceConnectedReport);
    connect(mBluetooth, &Bluetooth::Bluetooth_SignalDeviceIncompatible, this, &MainWindow::MainWindow_DeviceIncompatibleReport);
    connect(mBluetooth, &Bluetooth::Bluetooth_SignalDeviceUnreachable, this, &MainWindow::MainWindow_DeviceUnreachableReport);
    connect(mBluetooth, &Bluetooth::Bluetooth_SignalDeviceBeyondLimit, this, &MainWindow::MainWindow_DeviceBeyondLimitReport);
    connect(mBluetooth, &Bluetooth::Bluetooth_SignalSensorDataReady, mSensorModule, &SensorModule::SensorModule_SetSensorValues);
    connect(mBluetooth, &Bluetooth::Bluetooth_SignalDisconnected, this, &MainWindow::MainWindow_ClearResults);
    connect(mSensorModule, &SensorModule::SensorModule_SignalProvideResults, this, &MainWindow::MainWindow_ReportResults);
    connect(mUserInterface->actionDevices, &QAction::triggered, this, &MainWindow::MainWindow_EnableDevicesPage);
    connect(mUserInterface->bodyPartComboBox, &QComboBox::currentTextChanged, this, &MainWindow::MainWindow_DeviceSwitchBodyPart);
    connect(mUserInterface->exercisesComboBox, &QComboBox::currentTextChanged, this, [this]{MainWindow::MainWindow_Set3DScene(true, true);});
    connect(mUserInterface->actionMeasurement, &QAction::triggered, this, &MainWindow::MainWindow_EnableMeasurementPage);
    connect(mUserInterface->actionClients, &QAction::triggered, this, &MainWindow::MainWindow_EnableClientsPage);
    connect(mUserInterface->actionAbout, &QAction::triggered, this, &MainWindow::MainWindow_EnableAboutPage);
    connect(mUserInterface->actionSettings, &QAction::triggered, this, &MainWindow::MainWindow_EnableSettingsPage);
    connect(mUserInterface->actionQuit, &QAction::triggered, this, &MainWindow::MainWindow_CleanAndQuit);
}

MainWindow::~MainWindow()
{
    delete mTranslator;
    delete mUserInterface;
    delete mUpperLeftArmTransform;
    delete mUpperRightArmTransform;
    delete mLowerLeftArmTransform;
    delete mLowerRightArmTransform;
    delete mLeftThighTransform;
    delete mRightThighTransform;
    delete mLeftCalfTransform;
    delete mRightCalfTransform;
    delete mPrimaryBlueCoinTransform;
    delete mSecondaryBlueCoinTransform;
    delete mMinRangeTransform;
    delete mMaxRangeTransform;
    for(qint32 index = 0; index < Extensions::sMovementOrbsCount; index++) delete mMovementOrbsTransform.at(index);
    mMovementOrbsTransform.clear();
    delete mRangeMaterial;
    delete mGraphicsRootEntity;
    delete mGraphicsContainer;
    delete mBluetooth;
    delete mSensorModule;
    mExerciseList.clear();
    mSessionList.clear();
    mClientList.clear();
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    MainWindow_ClientResizeTable();
    MainWindow_HistoryResizeTable();
    mUserInterface->settingsBlankLabel1->setFixedWidth(mUserInterface->settingsMainLabel->width() / 2);
    if(mUserInterface->aboutAboutButton->isEnabled() == false)
    {
        qint32 imageSize = (this->height() / 2.2);
        mUserInterface->aboutImageLabel->setMaximumSize(imageSize, imageSize);
    }
    else
    {
        qint32 imageSize = (this->height() / 4);
        mUserInterface->aboutHowtoTextWidget->setFixedHeight(this->height() / 3);
        mUserInterface->aboutImageLabel->setMaximumSize(imageSize, imageSize);
    }

    QMainWindow::resizeEvent(event);
}

void MainWindow::MainWindow_InitializeUIProperties()
{
    mUserInterface->homeLabel->setText("Limb Range Analyzer");
    mUserInterface->devicesProgressBar->setMinimum(0);
    mUserInterface->devicesProgressBar->setMaximum(1000);
    mUserInterface->devicesProgressBar->setValue(0);
    mUserInterface->devicesProgressBar->hide();
    mUserInterface->devicesBlankLabel->show();
    mUserInterface->devicesConnectButton->setEnabled(false);
    mUserInterface->processingButton->setEnabled(false);
    mUserInterface->devicesListWidget->setSelectionMode(QAbstractItemView::SingleSelection);

    /*Body parts & exercises definition: ------------------------------------*/
    mExerciseTypeList = {{ProcessingSelection::eProcessingXY, ExerciseType::eLimbUpper},
                         {ProcessingSelection::eProcessingXY, ExerciseType::eLimbUpper},
                         {ProcessingSelection::eProcessingZ, ExerciseType::eLimbUpper},
                         {ProcessingSelection::eProcessingXY, ExerciseType::eLimbUpper},
                         {ProcessingSelection::eProcessingXY, ExerciseType::eLimbLower},
                         {ProcessingSelection::eProcessingXY, ExerciseType::eLimbUpperDual}};
    mExerciseIndexList = {0, 1, 2, 3, 4, 5};

    mUserInterface->clientTableWidget->setColumnCount(Extensions::sClientTableColumns);
    mUserInterface->clientTableWidget->setHorizontalHeaderLabels({tr("First name"),
                 tr("Last name"), tr("E-mail")});
    mUserInterface->clientTableWidget->setTextElideMode(Qt::ElideMiddle);
    mUserInterface->clientTableWidget->horizontalHeader()->setStretchLastSection(true);
    mUserInterface->clientTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

    mUserInterface->historyTableWidget->setColumnCount(Extensions::sHistoryTableColumns);
    mUserInterface->historyTableWidget->setHorizontalHeaderLabels({tr("Date"),
                 tr("Min"), tr("Max"), tr("Range")});
    mUserInterface->historyTableWidget->setTextElideMode(Qt::ElideMiddle);
    mUserInterface->historyTableWidget->horizontalHeader()->setStretchLastSection(true);
    mUserInterface->historyTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    mUserInterface->historyTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

    QPixmap aboutImage;
    if(aboutImage.load(":/images/images/LRALogoRounded.png")) mUserInterface->aboutImageLabel->setPixmap(aboutImage);
    mUserInterface->aboutImageLabel->setScaledContents(true);

    mUserInterface->actionDevices->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_D));
    mUserInterface->actionMeasurement->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_M));
    mUserInterface->actionClients->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_C));
    mUserInterface->actionAbout->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_A));
    mUserInterface->actionSettings->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_S));
    mUserInterface->actionQuit->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_Q));

    mUserInterface->statusbar->showMessage(tr("Choose a page from the main menu."));
}

void MainWindow::MainWindow_InitializeUIStrings()
{
    mUserInterface->bodyPartComboBox->clear();
    mUserInterface->bodyPartComboBox->addItems(QStringList({tr("All body parts"), tr("Left arm"),
         tr("Right arm"), tr("Left leg"), tr("Right leg")}));
    mExerciseNameList = QStringList({tr("Exercise I: arm biceps"), tr("Exercise II: arm sideways"),
         tr("Exercise III: arm horizontal"), tr("Exercise IV: arm up"), tr("Exercise V: knee up"),
         tr("Exercise VI: arm biceps (dual)")});
    mUserInterface->exercisesComboBox->clear();
    mUserInterface->exercisesComboBox->addItems(mExerciseNameList);
    mUserInterface->clientTableWidget->setHorizontalHeaderLabels({tr("First name"),
                 tr("Last name"), tr("E-mail")});
    mUserInterface->historyTableWidget->setHorizontalHeaderLabels({tr("Date"),
                 tr("Min"), tr("Max"), tr("Range")});

    mUserInterface->settingsLanguageComboBox->clear();
    mUserInterface->settingsLanguageComboBox->addItems({tr("English"), tr("Czech")});

    mUserInterface->aboutVersionLabel->setText(tr("Version: %1").arg(QApplication::applicationVersion()));
    mUserInterface->aboutHowtoTextEdit->setMarkdown(QString("<center><br /><h1>Limb Range Analyzer</h1></center> "
          "<br /><br />%1 <br /><br /><h3>%2</h3> <br />%3 <br /><br /><h3>%4</h3> <br />%5 <br /><br />"
          "<h3>%6</h3> <br />%7 <br /><br /><h3>%8</h3> <ul><li>%9</li><li>%10</li><li>%11</li></ul><br />")
          .arg(tr("<b>Limb Range Analyzer</b> is a diploma-thesis related application that uses <b>Bluetooth LE</b> technology "
          "for connection to <b>BlueCoin</b> wristband devices. Up to two such devices can be connected while they provide "
          "IMU sensors measurement data that is processed and visualised in this application in order to allow human "
          "limb range analysis. Limb Range Analyzer application provides multiple pages (enlisted below) that can be "
          "accessed through the main menu of user interface."), tr("About Connection Page"), tr("The <b>Connection Page</b> "
          "allows the <i>discovery</i> and <i>connection/disconnection</i> management of <b>Bluetooth LE</b> devices. Note "
          "that <b>BlueCoin</b> devices only can become successfully connected, any other device shall be marked as "
          "<i>incompatible</i>. If a previously discovered device goes out of reach scope while a connection request approaches, "
          "it will <i>not be possible</i> to establish connection. Connection states are color-marked and user is informed in case "
          "of connection failure."), tr("About Sensor Monitoring Page"), tr("The <b>Sensor Monitoring Page</b> is a key component of "
          "this application, it provides both <i>graphic output</i> of limb range analysis and corresponding <i>numerical values</i>. "
          "On the right side of screen, user is free to <i>choose an exercise</i> that gets measured and animated in the graphics "
          "section. If a client is selected (see client page info below), their exercise progress can be <i>saved</i> and <i>accessed</i> "
          "along with their previous records - the possibility to <i>observe changes</i> is thus secured. Note that previous device "
          "connection is mandatory for the measurement to function."), tr("About Client Page"), tr("The <b>Client Page</b> involves "
          "patient management, namely their <i>creation</i>, <i>removal</i> and <i>selection</i>. In case of creation, <i>first name</i>, "
          "<i>last name</i> and <i>e-mail address</i> have to be provided. The selection funcionality toggled on this page allows "
          "measurement results to be linked with corresponding client."), tr("Contents of Settings Page"), tr("<b>Enable debug "
          "informations</b>: This option <i>enables</i> or <i>disables</i> various debug informations displayed in application console "
          "output."), tr("<b>Enable unrestricted dual limb movement</b>: An option for dual limb measurement exercises, that allows limbs "
          "depicted in 3D graphics output to move and bend <i>freely</i>, unnaturally even."), tr("<b>Language</b>: Provides a choice of "
          "preferred <i>application language</i>. Note that only few languages are supported.")));

    mUserInterface->aboutFooterLabel->setText(tr("Tomáš Bartošík, ©2021-2022 Faculty of applied informatics, TBU in Zlin"));
}

void MainWindow::MainWindow_ClientResizeTable()
{
    auto index = 0;
    auto columnsWidth = (mUserInterface->clientTableWidget->width()
            - mUserInterface->clientTableWidget->verticalHeader()->width()
            - mUserInterface->clientTableWidget->verticalScrollBar()->width()) / (Extensions::sClientTableColumns * 3);
    for(auto ratio : {3, 3, 3})
    {
        mUserInterface->clientTableWidget->setColumnWidth(index, (columnsWidth * ratio));
        index++;
    }
}

void MainWindow::MainWindow_HistoryResizeTable()
{
    auto index = 0;
    auto columnsWidth = (mUserInterface->historyTableWidget->width()
            - mUserInterface->historyTableWidget->verticalHeader()->width()
            - mUserInterface->historyTableWidget->verticalScrollBar()->width()) / (Extensions::sHistoryTableColumns * 6);
    for(auto ratio : {10, 5, 5, 4})
    {
        mUserInterface->historyTableWidget->setColumnWidth(index, (columnsWidth * ratio));
        index++;
    }
}

void MainWindow::MainWindow_Create3DScene()
{
    mGraphicsRootEntity = new Qt3DCore::QEntity;
    mAnimation = new QPropertyAnimation(this, "size");

    Qt3DCore::QEntity *headEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DRender::QMesh *headMesh = new Qt3DRender::QMesh;
    headMesh->setSource(QUrl::fromLocalFile(":/objects/body/head.obj"));

    Qt3DCore::QEntity *torsoEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DRender::QMesh *torsoMesh = new Qt3DRender::QMesh;
    torsoMesh->setSource(QUrl::fromLocalFile(":/objects/body/torso.obj"));

    Qt3DCore::QEntity *upperLeftArmEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DCore::QEntity *upperRightArmEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DRender::QMesh *upperArmMesh = new Qt3DRender::QMesh;
    upperArmMesh->setSource(QUrl::fromLocalFile(":/objects/body/armUpper.obj"));

    Qt3DCore::QEntity *lowerLeftArmEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DCore::QEntity *lowerRightArmEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DRender::QMesh *lowerLeftArmMesh = new Qt3DRender::QMesh;
    Qt3DRender::QMesh *lowerRightArmMesh = new Qt3DRender::QMesh;
    lowerLeftArmMesh->setSource(QUrl::fromLocalFile(":/objects/body/armLowerLeft.obj"));
    lowerRightArmMesh->setSource(QUrl::fromLocalFile(":/objects/body/armLowerRight.obj"));

    Qt3DCore::QEntity *leftThighEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DCore::QEntity *rightThighEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DRender::QMesh *thighMesh = new Qt3DRender::QMesh;
    thighMesh->setSource(QUrl::fromLocalFile(":/objects/body/legUpper.obj"));

    Qt3DCore::QEntity *leftCalfEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DCore::QEntity *rightCalfEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DRender::QMesh *leftCalfMesh = new Qt3DRender::QMesh;
    Qt3DRender::QMesh *rightCalfMesh = new Qt3DRender::QMesh;
    leftCalfMesh->setSource(QUrl::fromLocalFile(":/objects/body/legLowerLeft.obj"));
    rightCalfMesh->setSource(QUrl::fromLocalFile(":/objects/body/legLowerRight.obj"));

    Qt3DCore::QEntity *primaryBlueCoinEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DCore::QEntity *secondaryBlueCoinEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DExtras::QTorusMesh *blueCoinMesh = new Qt3DExtras::QTorusMesh;
    blueCoinMesh->setRadius(1.5f);
    blueCoinMesh->setMinorRadius(0.75f);
    blueCoinMesh->setRings(24);
    Qt3DCore::QEntity *minRangeEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DCore::QEntity *maxRangeEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DExtras::QSphereMesh *rangeMesh = new Qt3DExtras::QSphereMesh;
    rangeMesh->setRadius(0.75f);
    rangeMesh->setRings(10);
    rangeMesh->setSlices(10);
    QList<Qt3DCore::QEntity*> movementOrbEntities;
    for(qint32 index = 0; index < Extensions::sMovementOrbsCount; index++) movementOrbEntities.append(new Qt3DCore::QEntity(mGraphicsRootEntity));
    Qt3DExtras::QSphereMesh *movementOrbMesh = new Qt3DExtras::QSphereMesh;
    movementOrbMesh->setRadius(0.35f);
    movementOrbMesh->setRings(6);
    movementOrbMesh->setSlices(6);

    Qt3DCore::QTransform *headTransform = new Qt3DCore::QTransform;
    headTransform->setScale3D(QVector3D(2.35f, 3.0f, 3.0f));
    headTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0), 180.0f));
    headTransform->setTranslation(QVector3D(0.0f, 10.4f, 0.4f));
    Qt3DCore::QTransform *torsoTransform = new Qt3DCore::QTransform;
    torsoTransform->setScale3D(QVector3D(7.0f, 6.6f, 5.4f));
    torsoTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 1), 180.0f));
    torsoTransform->setTranslation(QVector3D(0.0f, 0.0f, 1.0f));
    mUpperLeftArmTransform = new Qt3DCore::QTransform;
    mUpperLeftArmTransform->setScale3D(QVector3D(6.6f, 7.6f, 5.5f));
    mUpperLeftArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 1), 15.0f));
    mUpperLeftArmTransform->setTranslation(QVector3D(5.8f, 0.7f, 0.5f));
    mUpperRightArmTransform = new Qt3DCore::QTransform;
    mUpperRightArmTransform->setScale3D(QVector3D(6.6f, 7.6f, 5.5f));
    mUpperRightArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 1), -15.0f));
    mUpperRightArmTransform->setTranslation(QVector3D(-5.8f, 0.7f, 0.5f));
    mLowerLeftArmTransform = new Qt3DCore::QTransform;
    mLowerLeftArmTransform->setScale3D(QVector3D(5.8f, 6.2f, 6.0f));
    mLowerLeftArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -90.0f));
    mLowerLeftArmTransform->setTranslation(QVector3D(6.9f, -3.4f, 3.8f));
    mLowerRightArmTransform = new Qt3DCore::QTransform;
    mLowerRightArmTransform->setScale3D(QVector3D(5.8f, 6.2f, 6.0f));
    mLowerRightArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -90.0f));
    mLowerRightArmTransform->setTranslation(QVector3D(-6.9f, -3.4f, 3.8f));
    mLeftThighTransform = new Qt3DCore::QTransform;
    mLeftThighTransform->setScale3D(QVector3D(1.8f, 1.8f, 1.7f));
    mLeftThighTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -5.0f));
    mLeftThighTransform->setTranslation(QVector3D(2.5f, -13.4f, 0.8f));
    mRightThighTransform = new Qt3DCore::QTransform;
    mRightThighTransform->setScale3D(QVector3D(1.8f, 1.8f, 1.7f));
    mRightThighTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -5.0f));
    mRightThighTransform->setTranslation(QVector3D(-2.5f, -13.4f, 0.8f));
    mLeftCalfTransform = new Qt3DCore::QTransform;
    mLeftCalfTransform->setScale3D(QVector3D(1.6f, 1.7f, 1.5f));
    mLeftCalfTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), 5.0f));
    mLeftCalfTransform->setTranslation(QVector3D(2.5f, -22.8f, 0.6f));
    mRightCalfTransform = new Qt3DCore::QTransform;
    mRightCalfTransform->setScale3D(QVector3D(1.6f, 1.7f, 1.5f));
    mRightCalfTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), 5.0f));
    mRightCalfTransform->setTranslation(QVector3D(-2.5f, -22.8f, 0.6f));
    mPrimaryBlueCoinTransform = new Qt3DCore::QTransform;
    mPrimaryBlueCoinTransform->setScale3D(QVector3D(1.0f, 1.0f, 1.0f));
    mSecondaryBlueCoinTransform = new Qt3DCore::QTransform;
    mSecondaryBlueCoinTransform->setScale3D(QVector3D(1.0f, 1.0f, 1.0f));
    mMinRangeTransform = new Qt3DCore::QTransform;
    mMinRangeTransform->setScale3D(QVector3D(1.0f, 1.0f, 1.0f));
    mMaxRangeTransform = new Qt3DCore::QTransform;
    mMaxRangeTransform->setScale3D(QVector3D(1.0f, 1.0f, 1.0f));
    for(qint32 index = 0; index < Extensions::sMovementOrbsCount; index++) mMovementOrbsTransform.append(new Qt3DCore::QTransform);

    Qt3DExtras::QPhongMaterial *bodyMaterial = new Qt3DExtras::QPhongMaterial();
    bodyMaterial->setDiffuse(Qt::white);
    bodyMaterial->setShininess(0.05f);
    Qt3DExtras::QPhongMaterial *blueCoinMaterial = new Qt3DExtras::QPhongMaterial();
    blueCoinMaterial->setDiffuse(Qt::blue);
    Qt3DExtras::QPhongAlphaMaterial *movementOrbMaterial = new Qt3DExtras::QPhongAlphaMaterial();
    movementOrbMaterial->setDiffuse(QColor(QRgb(0x3d85e3)));
    movementOrbMaterial->setAlpha(0.33f);
    mRangeMaterial = new Qt3DExtras::QPhongMaterial();
    mRangeMaterial->setDiffuse(QColor(QRgb(0xde5f1b)));

    headEntity->addComponent(headMesh);
    headEntity->addComponent(bodyMaterial);
    headEntity->addComponent(headTransform);
    torsoEntity->addComponent(torsoMesh);
    torsoEntity->addComponent(bodyMaterial);
    torsoEntity->addComponent(torsoTransform);
    upperLeftArmEntity->addComponent(upperArmMesh);
    upperLeftArmEntity->addComponent(bodyMaterial);
    upperLeftArmEntity->addComponent(mUpperLeftArmTransform);
    upperRightArmEntity->addComponent(upperArmMesh);
    upperRightArmEntity->addComponent(bodyMaterial);
    upperRightArmEntity->addComponent(mUpperRightArmTransform);
    lowerLeftArmEntity->addComponent(lowerLeftArmMesh);
    lowerLeftArmEntity->addComponent(bodyMaterial);
    lowerLeftArmEntity->addComponent(mLowerLeftArmTransform);
    lowerRightArmEntity->addComponent(lowerRightArmMesh);
    lowerRightArmEntity->addComponent(bodyMaterial);
    lowerRightArmEntity->addComponent(mLowerRightArmTransform);
    leftThighEntity->addComponent(thighMesh);
    leftThighEntity->addComponent(bodyMaterial);
    leftThighEntity->addComponent(mLeftThighTransform);
    rightThighEntity->addComponent(thighMesh);
    rightThighEntity->addComponent(bodyMaterial);
    rightThighEntity->addComponent(mRightThighTransform);
    leftCalfEntity->addComponent(leftCalfMesh);
    leftCalfEntity->addComponent(bodyMaterial);
    leftCalfEntity->addComponent(mLeftCalfTransform);
    rightCalfEntity->addComponent(rightCalfMesh);
    rightCalfEntity->addComponent(bodyMaterial);
    rightCalfEntity->addComponent(mRightCalfTransform);
    primaryBlueCoinEntity->addComponent(blueCoinMesh);
    primaryBlueCoinEntity->addComponent(blueCoinMaterial);
    primaryBlueCoinEntity->addComponent(mPrimaryBlueCoinTransform);
    secondaryBlueCoinEntity->addComponent(blueCoinMesh);
    secondaryBlueCoinEntity->addComponent(blueCoinMaterial);
    secondaryBlueCoinEntity->addComponent(mSecondaryBlueCoinTransform);
    minRangeEntity->addComponent(rangeMesh);
    minRangeEntity->addComponent(mRangeMaterial);
    minRangeEntity->addComponent(mMinRangeTransform);
    maxRangeEntity->addComponent(rangeMesh);
    maxRangeEntity->addComponent(mRangeMaterial);
    maxRangeEntity->addComponent(mMaxRangeTransform);
    for(qint32 index = 0; index < Extensions::sMovementOrbsCount; index++)
    {
        movementOrbEntities.at(index)->addComponent(movementOrbMesh);
        movementOrbEntities.at(index)->addComponent(movementOrbMaterial);
        movementOrbEntities.at(index)->addComponent(mMovementOrbsTransform.at(index));
    }

    Qt3DRender::QCamera *camera = mGraphicsView->camera();
    camera->lens()->setPerspectiveProjection(45.0f, 16.0f/9.0f, 0.1f, 1000.0f);
    camera->setPosition(QVector3D(-20.0f, 20.0f, 40.0f));
    camera->setViewCenter(QVector3D(0, -5.0f, 0));

    Qt3DExtras::QOrbitCameraController *camController = new Qt3DExtras::QOrbitCameraController(mGraphicsRootEntity);
    camController->setLinearSpeed(50.0f);
    camController->setLookSpeed(180.0f);
    camController->setCamera(camera);

    Qt3DCore::QEntity *lightEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DRender::QPointLight *light = new Qt3DRender::QPointLight(lightEntity);
    light->setColor("white");
    light->setIntensity(0.8f);
    lightEntity->addComponent(light);
    Qt3DCore::QTransform *lightTransform = new Qt3DCore::QTransform(lightEntity);
    lightTransform->setTranslation(camera->position());
    lightEntity->addComponent(lightTransform);

    Qt3DCore::QEntity *secondLightEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DRender::QPointLight *secondLight = new Qt3DRender::QPointLight(secondLightEntity);
    secondLight->setColor("white");
    secondLight->setIntensity(1.0f);
    secondLightEntity->addComponent(secondLight);
    Qt3DCore::QTransform *secondLightTransform = new Qt3DCore::QTransform(secondLightEntity);
    secondLightTransform->setTranslation(QVector3D(25.0f, 60.0f, 0.0f));
    secondLightEntity->addComponent(secondLightTransform);

    Qt3DCore::QEntity *thirdLightEntity = new Qt3DCore::QEntity(mGraphicsRootEntity);
    Qt3DRender::QPointLight *thirdLight = new Qt3DRender::QPointLight(thirdLightEntity);
    thirdLight->setColor("white");
    thirdLight->setIntensity(1.0f);
    thirdLightEntity->addComponent(thirdLight);
    Qt3DCore::QTransform *thirdLightTransform = new Qt3DCore::QTransform(thirdLightEntity);
    thirdLightTransform->setTranslation(QVector3D(-25.0f, 60.0f, 0.0f));
    thirdLightEntity->addComponent(thirdLightTransform);

    mGraphicsView->setRootEntity(mGraphicsRootEntity);
    mGraphicsView->defaultFrameGraph()->setClearColor(QColor(QRgb(0x303030)));
    mGraphicsContainer = QWidget::createWindowContainer(mGraphicsView);
    mGraphicsContainer->setMinimumSize(100, 100);
    mGraphicsContainer->setMaximumSize(mGraphicsView->screen()->size());
    mGraphicsContainer->setFocusPolicy(Qt::TabFocus);
    mUserInterface->graphicsLayout->addWidget(mGraphicsContainer);
}

void MainWindow::MainWindow_Set3DScene(bool blueCoinOnly, bool clearResults)
{
    if(clearResults == true)
    {
        mUserInterface->procMinLine->clear();
        mUserInterface->procMaxLine->clear();
        mUserInterface->procRangeLine->clear();
    }

    if(mSelectedExercise == Extensions::sNoExerciseSelected)
    {
        if(blueCoinOnly == true && mUserInterface->exercisesComboBox->count() > 0)
        {
            qint32 selection = mUserInterface->exercisesComboBox->currentIndex();
            mSecondaryBlueCoinTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));
            switch(mExerciseIndexList.at(selection))
            {
                case 0: case 1: case 2:
                {
                    mPrimaryBlueCoinTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 0), 0.0f));

                    if(mBodyPart == BodyPart::eLeftArm) mPrimaryBlueCoinTransform->setTranslation(QVector3D(6.9f, -3.4f, 3.8f));
                    else mPrimaryBlueCoinTransform->setTranslation(QVector3D(-6.9f, -3.4f, 3.8f));
                } break;
                case 3:
                {
                    mPrimaryBlueCoinTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -90.0f));

                    if(mBodyPart == BodyPart::eLeftArm) mPrimaryBlueCoinTransform->setTranslation(QVector3D(5.8f, 0.7f, 0.5f));
                    else mPrimaryBlueCoinTransform->setTranslation(QVector3D(-5.8f, 0.7f, 0.5f));
                } break;
                case 4:
                {
                    if(mBodyPart == BodyPart::eLeftLeg)
                    {
                        mPrimaryBlueCoinTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -95.0f));
                        mPrimaryBlueCoinTransform->setTranslation(QVector3D(2.5f, -10.7f, 0.8f));
                    }
                    else
                    {
                        mPrimaryBlueCoinTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -95.0f));
                        mPrimaryBlueCoinTransform->setTranslation(QVector3D(-2.5f, -10.7f, 0.8f));
                    }
                } break;
                case 5: default:
                {
                    mPrimaryBlueCoinTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 0), 0.0f));
                    mSecondaryBlueCoinTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -90.0f));

                    if(mBodyPart == BodyPart::eLeftArm)
                    {
                        mPrimaryBlueCoinTransform->setTranslation(QVector3D(6.9f, -3.4f, 3.8f));
                        mSecondaryBlueCoinTransform->setTranslation(QVector3D(5.8f, 0.7f, 0.5f));
                    }
                    else
                    {
                        mPrimaryBlueCoinTransform->setTranslation(QVector3D(-6.9f, -3.4f, 3.8f));
                        mSecondaryBlueCoinTransform->setTranslation(QVector3D(-5.8f, 0.7f, 0.5f));
                    }
                } break;
            }
        }
    }
    else
    {
        mMinRangeTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));
        mMaxRangeTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));

        if(mSelectedExercise == 0) mPrimaryBlueCoinTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 0), 0.0f));
        else if(mSelectedExercise == 1) mPrimaryBlueCoinTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0), -90.0f));
        else if(mSelectedExercise == 2)
        {
            mPrimaryBlueCoinTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 0), 0.0f));

            if(mBodyPart == BodyPart::eLeftArm)
            {
                mUpperLeftArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -90.0f));
                mLowerLeftArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -90.0f));
            }
            else
            {
                mUpperRightArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -90.0f));
                mLowerRightArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -90.0f));
            }
        }
        else if(mSelectedExercise == 3)
        {
            if(mBodyPart == BodyPart::eLeftArm) mLowerLeftArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), 180));
            else mLowerRightArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), 180));
        }
        else if(mSelectedExercise == 4)
        {
            if(mBodyPart == BodyPart::eLeftLeg) mLeftCalfTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), 0));
            else mRightCalfTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), 0));
        }
    }
}

void MainWindow::MainWindow_Reset3DScene()
{
    mUpperLeftArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 1), 15.0f));
    mUpperLeftArmTransform->setTranslation(QVector3D(5.8f, 0.7f, 0.5f));
    mLowerLeftArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -90.0f));
    mLowerLeftArmTransform->setTranslation(QVector3D(6.9f, -3.4f, 3.8f));
    mUpperRightArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 1), -15.0f));
    mUpperRightArmTransform->setTranslation(QVector3D(-5.8f, 0.7f, 0.5f));
    mLowerRightArmTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -90.0f));
    mLowerRightArmTransform->setTranslation(QVector3D(-6.9f, -3.4f, 3.8f));
    mLeftThighTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -5.0f));
    mLeftThighTransform->setTranslation(QVector3D(2.5f, -13.4f, 0.8f));
    mLeftCalfTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), 5.0f));
    mLeftCalfTransform->setTranslation(QVector3D(2.5f, -22.8f, 0.6f));
    mRightThighTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), -5.0f));
    mRightThighTransform->setTranslation(QVector3D(-2.5f, -13.4f, 0.8f));
    mRightCalfTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), 5.0f));
    mRightCalfTransform->setTranslation(QVector3D(-2.5f, -22.8f, 0.6f));
    for(qint32 index = 0; index < Extensions::sMovementOrbsCount; index++) mMovementOrbsTransform.at(index)->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));
    mMovementCounter = 0;
}

void MainWindow::MainWindow_ReportResults()
{
    if(mUserInterface->processingButton->isEnabled() == false) mUserInterface->processingButton->setEnabled(true);

    qint16 primaryMin = mSensorModule->SensorModule_GetCurrentMin(DeviceType::ePrimary);
    qint16 primaryMax = mSensorModule->SensorModule_GetCurrentMax(DeviceType::ePrimary);
    qint16 primaryRange = mSensorModule->SensorModule_GetCurrentRange(DeviceType::ePrimary);

    if(mMeasurementRunning == true)
    {
        qint16 primaryAngle = mSensorModule->SensorModule_GetCurrentAngle(DeviceType::ePrimary);
        QColor orangeColor = QColor(QRgb(0xde5f1b));
        QColor yellowColor = QColor(QRgb(0xe0da19));
        QColor greenColor = QColor(QRgb(0x60d119));
        QVector3D primaryLimbTranslation;
        QVector3D secondaryLimbTranslation;

        if(mSelectedExercise == 0)          /*Exercise I: arm biceps -------------*/
        {
            if(primaryAngle >= -90 && primaryAngle <= 90)
            {
                mPrimaryBlueCoinTransform->setRotationX(primaryAngle);
                if(mBodyPart == BodyPart::eLeftArm)
                {
                    mLowerLeftArmTransform->setRotationX(-90.0f + primaryAngle);
                    if(primaryAngle > 0)  primaryLimbTranslation = QVector3D(6.9f, (-3.4f - (primaryAngle * 0.035f)), (3.8f - (primaryAngle * 0.032f)));
                    else primaryLimbTranslation = QVector3D(6.9f, (-3.4f - (primaryAngle * 0.03f)), (3.8f + (primaryAngle * 0.015f)));

                    mLowerLeftArmTransform->setTranslation(primaryLimbTranslation);
                }
                else
                {
                    mLowerRightArmTransform->setRotationX(-90.0f + primaryAngle);
                    if(primaryAngle > 0) primaryLimbTranslation = QVector3D(-6.9f, (-3.4f - (primaryAngle * 0.035f)), (3.8f - (primaryAngle * 0.032f)));
                    else primaryLimbTranslation = QVector3D(-6.9f, (-3.4f - (primaryAngle * 0.03f)), (3.8f + (primaryAngle * 0.015f)));

                    mLowerRightArmTransform->setTranslation(primaryLimbTranslation);
                }
                mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                if(primaryAngle >= primaryMax) mMaxRangeTransform->setTranslation(primaryLimbTranslation);
                else if(primaryAngle <= primaryMin) mMinRangeTransform->setTranslation(primaryLimbTranslation);
                if(primaryRange > 135) mRangeMaterial->setDiffuse(greenColor);
                else if(primaryRange > 90) mRangeMaterial->setDiffuse(yellowColor);
                else mRangeMaterial->setDiffuse(orangeColor);
            }
        }
        else if(mSelectedExercise == 1)     /*Exercise II: arm sideways ----------*/
        {
            if(primaryAngle >= -90 && primaryAngle <= 90)
            {
                qint16 mult = 90 - abs(primaryAngle);
                if(mBodyPart == BodyPart::eLeftArm)
                {
                    mPrimaryBlueCoinTransform->setRotationX(-primaryAngle);
                    QQuaternion rotation = QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 1), (90.0f - primaryAngle));
                    mUpperLeftArmTransform->setRotation(rotation);
                    mLowerLeftArmTransform->setRotation(rotation);
                    if(primaryAngle > 0)
                    {
                        mUpperLeftArmTransform->setTranslation(QVector3D((8.2f - (primaryAngle * 0.035)), (3.7f - (primaryAngle * 0.04)), 0.5f));
                        primaryLimbTranslation = QVector3D((15.7f - (primaryAngle * primaryAngle * 0.001) - (primaryAngle * 0.03)), (3.8f - (primaryAngle * mult * 0.001) - (primaryAngle * 0.12)), 0.5f);
                    }
                    else
                    {
                        mUpperLeftArmTransform->setTranslation(QVector3D((8.2f + (primaryAngle * 0.035)), (3.8f - (primaryAngle * 0.04)), 0.5f));
                        primaryLimbTranslation = QVector3D((15.7f + (primaryAngle * abs(primaryAngle) * 0.001) + (primaryAngle * 0.03)), (3.8f - (primaryAngle * mult * 0.001) - (primaryAngle * 0.12)), 0.5f);
                    }
                    mLowerLeftArmTransform->setTranslation(primaryLimbTranslation);
                    if(primaryAngle >= primaryMax) mMaxRangeTransform->setTranslation(primaryLimbTranslation);
                    else if(primaryAngle <= primaryMin) mMinRangeTransform->setTranslation(primaryLimbTranslation);
                }
                else
                {
                    primaryAngle = -primaryAngle;
                    mPrimaryBlueCoinTransform->setRotationX(-primaryAngle);
                    QQuaternion rotation = QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 1), (270.0f - primaryAngle));
                    mUpperRightArmTransform->setRotation(rotation);
                    mLowerRightArmTransform->setRotation(rotation);
                    if(primaryAngle > 0)
                    {
                        mUpperRightArmTransform->setTranslation(QVector3D((-8.2f + (primaryAngle * 0.035)), (3.7f + (primaryAngle * 0.04)), 0.5f));
                        primaryLimbTranslation = QVector3D((-15.7f + (primaryAngle * primaryAngle * 0.001) + (primaryAngle * 0.03)), (3.8f + (primaryAngle * mult * 0.001) + (primaryAngle * 0.12)), 0.5f);
                    }
                    else
                    {
                        mUpperRightArmTransform->setTranslation(QVector3D((-8.2f - (primaryAngle * 0.035)), (3.8f + (primaryAngle * 0.04)), 0.5f));
                        primaryLimbTranslation = QVector3D((-15.7f - (primaryAngle * abs(primaryAngle) * 0.001) - (primaryAngle * 0.03)), (3.8f + (primaryAngle * mult * 0.001) + (primaryAngle * 0.12)), 0.5f);
                    }
                    mLowerRightArmTransform->setTranslation(primaryLimbTranslation);
                    if(-primaryAngle >= primaryMax) mMaxRangeTransform->setTranslation(primaryLimbTranslation);
                    else if(-primaryAngle <= primaryMin) mMinRangeTransform->setTranslation(primaryLimbTranslation);
                }
                mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                if(primaryRange > 135) mRangeMaterial->setDiffuse(greenColor);
                else if(primaryRange > 90) mRangeMaterial->setDiffuse(yellowColor);
                else mRangeMaterial->setDiffuse(orangeColor);
            }
        }
        else if(mSelectedExercise == 2)     /*Exercise III: arm horizontal -------*/
        {
            qint16 mult = 90 - abs(primaryAngle);

            if(mBodyPart == BodyPart::eLeftArm)
            {
                if(primaryAngle < 0)
                {
                    if(abs(primaryAngle) <= 90)
                    {
                        mUpperLeftArmTransform->setRotationZ(-primaryAngle);
                        mLowerLeftArmTransform->setRotationZ(-primaryAngle);
                        mPrimaryBlueCoinTransform->setRotationY(-primaryAngle);
                        mUpperLeftArmTransform->setTranslation(QVector3D((6.2f - (primaryAngle * 0.03)), 3.7f, (3.4f + (primaryAngle * 0.03))));
                        primaryLimbTranslation = QVector3D((6.0f - (primaryAngle * mult * 0.001) - (primaryAngle * 0.12)), 3.7f, (11.3f - (primaryAngle * primaryAngle * 0.001) + (primaryAngle * 0.03)));
                        mLowerLeftArmTransform->setTranslation(primaryLimbTranslation);
                        mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                        if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                        if(primaryAngle <= primaryMin) mMinRangeTransform->setTranslation(primaryLimbTranslation);
                    }
                    else if(abs(primaryAngle) <= 130)
                    {
                        mUpperLeftArmTransform->setRotationZ(-primaryAngle);
                        mLowerLeftArmTransform->setRotationZ(-primaryAngle);
                        mPrimaryBlueCoinTransform->setRotationY(-primaryAngle);
                        mUpperLeftArmTransform->setTranslation(QVector3D((9.8f + (primaryAngle * 0.01)), 3.7f, (7.8f + (primaryAngle * 0.08))));
                        primaryLimbTranslation = QVector3D((17.1f + (mult * 0.062)), 3.7f, (20.2f + (primaryAngle * 0.22)));
                        mLowerLeftArmTransform->setTranslation(primaryLimbTranslation);
                        mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                        if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                        if(primaryAngle <= primaryMin) mMinRangeTransform->setTranslation(primaryLimbTranslation);
                    }
                }
                else
                {
                    if(primaryAngle <= 40)
                    {
                        mUpperLeftArmTransform->setRotationZ(-primaryAngle);
                        mLowerLeftArmTransform->setRotationZ(-primaryAngle);
                        mPrimaryBlueCoinTransform->setRotationY(-primaryAngle);
                        mUpperLeftArmTransform->setTranslation(QVector3D((6.2f - (primaryAngle * 0.1)), 3.7f, (3.4f + (primaryAngle * 0.04))));
                        primaryLimbTranslation = QVector3D((6.0f - (primaryAngle * mult * 0.001) - (primaryAngle * 0.18)), 3.7f, 11.4f);
                        mLowerLeftArmTransform->setTranslation(primaryLimbTranslation);
                        mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                        if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                        if(primaryAngle >= primaryMax) mMaxRangeTransform->setTranslation(primaryLimbTranslation);
                    }
                }
            }
            else
            {
                if(primaryAngle > 0)
                {
                    if(primaryAngle <= 90)
                    {
                        mUpperRightArmTransform->setRotationZ(-primaryAngle);
                        mLowerRightArmTransform->setRotationZ(-primaryAngle);
                        mPrimaryBlueCoinTransform->setRotationY(-primaryAngle);
                        mUpperRightArmTransform->setTranslation(QVector3D((-6.2f - (primaryAngle * 0.03)), 3.7f, (3.4f - (primaryAngle * 0.03))));
                        primaryLimbTranslation = QVector3D((-6.0f - (primaryAngle * mult * 0.001) - (primaryAngle * 0.12)), 3.7f, (11.3f - (primaryAngle * primaryAngle * 0.001) - (primaryAngle * 0.03)));
                        mLowerRightArmTransform->setTranslation(primaryLimbTranslation);
                        mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                        if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                        if(primaryAngle >= primaryMax) mMaxRangeTransform->setTranslation(primaryLimbTranslation);
                    }
                    else if(primaryAngle <= 130)
                    {
                        mUpperRightArmTransform->setRotationZ(-primaryAngle);
                        mLowerRightArmTransform->setRotationZ(-primaryAngle);
                        mPrimaryBlueCoinTransform->setRotationY(-primaryAngle);
                        mUpperRightArmTransform->setTranslation(QVector3D((-9.8f + (primaryAngle * 0.01)), 3.7f, (7.8f - (primaryAngle * 0.08))));
                        primaryLimbTranslation = QVector3D((-17.1f - (mult * 0.062)), 3.7f, (20.2f - (primaryAngle * 0.22)));
                        mLowerRightArmTransform->setTranslation(primaryLimbTranslation);
                        mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                        if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                        if(primaryAngle >= primaryMax) mMaxRangeTransform->setTranslation(primaryLimbTranslation);
                    }
                }
                else
                {
                    if(abs(primaryAngle) <= 40)
                    {
                        mUpperRightArmTransform->setRotationZ(-primaryAngle);
                        mLowerRightArmTransform->setRotationZ(-primaryAngle);
                        mPrimaryBlueCoinTransform->setRotationY(-primaryAngle);
                        mUpperRightArmTransform->setTranslation(QVector3D((-6.2f - (primaryAngle * 0.1)), 3.7f, (3.4f - (primaryAngle * 0.04))));
                        primaryLimbTranslation = QVector3D((-6.0f - (primaryAngle * mult * 0.001) - (primaryAngle * 0.18)), 3.7f, 11.4f);
                        mLowerRightArmTransform->setTranslation(primaryLimbTranslation);
                        mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                        if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                        if(primaryAngle <= primaryMin) mMinRangeTransform->setTranslation(primaryLimbTranslation);
                    }
                }
            }
            if(primaryRange > 135) mRangeMaterial->setDiffuse(greenColor);
            else if(primaryRange > 90) mRangeMaterial->setDiffuse(yellowColor);
            else mRangeMaterial->setDiffuse(orangeColor);
        }
        else if(mSelectedExercise == 3)     /*Exercise IV: arm up ----------------*/
        {
            float positionY = 0.0f;
            float positionZ = 0.0f;
            qint16 mult = abs(45 - abs(45 - abs(primaryAngle)));

            if(primaryAngle >= -90 && primaryAngle <= 90)
            {
                mPrimaryBlueCoinTransform->setRotationX(primaryAngle);
                if(mBodyPart == BodyPart::eLeftArm)
                {
                    mUpperLeftArmTransform->setRotationX(-90.0f + primaryAngle);
                    if(primaryAngle > 0)
                    {
                        mLowerLeftArmTransform->setRotationX(-primaryAngle / 4);
                        positionY = (3.8f - (primaryAngle * 0.035f));
                        positionZ = (3.7f - (primaryAngle * 0.042f));
                        secondaryLimbTranslation = QVector3D(6.8f, positionZ + (2.7f - (primaryAngle * 0.042) - (mult * 0.02)), positionY + (3.7f - (primaryAngle * 0.025) + (mult * 0.02)));
                    }
                    else
                    {
                        positionY = (3.8f + (primaryAngle * 0.033f));
                        positionZ = (3.7f - (primaryAngle * 0.045f));
                        secondaryLimbTranslation = QVector3D(6.8f, positionZ + (2.7f - (primaryAngle * 0.045) + (mult * 0.02)), positionY + (3.7f + (primaryAngle * 0.045) + (mult * 0.02)));
                    }
                    primaryLimbTranslation = QVector3D(5.8f, positionZ, positionY);
                    mUpperLeftArmTransform->setTranslation(primaryLimbTranslation);
                    mLowerLeftArmTransform->setTranslation(secondaryLimbTranslation);
                }
                else
                {
                    mUpperRightArmTransform->setRotationX(-90.0f + primaryAngle);
                    if(primaryAngle > 0)
                    {
                        mLowerRightArmTransform->setRotationX(-primaryAngle / 4);
                        positionY = (3.8f - (primaryAngle * 0.035f));
                        positionZ = (3.7f - (primaryAngle * 0.042f));
                        secondaryLimbTranslation = QVector3D(-6.8f, positionZ + (2.7f - (primaryAngle * 0.042) - (mult * 0.02)), positionY + (3.7f - (primaryAngle * 0.025) + (mult * 0.02)));
                    }
                    else
                    {
                        positionY = (3.8f + (primaryAngle * 0.033f));
                        positionZ = (3.7f - (primaryAngle * 0.045f));
                        secondaryLimbTranslation = QVector3D(-6.8f, positionZ + (2.7f - (primaryAngle * 0.045) + (mult * 0.02)), positionY + (3.7f + (primaryAngle * 0.045) + (mult * 0.02)));
                    }
                    primaryLimbTranslation = QVector3D(-5.8f, positionZ, positionY);
                    mUpperRightArmTransform->setTranslation(primaryLimbTranslation);
                    mLowerRightArmTransform->setTranslation(secondaryLimbTranslation);
                }
                mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                if(primaryAngle >= primaryMax) mMaxRangeTransform->setTranslation(primaryLimbTranslation);
                else if(primaryAngle <= primaryMin) mMinRangeTransform->setTranslation(primaryLimbTranslation);
                if(primaryRange > 135) mRangeMaterial->setDiffuse(greenColor);
                else if(primaryRange > 90) mRangeMaterial->setDiffuse(yellowColor);
                else mRangeMaterial->setDiffuse(orangeColor);
            }
        }
        else if(mSelectedExercise == 4)     /*Exercise V: knee up ----------------*/
        {
            float positionY = 0.0f;
            float positionZ = 0.0f;
            qint16 mult = abs(45 - abs(45 - abs(primaryAngle)));

            if(primaryAngle >= -85 && primaryAngle <= 90)
            {
                mPrimaryBlueCoinTransform->setRotationX(-primaryAngle);
                if(mBodyPart == BodyPart::eLeftLeg)
                {
                    mLeftThighTransform->setRotationX(-90.0f + primaryAngle);
                    if(primaryAngle > 0)
                    {
                        positionY = (7.6f - (primaryAngle * 0.082f) + (mult * 0.03));
                        positionZ = (-6.5f - (primaryAngle * 0.076f) - (mult * 0.03));
                        secondaryLimbTranslation = QVector3D(2.5f, positionZ - (7.3f + (primaryAngle * 0.026)), positionY + (2.0f - (primaryAngle * 0.02) + (mult * 0.018)));
                    }
                    else
                    {
                        mLeftCalfTransform->setRotationX(primaryAngle / 3);
                        positionY = (7.6f + (primaryAngle * 0.05f) + (mult * 0.02));
                        positionZ = (-6.5f - (primaryAngle * 0.09f) + (mult * 0.04));
                        secondaryLimbTranslation = QVector3D(2.5f, positionZ - (7.3f + (primaryAngle * 0.025) + (mult * 0.02)), positionY - ((primaryAngle * 0.035) - (mult * 0.01) - 2.0f));
                    }
                    primaryLimbTranslation = QVector3D(2.5f, positionZ, positionY);
                    mLeftThighTransform->setTranslation(primaryLimbTranslation);
                    mLeftCalfTransform->setTranslation(secondaryLimbTranslation);
                }
                else
                {
                    mRightThighTransform->setRotationX(-90.0f + primaryAngle);
                    if(primaryAngle > 0)
                    {
                        positionY = (7.6f - (primaryAngle * 0.082f) + (mult * 0.03));
                        positionZ = (-6.5f - (primaryAngle * 0.076f) - (mult * 0.03));
                        secondaryLimbTranslation = QVector3D(-2.5f, positionZ - (7.3f + (primaryAngle * 0.026)), positionY + (2.0f - (primaryAngle * 0.02) + (mult * 0.018)));
                    }
                    else
                    {
                        mRightCalfTransform->setRotationX(primaryAngle / 3);
                        positionY = (7.6f + (primaryAngle * 0.05f) + (mult * 0.02));
                        positionZ = (-6.5f - (primaryAngle * 0.09f) + (mult * 0.04));
                        secondaryLimbTranslation = QVector3D(-2.5f, positionZ - (7.3f + (primaryAngle * 0.025) + (mult * 0.02)), positionY - ((primaryAngle * 0.035) - (mult * 0.01) - 2.0f));
                    }
                    primaryLimbTranslation = QVector3D(-2.5f, positionZ, positionY);
                    mRightThighTransform->setTranslation(primaryLimbTranslation);
                    mRightCalfTransform->setTranslation(secondaryLimbTranslation);
                }
                mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                if(primaryAngle >= primaryMax) mMaxRangeTransform->setTranslation(primaryLimbTranslation);
                else if(primaryAngle <= primaryMin) mMinRangeTransform->setTranslation(primaryLimbTranslation);
                if(primaryRange > 135) mRangeMaterial->setDiffuse(greenColor);
                else if(primaryRange > 90) mRangeMaterial->setDiffuse(yellowColor);
                else mRangeMaterial->setDiffuse(orangeColor);
            }
        }
        else if(mSelectedExercise == 5)     /*Exercise VI: arm biceps (dual) -----*/
        {
            qint16 secondaryAngle = -mSensorModule->SensorModule_GetCurrentAngle(DeviceType::eSecondary);
            qint16 primaryAbs = abs(primaryAngle);
            qint16 secondaryAbs = abs(secondaryAngle);
            float positionY = 0.0f;
            float positionZ = 0.0f;
            bool angleAccepted = false;

            if(secondaryAngle >= -135 && secondaryAngle <= 90)
            {
                positionY = (3.5f - (secondaryAbs * 0.035));
                if(secondaryAngle >= -90) positionZ = (3.6f + (secondaryAngle * 0.035));
                else positionZ = (0.2f - ((90 + secondaryAngle) * 0.035));

                if(mSettings->Settings_GetUnrestrictedLimbMovement()) angleAccepted = true;
                else
                {
                    if(secondaryAngle < 0) angleAccepted = ((primaryAngle <= secondaryAbs) && (secondaryAngle + primaryAngle > -160));
                    else angleAccepted = ((primaryAngle <= 0) && (primaryAbs >= secondaryAngle) && (secondaryAngle + primaryAngle > -160));
                }

                if(angleAccepted == true)
                {
                    if(mBodyPart == BodyPart::eLeftArm)
                    {
                        mUpperLeftArmTransform->setRotationX(-90.0f - secondaryAngle);
                        secondaryLimbTranslation = QVector3D(6.4f, positionZ, positionY);

                        mUpperLeftArmTransform->setTranslation(secondaryLimbTranslation);
                    }
                    else
                    {
                        mUpperRightArmTransform->setRotationX(-90.0f - secondaryAngle);
                        secondaryLimbTranslation = QVector3D(-6.4f, positionZ, positionY);

                        mUpperRightArmTransform->setTranslation(secondaryLimbTranslation);
                    }
                    mSecondaryBlueCoinTransform->setRotationX(-secondaryAngle);
                    mSecondaryBlueCoinTransform->setTranslation(secondaryLimbTranslation);
                }

                if(primaryAngle >= -180 && primaryAngle <= 135 && angleAccepted == true)
                {
                    qint16 combinedMin = mSensorModule->SensorModule_GetCombinedMin();
                    qint16 combinedMax = mSensorModule->SensorModule_GetCombinedMax();
                    mPrimaryBlueCoinTransform->setRotationX(primaryAngle);

                    if(secondaryAngle >= 0 && primaryAngle < -90 /*(n/n\)*/) positionY += 8.2f - (secondaryAbs * 0.05) - (primaryAbs * 0.02) - (primaryAngle * primaryAngle * 0.00012) - (secondaryAngle * secondaryAngle * 0.0002);
                    else if(secondaryAngle >= 0 && primaryAngle >= -90 /*(n/n/)*/) positionY += 8.2f - (secondaryAbs * 0.05) - (primaryAbs * 0.03) - (secondaryAngle * secondaryAngle * 0.0002);
                    else if(secondaryAngle >= -90 && primaryAngle < 0/*(d\n/)*/) positionY += 8.7f - (secondaryAbs * 0.03) - ((90 + secondaryAngle) * 0.02) - (primaryAngle * primaryAngle * 0.0002) - (secondaryAngle * secondaryAngle * 0.0002);
                    else if(secondaryAngle >= -90 && primaryAngle >= 0/*(d\d\)*/) positionY += 8.6f - (secondaryAbs * 0.03) - (primaryAbs * 0.05) - (secondaryAngle * secondaryAngle * 0.0002);
                    else if(secondaryAngle < -90 && primaryAngle < 90 /*(d/d\)*/) positionY += 11.2f - (secondaryAbs * 0.05) - (primaryAbs * 0.05) - (secondaryAngle * secondaryAngle * 0.0002);
                    else if(secondaryAngle < -90 && primaryAngle >= 90 /*(d/d/)*/) positionY += 7.0f - (secondaryAbs * 0.04) - (primaryAbs * 0.025) - (secondaryAngle * secondaryAngle * 0.0002);

                    if(secondaryAngle >= 0 && primaryAngle < -90 /*(n/n\)*/) positionZ += 0.5f + (secondaryAngle * 0.065) - ((-180 - primaryAngle) * 0.05) - (secondaryAngle * secondaryAngle * 0.0002);
                    else if(secondaryAngle >= 0 && primaryAngle >= -90 /*(n/n/)*/) positionZ += (secondaryAngle * 0.055) - (primaryAngle * 0.065) + (primaryAngle * primaryAngle * primaryAngle * 0.000003);
                    else if(secondaryAngle >= -90 && primaryAngle < -90 /*(d\n\)*/) positionZ += -1.0f + (secondaryAngle * 0.065) - (primaryAngle * 0.05) + ((90 + primaryAngle) * 0.05) + (secondaryAngle * secondaryAngle * 0.0002);
                    else if(secondaryAngle >= -90 && primaryAngle < 0 /*(d\n/)*/) positionZ += (secondaryAngle * 0.065) - (primaryAngle * 0.04) + (secondaryAngle * secondaryAngle * 0.0002);
                    else if(secondaryAngle >= -90 && primaryAngle >= 0 /*(d\d\)*/) positionZ += (secondaryAngle * 0.065) - (primaryAngle * 0.05) + (secondaryAngle * secondaryAngle * 0.0002);
                    else if(secondaryAngle < -90 && primaryAngle < 90 /*(d/d\)*/) positionZ += (secondaryAngle * 0.065) - (primaryAngle * 0.04) + (secondaryAngle * secondaryAngle * 0.0002);
                    else if(secondaryAngle < -90 && primaryAngle >= 90 /*(d/d/)*/) positionZ += -2.4f + (secondaryAngle * 0.05) - ((180 - primaryAngle) * 0.045) + (secondaryAngle * secondaryAngle * 0.0003);

                    if(mBodyPart == BodyPart::eLeftArm)
                    {
                        mLowerLeftArmTransform->setRotationX(-90.0f + primaryAngle);
                        primaryLimbTranslation = QVector3D(7.2f, positionZ, positionY);

                        mLowerLeftArmTransform->setTranslation(primaryLimbTranslation);
                    }
                    else
                    {
                        mLowerRightArmTransform->setRotationX(-90.0f + primaryAngle);
                        primaryLimbTranslation = QVector3D(-7.2f, positionZ, positionY);

                        mLowerRightArmTransform->setTranslation(primaryLimbTranslation);
                    }

                    if((secondaryAngle - primaryAngle) > combinedMax)
                    {
                        combinedMax = (secondaryAngle - primaryAngle);
                        mSensorModule->SensorModule_SetCombinedMax(combinedMax);
                    }
                    else if((secondaryAngle - primaryAngle) < combinedMin)
                    {
                        combinedMin = (secondaryAngle - primaryAngle);
                        mSensorModule->SensorModule_SetCombinedMin(combinedMin);
                    }

                    mPrimaryBlueCoinTransform->setTranslation(primaryLimbTranslation);
                    if(mMovementCounter % 2 == 0) mMovementOrbsTransform.at(mMovementCounter / 2)->setTranslation(primaryLimbTranslation);
                    if((secondaryAngle - primaryAngle) >= combinedMax) mMaxRangeTransform->setTranslation(primaryLimbTranslation);
                    else if((secondaryAngle - primaryAngle) <= combinedMin) mMinRangeTransform->setTranslation(primaryLimbTranslation);
                    if((combinedMax - combinedMin) > 280) mRangeMaterial->setDiffuse(greenColor);
                    else if((combinedMax - combinedMin) > 180) mRangeMaterial->setDiffuse(yellowColor);
                    else mRangeMaterial->setDiffuse(orangeColor);
                }
            }
        }

        mMovementCounter++;
        if(mMovementCounter >= Extensions::sMovementOrbsCount * 2) mMovementCounter = 0;

        mUserInterface->procMinLine->setText(QString::number(primaryMin));
        mUserInterface->procMaxLine->setText(QString::number(primaryMax));
        mUserInterface->procRangeLine->setText(QString::number(primaryRange));
    }
}

void MainWindow::MainWindow_ClearResults()
{
    mUserInterface->processingButton->setEnabled(false);

    mUserInterface->procMinLine->clear();
    mUserInterface->procMaxLine->clear();
    mUserInterface->procRangeLine->clear();
}

void MainWindow::MainWindow_DevicesStartScanning()
{
    mUserInterface->devicesProgressBar->show();
    mUserInterface->devicesBlankLabel->hide();
    mUserInterface->devicesProgressBar->setValue(mUserInterface->devicesProgressBar->minimum());
    mUserInterface->devicesScanButton->setEnabled(false);
    mUserInterface->devicesConnectButton->setEnabled(false);
    mUserInterface->statusbar->showMessage(tr("Scanning for Bluetooth LE devices in vicinity..."));

    mBluetooth->Bluetooth_StartScanning();
}

void MainWindow::MainWindow_DevicesUpdateStatus()
{
    mUserInterface->devicesProgressBar->setValue(mUserInterface->devicesProgressBar->value() + 100);
}

void MainWindow::MainWindow_DevicesDoneScanning()
{
    QStringList deviceNameList;
    QStringList deviceAddressList;

    mUserInterface->devicesProgressBar->hide();
    mUserInterface->devicesBlankLabel->show();
    mUserInterface->devicesProgressBar->setValue(mUserInterface->devicesProgressBar->maximum());
    mUserInterface->devicesScanButton->setEnabled(true);

    mUserInterface->statusbar->showMessage(tr("Scanning for Bluetooth LE devices completed."));
    mUserInterface->devicesListWidget->clear();
    deviceNameList = mBluetooth->Bluetooth_GetDevicesList();
    deviceAddressList = mBluetooth->Bluetooth_GetAddressList();
    for(qint32 index = 0; (index < deviceNameList.count() && index < deviceAddressList.count()); index++)
    {
        mUserInterface->devicesListWidget->addItem(tr(" Device name: %1    Device address: %2")
              .arg(deviceNameList.at(index), deviceAddressList.at(index)));
    }
    if(mUserInterface->devicesListWidget->count() > 0)
    {
        mUserInterface->devicesConnectButton->setEnabled(true);
    }

    for(qint32 index = 0; index < mUserInterface->devicesListWidget->count(); index++)
    {
        QListWidgetItem *deviceSelection = mUserInterface->devicesListWidget->item(index);
        QFont font = deviceSelection->font();
        font.setPointSize(12);
        deviceSelection->setFont(font);
        QString deviceAddress = mBluetooth->Bluetooth_GetAddressList().at(index);
        if(mBluetooth->Bluetooth_IsDeviceConnected(deviceAddress) == true)
        {
            deviceSelection->setBackground(Qt::green);
        }
    }
}

void MainWindow::MainWindow_DevicesSelectionChanged()
{
    if(mUserInterface->devicesListWidget->selectedItems().isEmpty() == false)
    {
        QListWidgetItem *deviceSelection = mUserInterface->devicesListWidget->selectedItems().first();
        int deviceSelectionLine = mUserInterface->devicesListWidget->row(deviceSelection);
        QString deviceAddress = mBluetooth->Bluetooth_GetAddressList().at(deviceSelectionLine);

        if(mBluetooth->Bluetooth_IsDeviceConnected(deviceAddress) == true)
        {
            mUserInterface->devicesConnectButton->setText(tr("Disconnect from selected device"));
        }
        else mUserInterface->devicesConnectButton->setText(tr("Connect to selected device"));
    }
}

void MainWindow::MainWindow_DevicesConnect()
{
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowTitle(tr("Connection error"));
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));

    if(mSettings->Settings_GetDebugInformations()) qDebug() << tr("Going to connect to/disconnect from devices.");
    if(mUserInterface->devicesListWidget->selectedItems().isEmpty() == true)
    {
        errMsg.setText(tr("Choose one device in order to manage connection."));
        errMsg.exec();
    }
    else
    {
        mUserInterface->devicesConnectButton->setEnabled(false);
        QListWidgetItem *deviceSelection = mUserInterface->devicesListWidget->selectedItems().first();
        deviceSelection->setBackground(Qt::gray);
        mDeviceSelectionLine = mUserInterface->devicesListWidget->row(deviceSelection);
        QString deviceAddress = mBluetooth->Bluetooth_GetAddressList().at(mDeviceSelectionLine);

        if(mBluetooth->Bluetooth_IsDeviceConnected(deviceAddress) == true)
        {
            mUserInterface->statusbar->showMessage(tr("Disconnecting from selected device..."));
            if(mBluetooth->Bluetooth_GetConnectedDevicesCount() < 2) mBluetooth->Bluetooth_StopRetrievingSensorData();
            mBluetooth->Bluetooth_Disconnect(deviceAddress);
            deviceSelection->setBackground(Qt::white);
            mUserInterface->devicesConnectButton->setText(tr("Connect to selected device"));
            mUserInterface->statusbar->showMessage(tr("Disconnection completed."));
            mUserInterface->devicesConnectButton->setEnabled(true);
        }
        else
        {
            if(mSettings->Settings_GetDebugInformations()) qDebug() << tr("Device is to be connected.");
            mUserInterface->statusbar->showMessage(tr("Connecting to selected device..."));
            mBluetooth->Bluetooth_Connect(deviceAddress);
        }
    }
}

void MainWindow::MainWindow_DeviceSwitchBodyPart()
{
    mUserInterface->procMinLine->clear();
    mUserInterface->procMaxLine->clear();
    mUserInterface->procRangeLine->clear();

    switch(mUserInterface->bodyPartComboBox->currentIndex())
    {
        case 0: default: mBodyPart = BodyPart::eAll; break;
        case 1: mBodyPart = BodyPart::eLeftArm; break;
        case 2: mBodyPart = BodyPart::eRightArm; break;
        case 3: mBodyPart = BodyPart::eLeftLeg; break;
        case 4: mBodyPart = BodyPart::eRightLeg; break;
    }
    MainWindow_DeviceFilterExercises();
}

void MainWindow::MainWindow_DeviceFilterExercises()
{
    QStringList filteredExerciseList = QStringList();
    mExerciseIndexList.clear();
    mUserInterface->exercisesComboBox->clear();
    bool usingDual = (mBluetooth->Bluetooth_GetConnectedDevicesCount() == 2);
    if(mBodyPart == BodyPart::eAll && usingDual == true)
    {
        filteredExerciseList = mExerciseNameList;

        for(qint32 index = 0; index < mExerciseTypeList.count(); index++) mExerciseIndexList.append(index);
    }
    else if(mBodyPart == BodyPart::eAll && usingDual == false)
    {
        for(qint32 index = 0; index < mExerciseTypeList.count(); index++)
        {
            if(mExerciseTypeList.at(index).second != ExerciseType::eLimbUpperDual
            && mExerciseTypeList.at(index).second != ExerciseType::eLimbLowerDual)
            {
                filteredExerciseList.append(mExerciseNameList.at(index));
                mExerciseIndexList.append(index);
            }
        }
    }
    else
    {
        ExerciseType exerciseType = ExerciseType::eLimbUpper;
        ExerciseType exerciseTypeDual = ExerciseType::eLimbUpperDual;
        if(mBodyPart == BodyPart::eLeftLeg || mBodyPart == BodyPart::eRightLeg)
        {
            exerciseType = ExerciseType::eLimbLower;
            exerciseTypeDual = ExerciseType::eLimbLowerDual;
        }

        for(qint32 index = 0; index < mExerciseTypeList.count(); index++)
        {
            if((mExerciseTypeList.at(index).second == exerciseType) || (mExerciseTypeList.at(index).second == exerciseTypeDual && usingDual == true))
            {
                filteredExerciseList.append(mExerciseNameList.at(index));
                mExerciseIndexList.append(index);
            }
        }
    }
    mUserInterface->exercisesComboBox->addItems(filteredExerciseList);
}

void MainWindow::MainWindow_DeviceMeasure()
{
    if(mMeasurementRunning == false)
    {
        ProcessingSelection processingSelected = ProcessingSelection::eDisabled;
        mSelectedExercise = mUserInterface->exercisesComboBox->currentIndex();
        if(mBodyPart != BodyPart::eAll) mSelectedExercise = mExerciseIndexList.at(mSelectedExercise);

        MainWindow_Set3DScene(false, false);
        if(mSelectedExercise < mExerciseTypeList.count()) processingSelected = mExerciseTypeList.at(mSelectedExercise).first;

        if(processingSelected == ProcessingSelection::eProcessingXY) mBluetooth->Bluetooth_SetServiceRetrievalSpeed(100);
        else if(processingSelected == ProcessingSelection::eProcessingZ) mBluetooth->Bluetooth_SetServiceRetrievalSpeed(125);
        mSensorModule->SensorModule_SetProcessingStatus(processingSelected, DeviceType::ePrimary);
        mSensorModule->SensorModule_SetProcessingStatus(processingSelected, DeviceType::eSecondary);
        mUserInterface->processingButton->setText(tr("Stop measurement"));
        mUserInterface->bodyPartComboBox->setEnabled(false);
        mUserInterface->exercisesComboBox->setEnabled(false);
        mUserInterface->saveRecordButton->setEnabled(false);
        mUserInterface->displayHistoryButton->setEnabled(false);
        mUserInterface->statusbar->showMessage(tr("Measurement is now in process."));
        mMeasurementRunning = true;
    }
    else
    {
        MainWindow_Reset3DScene();

        mBluetooth->Bluetooth_SetServiceRetrievalSpeed(3000);
        mSensorModule->SensorModule_SetProcessingStatus(ProcessingSelection::eDisabled, DeviceType::ePrimary);
        mSensorModule->SensorModule_SetProcessingStatus(ProcessingSelection::eDisabled, DeviceType::eSecondary);
        mUserInterface->processingButton->setText(tr("Start measurement"));
        mUserInterface->bodyPartComboBox->setEnabled(true);
        mUserInterface->exercisesComboBox->setEnabled(true);
        mUserInterface->saveRecordButton->setEnabled(true);
        mUserInterface->displayHistoryButton->setEnabled(true);
        mUserInterface->statusbar->showMessage(tr("Measurement has been stopped."));
        mMeasurementRunning = false;
        mSelectedExercise = Extensions::sNoExerciseSelected;
        MainWindow_Set3DScene(true, false);
    }
}

void MainWindow::MainWindow_DeviceNoConnectionReport(const QString &error)
{
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowTitle(tr("Connection error"));
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setText(error);
    errMsg.exec();
}

void MainWindow::MainWindow_DeviceConnectedReport()
{
    QListWidgetItem *deviceSelection = mUserInterface->devicesListWidget->item(mDeviceSelectionLine);
    deviceSelection->setBackground(Qt::green);
    mUserInterface->devicesConnectButton->setText(tr("Disconnect from selected device"));
    mUserInterface->statusbar->showMessage(tr("Connection completed."));
    mUserInterface->devicesConnectButton->setEnabled(true);
    MainWindow_EnableMeasurementPage();
}

void MainWindow::MainWindow_DeviceIncompatibleReport()
{
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowTitle(tr("Compatibility error"));
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setText(tr("This device does not support required services."));
    errMsg.exec();

    QListWidgetItem *deviceSelection = mUserInterface->devicesListWidget->item(mDeviceSelectionLine);
    deviceSelection->setBackground(Qt::yellow);
    mUserInterface->statusbar->showMessage(tr("Devices with yellow background do not contain necessary service."));
    mUserInterface->devicesConnectButton->setEnabled(true);
}

void MainWindow::MainWindow_DeviceUnreachableReport()
{
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowTitle(tr("Access error"));
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setText(tr("This device is unfortunately unreachable."));
    errMsg.exec();

    QListWidgetItem *deviceSelection = mUserInterface->devicesListWidget->item(mDeviceSelectionLine);
    deviceSelection->setBackground(Qt::red);
    mUserInterface->statusbar->showMessage(tr("Devices with red background are unreachable."));
    mUserInterface->devicesConnectButton->setEnabled(true);
}

void MainWindow::MainWindow_DeviceBeyondLimitReport()
{
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowTitle(tr("Limit error"));
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setText(tr("All possible connections are already established."));
    errMsg.exec();

    QListWidgetItem *deviceSelection = mUserInterface->devicesListWidget->item(mDeviceSelectionLine);
    deviceSelection->setBackground(Qt::white);
    mUserInterface->statusbar->showMessage(tr("All possible connections are active, disconnect other devices first."));
    mUserInterface->devicesConnectButton->setEnabled(true);
}

void MainWindow::MainWindow_MeasurementZoomIn()
{
    QVector3D pos = mGraphicsView->camera()->position();
    mGraphicsView->camera()->setPosition(QVector3D(pos.x() * 0.9f, pos.y() * 0.9f, pos.z() * 0.9f));
}

void MainWindow::MainWindow_MeasurementZoomOut()
{
    QVector3D pos = mGraphicsView->camera()->position();
    mGraphicsView->camera()->setPosition(QVector3D(pos.x() * 1.1f, pos.y() * 1.1f, pos.z() * 1.1f));
}

void MainWindow::MainWindow_MeasurementRotateLeft()
{
    mGraphicsView->camera()->rotateAboutViewCenter(QQuaternion::fromEulerAngles(0, -10, 0));
}

void MainWindow::MainWindow_MeasurementRotateRight()
{
    mGraphicsView->camera()->rotateAboutViewCenter(QQuaternion::fromEulerAngles(0, 10, 0));
}

void MainWindow::MainWindow_MeasurementSaveRecord()
{
    QString today;
    QString exerciseID;
    qint16 exerciseNumber;
    qint16 min;
    qint16 max;
    qint16 range;
    bool overwrittenPrevious = false;
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setWindowTitle(tr("Record saving error"));

    if(mClientSelectionLine >= 0)
    {
        if(mUserInterface->procRangeLine->text().length() > 0)
        {
            today = QDateTime::currentDateTime().date().toString("yyyy-MM-dd");

            exerciseNumber = mUserInterface->exercisesComboBox->currentIndex();
            if(mBodyPart != BodyPart::eAll) exerciseNumber = mExerciseIndexList.at(exerciseNumber);

            if(mBodyPart == BodyPart::eLeftArm || mBodyPart == BodyPart::eLeftLeg) exerciseID = QString("L%1").arg(exerciseNumber);
            else exerciseID = QString("R%1").arg(exerciseNumber);

            min = mUserInterface->procMinLine->text().toShort();
            max = mUserInterface->procMaxLine->text().toShort();
            range = mUserInterface->procRangeLine->text().toShort();

            mSessionList = mClientList.at(mClientSelectionLine)->Client_GetSessionList();
            for(qint32 index = 0; index < mSessionList.count(); index++)
            {
                if(QString::compare(mSessionList.at(index)->Session_GetID(), today, Qt::CaseSensitive) == 0)
                {
                    mClientList.at(mClientSelectionLine)->Client_GetSessionList().at(index)->Session_AddExercise(QSharedPointer<Exercise>
                         (new Exercise(exerciseID, min, max, range)));
                    overwrittenPrevious = true;
                    mUserInterface->statusbar->showMessage(tr("Daily exercise record successfully overwritten."));
                }
            }

            if(mSessionList.isEmpty() || overwrittenPrevious == false)
            {
                mClientList.at(mClientSelectionLine)->Client_AddSession(QSharedPointer<Session>
                     (new Session(today, QList<QSharedPointer<Exercise>>())));
                mClientList.at(mClientSelectionLine)->Client_GetSessionList().last()->Session_AddExercise(QSharedPointer<Exercise>
                     (new Exercise(exerciseID, min, max, range)));
                mUserInterface->statusbar->showMessage(tr("Daily exercise record successfully created."));
            }

            mXmlParser.XmlParser_SaveClientData(mClientList);
        }
        else
        {
            errMsg.setText(tr("There is no measurement data ready."));
            errMsg.exec();
        }
    }
    else
    {
        errMsg.setText(tr("Client selection is empty."));
        errMsg.exec();

        MainWindow_EnableClientsPage();
    }
}

void MainWindow::MainWindow_MeasurementDisplayHistory()
{
    QString exerciseID;
    QStringList historyDateList;
    QList<qint16> historyMinList;
    QList<qint16> historyMaxList;
    QList<qint16> historyRangeList;
    qint16 exerciseNumber;
    bool recordFound = false;
    QTableWidgetItem *prototype = nullptr;
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setWindowTitle(tr("History display error"));

    if(mClientSelectionLine >= 0)
    {
        exerciseNumber = mUserInterface->exercisesComboBox->currentIndex();
        if(mBodyPart != BodyPart::eAll) exerciseNumber = mExerciseIndexList.at(exerciseNumber);

        if(mBodyPart == BodyPart::eLeftArm || mBodyPart == BodyPart::eLeftLeg) exerciseID = QString("L%1").arg(exerciseNumber);
        else exerciseID = QString("R%1").arg(exerciseNumber);

        mSessionList = mClientList.at(mClientSelectionLine)->Client_GetSessionList();
        for(qint32 sessionIndex = 0; sessionIndex < mSessionList.count(); sessionIndex++)
        {
            mExerciseList = mSessionList.at(sessionIndex)->Session_GetExerciseList();
            for(qint32 exerciseIndex = 0; exerciseIndex < mExerciseList.count(); exerciseIndex++)
            {
                if(QString::compare(mExerciseList.at(exerciseIndex)->Exercise_GetID(), exerciseID, Qt::CaseSensitive) == 0)
                {
                    historyDateList << mSessionList.at(sessionIndex)->Session_GetID();
                    historyMinList << mExerciseList.at(exerciseIndex)->Exercise_GetMin();
                    historyMaxList << mExerciseList.at(exerciseIndex)->Exercise_GetMax();
                    historyRangeList << mExerciseList.at(exerciseIndex)->Exercise_GetRange();
                    recordFound = true;
                    break;
                }
            }
        }

        if(recordFound == true)
        {
            mUserInterface->graphicsWidget->hide();
            mUserInterface->historyWidget->show();
            mUserInterface->actionMeasurement->setChecked(false);
            if(mBodyPart == BodyPart::eLeftArm || mBodyPart == BodyPart::eLeftLeg)
            {
                mUserInterface->selectedExerciseLabel->setText(tr("Selected exercise: %1, left limb")
                      .arg(mExerciseNameList.at(exerciseNumber)));
            }
            else
            {
                mUserInterface->selectedExerciseLabel->setText(tr("Selected exercise: %1, right limb")
                      .arg(mExerciseNameList.at(exerciseNumber)));
            }

            mUserInterface->historyMinTopLabel->setText(QString());
            mUserInterface->historyMinBottomLabel->setText(QString());
            mUserInterface->historyMaxBottomLabel->setText(QString());
            mUserInterface->historyMaxTopLabel->setText(QString());
            mUserInterface->historyMinBlankLabel->setFixedWidth(mUserInterface->historyMinLabel->width());
            mUserInterface->historyMaxBlankLabel->setFixedWidth(mUserInterface->historyMaxLabel->width());
            mUserInterface->historyMinProgressBar->setRange(0, 1);
            mUserInterface->historyMinProgressBar->setValue(0);
            mUserInterface->historyMaxProgressBar->setRange(0, 1);
            mUserInterface->historyMaxProgressBar->setValue(0);

            mUserInterface->historyTableWidget->setRowCount(historyDateList.count());
            for(qint32 row = 0; row < historyDateList.count(); row++)
            {
                QStringList rowList = {historyDateList.at(row), QString::number(historyMinList.at(row)),
                     QString::number(historyMaxList.at(row)), QString::number(historyRangeList.at(row))};
                for(qint32 column = 0; column < rowList.count(); column++)
                {
                    prototype = new QTableWidgetItem(rowList.at(column));
                    prototype->setTextAlignment(Qt::AlignCenter);
                    mUserInterface->historyTableWidget->setItem(row, column, prototype);
                }
            }
            mUserInterface->historyTableWidget->clearSelection();

            MainWindow_HistoryResizeTable();
        }
        else
        {
            errMsg.setText(tr("Client has no records for selected exercise."));
            errMsg.exec();
        }
    }
    else
    {
        errMsg.setText(tr("Client selection is empty."));
        errMsg.exec();

        MainWindow_EnableClientsPage();
    }
}

void MainWindow::MainWindow_ClientDrawTable()
{
    QTableWidgetItem *prototype = nullptr;

    mUserInterface->clientTableWidget->setRowCount(mClientList.count());
    for(qint32 row = 0; row < mClientList.count(); row++)
    {
        QStringList rowList = mClientList.at(row)->Client_GetAllFormatted();
        for(qint32 column = 0; column < (rowList.count() - 1); column++)
        {
            prototype = new QTableWidgetItem(rowList.at(column + 1));
            prototype->setTextAlignment(Qt::AlignCenter);
            mUserInterface->clientTableWidget->setItem(row, column, prototype);
        }
    }
}

QString MainWindow::MainWindow_ClientCheck(QStringList newEntry)
{
    QString result;
    qint16 correct = 0;

    if(newEntry.count() == Extensions::sClientTableColumns)
    {
        result = tr("First name should contain at least one character.");
        if(newEntry.at(0).size() < 1) return result;
        else correct++;

        result = tr("Last name should contain at least one character.");
        if(newEntry.at(1).size() < 1) return result;
        else correct++;

        result = tr("E-mail format does not match requirements.");
        static QRegularExpression mail("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}");
        if(!mail.match(newEntry.at(2)).hasMatch()) return result;
        else correct++;

        if(correct == 3) return QString("OK");
    }
    return tr("Some columns have not been filled.");
}

void MainWindow::MainWindow_ClientAdd()
{
    QStringList newEntry = QStringList();
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setWindowTitle(tr("Entry error"));

    qint32 row = mUserInterface->clientTableWidget->rowCount();
    if(mEditingEnabled == false)
    {
        mEditingEnabled = true;
        mUserInterface->clientTableWidget->insertRow(row);
        for(qint32 column = 0; column < Extensions::sClientTableColumns; column++)
        {
            QTableWidgetItem *prototype = new QTableWidgetItem();
            prototype->setTextAlignment(Qt::AlignCenter);
            mUserInterface->clientTableWidget->setItem(row, column, prototype);
        }
        mUserInterface->clientTableWidget->setCurrentCell(row, 0);
        mUserInterface->clientTableWidget->edit(mUserInterface->clientTableWidget->model()->index(row, 0));
        mUserInterface->newClientButton->setText(tr("Confirm new client entry"));
        mUserInterface->removeClientButton->hide();
        mUserInterface->statusbar->showMessage(tr("Please define client's name and e-mail address."));
        MainWindow_ClientResizeTable();
    }
    else
    {
        for(int column = 0; column < Extensions::sClientTableColumns; column++)
        {
            if(mUserInterface->clientTableWidget->item(row - 1, column) != nullptr)
            {
                newEntry << mUserInterface->clientTableWidget->item(row - 1, column)->text();
            }
        }
        QString entryCheckResult = MainWindow_ClientCheck(newEntry);
        if(QString::compare(entryCheckResult, QString("OK"), Qt::CaseSensitive) == 0)
        {
            bool isUnique = true;
            QString hashedID = mHasher.Hasher_CalculateHash(newEntry.join(""));

            for(qint32 index = 0; index < mClientList.count(); index++)
            {
                if(QString::compare(mClientList.at(index)->Client_GetID(), hashedID, Qt::CaseSensitive) == 0)
                {
                    isUnique = false;
                    break;
                }
            }

            if(isUnique == true)
            {
                mClient = QSharedPointer<Client>(new Client(hashedID, newEntry.at(0), newEntry.at(1), newEntry.at(2), QList<QSharedPointer<Session>> ()));
                mClientList.append(mClient);

                mXmlParser.XmlParser_SaveClientData(mClientList);

                mUserInterface->statusbar->showMessage(tr("New client was successfully created."));
                mUserInterface->clientTableWidget->removeRow(row - 1);
                MainWindow_EnableClientsPage();
            }
            else
            {
                errMsg.setText(tr("Such client already exists."));
                errMsg.exec();
            }
        }
        else
        {
            errMsg.setText(entryCheckResult);
            errMsg.exec();
        }
    }
}

void MainWindow::MainWindow_ClientRemove()
{
    QStringList removalIdList;
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setWindowTitle(tr("Removal error"));
    errMsg.setText(tr("Select one or more clients in order to remove them."));

    QModelIndexList selectedList = mUserInterface->clientTableWidget->selectionModel()->selectedRows();
    if(selectedList.count() < 1) errMsg.exec();
    else
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setWindowTitle(tr("Removal confirmation"));
        msgBox.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
        msgBox.setText(tr("Do you really want to remove selected client(s)?"));
        QAbstractButton* buttonYes = msgBox.addButton(tr("Yes"), QMessageBox::YesRole);
        msgBox.addButton(tr("No"), QMessageBox::NoRole);
        msgBox.exec();
        if(msgBox.clickedButton() == buttonYes)
        {
            std::sort(selectedList.begin(), selectedList.end());
            for(qint32 index = selectedList.count() - 1; index >= 0; index--)
            {
                mClientList.removeAt(selectedList.at(index).row());

                if(mClientSelectionLine == selectedList.at(index).row())
                {
                    mClientSelectionLine = -1;
                    mUserInterface->selectedClientLabel->setText(tr("Selected client: None"));
                }
            }

            mXmlParser.XmlParser_SaveClientData(mClientList);

            mUserInterface->statusbar->showMessage(tr("Selected client was successfully deleted."));
            MainWindow_EnableClientsPage();
        }
    }
}

void MainWindow::MainWindow_ClientSelect()
{
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/images/LRALogoIcon.ico"));
    errMsg.setWindowTitle(tr("Selection error"));
    qint32 previousClientSelectionLine = mClientSelectionLine;
    QModelIndexList selectedList = mUserInterface->clientTableWidget->selectionModel()->selectedRows();
    if(selectedList.count() == 1) mClientSelectionLine = selectedList.first().row();
    else
    {
        errMsg.setText(tr("Select exactly one client from the list below."));
        errMsg.exec();
        return;
    }

    mUserInterface->selectedClientLabel->setText(tr("Selected client: %1 %2")
          .arg(mClientList.at(mClientSelectionLine)->Client_GetFirstName(),
               mClientList.at(mClientSelectionLine)->Client_GetLastName()));
    mUserInterface->statusbar->showMessage(tr("Client was successfully selected."));

    for(qint32 column = 0; column < Extensions::sClientTableColumns; column++)
    {
        if(previousClientSelectionLine >= 0)
            mUserInterface->clientTableWidget->item(previousClientSelectionLine, column)->setBackground(Qt::white);

        mUserInterface->clientTableWidget->item(mClientSelectionLine, column)->setBackground(QColor(QRgb(0xffe680)));
    }
    mUserInterface->clientTableWidget->clearSelection();
}

void MainWindow::MainWindow_HistoryRecalculateBars()
{
    qint16 minTop = 0;
    qint16 min = 0;
    qint16 minBottom = 0;
    qint16 minReserve = 0;
    qint16 maxTop = 0;
    qint16 max = 0;
    qint16 maxBottom = 0;
    qint16 maxReserve = 0;
    QList<qint16> minList;
    QList<qint16> maxList;

    qint32 selectedRow = mUserInterface->historyTableWidget->selectionModel()->selectedRows().first().row();
    qint32 rowCount = mUserInterface->historyTableWidget->rowCount();

    for(qint16 row = 0; row < rowCount; row++)
    {
        minList << mUserInterface->historyTableWidget->item(row, 1)->text().toShort();
        maxList << mUserInterface->historyTableWidget->item(row, 2)->text().toShort();
    }

    if(rowCount == 1)
    {
        mUserInterface->historyMinTopLabel->setText(QString::number(minList.first()));
        mUserInterface->historyMinBottomLabel->setText(QString());
        mUserInterface->historyMaxBottomLabel->setText(QString());
        mUserInterface->historyMaxTopLabel->setText(QString::number(maxList.first()));
        mUserInterface->historyMinProgressBar->setRange(0, 1);
        mUserInterface->historyMinProgressBar->setValue(1);
        mUserInterface->historyMaxProgressBar->setRange(0, 1);
        mUserInterface->historyMaxProgressBar->setValue(1);
    }
    else
    {
        min = minList.at(selectedRow) * (-1);
        max = maxList.at(selectedRow);

        std::sort(minList.begin(), minList.end());
        std::sort(maxList.begin(), maxList.end());

        for(qint32 index = 0; index < minList.count(); index++) minList[index] = minList.at(index) * (-1);

        minTop = minList.first();
        minBottom = minList.last();
        minReserve = abs(minTop - minBottom) / 25;
        if(minReserve < 1) minReserve = 1;
        maxTop = maxList.last();
        maxBottom = maxList.first();
        maxReserve = abs(maxTop - maxBottom) / 25;
        if(maxReserve < 1) maxReserve = 1;

        mUserInterface->historyMinTopLabel->setText(QString::number(minTop * (-1)));
        mUserInterface->historyMinBottomLabel->setText(QString::number(minBottom * (-1)));
        mUserInterface->historyMaxBottomLabel->setText(QString::number(maxBottom));
        mUserInterface->historyMaxTopLabel->setText(QString::number(maxTop));
        mUserInterface->historyMinProgressBar->setRange((minBottom - minReserve), minTop);
        mUserInterface->historyMinProgressBar->setValue(min);
        mUserInterface->historyMaxProgressBar->setRange((maxBottom - maxReserve), maxTop);
        mUserInterface->historyMaxProgressBar->setValue(max);
    }
}

void MainWindow::MainWindow_AboutActivateAbout()
{
    mUserInterface->aboutAboutButton->setEnabled(false);
    mUserInterface->aboutHowtoButton->setEnabled(true);
    mUserInterface->aboutHowtoTextWidget->hide();
    mUserInterface->aboutVersionLabel->show();
    mUserInterface->aboutFooterLabel->show();
    mUserInterface->aboutVerticalSpacer->changeSize(mUserInterface->aboutVerticalSpacer->sizeHint().width(),
        mUserInterface->aboutVerticalSpacer->sizeHint().height(), QSizePolicy::Expanding, QSizePolicy::Expanding);
    mUserInterface->aboutImageLabel->setMaximumSize((this->height() / 2.2), (this->height() / 2.2));
}

void MainWindow::MainWindow_AboutActivateHowto()
{
    mUserInterface->aboutAboutButton->setEnabled(true);
    mUserInterface->aboutHowtoButton->setEnabled(false);
    mUserInterface->aboutVersionLabel->hide();
    mUserInterface->aboutFooterLabel->hide();
    mUserInterface->aboutVerticalSpacer->changeSize(mUserInterface->aboutVerticalSpacer->sizeHint().width(),
        mUserInterface->aboutVerticalSpacer->sizeHint().height(), QSizePolicy::Ignored, QSizePolicy::Ignored);
    mUserInterface->aboutHowtoTextWidget->show();
    mUserInterface->aboutHowtoTextWidget->setFixedHeight(this->height() / 3);
    mUserInterface->aboutImageLabel->setMaximumSize((this->height() / 4), (this->height() / 4));
}

void MainWindow::MainWindow_SettingsUpdate()
{
    QStringList deviceNameList;
    QStringList deviceAddressList;

    mSettings->Settings_SetDebugInformations(mUserInterface->settingsDebugCheckBox->isChecked());
    mSettings->Settings_SetUnrestrictedLimbMovement(mUserInterface->settingsUnrestrictedMovementCheckBox->isChecked());

    switch(mUserInterface->settingsLanguageComboBox->currentIndex())
    {
        case 0: default:
            mSettings->Settings_SetLanguage(Language::eEnglish);
            MainWindow_SwitchTranslation(Language::eEnglish);
            mUserInterface->settingsLanguageComboBox->setCurrentIndex(0);
            break;
        case 1:
            mSettings->Settings_SetLanguage(Language::eCzech);
            MainWindow_SwitchTranslation(Language::eCzech);
            mUserInterface->settingsLanguageComboBox->setCurrentIndex(1);
            break;
    }

    deviceNameList = mBluetooth->Bluetooth_GetDevicesList();
    deviceAddressList = mBluetooth->Bluetooth_GetAddressList();
    for(qint32 index = 0; (index < deviceNameList.count() && index < deviceAddressList.count()
    && index < mUserInterface->devicesListWidget->count()); index++)
    {
        mUserInterface->devicesListWidget->item(index)->setText(tr(" Device name: %1    Device address: %2")
             .arg(deviceNameList.at(index), deviceAddressList.at(index)));
    }

    mBluetooth->Bluetooth_SetDebugInformations(mSettings->Settings_GetDebugInformations());
    mXmlParser.XmlParser_SaveSettings(mSettings);

    mUserInterface->statusbar->showMessage(tr("Application settings have been overwritten."));
}

void MainWindow::MainWindow_SwitchTranslation(Language language)
{
    qApp->removeTranslator(mTranslator);

    switch(language)
    {
        case Language::eEnglish: default: mTranslator->load(":/languages/languages/LimbRangeAnalyzer_en_EN.qm"); break;
        case Language::eCzech: mTranslator->load(":/languages/languages/LimbRangeAnalyzer_cs_CZ.qm"); break;
    }

    qApp->installTranslator(mTranslator);
    mUserInterface->retranslateUi(this);

    MainWindow_InitializeUIStrings();
}

void MainWindow::MainWindow_EnableHomePage()
{
    mUserInterface->graphicsWidget->hide();
    mUserInterface->clientsWidget->hide();
    mUserInterface->historyWidget->hide();
    mUserInterface->aboutWidget->hide();
    mUserInterface->settingsWidget->hide();
    mUserInterface->devicesWidget->hide();
    mUserInterface->homeWidget->show();
}

void MainWindow::MainWindow_EnableDevicesPage()
{
    mUserInterface->homeWidget->hide();
    mUserInterface->graphicsWidget->hide();
    mUserInterface->clientsWidget->hide();
    mUserInterface->historyWidget->hide();
    mUserInterface->aboutWidget->hide();
    mUserInterface->settingsWidget->hide();
    mUserInterface->devicesWidget->show();
    mUserInterface->actionDevices->setChecked(true);
    mUserInterface->actionMeasurement->setChecked(false);
    mUserInterface->actionClients->setChecked(false);
    mUserInterface->actionAbout->setChecked(false);
    mUserInterface->actionSettings->setChecked(false);
}

void MainWindow::MainWindow_EnableMeasurementPage()
{
    mUserInterface->homeWidget->hide();
    mUserInterface->devicesWidget->hide();
    mUserInterface->clientsWidget->hide();
    mUserInterface->historyWidget->hide();
    mUserInterface->aboutWidget->hide();
    mUserInterface->settingsWidget->hide();
    mUserInterface->graphicsWidget->show();
    mUserInterface->actionDevices->setChecked(false);
    mUserInterface->actionMeasurement->setChecked(true);
    mUserInterface->actionClients->setChecked(false);
    mUserInterface->actionAbout->setChecked(false);
    mUserInterface->actionSettings->setChecked(false);
    MainWindow_DeviceFilterExercises();

    mAnimation = new QPropertyAnimation(this, "size");
    mAnimation->setDuration(50);
    mAnimation->setStartValue(QSize(window()->width(), window()->height()));
    mAnimation->setKeyValueAt(0.5, QSize(window()->width() + 1, window()->height()));
    mAnimation->setEndValue(QSize(window()->width(), window()->height()));
    mAnimation->start(QAbstractAnimation::DeleteWhenStopped);
}

void MainWindow::MainWindow_EnableClientsPage()
{
    mEditingEnabled = false;
    mUserInterface->homeWidget->hide();
    mUserInterface->graphicsWidget->hide();
    mUserInterface->devicesWidget->hide();
    mUserInterface->historyWidget->hide();
    mUserInterface->aboutWidget->hide();
    mUserInterface->settingsWidget->hide();
    mUserInterface->clientsWidget->show();
    mUserInterface->actionDevices->setChecked(false);
    mUserInterface->actionMeasurement->setChecked(false);
    mUserInterface->actionClients->setChecked(true);
    mUserInterface->actionAbout->setChecked(false);
    mUserInterface->actionSettings->setChecked(false);

    mUserInterface->newClientButton->setText(tr("Add new client"));
    mUserInterface->removeClientButton->show();

    mClientList = mXmlParser.XmlParser_LoadClientData();

    MainWindow_ClientDrawTable();
    MainWindow_ClientResizeTable();

    if(mClientSelectionLine >= 0)
    {
        for(qint32 column = 0; column < Extensions::sClientTableColumns; column++)
        {
            mUserInterface->clientTableWidget->item(mClientSelectionLine, column)->setBackground(QColor(QRgb(0xffe680)));
        }
        if(mClientList.length() > mClientSelectionLine)
        {
            mUserInterface->selectedClientLabel->setText(tr("Selected client: %1 %2")
                  .arg(mClientList.at(mClientSelectionLine)->Client_GetFirstName(),
                         mClientList.at(mClientSelectionLine)->Client_GetLastName()));
        }
    }
}

void MainWindow::MainWindow_EnableAboutPage()
{
    mUserInterface->homeWidget->hide();
    mUserInterface->graphicsWidget->hide();
    mUserInterface->clientsWidget->hide();
    mUserInterface->historyWidget->hide();
    mUserInterface->devicesWidget->hide();
    mUserInterface->settingsWidget->hide();
    mUserInterface->aboutWidget->show();
    mUserInterface->actionDevices->setChecked(false);
    mUserInterface->actionMeasurement->setChecked(false);
    mUserInterface->actionClients->setChecked(false);
    mUserInterface->actionAbout->setChecked(true);
    mUserInterface->actionSettings->setChecked(false);
    MainWindow_AboutActivateAbout();
}

void MainWindow::MainWindow_EnableSettingsPage()
{
    mUserInterface->homeWidget->hide();
    mUserInterface->graphicsWidget->hide();
    mUserInterface->clientsWidget->hide();
    mUserInterface->historyWidget->hide();
    mUserInterface->devicesWidget->hide();
    mUserInterface->aboutWidget->hide();
    mUserInterface->settingsWidget->show();
    mUserInterface->actionDevices->setChecked(false);
    mUserInterface->actionMeasurement->setChecked(false);
    mUserInterface->actionClients->setChecked(false);
    mUserInterface->actionAbout->setChecked(false);
    mUserInterface->actionSettings->setChecked(true);
    mUserInterface->settingsBlankLabel1->setFixedWidth(mUserInterface->settingsMainLabel->width() / 2);

    mUserInterface->settingsDebugCheckBox->setChecked(mSettings->Settings_GetDebugInformations());
    mUserInterface->settingsUnrestrictedMovementCheckBox->setChecked(mSettings->Settings_GetUnrestrictedLimbMovement());
    switch(mSettings->Settings_GetLanguage(true).toInt())
    {
        case 1: default: mUserInterface->settingsLanguageComboBox->setCurrentIndex(0); break;
        case 2: mUserInterface->settingsLanguageComboBox->setCurrentIndex(1); break;
    }
}

void MainWindow::MainWindow_CleanAndQuit()
{
    mBluetooth->Bluetooth_StopRetrievingSensorData();
    QStringList addressList = mBluetooth->Bluetooth_GetAddressList();
    if(addressList.isEmpty() == false)
    {
        for(qint32 index = 0; index < addressList.count(); index++)
        {
            if(mBluetooth->Bluetooth_IsDeviceConnected(addressList.at(index)) == true)
            {
                mBluetooth->Bluetooth_Disconnect(addressList.at(index));
            }
        }
    }

    this->close();
}
