/**
 * @file        settings.cpp
 * @author      Tomas Bartosik
 * @date        26.03.2022
 * @brief       definition file for Limb Range Analyzer settings class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/settings.hpp"

/*Class definition: ---------------------------------------------------------*/
Settings::Settings() : mEnableDebugInformations(false), mEnableUnrestrictedLimbMovement(false) {}
Settings::~Settings() {}

void Settings::Settings_SetDebugInformations(bool debugInformations) {mEnableDebugInformations = debugInformations;}
void Settings::Settings_SetUnrestrictedLimbMovement(bool unrestrictedLimbMovement) {mEnableUnrestrictedLimbMovement = unrestrictedLimbMovement;}
void Settings::Settings_SetLanguage(Language language) {mLanguage = language;}

bool Settings::Settings_GetDebugInformations() {return mEnableDebugInformations;}
bool Settings::Settings_GetUnrestrictedLimbMovement() {return mEnableUnrestrictedLimbMovement;}
QString Settings::Settings_GetLanguage(bool getByID)
{
    if(getByID == true) return QString::number(static_cast<qint32>(mLanguage));

    switch(static_cast<qint32>(mLanguage))
    {
        case 1: default: return QObject::tr("English");
        case 2: return QObject::tr("Czech");
    }
}
