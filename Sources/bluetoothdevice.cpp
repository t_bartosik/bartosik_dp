/**
 * @file        bluetoothdevice.cpp
 * @author      Tomas Bartosik
 * @date        22.07.2021
 * @brief       definition file for Bluetooth device class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/bluetoothdevice.hpp"

/*Class definition: ---------------------------------------------------------*/
BluetoothDevice::BluetoothDevice(){}
BluetoothDevice::BluetoothDevice(const QBluetoothDeviceInfo &device) : mDevice(device) {}
BluetoothDevice::~BluetoothDevice(){}

void BluetoothDevice::BluetoothDevice_SetDevice(const QBluetoothDeviceInfo &device)
{
    mDevice = QBluetoothDeviceInfo(device);
    Q_EMIT BluetoothDevice_SignalDeviceChanged();
}
QBluetoothDeviceInfo BluetoothDevice::BluetoothDevice_GetDevice() {return mDevice;}
QString BluetoothDevice::BluetoothDevice_GetName() const {return mDevice.name();}
QString BluetoothDevice::BluetoothDevice_GetAddress() const
{
    #ifdef Q_OS_MAC
        return mDevice.deviceUuid().toString();
    #else
        return mDevice.address().toString();
    #endif
}
