/**
 * @file        client.cpp
 * @author      Tomas Bartosik
 * @date        01.03.2022
 * @brief       implementation file for class Client
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2022 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/client.hpp"

/*Class definition: ---------------------------------------------------------*/
Client::Client() : mID(QString()), mFirstName(QString()), mLastName(QString()),
    mEmail(QString()), mSessionList(QList<QSharedPointer<Session>>()) {}
Client::Client(QString id, QString firstName, QString lastName, QString email, QList<QSharedPointer<Session>> sessionList)
    : mID(id), mFirstName(firstName), mLastName(lastName), mEmail(email), mSessionList(sessionList) {}
Client::~Client() {mSessionList.clear();}

QString& Client::Client_GetID() {return mID;}
QString& Client::Client_GetFirstName() {return mFirstName;}
QString& Client::Client_GetLastName() {return mLastName;}
QString& Client::Client_GetEmail() {return mEmail;}
QList<QSharedPointer<Session>>& Client::Client_GetSessionList() {return mSessionList;}
QStringList Client::Client_GetAllFormatted() {return {mID, mFirstName, mLastName, mEmail};}

void Client::Client_AddSession(QSharedPointer<Session> session) {mSessionList.append(session);}
