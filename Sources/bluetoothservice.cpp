/**
 * @file        bluetoothservice.cpp
 * @author      Tomas Bartosik
 * @date        27.07.2021
 * @brief       definition file for Bluetooth service
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/bluetoothservice.hpp"

/*Class definition: ---------------------------------------------------------*/
BluetoothService::BluetoothService(){}
BluetoothService::BluetoothService(QLowEnergyService *service) : mService(service) {mService->setParent(this);}
BluetoothService::~BluetoothService(){}

QLowEnergyService *BluetoothService::BluetoothService_GetService() const {return mService;}

QString BluetoothService::BluetoothService_GetUUID()
{
    if(!mService) return QString();
    else
    {
        const QBluetoothUuid uuid = mService->serviceUuid();
        bool success = false;

        quint16 uuid16 = uuid.toUInt16(&success);
        if(success) return QStringLiteral("0x") + QString::number(uuid16, 16);

        quint32 uuid32 = uuid.toUInt32(&success);
        if(success) return QStringLiteral("0x") + QString::number(uuid32, 16);

        return uuid.toString().remove(QLatin1Char('{')).remove(QLatin1Char('}'));
    }
}

